<?php

# EMFIT 404

# redirect diversion
$url = $_SERVER['REQUEST_URI'];

if (strpos($url, '/blog/') === 0) {
	require_once(TEMPLATE_INC . '/inc-redirects.php');
	redirect_blog($url);
}

else if (strpos($url, '/store/') === 0) {
	require_once(TEMPLATE_INC . '/inc-redirects.php');
	redirect_store($url);
}

# header
get_header();

?>

<section class="general-404">
<div class="container">
<div class="row">
<div class="col-sm-12 text-center">
Oops! Sorry, that page isn't available.
</div>
</div>
</div>
</section>

<?php
	
# footer
get_footer();
