<?php
/**
Template name: Member / Challenge
 */

# MEMBER / CHALLENGE

# check login
membertools_confirmuser();

# check challenge
$challengeid = memberchallenge_validate();
if (!$challengeid) { membertools_dashboard_redirect(); }

# challenge info
$GLOBALS['emfit']['challengeid'] = $challengeid;
$GLOBALS['emfit']['portalmode'] = 'member';

get_template_part('portal/portal-challenge');
