<?php

# ENVIRONMENT

# bugs
if (substr(getenv('SERVER_NAME'), -5) == '.test') {
	define('ENV_SERVER', 'bugs');
}

# beta
else if (substr(getenv('SERVER_NAME'), -20) == '.lemonadeisgreat.com') {
	define('ENV_SERVER', 'beta');
}

# server
else {
	define('ENV_SERVER', 'server');
}


# LOCATIONS

# paths
define('TEMPLATE_DIR', get_template_directory_uri());
define('TEMPLATE_ASSETS', get_template_directory_uri() . '/assets');
define('TEMPLATE_CSS', get_template_directory_uri() . '/css');
define('TEMPLATE_JS', get_template_directory_uri() . '/js');

define('TEMPLATE_INC', get_template_directory() . '/inc');
define('TEMPLATE_MEMBERS', get_template_directory() . '/members');
define('TEMPLATE_SHOPIFY', get_template_directory() . '/shopify');

# vars
define('CONTACT_EMAIL', 'em@emilyschromm.com');

# After Setup
function emfit_after_setup()
{

    add_theme_support( 'responsive-embeds' );

}
add_action( 'after_setup_theme', 'emfit_after_setup' );

function alx_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' );


# HELPERS

# tools
require_once(TEMPLATE_INC . '/inc-tools.php');
require_once(TEMPLATE_INC . '/inc-acf.php');
require_once(TEMPLATE_INC . '/inc-duplicate.php');
require_once(TEMPLATE_INC . '/inc-email.php');
require_once(TEMPLATE_INC . '/inc-mandrill.php');
require_once(TEMPLATE_INC . '/inc-uuid.php');

# post types
require_once(TEMPLATE_INC . '/inc-challenges.php');
require_once(TEMPLATE_INC . '/inc-members.php');
require_once(TEMPLATE_INC . '/inc-news.php');
require_once(TEMPLATE_INC . '/inc-overview.php');
require_once(TEMPLATE_INC . '/inc-products.php');
require_once(TEMPLATE_INC . '/inc-programs.php');
require_once(TEMPLATE_INC . '/inc-successstories.php');
require_once(TEMPLATE_INC . '/inc-webhooks.php');

# handoffs
require_once(TEMPLATE_MEMBERS . '/member-init.php');
require_once(TEMPLATE_SHOPIFY . '/shopify-init.php');


# SCRIPTS

add_action('wp_enqueue_scripts', 'emfit_scripts');

function emfit_scripts() {

	# css
	wp_enqueue_style('default', get_stylesheet_uri());

	# bootstrap (bundle includes popper.js)
	wp_enqueue_script('bootstrap-js', TEMPLATE_JS . '/bootstrap/4.3.1/bootstrap.min.js', array('jquery'), '4.3.1', true );
	# wp_enqueue_script('bootstrap-js', TEMPLATE_JS . '/bootstrap/4.1.3/bootstrap.bundle.min.js', array('jquery'), '4.1.3', true );
	
	# owl
	wp_enqueue_style('owl-css', TEMPLATE_JS . '/owl/2.3.4/assets/owl.carousel.min.css' );
	wp_enqueue_style('owl-css-theme', TEMPLATE_JS . '/owl/2.3.4/assets/owl.theme.default.min.css' );
	wp_enqueue_script('owl-js', TEMPLATE_JS . '/owl/2.3.4/owl.carousel.min.js', array('jquery'), '2.3.4');

	# lity
	wp_enqueue_style('lity', TEMPLATE_JS . '/lity/2.4.0/lity.min.css', false, '2.4.0', 'all');
	wp_enqueue_script('lity', TEMPLATE_JS. '/lity/2.4.0/lity.min.js', array('jquery'), '2.4.0');

	# datepicker
	wp_enqueue_style('datepicker', TEMPLATE_JS. '/datepicker/1.9.0/bootstrap-datepicker3.standalone.min.css', false, '1.9.0', 'all');
	wp_enqueue_script('datepicker', TEMPLATE_JS. '/datepicker/1.9.0/bootstrap-datepicker.min.js', array('jquery'), '1.9.0');

	# emfit
	wp_enqueue_style('emfit-css', TEMPLATE_CSS . '/emfit.css', false, '145');
	wp_enqueue_script('emfit-js', TEMPLATE_JS . '/emfit.js', array('jquery'), '93', true);

	# emfit / ajax
	wp_localize_script('emfit-js', 'ajaxurl', admin_url('admin-ajax.php'));		

}


# BETA

if (ENV_SERVER == 'beta') {
	require_once('beta/inc-beta.php');
	beta_checklogincookie();
}


# MAIN TOOLS

# globals
$GLOBALS['emfit'] = array(
	'pageid' => 'general',
	'acf' => array(),
	'member' => array(),
	'lists' => array(),
);

# site paths
define('EMFIT_CART', '/cart/');

# member paths
define('MEMBER_LOGIN', '/members/');
define('MEMBER_DASHBOARD', '/members/dashboard/');
define('MEMBER_PROGRAM', '/members/program/');
define('MEMBER_CHALLENGE', '/members/challenge/');

# sites
define('LINK_EMPACK', 'https://www.evolvedmotion.com');


# THEME SETUP

add_action('after_setup_theme', 'emfit_setup');

function emfit_setup() {
	add_theme_support('title-tag');
}


# CURL IPv4

add_action('http_api_curl', 'vantiq_curl_ipv4');

function vantiq_curl_ipv4 ($ch) {
	if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')) {
	   curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	}		
}


# ACF

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'EmFit Settings',
		'menu_title'	=> 'EmFit Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'		=> '59.9',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Challenge Panel',
		'menu_title'	=> 'Challenge Panel',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Facebook Groups',
		'menu_title' 	=> 'Facebook Groups',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Feature Panel',
		'menu_title'	=> 'Feature Panel',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Featured Products',
		'menu_title'	=> 'Featured Products',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Latest Update',
		'menu_title'	=> 'Latest Update',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Meal Plans',
		'menu_title' 	=> 'Meal Plans',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Mobile Nav',
		'menu_title' 	=> 'Mobile Nav',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Program Categories',
		'menu_title' 	=> 'Program Categories',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Programs Panel',
		'menu_title' 	=> 'Programs Panel',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Quiz Panel',
		'menu_title' 	=> 'Quiz Panel',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Resource Library',
		'menu_title' 	=> 'Resource Library',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tags / Programs',
		'menu_title' 	=> 'Tags / Programs',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tags / Supplements',
		'menu_title' 	=> 'Tags / Supplements',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tags / Swag',
		'menu_title' 	=> 'Tags / Swag',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}
