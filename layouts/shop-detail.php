<?php
	
# SHOP / DETAIL

# page flag
$GLOBALS['emfit']['pageid'] = 'shopdetail';

# product type
$producttype = 'swag';

# header
get_header();

?>

<?php # ALERT ?>

<div class="general-navalert">
<div class="container">

xxx

</div>
</div>


<?php # MAIN ?>

<section class="shop-main">
<div class="container">

<div class="panelwrap">

<?php # features ?>

<div class="featurewrap">

<div class="images"></div>

<div class="hilites">

<div class="name">
xxx
</div>

<div class="entry">

<div class="label">
xxx
</div>

<div class="blurb">
xxx
</div>

</div>
	
</div>

</div>

<?php # text ?>

<div class="textwrap">

<div class="price">
xxx
</div>

<div class="name">
xxx
</div>

<div class="type">
xxx
</div>

<div class="blurb">
xxx
</div>

<div class="variant sizes">

<div class="label">
Select a Size
</div>

<div class="options">
<a href="#" class="option size disabled" data-option="xs">XS</a>
<a href="#" class="option size" data-option="m">M</a>
</div>

</div>

<div class="variant colors">

<div class="label">
Select Color
</div>

<div class="options">
<a href="#" class="option color disabled" data-option="black">Black</a>
<a href="#" class="option color" data-option="gray">Gray</a>
</div>

</div>

<div class="buttonwrap">
<a class="pricebutton" href="#">
Add to Cart
<div class="price">$15</div>
</a>
</div>

<div class="favorite">
Favorite
</div>

<div class="details">

<div class="entry">

<div class="trigger"></div>

<div class="label">
xxx
</div>

<div class="text">
xxx
</div>

</div>

</div>

</div>

</div>

</div>
</section>


<?php # RELATED ?>

<?php if ($producttype == 'swag') { ?>
	<section class="product-related swag">
<div class="container">

<div class="paneltitle">
xxx
</div>

<div class="entries">

<div class="entry">

<div class="imagewrap"></div>

<div class="textwrap">

<div class="title">
xxx
</div>

<div class="type">
xxx
</div>

<div class="price">
xxx
</div>

</div>

</div>

</div>

</div>
</section>
<?php } ?>


<?php # PANELS ?>

<?php
if ( ($producttype == 'program') || ($producttype == 'challenge') ) {

# quiz
# get_template_part('panels/panel-quiz');

# intro offer
get_template_part('panels/panel-introoffer');
	
}	
?>


<?php
	
# footer
get_footer();
