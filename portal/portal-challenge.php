<?php
	
# EMFIT CHALLENGE

# page flag
$GLOBALS['emfit']['pageid'] = 'challenge';

# challenge info
$challengeid = $GLOBALS['emfit']['challengeid'];
$mode = $GLOBALS['emfit']['portalmode'];

# content
$hero = membertools_proginfo($challengeid);
$pdf = get_field('pdf_version', $challengeid);
$resources = get_field('challenge_resources', $challengeid);
$mealplan = get_field('challenge_mealplan', $challengeid);
$facebook = get_field('challenge_facebook', $challengeid);
$points = get_field('challenge_points', $challengeid);
$messages = get_field('challenge_messages', $challengeid);

# points
$totalpoints = 0;
if ( (isset($points['points_questions'])) && (!empty($points['points_questions'])) ) {
	$totalpoints = count($points['points_questions']);
}

# status
$status = array();
$needstartdate = false;
if ($mode == 'member') {
	$status = memberchallenge_status($challengeid);
	if (empty($status['settings']['start_date'])) { $needstartdate = true; }
	# print_r($status);
	# exit;
}

# gestalt
$gestalt = array(
	'mode' => $mode,
	'challengeid' => $challengeid,
	'nonce' => wp_create_nonce(MEMBER_CHALLENGE_NONCE),
);

if ($mode == 'member') {
	$gestalt['pageid'] = $GLOBALS['member']['pageid'];
	$gestalt['userid'] = $GLOBALS['member']['userid'];
}

$gestaltjson = json_encode($gestalt);

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero portal bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/portal/challenge-hero-2x.jpg);">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['name'] ?>!
</div>

<?php if (!empty($hero['version'])) { ?>
<div class="date textuc">
<?= $hero['version'] ?>
</div>	
<?php } ?>

</div>

</div>
</div>
</section>


<?php # NAV ?>

<?php if ($mode == 'admin') { ?>
<section class="portal-adminbar text-center textuc">
<div class="container height100">
<div class="flex height100">
Admin Preview
</div>
</div>
</section>
<?php } ?>

<section class="portal-navbar challenge text-center textuc">
<div class="container height100">

<div class="flex height100">
<div class="navwrap">

<a class="portalnavlink active" href="#" data-panel="workouts">Workouts</a>

<?php if ($resources['include_resources']) { ?>
<a class="portalnavlink" href="#" data-panel="resources">Resources</a>
<?php } ?>

<?php if ($mealplan['include_meal_plan']) { ?>
<a class="portalnavlink" href="#" data-panel="mealplan">Meal&nbsp;Plan</a>
<?php } ?>

<?php if ($facebook['include_facebook_groups']) { ?>
<a class="portalnavlink" href="#" data-panel="community">Community</a>
<?php } ?>

</div>
</div>

<div class="arrowwrap">
<a href="<?= MEMBER_DASHBOARD ?>" class="arrow arrowleft arrownav textlc">Go Back</a>
</div>

</div>
</section>


<?php # WORKOUTS ?>
	
<section class="challenge-panel" id="panel-workouts">
<?php if (!empty($messages)) { ?>

<div class="workoutdays">
<?php
	foreach ($messages as $key => $value) {
		$settings = $value['message_settings'];
		$messageid = $settings['message_day'];
		$daylabel = 'Day ' . $messageid;
		if ($mode == 'member') {
			if (isset($status['messages'][$messageid]['message_settings']['message_date'])) {
				$date = $status['messages'][$messageid]['message_settings']['message_date'];
				if (!empty($date)) {
					$daylabel = date('M j', strtotime($date));
				}
			}
		}
?>
<div class="entryday textlc flex navday-<?= $messageid ?> navdesk" data-message="<?= $messageid ?>">
<div class="livewrap">
	
<span id="navdaylabel-<?= $messageid ?>"><?= $daylabel ?></span>

<?php
if ($mode == 'member') {
	$style = '';
	$pointlabel = 'points';
	$daypoints = 0;
	if (isset($status['messages'][$messageid]['daypoints'])) {
		$daypoints = $status['messages'][$messageid]['daypoints'];
	}
	if ($daypoints == 0) { $style = 'display:none;'; }
	if ($daypoints == 1) { $pointlabel = 'point'; }
?>
<div class="navpoints textuc">	
<span class="navpoints-<?= $messageid ?>" style="<?= $style ?>"><?= $daypoints ?> <?= $pointlabel ?></span>
</div>
<?php } ?>

</div>
</div>
<?php } ?>
</div>

<div class="contentwrap workouts" id="content-workouts">

<?php if ( ($mode == 'member') && ($needstartdate) ) { ?>
<div class="portal-datewrap" id="challenge-datewrap">
<a href="#" id="challenge-datemodal"><button class="featurebutton btgreen">Choose a Start Date!</button></a>
</div>
<?php } ?>

<?php
	foreach ($messages as $key => $value) {
		$settings = $value['message_settings'];
		$messageid = $settings['message_day'];
		$daylabel = $mobilelabel =  'Day ' . $messageid;
		if ($mode == 'member') {
			if (isset($status['messages'][$messageid]['message_settings']['message_date'])) {
				$date = $status['messages'][$messageid]['message_settings']['message_date'];
				if (!empty($date)) {
					$mobilelabel = date('M j', strtotime($date));
					$daylabel .= ': ' .  date('F j', strtotime($date));
				}
			}
		}
?>

<?php # mobile nav ?>

<div class="workoutmobile">
<div class="entryday textlc flex navday-<?= $messageid ?> navmobile" data-message="<?= $messageid ?>">
<div class="livewrap">
	
<span id="navdaylabel-<?= $messageid ?>"><?= $mobilelabel ?></span>

<?php
if ($mode == 'member') {
	$style = '';
	$pointlabel = 'points';
	$daypoints = $status['messages'][$messageid]['daypoints'];
	if ($daypoints == 0) { $style = 'display:none;'; }
	if ($daypoints == 1) { $pointlabel = 'point'; }
?>
<div class="navpoints textuc">	
<span class="navpoints-<?= $messageid ?>" style="<?= $style ?>"><?= $status['messages'][$messageid]['daypoints'] ?> <?= $pointlabel ?></span>
</div>
<?php } ?>
	
</div>
</div>
</div>


<?php # content ?>

<div class="messageday" style="display:none;" id="message-<?= $messageid ?>">

<?php if ($mode == 'member') { ?>
<div class="pointheader flex">
<div class="livewrap">
<span class="label textuc">Today’s Points</span> <span class="pointcount emgreen"><span id="headerpoints-<?= $messageid ?>"><?= $status['messages'][$messageid]['daypoints'] ?></span> out of <?= $totalpoints ?></span>
</div>
</div>
<?php } ?>

<div class="headline">
<div class="bold">
<?= $daylabel ?>
</div>
<div class="lite">
<?= $settings['message_title'] ?>
</div>
</div>

<?php if ($mode == 'admin') { ?>
<p id="testemail-<?= $messageid ?>" style="margin-bottom:20px;"><a href="#" class="emailtest" data-day="<?= $messageid ?>">Send Test Email</a></p>
<?php } ?>
	
<?php if (!empty($value['message_image'])) { ?>
<div class="featureimage">
<img src="<?= $value['message_image'] ?>" alt="">		
</div>
<?php } ?>

<div class="bodytext">
<?= $value['message_content'] ?>
</div>

<?php if ($value['include_workout']) { ?>
<div class="subheadmain emgreen">
Today’s Workout
</div>
<div class="bodytext">
<?= $value['message_workout'] ?>
</div>
<?php } ?>

<?php if ($value['include_video']) { ?>
<div class="subhead">
Today’s Video
</div>
<div class="bodytext videowrap">
<?php echo $GLOBALS['wp_embed']->run_shortcode('[embed]' . $value['youtube_link'] . '[/embed]'); ?>	
</div>
<?php } ?>
	
<?php if ($value['include_footer_text']) { ?>
<div class="bodytext">
<?= $value['message_footer'] ?>
</div>
<?php } ?>

<?php if ($points['include_points']) { ?>
<div class="subhead">
Today’s Points
</div>

<?php if (!empty($points['intro_text'])) { ?>
<div class="bodytext">
<?= $points['intro_text'] ?>
</div>
<?php } ?>

<?php
	if (!empty($points['points_questions'])) {
		
		$memberpoints = array();
		$disabled = '';
		if ($mode == 'member') {
			$memberpoints = $status['messages'][$messageid]['points'];
			if ($needstartdate) { $disabled = 'disabled'; }
		}

		foreach ($points['points_questions'] as $pkey => $pvalue) {
			$questionid = $pkey + 1;
			$toggle = $disabled;
			if ( (isset($memberpoints[$questionid])) && ($memberpoints[$questionid] == 1) ) {
				$toggle .= ' checked';
			}
?>
<div class="pointwrap">
<div class="pointtoggle bgcover <?= $toggle ?>" data-messageid="<?= $messageid ?>" data-questionid="<?= $questionid ?>"></div>	
<div class="question flex">
<?= $pvalue['question'] ?>
</div>	
<div class="help">
<?= $pvalue['help_text'] ?>
</div>	
</div>	
<?php
		}
	}
}
?>

</div>
<?php } ?>

</div>

<?php } ?>
<div class="clearboth"></div>
</section>


<?php # RESOURCES ?>

<?php if ($resources['include_resources']) { ?>
<section class="challenge-panel" id="panel-resources" style="display:none;">
<div class="contentwrap">

<?php if (!empty($resources['intro_text'])) { ?>
<div class="bodytext">
<?= $resources['intro_text'] ?>
</div>
<?php } ?>

<?php if (!empty($pdf)) { ?>
<div class="resourcelist">
<div class="listtitle">
Challenge PDF Version
</div>	
<p><a href="<?= $pdf ?>" target="_blank">Download</a></p>
</div>
<?php } ?>

<?php
	if (!empty($resources['resource_lists'])) {
		foreach ($resources['resource_lists'] as $key => $value) {
?>
<div class="resourcelist">
<div class="listtitle">
<?= $value['resource_list_title'] ?>	
</div>	
<?php
			if (!empty($value['resource_files'])) {
				foreach ($value['resource_files'] as $fkey => $vfalue) {
				
					$resourceid = $vfalue['challenge_resource_select'];
					$entry = membertools_resource($resourceid);

					if (!$entry) { continue; }

					# label
					$label = $entry['label'];

					# author
					$includecredit = false;
					$author = array();
					if ($entry['include_author_credit']) {
						if (!empty($entry['author']['author_name'])) {
							$includecredit = true;
							$author = $entry['author'];
						}
					}
					
					# file
					if ($entry['resource_type'] == 'file') {
						if (empty($entry['file'])) { continue; }
						$link = $entry['file'];
						if (empty($label)) { $label = 'Download File'; }
					}

					# page
					if ($entry['resource_type'] == 'page') {
						if (empty($entry['website_page'])) { continue; }
						$link = $entry['website_page'];
						if (empty($label)) { $label = 'Visit Page'; }
					}

					# site
					if ($entry['resource_type'] == 'url') {
						if (empty($entry['offsite_link'])) { continue; }
						$link = $entry['offsite_link'];
						if (empty($label)) { $label = 'Visit Site'; }
					}

?>
<p><a href="<?= $link ?>" target="_blank"><?= $label ?></a>
<?php
					if ($includecredit) {
						
						# author only
						if (empty($author['author_url'])) {
?>
by <?= $author['author_name'] ?>
<?php			
						}
				
						# with link
						else {
?>
by <a href="<?= $author['author_url'] ?>" target="_blank"><?= $author['author_name'] ?></a>
<?php			
						}
					}
?>
</p>
<?php				
				}
			}
?>
</div>
<?php
		}
	}
	
	if (!empty($resources['footer_text'])) { ?>
<div class="bodytext">
<?= $resources['footer_text'] ?>
</div>
<?php } ?>

</div>
</section>
<?php } ?>


<?php # MEALPLAN ?>

<?php if ($resources['include_resources']) { ?>
<section class="challenge-panel" id="panel-mealplan" style="display:none;">
<div class="contentwrap">

<?php if (!empty($mealplan['intro_text'])) { ?>
<div class="bodytext">
<?= $mealplan['intro_text'] ?>
</div>
<?php } ?>

<?php if (!empty($mealplan['mealplan_list'])) { ?>
<div class="resourcelist">
<?php
		foreach ($mealplan['mealplan_list'] as $key => $value) {

			$planid = $value['challenge_mealplan_select'];
			$entry = membertools_mealplan($planid);

			if (!$entry) { continue; }

			# file
			if (empty($entry['file'])) { continue; }
			
			# label
			$label = $entry['label'];

			# author
			$includecredit = false;
			$author = array();
			if ($entry['include_author_credit']) {
				if (!empty($entry['author']['author_name'])) {
					$includecredit = true;
					$author = $entry['author'];
				}
			}
?>
<p><a href="<?= $entry['file'] ?>" target="_blank"><?= $label ?></a>
<?php
			if ($includecredit) {
				
				# author only
				if (empty($author['author_url'])) {
?>
by <?= $author['author_name'] ?>
<?php			
				}
		
				# with link
				else {
?>
by <a href="<?= $author['author_url'] ?>" target="_blank"><?= $author['author_name'] ?></a>
<?php			
				}
			}
?>
</p>
<?php } ?>
</div>
<?php
	}
	
	if (!empty($mealplan['footer_text'])) { ?>
<div class="bodytext">
<?= $mealplan['footer_text'] ?>
</div>
<?php } ?>

</div>
</section>
<?php } ?>


<?php # FACEBOOK ?>

<?php if ($facebook['include_facebook_groups']) { ?>
<section class="challenge-panel" id="panel-community" style="display:none;">
<div class="contentwrap">

<?php if (!empty($facebook['intro_text'])) { ?>
<div class="bodytext">
<?= $facebook['intro_text'] ?>
</div>
<?php } ?>

<?php if (!empty($facebook['facebook_list'])) { ?>
<div class="resourcelist">
<?php
		foreach ($facebook['facebook_list'] as $key => $value) {

			$groupid = $value['challenge_facebook_select'];
			$entry = membertools_fbgroup($groupid);

			if (!$entry) { continue; }
			if (empty($entry['group_url'])) { continue; }
			
			# label
			$label = $entry['group_name'];
			if (empty($label)) { $label = 'Facebook Group'; }

?>
<p><a href="<?= $entry['group_url'] ?>" target="_blank"><?= $label ?></a></p>
<?php } ?>
</div>
<?php
	}
	
	if (!empty($facebook['footer_text'])) { ?>
<div class="bodytext">
<?= $facebook['footer_text'] ?>
</div>
<?php } ?>

</div>
</section>
<?php } ?>


<?php # GESTALT ?>

<script type="application/json" id="challenge-gestalt">
<?= $gestaltjson ?>
</script>


<?php

# DATEPICKER

if ( ($mode == 'member') && ($needstartdate) ) { 
	get_template_part('portal/portal-datepicker');
}

?>


<?php
	
# footer
get_footer();

/*
Array
(
    [challenge_resources] => Array
        (
            [include_resources] => 1
            [intro_text] => 
            [resource_lists] => Array
                (
                    [0] => Array
                        (
                            [resource_list_title] => 
                            [resource_files] => Array
                                (
                                    [0] => Array
                                        (
                                            [challenge_resource_select] => 632c8e23-445d-4a0e-8bae-124651aed5e0
                                        )

                                    [1] => Array
                                        (
                                            [challenge_resource_select] => 2bfd5ae5-8919-48ae-a108-4c27415ed017
                                        )

                                )

                        )

                )

            [additional_content] => 
        )

    [challenge_mealplan] => Array
        (
            [include_meal_plan] => 
            [intro_text] => 
            [mealplan_list] => 
        )

    [challenge_facebook] => Array
        (
            [include_facebook_groups] => 
            [intro_text] => 
            [facebook_list] => 
        )

    [challenge_points] => Array
        (
            [include_points] => 1
            [intro_text] => 
            [points_questions] => Array
                (
                    [0] => Array
                        (
                            [question] => Breating Before Eating
                            [help_text] => blah blah
                        )

                    [1] => Array
                        (
                            [question] => No Gluten
                            [help_text] => blah blah
                        )

                    [2] => Array
                        (
                            [question] => Sleep
                            [help_text] => God yes.
                        )

                )

        )

    [challenge_messages] => Array
        (
            [0] => Array
                (
                    [message_settings] => Array
                        (
                            [message_day] => 1
                            [message_title] => 
                        )

                    [message_image] => 
                    [message_content] => 
                    [include_workout] => 
                    [message_workout] => 
                    [include_video] => 
                    [video_label] => 
                    [youtube_link] => 
                    [include_footer_text] => 
                    [message_footer] => 
                    [challenge_email] => Array
                        (
                            [email_subject] => 
                            [email_content] => 
                        )

                )

            [1] => Array
                (
                    [message_settings] => Array
                        (
                            [message_day] => 1
                            [message_title] => 
                        )

                    [message_image] => 
                    [message_content] => 
                    [include_workout] => 
                    [message_workout] => 
                    [include_video] => 
                    [video_label] => 
                    [youtube_link] => 
                    [include_footer_text] => 
                    [message_footer] => 
                    [challenge_email] => Array
                        (
                            [email_subject] => 
                            [email_content] => 
                        )

                )

        )

)


STATUS

Array
(
    [rowid] => 1
    [settings] => Array
        (
            [challenge_id] => 648
            [start_date] => 
            [end_date] => 
        )

    [messages] => Array
        (
            [1] => Array
                (
                    [message_settings] => Array
                        (
                            [message_id] => 1
                            [message_date] => 
                        )

                    [points] => Array
                        (
                            [1] => 1
                            [2] => 0
                            [3] => 1
                        )

                    [daypoints] => 2
                )

            [2] => Array
                (
                    [message_settings] => Array
                        (
                            [message_id] => 2
                            [message_date] => 
                        )

                    [points] => Array
                        (
                            [1] => 0
                            [2] => 0
                            [3] => 0
                        )

                    [daypoints] => 0
                )

        )

)

*/
