<?php
	
# EMFIT PROGRAM

# page flag
$GLOBALS['emfit']['pageid'] = 'program';

# program info
$programid = $GLOBALS['emfit']['programid'];
$mode = $GLOBALS['emfit']['portalmode'];

# content
$hero = membertools_proginfo($programid);
$pdf = get_field('pdf_version', $programid);
$intro = get_field('introduction', $programid);
$workouts = get_field('Workouts', $programid);
$video    = get_field( 'program_video', $programid );

# status
$status = array();
$needstartdate = false; // false
if ($mode == 'member') {
	$status = memberprogram_status($programid);
	if (empty($status['settings']['start_date'])) { $needstartdate = true; }
	# print_r($status);
	# exit;
}

# gestalt
$gestalt = array(
	'mode' => $mode,
	'programid' => $programid,
	'nonce' => wp_create_nonce(MEMBER_PROGRAM_NONCE),
);

if ($mode == 'member') {
	$gestalt['pageid'] = $GLOBALS['member']['pageid'];
	$gestalt['userid'] = $GLOBALS['member']['userid'];
}

$gestaltjson = json_encode($gestalt);

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero portal bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/portal/program-hero-2x.jpg);">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['name'] ?>!
</div>

<?php if (!empty($hero['version'])) { ?>
<div class="date textuc">
<?= $hero['version'] ?>
</div>	
<?php } ?>

</div>

</div>
</div>
</section>


<?php # NAV ?>

<?php if ($mode == 'admin') { ?>
<section class="portal-adminbar text-center textuc">
<div class="container height100">
<div class="flex height100">
Admin Preview
</div>
</div>
</section>
<?php } ?>

<section class="portal-navbar program text-center textuc">
<div class="container height100">

<div class="flex height100">
<div class="navwrap">
<a class="portalnavlink active" href="#" data-panel="intro">Intro</a>
<a class="portalnavlink" href="#" data-panel="workouts">Workouts</a>
</div>
</div>

<div class="arrowwrap">
<a href="<?= MEMBER_DASHBOARD ?>" class="arrow arrowleft arrownav textlc">Go Back</a>
</div>

</div>
</section>


<?php # INTRO ?>
	
<section class="program-panel" id="panel-intro">
<div class="contentwrap intro">

<?php if ( ($mode == 'member') && ($needstartdate) ) { ?>
<div class="portal-datewrap" id="program-datewrap">
<a href="#" id="program-datemodal"><button class="featurebutton btgreen">Choose a Start Date!</button></a>
</div>
<?php } ?>

<div class="introtitle">
<?= $intro['intro_title'] ?>
</div>

<div class="bodytext">

<?php if (!empty($pdf)) { ?>
<p><a class="pdflink" href="<?= $pdf ?>" target="_blank">Download PDF Version</a></p>
<?php } ?>
<?= $intro['intro_content'] ?>
</div>

</div>
</section>


<?php # WORKOUTS ?>
	
<section class="program-panel" id="panel-workouts" style="display:none;">

<div class="workoutweeks">
<?php
$week = '';
foreach ($workouts as $key => $value) {
	$settings = $value['workout_settings'];
	if ($settings['workout_week'] == $week) { continue; }
	$week = $settings['workout_week'];
?>
<div class="entryweek textlc flex navweek-<?= $week ?>" data-week="<?= $week ?>">
Week <?= $settings['workout_week'] ?>
</div>
<?php } ?>
</div>

<div class="contentwrap">

<?php
$week = '';
$weekdiv = '';
foreach ($workouts as $key => $value) {

	# print_r($value);

	$settings = $value['workout_settings'];
	$workoutweek = $settings['workout_week'];
	$workoutday = $settings['workout_day'];
	$workoutid = $workoutweek . '-' . $workoutday;
	$headclass = '';

	$daylabel = 'Day ' . $workoutday;

	$type = $settings['workout_type'];
	if ($type == 'rest') {
		$daylabel .= ': Rest Day';
		$headclass = 'rest ';
	}

	$score = array();
	$complete = '';
	$comments = array();

	if ( ($mode == 'member') && (!$needstartdate) ) {

		$date = $status['workouts'][$key]['workout_settings']['workout_date'];
		$displaydate = date('D, F j', strtotime($date));
		$daylabel .= '<span class="displaydate textuc">' . $displaydate . '</span>';

		$score = unserialize($status['workouts'][$key]['workout_settings']['workout_score']);
		# print_r($score);

		if ($score['all'] == 1) { $complete = 'hilite'; }

		$stepdata = $status['workouts'][$key]['workout_steps'];
		if (!empty($stepdata)) {
			foreach ($stepdata as $skey => $svalue) {
				$stepid = $svalue['step_id'];
				$comment = $svalue['step_comment'];
				if (!empty($comment)) {
					$comments[$stepid] = $comment;
				}
			}
		}

	}
	
	# $email = $value['workout_email'];
	# $email['email_subject'] 

	# new week
	if ($workoutweek != $week) {
		$week = $workoutweek;
		if ($key != 0) { echo "</div>\n"; }
?>
<div class="workoutmobile">
<div class="entryweek textlc flex navweek-<?= $week ?> navmobile" data-week="<?= $week ?>">
Week <?= $settings['workout_week'] ?>
</div>
</div>

<div class="weekwrap" style="display:none;" id="week-<?= $week ?>">
<?php		
	}

?>
<div class="entryday">

<div class="dayhead head<?= $week ?> flex <?= $headclass ?>" data-week="<?= $week ?>">
<div class="daylabel">
<?= $daylabel ?>
</div>
<?php if ( ($mode == 'member') && (!$needstartdate) && ($type != 'rest') ) { ?>
<div class="complete flex textuc <?= $complete ?>" id="complete-<?= $workoutid ?>">
Complete!
</div>
<?php } ?>
</div>

<div class="daycontent week<?= $week ?>" style="display:none;">

<?php
	if (!empty($value['workout_steps'])) {
		
		foreach ($value['workout_steps'] as $skey => $svalue) {
			
			$settings = $svalue['step_settings'];
			$steplabel = $settings['step_id'] . '.';
			$stepid = $settings['step_id'];
			
			if ($settings['step_type'] == 'warmup') {
				$steplabel = 'Warmup';
				$stepid = 'W';
			}

			$toggle = '';
			if ($mode == 'member') {
				if ($needstartdate) { $toggle = 'disabled'; }
				else if ( (isset($score[$stepid])) && ($score[$stepid] == 1) ) { $toggle = 'checked'; }
			}

			$commentid = $workoutid . '-' . $stepid;

?>
<div class="stepwrap">

<?php if ($mode == 'admin') { ?>
<p id="testemail-<?= $workoutid ?>"><a href="#" class="emailtest" data-week="<?= $workoutweek ?>" data-day="<?= $workoutday ?>">Send Test Email</a></p>
<?php } ?>

<div class="daystep empurple">

<span class="steplabel">
<?= $steplabel ?> <?= $settings['step_name'] ?>
</span>

<span class="stepnotice">
Did You Complete This?
</span>

<div class="steptoggle bgcover <?= $toggle ?>" data-workoutid="<?= $workoutid ?>" data-stepid="<?= $stepid ?>"></div>	

</div>


<?php # instructions ?>

<div class="instructions">
<?= $svalue['step_instructions'] ?>
</div>


<?php # videos ?>

<?php if (!empty($svalue['step_videos'])) { ?>
<div class="videowrap">

<?php foreach ($svalue['step_videos'] as $vkey => $vvalue) { ?>
<div class="videoentry flex">
<a class="videotoggle bgcover" href="<?= $vvalue['youtube_link'] ?>" data-lity></a>
VIDEO: <?= $vvalue['video_title'] ?>
</div>
<?php } ?>

</div>
<?php } ?>


<?php # comments ?>

<?php if ( ($mode == 'member') && (!$needstartdate) && ($stepid != 'W') ) { ?>
<div class="commentwrap">

<?php if (isset($comments[$stepid])) { ?>
<div class="entrywrap" >
<div class="programcomment"><?= $comments[$stepid] ?></div>
</div>
	
<?php } else { ?>
<div class="entrywrap">
<textarea class="programcomment" placeholder="enter your results" rows="2" maxlength="300" id="comment-<?= $commentid ?>"></textarea>
<div class="programcomment" id="livecomment-<?= $commentid ?>" style="display:none;"></div>
</div>

<div class="buttonwrap" id="commentbutton-<?= $commentid ?>">
<button class="featurebutton btgreen commentsubmit" data-workoutid="<?= $workoutid ?>" data-stepid="<?= $stepid ?>">Submit</button>
</div>

<?php } ?>

</div>
<?php } ?>


<?php # end foreach ?>

</div>
<?php
	}
}	
?>


<?php # end daycontent ?>

</div>
</div>


<?php # end workouts ?>

<?php } ?>


<?php # end content wrap ?>

</div>
</section>


<?php # GESTALT ?>

<script type="application/json" id="program-gestalt">
<?= $gestaltjson ?>
</script>


<?php

# DATEPICKER

if ( ($mode == 'member') && ($needstartdate) ) { 
	get_template_part('portal/portal-datepicker');
}

?>


<?php
	
# footer
get_footer();


/*

Array
(
    [rowid] => 1
    [settings] => Array
        (
            [program_id] => 135
            [start_date] => 2019-09-02
            [end_date] => 2019-09-16
        )

    [workouts] => Array
        (
            [0] => Array
                (
                    [workout_settings] => Array
                        (
                            [workout_id] => 1-1
                            [workout_date] => 2019-09-02
                            [workout_score] => a:1:{s:3:"all";i:0;}
                        )

                    [workout_steps] => 
                )

            [1] => Array
                (
                    [workout_settings] => Array
                        (
                            [workout_id] => 1-2
                            [workout_date] => 2019-09-03
                            [workout_score] => a:4:{s:3:"all";i:0;s:1:"W";i:0;s:1:"A";i:0;s:1:"B";i:0;}
                        )

                    [workout_steps] => Array
                        (
                            [0] => Array
                                (
                                    [step_id] => A
                                    [step_comment] => 
                                    [step_images] => 
                                )

                            [1] => Array
                                (
                                    [step_id] => B
                                    [step_comment] => 
                                    [step_images] => 
                                )

                        )

                )

            [2] => Array
                (
                    [workout_settings] => Array
                        (
                            [workout_id] => 2-1
                            [workout_date] => 2019-09-09
                            [workout_score] => a:4:{s:3:"all";i:0;s:1:"W";i:0;s:1:"A";i:0;s:1:"B";i:0;}
                        )

                    [workout_steps] => Array
                        (
                            [0] => Array
                                (
                                    [step_id] => A
                                    [step_status] => pending
                                    [step_comment] => 
                                    [step_images] => 
                                )

                            [1] => Array
                                (
                                    [step_id] => B
                                    [step_status] => pending
                                    [step_comment] => 
                                    [step_images] => 
                                )

                        )

                )

            [3] => Array
                (
                    [workout_settings] => Array
                        (
                            [workout_id] => 3-1
                            [workout_date] => 2019-09-16
                            [workout_score] => a:1:{s:3:"all";i:0;}
                        )

                    [workout_steps] => 
                )

        )

)


Array
(
    [Workouts] => Array
        (
            [0] => Array
                (
                    [acf_fc_layout] => workout
                    [workout_settings] => Array
                        (
                            [workout_week] => 1
                            [workout_day] => 1
                            [workout_type] => rest
                        )

                    [workout_steps] => 
                    [workout_email] => Array
                        (
                            [email_subject] => 
                            [email_title] => 
                            [email_content] => 
                        )

                )

            [1] => Array
                (
                    [acf_fc_layout] => workout
                    [workout_settings] => Array
                        (
                            [workout_week] => 1
                            [workout_day] => 2
                            [workout_type] => workout
                        )

                    [workout_steps] => Array
                        (
                            [0] => Array
                                (
                                    [step_settings] => Array
                                        (
                                            [step_type] => warmup
                                            [step_id] => A
                                            [step_name] => 
                                        )

                                    [step_instructions] => Warm-up<br />
3x<br />
10/10 Table Top Hip Circles (Both directions)<br />
20/20 Jane Fonda Circles<br />
20 Leg Lifts on the Wall
                                    [step_videos] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [video_title] => EmFit: Jane Fonda Kickbacks
                                                    [youtube_link] => https://youtu.be/81buIhSrfGY
                                                )

                                            [1] => Array
                                                (
                                                    [video_title] => EmFit: Table Top Hip Circles
                                                    [youtube_link] => https://youtu.be/gK-fRNSbG24
                                                )

                                            [2] => Array
                                                (
                                                    [video_title] => EmFit: Leg Lifts on the Wall
                                                    [youtube_link] => https://youtu.be/L78lT5JqOFE
                                                )

                                        )

                                )

                            [1] => Array
                                (
                                    [step_settings] => Array
                                        (
                                            [step_type] => exercise
                                            [step_id] => A
                                            [step_name] => Repeat 4x:
                                        )

                                    [step_instructions] => 12(per leg) Single Leg Box Squat<br />
20 Box Cross-overs<br />
20 Empack Crunches
                                    [step_videos] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [video_title] => Emfit: Modified Box Single Leg Squat
                                                    [youtube_link] => https://youtu.be/81buIhSrfGY
                                                )

                                            [1] => Array
                                                (
                                                    [video_title] => Emfit: Crunches w/ Empack
                                                    [youtube_link] => https://youtu.be/gK-fRNSbG24
                                                )

                                            [2] => Array
                                                (
                                                    [video_title] => Lateral Box Walk Overs
                                                    [youtube_link] => https://youtu.be/L78lT5JqOFE
                                                )

                                        )

                                )

                            [2] => Array
                                (
                                    [step_settings] => Array
                                        (
                                            [step_type] => exercise
                                            [step_id] => B
                                            [step_name] => Step B
                                        )

                                    [step_instructions] => oh hai
                                    [step_videos] => 
                                )

                        )

                    [workout_email] => Array
                        (
                            [email_subject] => 
                            [email_title] => 
                            [email_content] => 
                        )

                )

        )

    [introduction] => Array
        (
            [intro_title] => Program Intro
            [intro_content] => <p>Bacon ipsum dolor amet short ribs sirloin hamburger, doner leberkas landjaeger ball tip salami alcatra. Ham flank pastrami beef ribs meatball, alcatra biltong hamburger frankfurter doner. Cupim t-bone ham hock porchetta, pork loin rump tongue hamburger shoulder shank cow flank. Drumstick jerky turkey ham pig brisket pork chop prosciutto chuck alcatra capicola porchetta pastrami shoulder. Boudin tongue chicken tenderloin ham sausage capicola. Prosciutto leberkas ball tip capicola, bresaola burgdoggen chuck pork belly tongue pork chop cupim shank kielbasa sausage.</p>
<p>Tail pork chop cupim flank buffalo strip steak. Turkey spare ribs turducken beef ribs alcatra ribeye hamburger kielbasa porchetta brisket sirloin cow buffalo. Short ribs biltong strip steak sirloin pancetta, pork chop bacon ball tip beef ribs. Frankfurter pancetta hamburger corned beef, strip steak kielbasa ribeye short ribs andouille ball tip ham. Beef ribs burgdoggen shankle alcatra turducken bresaola tenderloin meatball strip steak landjaeger short ribs ball tip. Doner pork belly short loin pancetta tri-tip rump strip steak landjaeger capicola fatback shank kielbasa. Sirloin pastrami turkey corned beef brisket tail kevin.</p>

        )

)

*/
