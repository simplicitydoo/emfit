<?php
	
# EMFIT DATEPICKER

?>

<div class="modal fade datepickerwrap" id="modal-datepickier" tabindex="-1" role="dialog" aria-labelledby="Chose Your Start Date">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="livewrap">

<div class="headerwrap">

<div class="closewrap text-right">
<button type="button" class="btn btn-modal textlc closebutton" data-dismiss="modal" aria-label="Close" id="datepickerclose">Close</button>
</div>

<div class="pickerintro text-center">
Choose a start date below to begin!
</div>

</div>

<div class="calendarwrap">

<div id="datepicker"></div>

<div class="buttonwrap">
<a class="pricebutton btgreen" href="#" id="pickerdate">
<div class="label">Submit Date</div><div class="price" id="datepicker-date"></div>
</a>
</div>

<div class="messagewrap text-center" id="datepickermessage" style="display:none;">
</div>

</div>

</div>
</div>
</div>
</div>
