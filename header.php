<?php

# EMFIT HEADER

# nav
$topnav = get_field('nav_topbar', 'options');
$headernav = get_field('nav_headerbar', 'options');
$mobilenav = get_field('settings_mobilenav', 'options');

# load cart
$cart = shopifycart_load();

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="shortcut icon" href="<?= TEMPLATE_ASSETS ?>/elements/favicon_0.png" type="image/png">

<?php if (ENV_SERVER == 'server') { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T8SQVTJ');</script>
<!-- End Google Tag Manager -->	
<?php } ?>

<?php wp_head(); ?>

</head>

<body class="emfitbody" id="emfitbody">

<?php wp_body_open(); ?>

<?php if (ENV_SERVER == 'server') { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8SQVTJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php } ?>


<?php # MAIN CONTENT ?>

<div class="maincontent" id="maincontent">


<?php # desktop ?>

<header class="emfitheader" id="header">

<div class="headertop">
<div class="container flex height100">
<div class="items textuc">
<?php
if (!empty($topnav)) {
	foreach ($topnav as $key => $value) {
		$link = $value['link'];
?>
<a href="<?= $link['page'] ?>"><?= $link['label'] ?></a>
<?php
	}
}
?>
</div>
</div>
</div>

<div class="headerwrap">

<div class="container flex flexrow flexalign flexspace height100">

<div class="logowrap">
<a class="headerlogo bgcontain hidetext navclear" href="/">EmFit</a>
</div>

<div class="midwrap textuc">

<a href="<?= $headernav['findmyfit'] ?>" class="navclear">
Find<span>My</span>Fit
<div class="hoverwrap"><div class="arrowwrap bgcover"></div></div>
</a>

<a href="<?= $headernav['shop'] ?>" class="navclear navtrigger" data-subnav="shop" id="navtrigger-shop">
Shop
<div class="hoverwrap"><div class="arrowwrap bgcover"></div></div>
</a>

<a href="<?= $headernav['news_media'] ?>" class="navclear navtrigger" data-subnav="news" id="navtrigger-news">
News + Media
<div class="hoverwrap"><div class="arrowwrap bgcover"></div></div>
</a>

</div>

<div class="rightwrap textuc text-right">

<?php if (is_user_logged_in()) { ?>
<a href="<?= MEMBER_DASHBOARD ?>" class="navclear">My Account</a>
<?php } else { ?>
<a href="<?= $headernav['signin'] ?>" class="navclear">Sign In / Sign Up</a>	
<?php } ?>

<?php if (!empty($cart['contents'])) { ?>
<a href="<?= EMFIT_CART ?>" class="navclear">My Cart</a>
<?php } ?>

</div>

</div>

<div class="dropdownwrap">

<div class="headersubnav flex textlc" data-subnav="shop" id="headersubnav-shop" style="display:none;">
<div class="navwrap text-center">
<?php
foreach ($headernav['shop_subnav'] as $key => $value) {
	$url = $value['page_link'];
	$target = '';
	if ($value['link_type'] == 'offsite') {
		$url = $value['offsite_link'];
		$target = 'target="_blank"';
	}
?>
<a href="<?= $url ?>" <?= $target ?>><?= $value['label'] ?></a>	
<?php } ?>
</div>
</div>

<div class="headersubnav flex textlc" data-subnav="news" id="headersubnav-news" style="display:none;">
<div class="navwrap text-center">
<?php
foreach ($headernav['news_subnav'] as $key => $value) {
	$url = $value['page_link'];
	$target = '';
	if ($value['link_type'] == 'offsite') {
		$url = $value['offsite_link'];
		$target = 'target="_blank"';
	}
?>
<a href="<?= $url ?>" <?= $target ?>><?= $value['label'] ?></a>	
<?php } ?>
</div>
</div>

</div>

</div>

</header>


<?php # mobile ?>

<header class="headermobile flex">

<div class="hamburger" id="hamburger">
<div class="line"></div>
<div class="line middle"></div>
<div class="line"></div>
</div>

<a class="headerlogo bgcontain hidetext" href="/">EmFit</a>
	
<div class="slidernav text-center flex flexrow" id="slidernav" style="display:none;">

<div class="column">
<?php
if (!empty($mobilenav['left_column'])) {
	
	foreach ($mobilenav['left_column'] as $key => $value) {
		
		$label = $value['link_label'];
		$url = $value['page'];
		$target = '';
		
		if ($value['link_type'] == 'offsite') {
			$url = $value['url'];
			$target = 'target="_blank"';
		}		
?>
<a href="<?= $url ?>" <?= $target ?>><?= $label ?></a>
<?php
	}	
}
?>
</div>

<div class="column">

<?php if (is_user_logged_in()) { ?>
<a href="<?= MEMBER_DASHBOARD ?>">My Account</a>
<?php } else { ?>
<a href="<?= $headernav['signin'] ?>">Sign In / Sign Up</a>	
<?php } ?>

<?php if (!empty($cart['contents'])) { ?>
<a href="<?= EMFIT_CART ?>">My Cart</a>
<?php } ?>

<?php
if (!empty($mobilenav['right_column'])) {
	
	foreach ($mobilenav['right_column'] as $key => $value) {
		
		$label = $value['link_label'];
		$url = $value['page'];
		$target = '';
		
		if ($value['link_type'] == 'offsite') {
			$url = $value['url'];
			$target = 'target="_blank"';
		}		
?>
<a href="<?= $url ?>" <?= $target ?>><?= $label ?></a>
<?php
	}	
}
?>

</div>

</div>

</header>
