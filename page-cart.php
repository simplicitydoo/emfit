<?php
/**
Template name: Shopping Cart
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'cart';

# load cart
$cart = shopifycart_load();
# print_r($cart);

# load id list
$idlist = shopifyproducts_idlist_load();
# print_r($idlist);

# quantity nonce
$nonce = wp_create_nonce(CART_QUANTITY_NONCE);

# header
get_header();

?>

<?php # SUBHERO ?>

<div class="product-subhero bgorange">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 text-center flex height100">
<div class="blurbwrap">
if you have any questions, just <a href="/contact/">contact us</a> or visit the <a href="/faq/">FAQ page</a>
</div>
</div>
</div>
</div>
</div>


<?php # HEADER ?>

<div class="cart-header">
<div class="container">
<div class="row">
<div class="col-sm-12">

<div class="headerwrap flex flexrow flexspace">

<div class="title textuc">
Shopping Cart
</div>

<?php if (!empty($cart['contents'])) { ?>
<div class="buttonwrap">

<button class="featurebutton removeall noshadow" id="button-removeall">Remove All</button>

<form method="post" action="<?php echo admin_url('admin-post.php') ?>" id="form-removeall">
<input type="hidden" name="action" value="emfitcart_removeall">
</form>

</div>
<?php } ?>

</div>

</div>
</div>
</div>
</div>


<?php # ENTRIES ?>

<section class="cart-entries">
<div class="container">
<div class="row">
<div class="col-sm-12 col-xl-10 offset-xl-1">
	
<?php if (empty($cart['contents'])) { ?>
Your Cart is empty	
<?php } else { ?>

<div class="cartwrap flex flexrow flexspace">
<div class="entries">
<?php

	$cartcount = 0;
	$subtotal = 0;
	$requirelogin = false;

	foreach ($cart['contents'] as $comboid => $value) {

		list($shopifyid, $variantid) = explode(':', $comboid);
		if (!isset($idlist[$shopifyid]['variants'][$variantid])) { continue; }
		if ($idlist[$shopifyid]['status'] != 'publish') { continue; }

		$cartcount++;

		$product = $idlist[$shopifyid];
		$variant = $product['variants'][$variantid];
		$productid = $product['postid'];
		$thumbnail = get_field('list_thumbnail', $productid);
		$productname =  $product['name'];
		$type = $product['type'];
		$price = $variant['price'];
		$quantity = $value;
		$subtotal += ($price * $quantity);

		$variants = get_field('shopify_product_variants', $productid);
		$shopifyvariant = array();
		foreach ($variants as $vkey => $ventry) {
			if ($variantid == $ventry['shopify_variant']['shopify_variant_ids']['shopify_variant_id']) {
				$shopifyvariant = $ventry['shopify_variant'];
			}
		}

		if ( ($type == 'strength') || ($type == 'challenge') ) {
			$requirelogin = true;
			$thumbnail = $variants[0]['shopify_variant']['program_thumbnail'];
			if ($product['format'] == 'variants') {
				$version = $variant['name'];
				if (ctype_digit($variant['name'])) { $version = 'Version ' . $variant['name']; }
				$productname .= '<br>' . $version;
				foreach ($variants as $vkey => $ventry) {
					if ($variantid == $ventry['shopify_variant']['shopify_variant_ids']['shopify_variant_id']) {
						$thumbnail = $ventry['shopify_variant']['program_thumbnail'];
					}
				}
			}
		}

		else if ($product['format'] != 'single') {
			$productname .= '<br>' . $variant['name'];
		}

		$maxquantity = '';

		if ( ($type != 'strength') && ($type != 'challenge') ) {
			$inventory = $shopifyvariant['shopify_variant_inventory'];
			if ($inventory['inventory_policy'] == 'deny') {
				$maxquantity = $inventory['available'];
			}
		}
		
?>
<div class="entry">

<div class="thumb bgcover" style="background-image:url(<?= $thumbnail ?>);"></div>

<div class="details">
<div class="innerwrap flex flexspace">

<div class="info">
	
<div class="name">
<?= $productname ?>
</div>

<div class="price">
$<?= $price ?>
</div>

<?php if ( ($type != 'strength') && ($type != 'challenge') ) { ?>
<div class="quantity">
Quantity:
<input type="number" name="quantity" class="optionamount" min="1" max="<?= $maxquantity ?>" step="1" value="<?= $quantity ?>" data-productid="<?= $shopifyid ?>" data-variantid="<?= $variantid ?>" data-nonce="<?= $nonce ?>">
</div>
<?php } ?>

</div>

<div class="actions">

<div class="action textuc">
<a href="#" class="removeitem" data-removeid=<?= $cartcount ?>>Remove</a>
</div>

<form method="post" action="<?php echo admin_url('admin-post.php') ?>" id="remove-<?= $cartcount ?>">
<input type="hidden" name="action" value="emfitcart_remove">
<input type="hidden" name="productid" value="<?= $shopifyid ?>">
<input type="hidden" name="variantid" value="<?= $variantid ?>">
</form>

</div>

</div>
</div>

</div>

<?php } ?>
</div>

<div class="summarywrap">
<div class="summary">
	
<div class="subtotal flex flexrow flexspace textuc emgreen">
<div class="label">
Order Total
</div>
<div class="amount">
$<span id="cart-subtotal"><?= $subtotal ?></span>
</div>
</div>


<?php if ( (!$requirelogin) || (is_user_logged_in()) ) { ?>
<div class="buttonwrap" id="checkoutwrap">
<button class="featurebutton checkout" id="button-checkout">Checkout</button>
</div>

<div class="message text-center" id="checkoutmessage" style="display:none;">
Preparing your checkout…
</div>

<form method="post" action="<?php echo admin_url('admin-post.php') ?>" id="form-checkout">
<input type="hidden" name="action" value="emfitcart_checkout">
</form>

<?php } else { ?>
<div class="buttonwrap" id="checkoutwrap">
<a href="<?= MEMBER_LOGIN ?>?return=cart"><button class="featurebutton checkout">Sign In / Sign Up</button></a>
</div>

<div class="message info text-center">
Create or log into your account to continue!
</div>

<?php } ?>

</div>
</div>

</div>
<?php } ?>

</div>
</div>
</div>
</section>


<?php
	
# footer
get_footer();
