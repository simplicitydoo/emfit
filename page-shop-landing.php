<?php
/**
Template name: Shop / Landing
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'shoplanding';

# content
$hero = get_field('hero_panel');
$entries = get_field('menu_entries');

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # ENTRIES ?>

<section class="shop-menu">

<div class="shopwrap flex flexrow flexwrap">
<?php
foreach ($entries as $key => $entry) {
	
	$url = $entry['page_link'];
	$target = '';
	
	if ($entry['link_type'] == 'offsite_link') {
		$url = $entry['page_link'];
		$target = 'target="_blank"';
	}
	
?>
<a class="entry bgcover" href="<?= $url ?>" <?= $target ?> style="background-image:url(<?= $entry['background_image'] ?>);">
<div class="labelwrap">
<div class="label text-center textuc flex">
<?= $entry['menu_label'] ?>
</div>
</div>
</a>
<?php } ?>
</div>

</section>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();

/*
Array
(
    [0] => Array
        (
            [menu_label] => Swag
            [link_type] => page
            [page_link] => http://www.emfit.test/swag/
            [offsite_link] => 
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/09/shop-swag-2x.jpg
        )

    [1] => Array
        (
            [menu_label] => Supplements
            [link_type] => page
            [page_link] => http://www.emfit.test/supplements/
            [offsite_link] => 
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/09/shop-supplements-2x.jpg
        )

    [2] => Array
        (
            [menu_label] => The EmPack
            [link_type] => offsite
            [page_link] => 
            [offsite_link] => https://www.evolvedmotion.com/
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/09/shop-empack-2x.jpg
        )

    [3] => Array
        (
            [menu_label] => Herbal element
            [link_type] => offsite
            [page_link] => 
            [offsite_link] => https://herbalelement.com/
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/09/shop-element-2x.jpg
        )

    [4] => Array
        (
            [menu_label] => Body Awareness
            [link_type] => offsite
            [page_link] => 
            [offsite_link] => https://www.thebodyawarenessproject.com/
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/09/shop-awareness-2x.jpg
        )

)
*/