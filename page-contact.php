<?php
/**
Template name: Contact
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'contact';

# content
$hero = get_field('hero_panel');
$formpanel = get_field('form_panel');

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # FORM ?>

<?php
$upper = 'subtitle';
$lower = 'title';

if ($formpanel['bold_text'] == 'upper') {
	$upper = 'title';
	$lower = 'subtitle';
}
?>

<section class="contact-form">
<div class="container">

<div class="rowwrap flex flexrow">

<div class="titlewrap emgreen">

<div class="<?= $upper ?>">
<?= $formpanel['upper_title'] ?>
</div>

<div class="<?= $lower ?>">
<?= $formpanel['lower_title'] ?>
</div>

</div>

<div class="formwrap">
<div class="formlive">
<?php get_template_part('panels/panel-emform'); ?>

</div>
</div>

</div>

</div>
</div>

</div>
</section>



<?php # FEATURE TABS ?>

<?php get_template_part('panels/panel-featuretabs'); ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();
