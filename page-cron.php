<?php
/**
Template name: Geek / Cron
 */

# DAILY EMAIL TRIGGER

# slug: c5ceeddac30ea7f4e10540c74e
# key: a3714f132df722c8c4451341c3

# oops
if (!isset($_GET['key'])) { wp_die(); }

# key
$key = $_GET['key'];

# validate
if ($key != 'a3714f132df722c8c4451341c3') { wp_die(); }

# go for it
email_cron();

# echo "done";

# bye!
wp_die();
