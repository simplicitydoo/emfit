<?php

# EMFIT / OVERVIEW

# page flag
$GLOBALS['emfit']['pageid'] = 'overview';

# content
$program = get_field('program_info');
$callouts = get_field('hero_callouts');
$intro = get_field('introduction');
$hilites = get_field('highlights');
$bullets = get_field('bullet_points');
$info = get_field('additional_info');
$successid = get_field('related_success_story'); // 808
$related = get_field('related_programs');

$successpanel = array();
if (!empty($successid)) { $successpanel = success_panel($successid); }

# hero
$hero = get_field('interactive_hero');
$herovideo = get_field('hero_video');

# button
$productid = $program['program'];
$variant = array();
$button = products_button($productid);

if (!empty($button)) {
	$version = $program['version'];
	if (isset($button['variants'][$version])) {
		$variant = $button['variants'][$version];
	}
	else { $variant = array_shift($button['variants']); }
}

# colors
$procolor = get_field('program_color', $productid);
if (!$procolor) { $procolor = 'propurple'; }

# header
get_header();

?>

<?php # HERO ?>

<section class="overview-hero <?= $hero['hero_type'] ?>">

<?php if ($hero['hero_type'] != 'none') { ?>
<div class="calloutbg"></div>
<?php } ?>

<div class="container height100">
<div class="panelwrap height100">

<div class="headerwrap flex flexrow flexspace">

<div class="headerleft">

<div class="name textlc <?= $procolor ?>">
<?= $program['title'] ?>
</div>

<div class="arrowwrap">
<a class="arrow arrowleft" href="/findmyfit/">Go Back</a>
</div>

</div>

<?php if (!empty($button)) { ?>
<div class="headerright">
<div class="buttonwrap">
<a class="pricebutton btgreen btlarge cartbutton" href="#" data-productid="<?= $button['shopifyid'] ?>" data-variantid="<?= $variant['variantid'] ?>" data-nonce="<?= $button['nonce'] ?>">
<div class="label">Get Started Now</div><div class="price"><?= $variant['price'] ?></div>
</a>
</div>
</div>
<?php } ?>

</div>

<?php
if ($hero['hero_type'] != 'none') {
	
	$back1 = '2';
	$next1 = '2';

	$back2 = '1';
	$next2 = '1';

	$back3 = '2';
	$next3 = '1';

	$back4 = '3';
	$next4 = '1';

	if ($hero['hero_type'] == 'build') {
		$back1 = '3';
		$next2 = '3';
	}

	if ($hero['hero_type'] == 'burn') {
		$back1 = '4';
		$next2 = '3';
		$next3 = '4';		
	}
	
?>

<div class="calloutwrap <?= $hero['hero_type'] ?> bgcover">

<div class="overdot dot1 bgcover" data-callout="1" id="overdot-1"></div>
<div class="overdot dot2 bgcover" data-callout="2" id="overdot-2"></div>

<?php if ( ($hero['hero_type'] == 'build') || ($hero['hero_type'] == 'burn') ) { ?>
<div class="overdot dot3 bgcover" data-callout="3" id="overdot-3"></div>
<?php } ?>

<?php if ($hero['hero_type'] == 'burn') { ?>
<div class="overdot dot4 bgcover" data-callout="4" id="overdot-4"></div>
<?php } ?>

<div class="callout callout1" id="callout-1">
<a href="#" class="closebutton calloutclose textuc">Close</a>
<div class="title textlc">	
<?= $hero['callout_1']['title'] ?>
</div>
<div class="blurb">	
<?= $hero['callout_1']['blurb'] ?>
</div>
<div class="arrowwrap flex flexrow flexspace">	
<a href="#" class="calloutarrow arrowleft flex" data-callout="<?= $back1 ?>">back</a>
<a href="#" class="calloutarrow arrowright flex" data-callout="<?= $next1 ?>">next</a>
</div>
</div>

<div class="callout callout2" id="callout-2">
<a href="#" class="closebutton calloutclose textuc">Close</a>
<div class="title textlc">	
<?= $hero['callout_2']['title'] ?>
</div>
<div class="blurb">	
<?= $hero['callout_2']['blurb'] ?>
</div>
<div class="arrowwrap flex flexrow flexspace">	
<a href="#" class="calloutarrow arrowleft flex" data-callout="<?= $back2 ?>">back</a>
<a href="#" class="calloutarrow arrowright flex" data-callout="<?= $next2 ?>">next</a>
</div>
</div>

<?php if ( ($hero['hero_type'] == 'build') || ($hero['hero_type'] == 'burn') ) { ?>
<div class="callout callout3" id="callout-3">
<a href="#" class="closebutton calloutclose textuc">Close</a>
<div class="title textlc">	
<?= $hero['callout_3']['title'] ?>
</div>
<div class="blurb">	
<?= $hero['callout_3']['blurb'] ?>
</div>
<div class="arrowwrap flex flexrow flexspace">	
<a href="#" class="calloutarrow arrowleft flex" data-callout="<?= $back3 ?>">back</a>
<a href="#" class="calloutarrow arrowright flex" data-callout="<?= $next3 ?>">next</a>
</div>
</div>
<?php } ?>

<?php if ($hero['hero_type'] == 'burn') { ?>
<div class="callout callout4" id="callout-4">
<a href="#" class="closebutton calloutclose textuc">Close</a>
<div class="title textlc">	
<?= $hero['callout_4']['title'] ?>
</div>
<div class="blurb">	
<?= $hero['callout_4']['blurb'] ?>
</div>
<div class="arrowwrap flex flexrow flexspace">	
<a href="#" class="calloutarrow arrowleft flex" data-callout="<?= $back4 ?>">back</a>
<a href="#" class="calloutarrow arrowright flex" data-callout="<?= $next4 ?>">next</a>
</div>
</div>
<?php } ?>

</div>
</div>
	
<?php } ?>

<?php if ($herovideo['include_hero_video']) { ?>
<div class="herobutton text-center">
<a class="pricebutton btgreen" href="<?= $herovideo['youtube_url'] ?>" data-lity>
<div class="label"><?= $herovideo['label_text'] ?></div><div class="arrow arrowright"></div>
</a>
</div>
<?php } ?>

</div>
</section>


<?php # NAV ?>

<div class="overview-nav flex">
<div class="container">

<div class="navwrap textuc text-center">
<a class="overviewnavitem" href="#" data-scroll="overview-intro">Introduction</a>
<a class="overviewnavitem" href="#" data-scroll="overview-hilites">Highlights</a>
<a class="overviewnavitem" href="#" data-scroll="overview-details">What&nbsp;You&nbsp;Get<span class="extra"> + Need</span></a>
<a class="overviewnavitem" href="#" data-scroll="overview-info">Additional Info</a>
</div>

</div>
</div>


<?php # INTRO ?>

<section class="overview-intro text-center bg<?= $procolor ?>" id="overview-intro">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
	
<div class="name">
<?= $program['title'] ?>
</div>

<div class="title">
Introduction
</div>

<div class="hilite">
<?= $intro['bold_text'] ?>
</div>

<div class="text">
<?= $intro['regular_text'] ?>
</div>

</div>
</div>
</div>
</section>


<?php # HILITES ?>

<section class="overview-hilites"  id="overview-hilites">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1">

<div class="titlerow">

<div class="name">
<?= $program['title'] ?>
</div>

<div class="title <?= $procolor ?>">
Highlights
</div>

</div>

<div class="hilites hi<?= $procolor ?> flex flexrow flexspace flexwrap">

<?php
if (!empty($hilites)) {
	foreach ($hilites as $key => $value) {
?>
<div class="entry">
<div class="label textuc">
<?= $value['title'] ?>
</div>
<div class="blurb">
<?= $value['text'] ?>
</div>
</div>
<?php		
	}
}
?>

</div>

</div>
</div>
</div>
</section>


<?php # DETAILS ?>

<section class="overview-details" id="overview-details">
<div class="container">
<div class="row">

<div class="col-sm-12 col-lg-10 offset-lg-1">

<div class="titlerow text-center">

<div class="name">
<?= $program['title'] ?>
</div>

<div class="title emorange">
What You Get + Need
</div>

</div>

</div>

<div class="rowwrap flex flexrow flexspace">

<div class="selections textlc">
<div class="detailselector flex selected" data-selection="get">What You Get</div>
<div class="detailselector flex" data-selection="need">What You Need</div>
</div>

<div class="details">

<div class="detailset" id="details-get">
<?php
if (!empty($bullets['what_you_get'])) {
	foreach ($bullets['what_you_get'] as $key => $value) {
?>
<div class="detail">
<?= $value['item'] ?>
</div>
<?php		
	}
}
?>
</div>

<div class="detailset" id="details-need" style="display:none;">
<?php
if (!empty($bullets['what_you_need'])) {
	foreach ($bullets['what_you_need'] as $key => $value) {
?>
<div class="detail">
<?= $value['item'] ?>
</div>
<?php		
	}
}
?>
</div>

</div>

</div>

</div>
</div>
</section>


<?php # INFO ?>

<section class="overview-info bgcover" style="background-image:url(<?= $info['background_image'] ?>);" id="overview-info">
<div class="scrim height100">
<div class="container">

<div class="rowwrap flex flexrow flexspace flexalign">

<div class="titlewrap flex">

<div class="name">
<?= $program['title'] ?>
</div>

<div class="title <?= $procolor ?>">
Additional Info
</div>

</div>

<div class="infowrap">

<?php
if (!empty($info['faq'])) {
	foreach ($info['faq'] as $key => $value) {
?>
<div class="entry">
<div class="question">
<?= $value['question'] ?>
</div>
<div class="answer">
<?= $value['answer'] ?>
</div>
</div>
<?php		
	}
}
?>

</div>

</div>

</div>
</div>
</section>


<?php # GET IN TOUCH ?>

<section class="overview-intouch text-center">
<div class="container">

<div class="blurb">
If you need more guidance or have ANY questions please contact us!
</div>

<div class="buttonwrap">
<a href="/contact/"><button class="featurebutton btgreen">Get in Touch Now!</button></a>
</div>

</div>
</section>


<?php # SUCCESS ?>

<?php if (!empty($successpanel)) { ?>
<section class="panel-success single">

<div class="successentry">

<div class="imagewrap bgcover" style="background-image:url(<?= $successpanel['after'] ?>);"></div>

<div class="textwrap flex">

<?php if (!empty($successpanel['kicker'])) { ?>
<div class="kicker textuc">
<?= $successpanel['kicker'] ?>
</div>
<?php } ?>

<div class="title">
<?= $successpanel['displayname'] ?>
</div>

<div class="excerpt">
<?= $successpanel['excerpt'] ?>
</div>

<div class="actionwrap">

<div class="buttonwrap">
<a href="/success-stories/"><button class="featurebutton btbrown">View All Success Stories</button></a>
</div>

</div>

</div>

</div>

</section>
<?php } ?>


<?php # GALLERY ?>


<?php # QUIZ ?>

<?php get_template_part('panels/panel-quiz'); ?>


<?php # RELATED ?>

<?php if (!empty($related)) { ?>
<section class="overview-related">
<div class="container">

<div class="titlewrap text-center">

<div class="subhead">
Some Suggestions
</div>

<div class="title">
Other Programs You Might Like
</div>

</div>

<div class="scroller">
<div class="programwrap flex flexrow flexspace">

<?php
	foreach ($related as $key => $value) {

		$overviewid = $value['program'];
		$entry = products_overview_related($overviewid);
		if (empty($entry)) { continue; }
			
?>
<a href="<?= $entry['overviewurl'] ?>" class="programentry">

<div class="header textuc flex">
<div class="flex flexrow flexspace">
<div class="price">
<?= $entry['price'] ?>
</div>
<div class="favorite" style="display:none;">
Favorite
</div>
</div>
</div>

<div class="imagewrap bgcover" style="background-image:url(<?= $entry['image'] ?>);"></div>

<div class="content text-center">
	
<div class="type textuc">
<?= $entry['type'] ?>
</div>

<div class="namewrap <?= $entry['procolor'] ?>">
<div class="name">
<?= $entry['name'] ?>
</div>
<?php if (!empty($entry['variantname'])) { ?>
<div class="version textlc">
<?= $entry['variantname'] ?>
</div>
<?php } ?>
</div>

<div class="textwrap">
<?= $entry['blurb'] ?>
</div>

</div>

</a>	
<?php } ?>

</div>
</div>

</div>
</section>
<?php } ?>


<?php # GIFT CARD ?>

<?php get_template_part('panels/panel-giftcard'); ?>


<?php
	
# footer
get_footer();
