<?php

# EMFIT / WEBHOOKS

add_action('init', 'setup_webhooks_posttype');

function setup_webhooks_posttype() {

	$labels = array(
		'name' => __('Webhook'),
		'singular_name' => __('Webhook'),
		'add_new' => __('Add Webhook'),
		'add_new_item' => __('Add Webhook'),
		'edit_item' => __('Edit Webhook'),
		'new_item' => __('New Webhook'),
		'all_items' => __('All Webhooks'),
		'view_item' => __('View Webhook'),
		'search_items' => __('Search Webhooks'),
		'not_found' => __('No Webhooks found'),
		'not_found_in_trash' => __('No Webhook found in the Trash'),
		'menu_name' => 'Webhooks',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Webhook template',
		'supports' => array('title'),
		'show_in_admin_bar' => false,
		'menu_position' => 90,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'z',
			'with_front' => false,
		),
	);

	register_post_type('emfit_webhook', $arguments);

}

add_action('add_meta_boxes_emfit_webhook', 'setup_webhooks_posttype_metabox', 100);

function setup_webhooks_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_webhook', 'normal');
}

