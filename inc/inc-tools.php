<?php
	
# EMFIT TOOLS

# custom query vars
add_filter('query_vars', 'tools_queryvars');

function tools_queryvars ($vars) {

	$vars[] = 'filter';
	$vars[] = 'sort';
	$vars[] = 'programid';
	$vars[] = 'challengeid';

	return $vars;

}


# return json
function tools_returnjson ($response = array()) {
	
	# headers
	header('Pragma: no-cache');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	if (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
		header('Content-type: application/json');
	}
	else { header('Content-type: text/plain'); }
	
	# default reponse
	if (empty($response)) {
		$response['status'] = 'error';
	}

	# away you go
	echo json_encode($response);
	
	# bye!
	wp_die();
	
}


# admin only
function tools_adminonly() {

	if (!current_user_can('edit_posts')) {
		wp_redirect(site_url());	
		exit;
	}
	
}


# CONTACT FORM

# nonce
define('FORM_CONTACT_NONCE', 'form-contact');

add_action('wp_ajax_emform', 'tools_contactform');
add_action('wp_ajax_nopriv_emform', 'tools_contactform');

function tools_contactform() {

	# check nonce
	check_ajax_referer(FORM_CONTACT_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['name'])) || (!isset($_POST['company'])) || (!isset($_POST['email'])) || (!isset($_POST['message'])) ) { wp_die(); }

	# convenience
	$name = wp_strip_all_tags(trim($_POST['name']));
	$company = wp_strip_all_tags(trim($_POST['company']));
	$email = trim($_POST['email']);
	$message = wp_strip_all_tags(trim($_POST['message']));

	# we good?
	if ( (empty($email)) || (empty($message)) ) { tools_returnjson(); }
	if (!is_email($email)) { tools_returnjson(); }
	if (strlen($message) > 400) { tools_returnjson(); }

	# futz
	if (empty($name)) { $name = '(none)'; }
	if (empty($company)) { $company = '(none)'; }

	$content = <<<EOD
Name: $name

Company: $company

Email: $email

Message:
$message

EOD;

	# settings
	$to = CONTACT_EMAIL;
	# $to = 'geek@lemonadeisgreat.com';
	$subject = 'EmFit Contact Form';

	$headers = array(
		"Reply-To: <$email>;",
	);

	# fire!
	if (ENV_SERVER != 'bugs') {
		wp_mail($to, $subject, $content, $headers);	
	}

	# init response
	$response = array(
		'status' => 'success',
	);

	# back at ya
	tools_returnjson($response);
	
}


# OFFER FORM

# nonce
define('FORM_OFFER_NONCE', 'form-offer');

add_action('wp_ajax_offerform', 'tools_offerform');
add_action('wp_ajax_nopriv_offerform', 'tools_offerform');

function tools_offerform() {

	# check nonce
	check_ajax_referer(FORM_OFFER_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['name'])) || (!isset($_POST['email'])) ) { wp_die(); }

	# convenience
	$name = wp_strip_all_tags(trim($_POST['name']));
	$email = trim($_POST['email']);

	# we good?
	if ( (empty($name)) || (empty($email)) ) { tools_returnjson(); }
	if (!is_email($email)) { tools_returnjson(); }

	# fake the form
	# https://app.convertkit.com/forms/designers/1054115/edit
	$formid = '1054115';
	$formurl = "https://app.convertkit.com/forms/$formid/subscriptions";

	$params = array(
	    "fields[first_name]" => $name,
	    "email_address" => $email,
	);

	$ch = curl_init();
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_URL, $formurl);
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($params));
	$result = curl_exec($ch);

	if (ENV_SERVER == 'bugs') {
		file_put_contents('/z/logs/temp/offerform-' . time() . '.txt', var_export($result, TRUE));
	}

	# init response
	$response = array(
		'status' => 'success',
	);

	# back at ya
	tools_returnjson($response);

}
