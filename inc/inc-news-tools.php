<?php

# EMFIT / NEWS + PODCASE TOOLS


# NEWS / CATEGORIES

function news_categories() {

	# acf entry
	$categories = array(
		'podcast' => 'Podcast',
		'television' => 'Television',
	);

	return $categories;

}


# NEWS / ENTRIES

function news_entries ($batch = 0) {
	
	# settings
	$items = 10;
	$offset = $batch * $items;
	
	# init arrays
	$entries = array();

	# args
	$args = array(
		'post_type' => 'emfit_news',
		'post_status' => 'publish',
		'posts_per_page' => $items,
		'order' => 'DESC',
		'orderby' => 'date',
		'offset' => $offset,
	);
	
	# fetch!
	$newsposts = get_posts($args);
			
	# oops
	if (empty($newsposts)) { return $entries; }

	# roll 'em
	foreach ($newsposts as $newspost) {

		$id = $newspost->ID;
		$date = get_the_date('', $newspost);
		$url = get_the_permalink($newspost);

		$category = get_field('news_category', $id);
		$source = get_field('news_source', $id);
		$title = get_field('news_title', $id);
		$listview = get_field('list_view', $id);
		$logo = $listview['logo'];	
		$excerpt = $listview['excerpt'];	
		$logoimage = '';
		if (isset($listview['logo_image'])) { $logoimage = $listview['logo_image']; }

		$entries[] = array(
			'category' => $category,
			'logo' => $logo,
			'logo_image' => $logoimage,
			'source' => $source,
			'title' => $title,
			'date' => $date,
			'excerpt' => $excerpt,
			'url' => $url,
		);

	}

	// increment batch
	$batch++;
	if (count($newsposts) < $items) { $batch = 0; }

	# set 'em up
	$newsbatch = array(
		'entries' => $entries,
		'nextbatch' => $batch,
	);
	
	# back at ya
	return $newsbatch;
		
}


# NEWS / LOAD MORE

# nonce
define('PAGE_NEWS_NONCE', 'page-news');

add_action('wp_ajax_news_loadmore', 'news_loadmore');
add_action('wp_ajax_nopriv_news_loadmore', 'news_loadmore');

function news_loadmore() {

	# check nonce
	check_ajax_referer(PAGE_NEWS_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['nextbatch'])) || (!ctype_digit($_POST['nextbatch'])) ) { wp_die(); }

	# convenience
	$nextbatch = intval($_POST['nextbatch']);

	# get entries
	$newsbatch = news_entries($nextbatch);

	# oops
	if (empty($newsbatch)) { tools_returnjson(); }

	# init response
	$response = array(
		'status' => 'success',
		'batch' => $newsbatch,
	);
	
	# back at ya
	tools_returnjson($response);
	
}


# PODCAST / ENTRIES

function podcast_entries ($batch = 0) {
	
	# settings
	$items = 9;
	$offset = $batch * $items;
	
	# init arrays
	$entries = array();

	# args
	$args = array(
		'post_type' => 'emfit_news',
		'post_status' => 'publish',
		'posts_per_page' => $items,
		'order' => 'DESC',
		'orderby' => 'date',
		'offset' => $offset,
		'meta_query' => array(
			array (
				'key' => 'news_category',
				'value' => 'podcast',
				'compare' => '=',
			),
		),
	);
	
	# fetch!
	$podcasts = get_posts($args);
			
	# oops
	if (empty($podcasts)) { return $entries; }

	# roll 'em
	foreach ($podcasts as $podcast) {

		$id = $podcast->ID;
		$date = get_the_date('', $podcast);
		$url = get_the_permalink($podcast);

		$source = get_field('news_source', $id);
		$title = get_field('news_title', $id);
		$info = get_field('podcast', $id);

		$image = $info['feature_image'];

		$entries[] = array(
			'image' => $image,
			'source' => $source,
			'title' => $title,
			'date' => $date,
			'url' => $url,
		);

	}

	// increment batch
	$batch++;
	if (count($podcasts) < $items) { $batch = 0; }

	# set 'em up
	$podbatch = array(
		'entries' => $entries,
		'nextbatch' => $batch,
	);
	
	# back at ya
	return $podbatch;
		
}


# PODCAST / LOAD MORE

# nonce
define('PAGE_PODCAST_NONCE', 'page-news');

add_action('wp_ajax_podcast_loadmore', 'podcast_loadmore');
add_action('wp_ajax_nopriv_podcast_loadmore', 'podcast_loadmore');

function podcast_loadmore() {

	# check nonce
	check_ajax_referer(PAGE_PODCAST_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['nextbatch'])) || (!ctype_digit($_POST['nextbatch'])) ) { wp_die(); }

	# convenience
	$nextbatch = intval($_POST['nextbatch']);

	# get entries
	$podbatch = podcast_entries($nextbatch);

	# oops
	if (empty($podbatch)) { tools_returnjson(); }

	# init response
	$response = array(
		'status' => 'success',
		'batch' => $podbatch,
	);
	
	# back at ya
	tools_returnjson($response);
	
}


# ALL NEWS
# geek function

function news_all() {
	
	# args
	$args = array(
		'post_type' => 'emfit_news',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'orderby' => 'date',
	);
	
	# fetch!
	$newsposts = get_posts($args);
			
	# oops
	if (empty($newsposts)) { return; }

	# hidden field
	echo '<div style="display:none">' . "\n";
		
	# roll 'em
	foreach ($newsposts as $newspost) {
		$url = get_the_permalink($newspost);
		echo "$url\n";
	}

	# wrap it up
	echo "</div>\n\n";

	# back at ya
	return;
		
}

/*
    [0] => WP_Post Object
        (
            [ID] => 1289
            [post_author] => 7
            [post_date] => 2018-08-23 19:24:34
            [post_date_gmt] => 2018-08-23 19:24:34
            [post_title] => DX Is Not A to B, It’s a Mindset - Will Lassalle
            [post_excerpt] => 
            [post_status] => publish
            [comment_status] => closed
            [ping_status] => closed
            [post_password] => 
            [post_name] => dx-is-not-a-to-b-its-a-mindset-will-lassalle
            [to_ping] => 
            [pinged] => 
            [post_modified] => 2018-08-23 19:24:34
            [post_modified_gmt] => 2018-08-23 19:24:34
            [post_content_filtered] => 
            [post_parent] => 0
            [guid] => http://www.vantiq.test/?post_type=vantiqtv&#038;p=1289
            [menu_order] => 0
            [post_type] => vantiqtv
            [post_mime_type] => 
            [comment_count] => 0
            [filter] => raw

*/
