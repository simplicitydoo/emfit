<?php

# DUPLICATE PAGE


# INIT ACTIONS

add_action('admin_init', 'vduplicator_init');

function vduplicator_init() {

	global $post, $typenow, $current_screen;

	$posttype = '';

	if (!is_admin()) { return; }
	else if ($post && $post->post_type) { $posttype = $post->post_type; }
	else if ($typenow) { $posttype = $typenow; }
	else if ($current_screen && $current_screen->post_type) { $posttype = $current_screen->post_type; }
	else if (isset($_REQUEST['post'])) { $posttype = get_post_type($_REQUEST['post']); }

	# echo "Posttype: $posttype";

	# programs/challenges only
	if ( ($posttype != 'emfit_challenge') && ($posttype != 'emfit_program') && ($posttype != 'emfit_overview') ) { return; }
	if ($posttype == 'acf-field-group') { return; }

	# list actions
	add_filter('post_row_actions', 'vduplicator_postlist', 10, 2);
	add_filter('page_row_actions', 'vduplicator_postlist', 10, 2);

	# create revision
    add_action('admin_action_vduplicator_create', 'vduplicator_create');
	
}


# UI TOOLS

# revision link
function vduplicator_postlist ($actions, $post) {

	$link = vduplicator_createlink($post);

	if (!empty($link)) {
		# http://www.vantiq.test/wp-admin/admin.php?action=vduplicator_create&post=1777&_wpnonce=7cca477a55
		$actions['vduplicator'] = '<a href="' . $link . '" title="Duplicate This">Duplicate This</a>';
	}

	return $actions;

}

# create revision link with nonce
function vduplicator_createlink ($post) {

	# post id
	$postid = $post->ID;

	# link / referrer
	$link = wp_nonce_url(admin_url('admin.php?action=vduplicator_create&post=' . $postid), 'vduplicator-create-' . $postid);

	# back at ya
	return $link;

}


# CREATE DUPLICATE

# bail from admin
function vduplicator_adminbail ($message = 'Invalid Duplicate Request') {
	wp_die($message);
}

# call from admin
function vduplicator_create() {

	# bad id
	if ( (!isset($_REQUEST['post'])) || (!ctype_digit($_REQUEST['post'])) ) { vduplicator_adminbail(); }

	# post id
	$postid = intval($_REQUEST['post']);

	# bad user
	if (!current_user_can('edit_post', $postid)) { vduplicator_adminbail(); }

	# bad referral
	check_admin_referer('vduplicator-create-' . $postid);

	# roll 'em
	$newid = vduplicator_duplicate($postid);

	# oops
	if (!$newid) { vduplicator_adminbail(); }

	# on your way
	wp_redirect(admin_url('post.php?action=edit&post=' . $newid));
	exit;

}

# create duplicate post
function vduplicator_duplicate ($originalid = 0) {

	# safety first!
	if (!$originalid) { return false; }

	# get post
	$post = get_post($originalid);
	if (empty($post)) { return false; }

	# duplicate main post
    $authorid = get_current_user_id();	

	$args = array(
		'post_status' => 'draft',
		'post_author' => $authorid,
		'menu_order' => $post->menu_order,
		'comment_status' => $post->comment_status,
		'ping_status' => $post->ping_status,
		'post_content' => $post->post_content,
		'post_excerpt' => $post->post_excerpt,
		'post_mime_type' => $post->post_mime_type,
		'post_parent' => $post->post_parent,
		'post_password' => $post->post_password,
		'post_title' => $post->post_title,
		'post_type' => $post->post_type,
		'post_date' => $post->post_date,
		'post_date_gmt' => $post->post_date_gmt,
	);

	$newid = wp_insert_post($args);

	# oops
	if (!$newid) { return false; }

	# update slug
	$slug = wp_unique_post_slug($post->post_name, $newid, 'publish', $post->post_type, $post->post_parent);
	$args = array(
		'ID' => $newid,
		'post_name' => $slug,
	);
	wp_update_post($args);

	# transfer meta
	vduplicator_transfermeta ($originalid, $newid, $post->post_type);
	
	# back at ya
	return $newid;

}

# transfer meta
function vduplicator_transfermeta ($originalid = 0, $newid = 0, $posttype = '') {

	# copy categories
	global $wpdb;
	
	if (isset($wpdb->terms)) {
		
		# clear default category
		wp_set_object_terms($newid, NULL, 'category');
		
		# set new categories
		$taxonomies = get_object_taxonomies($posttype);
		
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($originalid, $taxonomy, array('orderby' => 'term_order'));
			$terms = array();
			for ($i=0; $i<count($post_terms); $i++) {
				$terms[] = $post_terms[$i]->slug;
			}
			wp_set_object_terms($newid, $terms, $taxonomy);
		}

	}

	# remove old meta
	$metakeys = get_post_custom_keys($newid);
	if (!empty($metakeys)) {
		foreach ($metakeys as $metakey) {
			delete_metadata('post', $newid, $metakey);
		}
	}

	# add new meta
	$metakeys = get_post_custom_keys($originalid);

	if (!empty($metakeys)) {
		foreach ($metakeys as $metakey) {
			$metavalues = get_post_custom_values($metakey, $originalid);
			foreach ($metavalues as $metavalue) {
				$metavalue = maybe_unserialize($metavalue);
				add_metadata('post', $newid, $metakey, $metavalue);
			}
		}
	}

}
