<?php

# EMFIT CHALLENGE TOOLS


# FACEBOOK SELECT

add_filter('acf/load_field/name=challenge_facebook_select', 'challenge_facebook_select');

function challenge_facebook_select ($field) {

	# already done?
	if (isset($GLOBALS['emfit']['act']['challenge_facebook_select'])) {
		$field['choices'] = $GLOBALS['emfit']['act']['challenge_facebook_select'];
		return $field;
	}

	# reset choices
	$field['choices'] = array();	

	# gather entries
	$entries = get_field('settings_facebook_groups', 'options');
	
	# oops
	if (empty($entries)) { return $field; }

	# roll' em
	foreach ($entries as $key => $value) {
		$label = $value['group_name'];
		$uuid = $value['uuid'];
		$field['choices'][$uuid] = $label;
	}

	# set global
	$GLOBALS['emfit']['act']['challenge_facebook_select'] = $field['choices'];

	# back at ya
	return $field;
	
}


# MEAL PLAN SELECT

add_filter('acf/load_field/name=challenge_mealplan_select', 'challenge_mealplan_select');

function challenge_mealplan_select ($field) {

	# already done?
	if (isset($GLOBALS['emfit']['act']['challenge_mealplan_select'])) {
		$field['choices'] = $GLOBALS['emfit']['act']['challenge_mealplan_select'];
		return $field;
	}

	# reset choices
	$field['choices'] = array();	

	# gather entries
	$entries = get_field('settings_meal_plans', 'options');
	
	# oops
	if (empty($entries)) { return $field; }

	# roll' em
	foreach ($entries as $key => $value) {
		$label = $value['label'];
		$uuid = $value['uuid'];
		$field['choices'][$uuid] = $label;
	}

	# set global
	$GLOBALS['emfit']['act']['challenge_mealplan_select'] = $field['choices'];

	# back at ya
	return $field;
	
}


# RESOURCE SELECT

add_filter('acf/load_field/name=challenge_resource_select', 'challenge_resource_select');

function challenge_resource_select ($field) {

	# already done?
	if (isset($GLOBALS['emfit']['act']['challenge_resource_select'])) {
		$field['choices'] = $GLOBALS['emfit']['act']['challenge_resource_select'];
		return $field;
	}

	# reset choices
	$field['choices'] = array();	

	# gather entries
	$entries = get_field('settings_resource_library', 'options');
	
	# oops
	if (empty($entries)) { return $field; }

	# roll' em
	foreach ($entries as $key => $value) {
		$label = $value['label'];
		$uuid = $value['uuid'];
		$field['choices'][$uuid] = $label;
	}

	# set global
	$GLOBALS['emfit']['act']['challenge_resource_select'] = $field['choices'];

	# back at ya
	return $field;
	
}
