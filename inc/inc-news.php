<?php

# EMFIT / NEWS

# tools
require_once(TEMPLATE_INC . '/inc-news-tools.php');

# post type
add_action('init', 'setup_news_posttype');

function setup_news_posttype() {

	$labels = array(
		'name' => __('News'),
		'singular_name' => __('News'),
		'add_new' => __('Add News'),
		'add_new_item' => __('Add News'),
		'edit_item' => __('Edit News'),
		'new_item' => __('New News'),
		'all_items' => __('All News'),
		'view_item' => __('View News'),
		'search_items' => __('Search News'),
		'not_found' => __('No News found'),
		'not_found_in_trash' => __('No News found in the Trash'),
		'menu_name' => 'News',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit News template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'news',
			'with_front' => false,
		),
	);

	register_post_type('emfit_news', $arguments);

}

add_action('add_meta_boxes_emfit_news', 'setup_news_posttype_metabox', 100);

function setup_news_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_news', 'normal');
}
