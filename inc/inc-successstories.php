<?php

# EMFIT / SUCCESS STORIES

add_action('init', 'success_setup_posttype');

function success_setup_posttype() {

	$labels = array(
		'name' => __('Success Stories'),
		'singular_name' => __('Success Story'),
		'add_new' => __('Add Success Story'),
		'add_new_item' => __('Add Success Story'),
		'edit_item' => __('Edit Success Story'),
		'new_item' => __('New Success Story'),
		'all_items' => __('All Success Stories'),
		'view_item' => __('View Success Story'),
		'search_items' => __('Search Success Stories'),
		'not_found' => __('No Success Stories found'),
		'not_found_in_trash' => __('No Success Stories found in the Trash'),
		'menu_name' => 'Success Stories',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Success template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'success',
			'with_front' => false,
		),
	);

	register_post_type('emfit_success', $arguments);

}

add_action('add_meta_boxes_emfit_success', 'setup_success_posttype_metabox', 100);

function setup_success_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_success', 'normal');
}


# GET ENTRIES

function success_list() {

	# init entries
	$entries = array();

	# args
	$args = array(
		'post_type' => 'emfit_success',
		'post_status' => 'publish',
		'order' => 'ASC',
		'orderby' => 'title',
		'posts_per_page' => -1,
	);

	# fetch!
	$posts = get_posts($args);

	# oops
	if (empty($posts)) { return $entries; }

	# roll 'em
	foreach ($posts as $post) {

		$id = $post->ID;
		# $url = get_the_permalink($post);

		$type = get_field('type', $id);
		$images = get_field('images', $id);
		# $text = get_field('story', $id);

		$name = get_field('name', $id);
		if (empty($name)) { $name = $post->post_title; }

		$entries[] = array(
			'id' => $id,
			'type' => $type,
			'images' => $images,
			'name' => $name,
			# 'text' => $text,
			# 'url' => $url,
		);

	}

	# back at ya
	return $entries;

}


# SUCCESS MODAL

# nonce
define('PAGE_SUCCESS_NONCE', 'page-success');

# actions
add_action('wp_ajax_success_popup', 'success_modal');
add_action('wp_ajax_nopriv_success_popup', 'success_modal');

function success_modal() {

	# check nonce
	check_ajax_referer(PAGE_SUCCESS_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['successid'])) || (!ctype_digit($_POST['successid'])) ) { wp_die(); }

	# convenience
	$successid = intval($_POST['successid']);
	
	# fetch!
	$post = get_post($successid);
	if (empty($post)) { wp_die(); }

	# compile
	$kicker = '';
	$type = get_field('type', $successid);
	if ($type != 'other') {
		$kicker = 'Success in ' . $type;
	}

	$images = get_field('images', $successid);
	$before = $images['before'];
	$after = $images['after'];

	if (empty($after)) {
		if (!empty($before)) { $after = $before; }
		$before = '';
	}

	$name = get_field('name', $successid);
	if (empty($name)) { $name = $post->post_title; }

	$text = get_field('story', $successid);

	$entry = array(
		'kicker' => $kicker,
		'before' => $before,
		'after' => $after,
		'displayname' => $name,
		'text' => $text,
	);

	# init response
	$response = array(
		'status' => 'success',
		'entry' => $entry,
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}


# SUCCESS PANEL

function success_panel ($successid = 0) {

	# init entry
	$entry = array();

	# safety first!
	if ($successid == 0) { return $entry; }

	# fetch!
	$post = get_post($successid);
	if (empty($post)) { return $entry; }
	
	# compile
	$kicker = '';
	$type = get_field('type', $successid);
	if ($type != 'other') {
		$kicker = 'Success in ' . $type;
	}

	$images = get_field('images', $successid);
	$before = $images['before'];
	$after = $images['after'];

	if (empty($after)) {
		if (!empty($before)) { $after = $before; }
	}

	$name = get_field('name', $successid);
	if (empty($name)) { $name = $post->post_title; }

	$excerpt = get_field('story', $successid);
	if (strlen($excerpt) > 650) {
		$excerpt = substr($excerpt, 0, strrpos(substr($excerpt, 0, 650), ' '));
		$excerpt .= '&hellip;';		
	}

	$entry = array(
		'kicker' => $kicker,
		'after' => $after,
		'displayname' => $name,
		'excerpt' => $excerpt,
	);

	# back at ya
	return $entry;
	
}


# SUCCESS ALL / CAROUSEL

function success_all() {

	# init entries
	$entries = array();

	# args
	$args = array(
		'post_type' => 'emfit_success',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'title',
	);
	
	# fetch!
	$posts = get_posts($args);
			
	# oops
	if (empty($posts)) { return $entries; }

	# roll 'em
	foreach ($posts as $post) {

		$successid = $post->ID;

		# compile
		$kicker = '';
		$type = get_field('type', $successid);
		if ($type != 'other') {
			$kicker = 'Success in ' . $type;
		}
	
		$images = get_field('images', $successid);
		$before = $images['before'];
		$after = $images['after'];
	
		if (empty($after)) {
			if (!empty($before)) { $after = $before; }
		}
	
		$name = get_field('name', $successid);
		if (empty($name)) { $name = $post->post_title; }
	
		$excerpt = get_field('story', $successid);
		if (strlen($excerpt) > 650) {
			$excerpt = substr($excerpt, 0, strrpos(substr($excerpt, 0, 650), ' '));
			$excerpt .= '&hellip;';		
		}
	
		$entries[] = array(
			'type' => $type,
			'kicker' => $kicker,
			'before' => $before,
			'after' => $after,
			'displayname' => $name,
			'excerpt' => $excerpt,
		);

	}

	# back at ya
	return $entries;
	
}
