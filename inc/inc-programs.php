<?php

# EMFIT / PROGRAMS

add_action('init', 'setup_programs_posttype');

function setup_programs_posttype() {

	$labels = array(
		'name' => __('EmFit Program'),
		'singular_name' => __('EmFit Program'),
		'add_new' => __('Add Program'),
		'add_new_item' => __('Add Program'),
		'edit_item' => __('Edit Program'),
		'new_item' => __('New Program'),
		'all_items' => __('All Programs'),
		'view_item' => __('View Program'),
		'search_items' => __('Search Programs'),
		'not_found' => __('No Programs found'),
		'not_found_in_trash' => __('No Programs found in the Trash'),
		'menu_name' => 'Programs',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Program template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'program',
			'with_front' => false,
		),
	);

	register_post_type('emfit_program', $arguments);

}

add_action('add_meta_boxes_emfit_program', 'setup_programs_posttype_metabox', 100);

function setup_programs_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_program', 'normal');
}
