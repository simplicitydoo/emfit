<?php

# EMFIT / OVERVIEW

add_action('init', 'setup_overview_posttype');

function setup_overview_posttype() {

	$labels = array(
		'name' => __('Overviews'),
		'singular_name' => __('Overview'),
		'add_new' => __('Add Overview'),
		'add_new_item' => __('Add Overview'),
		'edit_item' => __('Edit Overview'),
		'new_item' => __('New Overview'),
		'all_items' => __('All Overviews'),
		'view_item' => __('View Overview'),
		'search_items' => __('Search Overviews'),
		'not_found' => __('No Overviews found'),
		'not_found_in_trash' => __('No Overviews found in the Trash'),
		'menu_name' => 'Overviews',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Overview template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'overview',
			'with_front' => false,
		),
	);

	register_post_type('emfit_overview', $arguments);

}

# add_action('add_meta_boxes_emfit_overview', 'setup_overview_posttype_metabox', 100);

function setup_overview_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_overview', 'normal');
}
