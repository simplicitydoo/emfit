<?php

# EMFIT / ACF


# POST TYPE

add_action('init', 'setup_acfstub_posttype');

function setup_acfstub_posttype() {

	$labels = array(
		'name' => __('TechTool'),
		'menu_name' => 'TechTools',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'public' => false,
		'show_ui' => true,
		'show_in_admin_bar' => false,
		'menu_position' => 90,
		'has_archive' => false,
		'hierarchical' => false,
	);

	register_post_type('acfstub', $arguments);

}

add_action('add_meta_boxes_acfstub', 'setup_acfstub_posttype_metabox', 100);

function setup_acfstub_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'acfstub', 'normal');
}


# DISABLE FIELD
# use class acfdisable in setup

add_action('admin_head', 'acf_disable_input');

function acf_disable_input() {
  echo '<style>
.acfdisable input {
	color: rgba(51,51,51,.5);
	border-color: rgba(222,222,222,.75);
	background: #f8f8f8;
	pointer-events: none;
}
</style>';
}


# CART BUTTON VARIANTS

add_filter('acf/load_field/name=cartbutton_select_variant', 'acf_cartbuttonvariants');

function acf_cartbuttonvariants ($field) {

	# get button
	$productid = get_the_ID();
	$button = products_button($productid);
	if (!$button) { return $field; }
	if ($button['single']) { return $field; }
	if (empty($button['variants'])) { return $field; }

	# print_r($button); return $field;	

	# init field
	$field['choices'] = array();

	# cycle through variants
	foreach ($button['variants'] as $name => $value) {
		$variantid = $value['variantid'];
		$field['choices'][$variantid] = $name;
	}

    return $field;	
	
}
