<?php
	
# EMFIT / EMAIL

# init email db
define('EMFIT_EMAILDB', $wpdb->prefix . 'emfitemail');
# email_db();


# PROGRAM

# test email / admin only
add_action('wp_ajax_program_test', 'email_programtest');

function email_programtest() {

	# check nonce
	check_ajax_referer(MEMBER_PROGRAM_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['programid'])) || (!isset($_POST['week'])) || (!isset($_POST['day'])) ) { wp_die(); }
	if ( (!ctype_digit($_POST['programid'])) || (!ctype_digit($_POST['week'])) || (!ctype_digit($_POST['day'])) ) { wp_die(); }

	# convenience
	$programid = intval($_POST['programid']);
	$week = intval($_POST['week']);
	$day = intval($_POST['day']);

	# user
	$userid = get_current_user_id();
	if ($userid == 0) { wp_die(); }

	# away you go
	$success = email_program($programid, $week, $day, $userid, true);

	# oops
	if (!$success) { membertools_ajax_return(); }
	
	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);

}

# full email
function email_program ($programid = 0, $workoutweek = 0, $workoutday = 0,  $userid = 0, $admin = false) {

	# safety first!
	if ( (!$programid) || (!$workoutweek) || (!$workoutday) || (!$userid) ) { return false; }

	# get workouts
	$workouts = get_field('Workouts', $programid);
	if (empty($workouts)) { return false; }	

	# init workout
	$workout = array();
	
	# find the fish
	foreach ($workouts as $key => $value) {
		$settings = $value['workout_settings'];
		if ( ($settings['workout_week'] != $workoutweek) || ($settings['workout_day'] != $workoutday) ) { continue; }
		$workout = $value;
		break;
	}	

	# oops
	if (empty($workout)) { return false; }

	# load id lists
	$idlist = shopifyproducts_idlist_load();
	$programlist = shopifyproducts_programlist_load();

	$shopifyid = $programlist[$programid]['shopifyid'];
	$variantid = $programlist[$programid]['variantid'];

	$product = $idlist[$shopifyid];
	$variant = $product['variants'][$variantid];

	$programname = '<strong>' . $product['name'] . '</strong>';
	$previewtext = $product['name'];

	if ( ($variant['name'] != '[default]') && (!empty($variant['name'])) ) {
		if (ctype_digit($variant['name'])) {
			$programname .= '<br>Version ' . $variant['name'];
			$previewtext .= ' - Version ' . $variant['name'];
		}
		else {
			$programname .= '<br>' . $variant['name'];
			$previewtext .= ' - ' . $variant['name'];
		}
	}

	$previewtext .= ': Week ' . $workoutweek . ' / Day ' . $workoutday;

	$subject = $previewtext;
	if (!empty($workout['workout_email']['email_subject'])) { $subject = $workout['workout_email']['email_subject']; }

	$link = site_url('/members/program/?programid=' . $programid);
	if ($admin) { $link = get_permalink($programid); }

	# init content
	$content = '';

	# title block
	$content .= <<<ENDTEMPLATE
<p style="font-size:24px;text-align:center;margin:20px 0 10px;">$programname<p>
<p style="font-size:16px;text-align:center;margin-bottom:20px;"><a href="$link">View Program Online</a></p>
ENDTEMPLATE;

	# day
	$content .= <<<ENDTEMPLATE
<p style="font-size:30px;text-align:center;margin-bottom:20px;"><strong>Week $workoutweek / Day $workoutday</strong></p>
ENDTEMPLATE;

	
	if (!empty($workout['workout_steps'])) {
		
		foreach ($workout['workout_steps'] as $skey => $svalue) {
			
			$settings = $svalue['step_settings'];

			# step label
			$steplabel = $settings['step_id'] . '. ';			
			if ($settings['step_type'] == 'warmup') { $steplabel = 'Warmup'; }
			else { $steplabel .= $settings['step_name']; }

			$content .= <<<ENDTEMPLATE
<p style="font-size:24px;margin-bottom:20px;color:#800C77;"><strong>$steplabel</strong></p>
ENDTEMPLATE;

			# instructions
			$instructions = $svalue['step_instructions'];

			$content .= <<<ENDTEMPLATE
<p style="margin-bottom:20px;font-weight:bold;line-height:1.5;">$instructions</p>
ENDTEMPLATE;

			# video
			if (!empty($svalue['step_videos'])) {			

				$content .= <<<ENDTEMPLATE
<p style="margin-bottom:20px;font-weight:bold;line-height:1.5;">
ENDTEMPLATE;

				foreach ($svalue['step_videos'] as $vkey => $vvalue) {
					$vlink = $vvalue['youtube_link'];
					$vlaabel = $vvalue['video_title'];
					$content .= <<<ENDTEMPLATE
<a href="$vlink">VIDEO: $vlaabel</a><br>
ENDTEMPLATE;
				}
	
				$content .= '</p>';

			}
			
		}
		
	}

	# compile message
	$message = email_template($content, $previewtext);

	# echo $message;
	# exit;

	# get user
	$user = get_userdata($userid);
	$emailto = $user->user_email;

	# fire!
	$success = mandrill_send($subject, $emailto, $message);

	# back at ya
	return $success;

}


# CHALLENGE

# test email / admin only
add_action('wp_ajax_challenge_test', 'email_challengetest');

function email_challengetest() {

	# check nonce
	check_ajax_referer(MEMBER_CHALLENGE_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['challengeid'])) || (!isset($_POST['day'])) ) { wp_die(); }
	if ( (!ctype_digit($_POST['challengeid'])) || (!ctype_digit($_POST['day'])) ) { wp_die(); }

	# convenience
	$challengeid = intval($_POST['challengeid']);
	$day = intval($_POST['day']);

	# user
	$userid = get_current_user_id();
	if ($userid == 0) { wp_die(); }

	# away you go
	$success = email_challenge($challengeid, $day, $userid, true);

	# oops
	if (!$success) { membertools_ajax_return(); }
	
	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);

}

# full email
function email_challenge ($challengeid = 0, $challengeday = 0,  $userid = 0, $admin = false) {

	# safety first!
	if ( (!$challengeid) || (!$challengeday) || (!$userid) ) { return false; }

	# get messages
	$messages = get_field('challenge_messages', $challengeid);
	if (empty($messages)) { return false; }	

	# init message
	$message = array();
	
	# find the fish
	foreach ($messages as $key => $value) {
		$settings = $value['message_settings'];
		if ($settings['message_day'] != $challengeday) { continue; }
		$message = $value;
		break;
	}	

	# oops
	if (empty($message)) { return false; }

	# get elements
	$title = $message['message_settings']['message_title'];
	$mainimage = $message['message_image'];
	$maincontent = $message['message_content'];
	$useworkout = $message['include_workout'];
	$workout = $message['message_workout'];
	$usevideo = $message['include_video'];
	$videolabel = $message['video_label'];
	$videolink = $message['youtube_link'];
	$usefooter = $message['include_footer_text'];
	$footertext = $message['message_footer'];

	if (empty($videolabel)) { $videolabel = 'Watch Video'; }

	# load id lists
	$idlist = shopifyproducts_idlist_load();
	$programlist = shopifyproducts_programlist_load();

	$shopifyid = $programlist[$challengeid]['shopifyid'];
	$variantid = $programlist[$challengeid]['variantid'];

	$product = $idlist[$shopifyid];
	$variant = $product['variants'][$variantid];

	$challengename = '<strong>' . $product['name'] . '</strong>';
	$previewtext = $product['name'];

	if ( ($variant['name'] != '[default]') && (!empty($variant['name'])) ) {
		if (ctype_digit($variant['name'])) {
			$challengename .= '<br>Version ' . $variant['name'];
			$previewtext .= ' - Version ' . $variant['name'];
		}
		else {
			$challengename .= '<br>' . $variant['name'];
			$previewtext .= ' - ' . $variant['name'];
		}
	}

	$previewtext .= ': Day ' . $challengeday;

	$subject = $title;
	if (empty($subject)) { $subject = $previewtext; }

	$link = site_url('/members/challenge/?challengeid=' . $challengeid);
	if ($admin) { $link = get_permalink($challengeid); }

	# init content
	$content = '';

	# title block
	$content .= <<<ENDTEMPLATE
<p style="font-size:24px;text-align:center;margin:20px 0 10px;">$challengename<p>
<p style="font-size:16px;text-align:center;margin-bottom:20px;"><a href="$link">View Challenge Online</a></p>
ENDTEMPLATE;

	# day
	$content .= <<<ENDTEMPLATE
<p style="font-size:30px;text-align:center;margin-bottom:20px;"><strong>Day $challengeday: $title</strong></p>
ENDTEMPLATE;

	# image
	if (!empty($mainimage)) {
		$content .= <<<ENDTEMPLATE
<p style="margin-bottom:20px;"><img src="$mainimage" style="width:100%;"></p>
ENDTEMPLATE;
	}

	# main content
	$content .= $maincontent;
	
	# workout
	if ( ($useworkout) && (!empty($workout)) ) {
		$content .= <<<ENDTEMPLATE
<p style="font-size:24px;margin:20px 0;color:#5BC095;"><strong>Today’s Workout</strong></p>
ENDTEMPLATE;
		$content .= $workout;
	}

	# video
	if ( ($usevideo) && (!empty($videolink)) ) {
		$content .= <<<ENDTEMPLATE
<p style="font-size:24px;margin:20px 0;color:#5BC095;"><strong>Today’s Video</strong></p>
<p style="margin-bottom:20px;font-weight:bold;"><a href="$videolink">$videolabel</a></p>
ENDTEMPLATE;
	}

	# footer
	if ( ($usefooter) && (!empty($footertext)) ) {
		$content .= $footertext;
	}

	# compile message
	$message = email_template($content, $previewtext);

	# get user
	$user = get_userdata($userid);
	$emailto = $user->user_email;

	# fire!
	$success = mandrill_send($subject, $emailto, $message);

	# back at ya
	return $success;
			
}


# TEMPLATE

function email_template ($content = '', $previewtext = '') {
	
	# logo
	$logo = TEMPLATE_ASSETS . '/auto/email-logo-2x.png';
	
	# THE WORKS

	$template = <<<ENDTEMPLATE
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>EmFit Email</title>
<style type="text/css">
/* -------------------------------------
RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 620px) {
table[class=body] h1 {
font-size: 28px !important;
margin-bottom: 10px !important; }
table[class=body] p,
table[class=body] ul,
table[class=body] ol,
table[class=body] td,
table[class=body] span,
table[class=body] a {
font-size: 16px !important; }
table[class=body] .wrapper,
table[class=body] .article {
padding: 10px !important; }
table[class=body] .content {
padding: 0 !important; }
table[class=body] .container {
padding: 0 !important;
width: 100% !important; }
table[class=body] .main {
border-left-width: 0 !important;
border-radius: 0 !important;
border-right-width: 0 !important; }
table[class=body] .btn table {
width: 100% !important; }
table[class=body] .btn a {
width: 100% !important; }
table[class=body] .img-responsive {
height: auto !important;
max-width: 100% !important;
width: auto !important; }}
/* -------------------------------------
PRESERVE THESE STYLES IN THE HEAD
------------------------------------- */
@media all {
.ExternalClass {
width: 100%; }
.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
line-height: 100%; }
.apple-link a {
color: inherit !important;
font-family: inherit !important;
font-size: inherit !important;
font-weight: inherit !important;
line-height: inherit !important;
text-decoration: none !important; }
</style>
</head>
<body class="" style="background-color:#fff;font-family:arial,helvetica,sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#fff;width:100%;">
<tr>
<td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
<td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
<div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
<!-- CENTERED WHITE CONTAINER -->
<span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">$previewtext</span>
<table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;border-radius:3px;width:100%;">
<!-- MAIN CONTENT -->
<tr>
<td class="wrapper" style="font-family:arial,helvetica,sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
<tr><td align="center">
<img src="$logo" width="284" height="53" alt="EmFit">
</td></tr>
<tr><td>
$content
</td></tr>
</table>
</td>
</tr>
</table>
</div>
</td>
<td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
</tr>
</table>
</body>
</html>
ENDTEMPLATE;

# back at ya
return $template;
	
}


# EMAIL DATABASE
# https://codex.wordpress.org/Creating_Tables_with_Plugins

function email_db() {

	# already there
	$installed = get_option('emfitemaildb_version');
	if ($installed == '1') { return; }

	# create table
	global $wpdb;
	$table_name = EMFIT_EMAILDB; 
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
id mediumint(9) NOT NULL AUTO_INCREMENT,
dateid varchar(10),
userid int DEFAULT 0,
type varchar(1),
programid int DEFAULT 0,
week int DEFAULT 0,
day int DEFAULT 0,
PRIMARY KEY (id),
KEY dateid (dateid)
) $charset_collate;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

	update_option('emfitemaildb_version', '1');   

	# echo 'Table added!';

}


# EMAIL CRON

function email_cron() {

	echo 'Cron start<br>';

	# init db
	global $wpdb;

	# get date
	$dateid = date('Y-m-d');
	# $dateid = '2019-09-18';

	echo $dateid . '<br>';

	# zap old entries
	$wpdb->query('DELETE FROM ' . EMFIT_EMAILDB . ' WHERE dateid < "' . $dateid . '"');

	# get today's entries
	$list = $wpdb->get_results('SELECT * FROM ' . EMFIT_EMAILDB . ' WHERE dateid = "' . $dateid . '"');
	# print_r($list);
	if (empty($list)) { return 'empty'; }

	# get mandrill quota
	$quota = mandrill_quota();

	echo $quota . '<br>';

	if (ENV_SERVER == 'server') {
		file_put_contents('/z/logs/emcron/cron-quota-' . time() . '.txt', $quota);
	}

	# oops
	if ($quota == 0) { return 'error'; }
	
	# var_dump($list);
	
	# roll 'em
	foreach ($list as $key => $entry) {
		
		# echo '<br>entry';
		
		# convenience
		$rowid = intval($entry->id);
		$userid = intval($entry->userid);
		$type = $entry->type;
		$programid = intval($entry->programid);
		$week = intval($entry->week);
		$day = intval($entry->day);

		$success = false;

		# program
		if ($type == 'P') {
			continue;
			# $success = email_program($programid, $week, $day, $userid);
		}
		
		# challenge
		else if ($type == 'C') {
			$success = email_challenge($programid, $day, $userid);
		}

		# zap entry
		if ($success) {
			$result = $wpdb->get_results('DELETE FROM ' . EMFIT_EMAILDB . ' WHERE id = ' . $rowid);
		}
		
		# wait a sec
		usleep($quota);
		
	}

}
