<?php

# EMFIT / CHALLENGES

# tools
require_once(TEMPLATE_INC . '/inc-challenges-tools.php');

add_action('init', 'setup_challenges_posttype');

function setup_challenges_posttype() {

	$labels = array(
		'name' => __('EmFit Challenge'),
		'singular_name' => __('EmFit Challenge'),
		'add_new' => __('Add Challenge'),
		'add_new_item' => __('Add Challenge'),
		'edit_item' => __('Edit Challenge'),
		'new_item' => __('New Challenge'),
		'all_items' => __('All Challenges'),
		'view_item' => __('View Challenge'),
		'search_items' => __('Search Challenges'),
		'not_found' => __('No Challenges found'),
		'not_found_in_trash' => __('No Challenges found in the Trash'),
		'menu_name' => 'Challenges',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Challenge template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'challenge',
			'with_front' => false,
		),
	);

	register_post_type('emfit_challenge', $arguments);

}

add_action('add_meta_boxes_emfit_challenge', 'setup_challenges_posttype_metabox', 100);

function setup_challenges_posttype_metabox() {
	remove_meta_box('wpseo_meta', 'emfit_challenge', 'normal');
}
