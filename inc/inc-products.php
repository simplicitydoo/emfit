<?php

# EMFIT / PRODUCTS

# tools
require_once(TEMPLATE_INC . '/inc-products-tools.php');

# post type
add_action('init', 'setup_products_posttype');

function setup_products_posttype() {

	$labels = array(
		'name' => __('EmFit Product'),
		'singular_name' => __('EmFit Product'),
		'add_new' => __('Add Product'),
		'add_new_item' => __('Add Product'),
		'edit_item' => __('Edit Product'),
		'new_item' => __('New Product'),
		'all_items' => __('All Products'),
		'view_item' => __('View Product'),
		'search_items' => __('Search Products'),
		'not_found' => __('No Products found'),
		'not_found_in_trash' => __('No Products found in the Trash'),
		'menu_name' => 'Products',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Product template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'product',
			'with_front' => false,
		),
	);

	register_post_type('emfit_product', $arguments);

}


# SAVE POST

# revision priority: 20
# add_action('acf/save_post', 'setup_products_savepost', 30);
add_action('save_post_emfit_product', 'setup_products_savepost', 10, 1);

function setup_products_savepost ($postid) {

	# go away
	if (!is_admin()) { return; }

	# regenerate product list
	shopifyproducts_idlist();

}
