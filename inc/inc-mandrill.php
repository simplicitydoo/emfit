<?php
	
# EMFIT / MANDRILL

# SEND

function mandrill_send ($subject = '', $emailto = '', $content = '') {

	# safety first!
	if ( (empty($subject)) || (empty($emailto)) || (empty($content)) ) { return false; }

	# compile
	$compile = array(
		'key' => 'prruBLl9JDHYi7DtlGA2zg',
		'message' => array(
			'html' => $content,
			'subject' => $subject,
			'from_email' => CONTACT_EMAIL,
			'from_name' => 'Emily Schromm, EmFit',
			'to' => array(
				'to' => array(
					'email' => $emailto,
				),
			),
			'headers' => array(
				'Reply-To' => CONTACT_EMAIL,
			),
		),
	);

	$url = 'https://mandrillapp.com/api/1.0/messages/send.json';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($compile));
	$result = curl_exec($ch);
	# print_r($result);

	# errors?
	$errnum = curl_errno($ch);

	# close curl
	curl_close($ch);

	# curl error
	if ($errnum != 0) { return false; }

	# decode result
	$success = false;
	$response = json_decode($result, true);
	if ($response['status'] == 'sent') { $success = true; }
	
	# back at ya
	return $success;
		
}


# TEST

function mandrill_test() {

	# compile
	$compile = array(
		'key' => 'prruBLl9JDHYi7DtlGA2zg',
		'message' => array(
			'html' => 'Hola!',
			'subject' => 'Mandrill test',
			'from_email' => 'geek@lemonadeisgreat.com',
			'from_name' => 'Nojo Geek',
			'to' => array(
				'to' => array(
					'email' => 'mike@mikeleeandme.com',
				),
			),
			'headers' => array(
				'Reply-To' => 'geek@lemonadeisgreat.com',
			),
		),
	);

	$url = 'https://mandrillapp.com/api/1.0/messages/send.json';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($compile));
	$result = curl_exec($ch);

	# errors?
	# [{"email":"geek@lemonadeisgreat.com","status":"sent","_id":"5f0af02955504a1fab43aca97057996c","reject_reason":null}]
	# $errnum = curl_errno($ch);

	# close curl
	curl_close($ch);

	# print_r($result);

	# yay!
	echo 'Done!';
			
}


# QUOTA CHECK

function mandrill_quota() {

	$key = array(
		'key' => 'prruBLl9JDHYi7DtlGA2zg',
	);

	$jsonkey = json_encode($key);
	$url = 'https://mandrillapp.com/api/1.0/users/info.json';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($key));
	$result = curl_exec($ch);

	# errors?
	$errnum = curl_errno($ch);

	# close curl
	curl_close($ch);

	# oops
	if ($errnum != 0) { return 0; }

	# print_r($result);
	# exit;

	$settings = json_decode($result, true);
	$quota = intval($settings['hourly_quota']);
	$microsecond = ceil((3600 / $quota) * 1000000);
	
	return $microsecond;
	# exit;
		
}


# PHPMAILER

add_action('phpmailer_init', 'maindrill_phpmailer');

function maindrill_phpmailer ($phpmailer) {

	$phpmailer->isSMTP();
	$phpmailer->SMTPAuth = true;
	$phpmailer->SMTPSecure = 'tls';
	$phpmailer->SMTPKeepAlive = true;
	$phpmailer->Host = 'smtp.mandrillapp.com';
	$phpmailer->Port = 587;
	$phpmailer->Username = 'schromm.emily@gmail.com';
	$phpmailer->Password = 'prruBLl9JDHYi7DtlGA2zg';

	$phpmailer->From = CONTACT_EMAIL;
	$phpmailer->FromName = 'Emily Schromm, EmFit';

}
