<?php

# EMFIT / PRODUCTS TOOLS


# PRODUCT BUTTON

function products_button ($producttid = 0) {

	# oops
	if (empty($producttid)) { return false; }

	# details
	$details = get_field('shopify_details', $producttid);
	if (empty($details)) { return false; }

	# shopifyid
	$settings = $details['shopify_product_settings'];
	$shopifyid = $settings['shopify_product_id'];
	$format = $settings['shopify_product_format'];

	$single = true;
	if ($format == 'variants') { $single = false; }

	# variant
	$variants = get_field('shopify_product_variants', $producttid);
	if (empty($variants)) { return false; }

	$variantlist = array();
	foreach ($variants as $key => $value) {

		$entry = $value['shopify_variant'];
		$name = $entry['shopify_variant_name'];
		$id = $entry['shopify_variant_ids']['shopify_variant_id'];
		$price = $entry['shopify_variant_details']['shopify_price'];

		if ($name == '[default]') { $name = 'single'; }

		$price = str_replace('.00', '', $price);
		if (intval($price) == 0) { $price = 'Free'; }
		else { $price = '$' . $price; }

		$variantlist[$name] = array(
			'variantid' => $id,
			'price' => $price,
		);

	}

	# nonce
	$nonce = '';
	if (isset($GLOBALS['emfit']['btnonce'])) { $nonce = $GLOBALS['emfit']['btnonce']; }	
	else { $nonce = wp_create_nonce(CART_BUTTON_NONCE); }
	
	# wrap 'em up
	$data = array(
		'shopifyid' => $shopifyid,
		'single' => $single,
		'variants' => $variantlist,
		'url' => get_permalink($producttid),
		'nonce' => $nonce,
	);

	# print_r($data);
	# exit;

	# back at ya
	return $data;

}


# PROGRAM / INFO

function products_program_info ($producttid = 0, $version = '1') {

	# oops
	if ($producttid == 0) { return false; }

	# price
	$price = '';

	$variants = get_field('shopify_product_variants', $producttid);
	if (empty($variants)) { return false; }

	$details = get_field('shopify_details', $id);
	$format = $details['shopify_product_settings']['shopify_product_format'];

	if ($format == 'single') {
		$vdetails = $variants[0]['shopify_variant']['shopify_variant_details'];
		$price = $vdetails['shopify_price'];
	}

	else {
		foreach ($variants as $key => $value) {
			$variant = $value['shopify_variant'];
			$vname = $variant['shopify_variant_name'];
			if ($vname != $version) { continue; }
			$vdetails = $variant['shopify_variant_details'];
			$price = $vdetails['shopify_price'];
			break;
		}
	}

	if (empty($price)) { return false; }

	# url	
	$url = get_the_permalink($producttid);

	# wrap 'em up
	$info = array(
		'price' => $price,
		'url' => $url,
	);

	# back at ya
	return $info;
	
}


# PRODUCT / RELATED

function products_related ($productid = 0) {

	# oops
	if ($productid == 0) { return false; }

	# new label
	$newlabel = get_field('new_label', $productid);

	# product type
	$details = get_field('shopify_details', $productid);
	if (empty($details)) { return false; }
	$producttype = $details['shopify_product_settings']['shopify_product_type'];

	# image
	$image = get_field('list_thumbnail', $productid);
	if (empty($image)) { return false; }
	
	# name
	$name = get_field('product_name', $productid);
	if (empty($name)) { $name = get_the_title($productid); }

	# type
	$type = '';
	$typefield = '';
	if ($producttype == 'supplement') { $typefield = 'shop_supplement_type'; }
	else if ($producttype == 'swag') { $typefield = 'shop_swag_type'; }

	if (!empty($typefield)) { $type = get_field($typefield, $productid); }

	# price
	$variants = get_field('shopify_product_variants', $productid);
	if (empty($variants)) { return false; }
	$details = $variants[0]['shopify_variant']['shopify_variant_details'];
	$price = $details['shopify_price'];

	$price = str_replace('.00', '', $price);
	if (intval($price) == 0) { $price = 'Free'; }
	else { $price = '$' . $price; }

	# url	
	$url = get_the_permalink($productid);

	# wrap 'em up
	$info = array(
		'url' => $url,
		'newlabel' => $newlabel,
		'image' => $image,
		'name' => $name,
		'type' => $type,
		'price' => $price,
	);

	# back at ya
	return $info;

}


# OVERVIEW / RELATED

function products_overview_related ($overviewid = 0) {

	# oops
	if ($overviewid == 0) { return false; }

	$programinfo = get_field('program_info', $overviewid);
	$productid = $programinfo['program'];

	# type
	$details = get_field('shopify_details', $productid);
	if (empty($details)) { return false; }
	$type = $details['shopify_product_settings']['shopify_product_type'];
	
	# name
	$name = get_field('product_name', $productid);
	if (empty($name)) { $name = get_the_title($productid); }

	# blurb
	$blurb = get_field('list_text', $productid);

	# variant
	$variants = get_field('shopify_product_variants', $productid);
	if (empty($variants)) { return false; }

	$variantkey = 0;
	$variantname = '';

	if (count($variants) > 1) {
		$version = $programinfo['version'];
		foreach ($variants as $key => $value) {
			$variant = $value['shopify_variant'];
			if ($variant['shopify_variant_name'] != $version) { continue; }
			$variantkey = $key;
			$variantname = 'Version ' . $version;
			break;
		}
	}

	$variant = $variants[$variantkey]['shopify_variant'];
	$image = $variant['program_thumbnail'];

	# price
	$price = $variant['shopify_variant_details']['shopify_price'];
	$price = str_replace('.00', '', $price);
	if (intval($price) == 0) { $price = 'Free'; }
	else { $price = '$' . $price; }

	# color
	$procolor = get_field('program_color', $productid);

	# url	
	$producturl = get_the_permalink($productid);
	$overviewurl = get_the_permalink($overviewid);

	# wrap 'em up
	$info = array(
		'image' => $image,
		'type' => $type,
		'name' => $name,
		'variantname' => $variantname,
		'blurb' => $blurb,
		'price' => $price,
		'procolor' => $procolor,
		'producturl' => $producturl,
		'overviewurl' => $overviewurl,
	);

	# back at ya
	return $info;

}


# LIST / GENERAL

function products_list ($producttype = '') {
	
	# init entries
	$entries = array();
	$new = array();

	# oops
	if ( ($producttype != 'supplement') && ($producttype != 'swag') ) { return $entries; }

	# init args
	$args = array(
		'post_type' => 'emfit_product',
		'post_status' => 'publish',
		'order' => 'ASC',
		'orderby' => 'title',
		'numberposts' => -1,
		'meta_query' => array(
			array (
				'key' => 'shopify_details_shopify_product_settings_shopify_product_type',
				'value' => $producttype,
				'compare' => '=',
			),
		),
	);

	$sorter = 'latest';
	if (isset($_GET['sort'])) { $sorter = trim(strtolower($_GET['sort'])); };

	if ($sorter == 'type') {

		if ($producttype == 'supplement') {
			$args['meta_key'] = 'shop_supplement_type';
			$args['orderby'] = 'meta_value';
		}

		if ($producttype == 'swag') {
			$args['meta_key'] = 'shop_swag_type';
			$args['orderby'] = 'meta_value';
		}
		
	}


	# fetch!
	$products = get_posts($args);

	# oops
	if (empty($products)) { return $entries; }

	# roll 'em
	foreach ($products as $product) {

		$id = $product->ID;
		$url = get_the_permalink($product);

		$newlabel = get_field('new_label', $id);
		$image = get_field('list_thumbnail', $id);

		$name = get_field('product_name', $id);
		if (empty($name)) { $name = $product->post_title; }

		$variants = get_field('shopify_product_variants', $id);
		$details = $variants[0]['shopify_variant']['shopify_variant_details'];
		$price = $details['shopify_price'];
		$compare = $details['shopify_compare_price'];

		# type
		$typefield = 'shop_supplement_type';
		if ($producttype == 'swag') { $typefield = 'shop_swag_type'; }	
		$type = get_field($typefield, $id);

		if ($producttype == 'supplement') {
			$type = str_replace('-', ' ', $type);
		}

		$entry = array(
			'newlabel' => $newlabel,
			'image' => $image,
			'name' => $name,
			'type' => $type,
			'price' => $price,
			'compare' => $compare,
			'url' => $url,
		);

		if ( ($sorter == 'latest') && ($newlabel) ) { $new[$id] = $entry; }
		else { $entries[$id] = $entry; }
	}

	# merge entries
	$productlist = $new + $entries;
	
	# back at ya
	return $productlist;
		
}


# PROGRAMS / LIST

function products_programs_list() {
	
	# init entries
	$entries = array();

	# args
	$args = array(
		'post_type' => 'emfit_product',
		'post_status' => 'publish',
		'order' => 'ASC',
		'orderby' => 'title',
		'numberposts' => -1,
		'meta_query' => array(
			'relation' => 'OR',
			array (
				'key' => 'shopify_details_shopify_product_settings_shopify_product_type',
				'value' => 'challenge',
				'compare' => '=',
			),
			array (
				'key' => 'shopify_details_shopify_product_settings_shopify_product_type',
				'value' => 'strength',
				'compare' => '=',
			),
		),
	);
	
	# fetch!
	$products = get_posts($args);
			
	# oops
	if (empty($products)) { return $entries; }

	# roll 'em
	foreach ($products as $product) {

		$id = $product->ID;
		$url = get_the_permalink($product);

		$newlabel = get_field('new_label', $id);
		# $image = get_field('program_list_thumbnail', $id);
		$tags = get_field('shop_program_tags', $id);
		$cats = get_field('shop_program_categories', $id);

		$classes = '';
		if (!empty($cats)) {
			foreach ($cats as $key => $value) {
				if (empty($value)) { continue; }
				foreach ($value as $skey => $svelue) {
					$classes .= $svelue . ' ';
				}
			}
		}
		if (!empty($tags)) {
			foreach ($tags as $key => $value) {
				$classes .= $value . ' ';
			}
		}

		$name = get_field('product_name', $id);
		if (empty($name)) { $name = $product->post_title; }
		
		$details = get_field('shopify_details', $id);
		$format = $details['shopify_product_settings']['shopify_product_format'];
		$type = $details['shopify_product_settings']['shopify_product_type'];
		$variantlabel = '';
		$blurb = get_field('list_text', $id);
		$procolor = get_field('program_color', $id);

		if ($format == 'variants') {
			$options = get_field('shopify_product_options', $id);
			$variantlabel = $options['shopify_product_option_1'];
		}

		$variants = get_field('shopify_product_variants', $id);

		foreach ($variants as $key => $variant) {
			
			$vdetails = $variant['shopify_variant']['shopify_variant_details'];
			$price = $vdetails['shopify_price'];
			$compare = $vdetails['shopify_compare_price'];
			$image = $variant['shopify_variant']['program_thumbnail'];

			$variantname = '';
			$varianturl = $url;

			if ($format == 'variants') {
				$variantname = $variant['shopify_variant']['shopify_variant_name'];
				if (ctype_digit($variant['shopify_variant']['shopify_variant_name'])) {
					$variantname = $variantlabel . ' ' . $variant['shopify_variant']['shopify_variant_name'];
					$varianturl .= '?version=' . $variant['shopify_variant']['shopify_variant_name'];
				}
			}

			if ( (isset($variant['shopify_variant']['program_overview'])) && (!empty($variant['shopify_variant']['program_overview'])) ) {
				$varianturl = $variant['shopify_variant']['program_overview'];
			}

			$entries[] = array(
				'newlabel' => $newlabel,
				'image' => $image,
				'type' => $type,
				'name' => $name,
				'variantname' => $variantname,
				'blurb' => $blurb,
				'price' => $price,
				'compare' => $compare,
				'url' => $varianturl,
				'tags' => $tags,
				'cats' => $cats,
				'classes' => $classes,
				'procolor' => $procolor,
			);
			
		}

	}

	# print_r($entries);
	# exit;
	
	# back at ya
	return $entries;
		
}


# PROGRAMS / CATEGORIES

function products_programs_cats ($programs = array()) {

	# init cats
	$cats = array(
		'cat_features' => array(),
		'cat_workout' => array(),
		'cat_focus' => array(),
		'cat_nutrition' => array(),
		'cat_price' => array(),
	);

	# oops
	if (empty($programs)) { return $cats; }

	# roll 'em
	foreach ($programs as $key => $value) {
		
		if (empty($value['cats'])) { continue; }
		$cat = $value['cats'];

		# features
		if ( (isset($cat['cat_features'])) && (!empty($cat['cat_features'])) ) {
			foreach ($cat['cat_features'] as $skey => $svalue) {
				$cats['cat_features'][$svalue] = 1;
			}
		}

		# workout
		if ( (isset($cat['cat_workout'])) && (!empty($cat['cat_workout'])) ) {
			foreach ($cat['cat_workout'] as $skey => $svalue) {
				$cats['cat_workout'][$svalue] = 1;
			}
		}

		# focus
		if ( (isset($cat['cat_focus'])) && (!empty($cat['cat_focus'])) ) {
			foreach ($cat['cat_focus'] as $skey => $svalue) {
				$cats['cat_focus'][$svalue] = 1;
			}
		}

		# nutrition
		if ( (isset($cat['cat_nutrition'])) && (!empty($cat['cat_nutrition'])) ) {
			foreach ($cat['cat_nutrition'] as $skey => $svalue) {
				$cats['cat_nutrition'][$svalue] = 1;
			}
		}
		
	}

	# back at ya
	return $cats;

}


# PROGRAMS / TAGS

function products_programs_tags ($programs = array()) {

	$taglist = array();

	# oops
	if (empty($programs)) { return $taglist; }

	# gather tags
	$tags = get_field('tags_programs', 'options');
	
	# oops
	if (empty($tags)) { return $taglist; }
	
	# roll 'em
	$programtags = array();

	foreach ($programs as $key => $value) {
		if (empty($value['tags'])) { continue; }
		foreach ($value['tags'] as $skey => $svalue) {
			$programtags[$svalue] = 1;
		}
	}

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['tag'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		if (isset($programtags[$coded])) { $taglist[$coded] = $tag; }
	}

	# back at ya
	return $taglist;

}


# PROGRAMS / CATEGORY MENU

function products_programs_catmenu() {

	# init nav
	$nav = array(
		'cat_features' => array(
			'name' => 'Features',
			'entries' => array(),
		),
		'cat_workout' => array(
			'name' => 'Workout Space',
			'entries' => array(),
		),
		'cat_focus' => array(
			'name' => 'Focus',
			'entries' => array(),
		),
		'cat_nutrition' => array(
			'name' => 'Nutrition Interests',
			'entries' => array(),
		),
		'cat_price' => array(
			'name' => 'Price',
			'entries' => array(),
		),
	);

	# features
	$tags = get_field('programcat_features', 'options');
	if (!empty($tags)) {
		foreach ($tags as $key => $value) {
			$tag = $value['subcategory'];
			$coded = trim(strtolower($tag));
			$coded	 = str_replace(' ', '-', $coded);
			$nav['cat_features']['entries'][$coded] = $tag;
		}
	}

	# workout
	$tags = get_field('programcat_workoutspace', 'options');
	if (!empty($tags)) {
		foreach ($tags as $key => $value) {
			$tag = $value['subcategory'];
			$coded = trim(strtolower($tag));
			$coded	 = str_replace(' ', '-', $coded);
			$nav['cat_workout']['entries'][$coded] = $tag;
		}
	}

	# focus
	$tags = get_field('programcat_focus', 'options');
	if (!empty($tags)) {
		foreach ($tags as $key => $value) {
			$tag = $value['subcategory'];
			$coded = trim(strtolower($tag));
			$coded	 = str_replace(' ', '-', $coded);
			$nav['cat_focus']['entries'][$coded] = $tag;
		}
	}

	# nutrition
	$tags = get_field('programcat_nutrition', 'options');
	if (!empty($tags)) {
		foreach ($tags as $key => $value) {
			$tag = $value['subcategory'];
			$coded = trim(strtolower($tag));
			$coded	 = str_replace(' ', '-', $coded);
			$nav['cat_nutrition']['entries'][$coded] = $tag;
		}
	}

	# back at ya
	return $nav;

}


# TAGS / PROGRAM

add_filter('acf/load_field/name=shop_program_tags', 'products_acf_program');

function products_acf_program ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('tags_programs', 'options');
	
	# oops
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['tag'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;
	
}


# TAGS / SUPPLEMENT

add_filter('acf/load_field/name=shop_supplement_type', 'products_acf_supplement');

function products_acf_supplement ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('tags_supplements', 'options');
	
	# oops
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['tag'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;
	
}


# TAGS / SWAG

add_filter('acf/load_field/name=shop_swag_type', 'products_acf_swagtype');

function products_acf_swagtype ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('tags_swag', 'options');
	
	# oops
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['tag'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;
	
}


# PROGRAM CATEGORIES

# features
add_filter('acf/load_field/name=cat_features', 'products_acf_features');

function products_acf_features ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('programcat_features', 'options');
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['subcategory'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;

}

# workouts
add_filter('acf/load_field/name=cat_workout', 'products_acf_workouts');

function products_acf_workouts ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('programcat_workoutspace', 'options');
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['subcategory'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;

}

# focus
add_filter('acf/load_field/name=cat_focus', 'products_acf_focus');

function products_acf_focus ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('programcat_focus', 'options');
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['subcategory'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;

}

# nutrition
add_filter('acf/load_field/name=cat_nutrition', 'products_acf_nutrition');

function products_acf_nutrition ($field) {

	# reset choices
	$field['choices'] = array();	

	# gather tags
	$tags = get_field('programcat_nutrition', 'options');
	if (empty($tags)) { return $field; }

	# roll' em
	foreach ($tags as $key => $value) {
		$tag = $value['subcategory'];
		$coded = trim(strtolower($tag));
		$coded	 = str_replace(' ', '-', $coded);
		$field['choices'][$coded] = $tag;
	}

	# back at ya
	return $field;

}


# PRODUCT PAGE

function products_page ($displaytype = 'standard') {

/*
	# ALL
	x $producttype = $product['producttype'];
	x $productname = $product['productname'];
	x $productblurb = $product['productblurb'];
	x $productid = $product['productid'];
	x $currentid = $product['currentid'];
	x $images = $product['images'];
	x $variants = $product['variants'];
	x $options = $product['options'];
	x $unavailable = $product['unavailable'];
	x $jsonoptions = $product['jsonoptions'];
	x $related = $product['related'];

	# STANDARD
	x $faq = $product['faq'];

	# CUSTOM	
	$herobg = $product['herobg'];
	$addnote = $product['addnote'];
	$testimonials = $product['testimonials'];
	$carousel = $product['carousel'];
	$video = $product['video'];
	$panels = $product['panels'];
*/


	# get page id
	$pageid = get_the_ID();

	# display sizes
	$sizelist = array(
		'small' => 'S',
		'medium' => 'M',
		'large' => 'L',
	);

	# cart
	$cart = shopifycart_load();
	# print_r($cart); exit;

	# init product
	$product = array();
	
	# settings
	$details = get_field('shopify_details', $pageid);
	$settings = $details['shopify_product_settings'];
	$productid = $settings['shopify_product_id'];
	$format = $settings['shopify_product_format'];

	$product['pageid'] = $pageid;
	$product['productid'] = $productid;

	$producttype = $settings['shopify_product_type'];
	if ( ($producttype == 'strength') || ($producttype == 'challenge') ) {
		$producttype = 'program';
	}

	else if ( ($producttype != 'supplement') && ($producttype != 'swag') ) {
		$producttype = 'other';
	}

	$product['producttype'] = $producttype;

	# version
	$version = false;
	if ( (isset($_GET['version'])) && (ctype_digit($_GET['version'])) && (strlen($_GET['version']) == 1) ) {
		$version = $_GET['version'];
		# echo $version . '<br>';
	}

	# name
	$productname = get_field('product_name', $pageid);
	if (empty($productname)) { $productname = get_the_title($pageid); }
	$product['productname'] = $productname;

	# shopify variants
	$productvariants = get_field('shopify_product_variants', $pageid);
	$productoptions = get_field('shopify_product_options', $pageid);
	
	# options
	$options = array();
	foreach ($productoptions as $key => $value) {
		if (empty($value)) { continue; }
		$optionkey = str_replace('shopify_product_', '', $key);
		$optionid = strtolower(str_replace(' ', '', $value));
		$options[$optionkey] = array(
			'name' => $value,
			'optionid' => $optionid,
			'options' => array(),
		);
	}

	# variants
	$variants = array();
	$available = array();
	$currentid = '';
	foreach ($productvariants as $key => $value) {
	
		# variant info
		$variant = $value['shopify_variant'];
		$details = $variant['shopify_variant_details'];
		$inventory = $variant['shopify_variant_inventory'];
	
		if ( ($producttype != 'program') && (intval($inventory['available']) < 1) && ($inventory['inventory_policy'] == 'deny') ) {
			continue;
		}
	
		# variant id
		$current = false;
		$variantid = $variant['shopify_variant_ids']['shopify_variant_id'];

		# variant name
		$name = $variant['shopify_variant_name'];
		if ($name == '[default]') { $name = 'single'; }

		# selected version?
		if ($version) {
			if ($version == $variant['shopify_variant_name']) {
				# echo 'choice: ' . $variant['shopify_variant_name'] . '<br>';
				$currentid = $variantid;
				$current = true;
			}
		}
		
		else if (empty($currentid)) {
			$currentid = $variantid;
			$current = true;
		}
		
		# variant in cart
		$incart = false;
		$cartid = "$productid:$variantid";
		if (isset($cart['contents'][$cartid])) { $incart = true; }
	
		# variant price
		$price = str_replace('.00', '', $details['shopify_price']);
		$compareprice = str_replace('.00', '', $details['shopify_compare_price']);
	
		if (intval($price) == 0) { $price = 'Free'; }
		else { $price = '$' . $price; }
		$compareprice = '$' . $compareprice;
	
		# variant quantity
		$maxquantity = '';
		if ($inventory['inventory_policy'] == 'deny') { $maxquantity = $inventory['available']; }
	
		$variants[$variantid] = array(
			'name' => $name,
			'price' => $price,
			'compareprice' => $compareprice,
			'hilites' => '',
			'current' => $current,
			'cart' => $incart,
			'maxquantity' => $maxquantity,
		);
	
		if (isset($variant['program_hilites'])) {
			$variants[$variantid]['hilites'] = $variant['program_hilites'];
		}
	
		# options
		if (!empty($options)) {
	
			$comboid = '';
	
			if (!empty($details['shopify_option_1'])) {
				$optionname = $options['option_1']['name'];
				$optionid = $options['option_1']['optionid'];
				$optionvalue = $details['shopify_option_1'];
				$valueid = strtolower(str_replace(' ', '', $optionvalue));
				if ( ($optionid == 'size') && (isset($sizelist[$valueid])) ) {
					$optionvalue = $sizelist[$valueid];
				}
				$options['option_1']['options'][$valueid] = $optionvalue;
				$variants[$variantid][$optionid] = $valueid;
				$comboid .= $valueid . '-';
			}
		
			if (!empty($details['shopify_option_2'])) {
				$optionname = $options['option_2']['name'];
				$optionid = $options['option_2']['optionid'];
				$optionvalue = $details['shopify_option_2'];
				$valueid = strtolower(str_replace(' ', '', $optionvalue));
				if ( ($optionid == 'size') && (isset($sizelist[$valueid])) ) {
					$optionvalue = $sizelist[$valueid];
				}
				$options['option_2']['options'][$valueid] = $optionvalue;
				$variants[$variantid][$optionid] = $valueid;
				$comboid .= $valueid . '-';
			}
		
			if (!empty($details['shopify_option_3'])) {
				$optionname = $options['option_3']['name'];
				$optionid = $options['option_3']['optionid'];
				$optionvalue = $details['shopify_option_3'];
				$valueid = strtolower(str_replace(' ', '', $optionvalue));
				if ( ($optionid == 'size') && (isset($sizelist[$valueid])) ) {
					$optionvalue = $sizelist[$valueid];
				}
				$options['option_3']['options'][$valueid] = $optionvalue;
				$variants[$variantid][$optionid] = $valueid;
				$comboid .= $valueid . '-';
			}
	
			$comboid = rtrim($comboid, '-');
			$available[$comboid] = $variantid;
		}
	
	}
	
	$product['variants'] = $variants;
	$product['options'] = $options;
	$product['currentid'] = $currentid;
	
	# json options
	$jsonoptions = array();
	foreach ($options as $key => $value) {
		$optionid = $value['optionid'];
		$jsonoptions[$optionid] = $value['options'];
	}
	$product['jsonoptions'] = $jsonoptions;
	
	# unavailable
	$unavailable = false;
	if (empty($variants)) { $unavailable = true; }
	$product['unavailable'] = $unavailable;

	# related
	$related = array();
	$relatedproducts = get_field('related_products', $pageid);
	if (!empty($relatedproducts)) {
		foreach ($relatedproducts as $key => $value) {
			$relatedid = $value['related_product'];
			$related[] = $relatedid;
		}
	}
	$product['related'] = $related;

	
	# STANDARD DISPLAY
	
	if ($displaytype == 'standard') {

		# faq
		$faq = get_field('product_details', $pageid);
		$product['faq'] = $faq;

		# blurb
		$productblurb = get_field('product_short_description', $pageid);
		$product['productblurb'] = $productblurb;

		if ($producttype == 'program') { $images = get_field('shop_program_images', $pageid); }
		if ($producttype == 'supplement') { $images = get_field('shop_supplement_images', $pageid); }
		if ($producttype == 'swag') { $images = get_field('shop_swag_images', $pageid); }
		if ($producttype == 'other') { $images['main_image'] = get_field('shop_product_image', $pageid); }
		$product['images'] = $images;
		
	}


	# CUSTOM DISPLAY

	if ($displaytype == 'custom') {

		$custom = get_field('custom_display', $pageid);

		$product['hilite_color'] = '';
		if ( (isset($custom['custom_highlight_color'])) && (!empty($custom['custom_highlight_color'])) ) {
			$hilite = '#' . $custom['custom_highlight_color'];
			$hilite = str_replace('##', '#', $hilite);
			$product['hilite_color'] = $hilite;
		}

		$product['herobg'] = $custom['custom_hero_image'];
		$product['images'] = $custom['custom_product_images'];
		$product['productblurb'] = $custom['custom_short_description'];
		$product['optionsubheads'] = $custom['custom_option_subheads'];

		$product['addnote'] = array();
		if ($custom['custom_include_additional_note']) {
			$product['addnote'] = $custom['custom_additional_note'];			
		}

		$product['testimonials'] = array();
		if ($custom['custom_include_testimonials']) {
			$product['testimonials'] = $custom['custom_testimonials_panel'];			
		}

		$product['carousel'] = array();
		if ($custom['custom_include_carousel']) {
			$product['carousel'] = $custom['custom_carousel'];			
		}

		$product['video'] = array();
		if ($custom['custom_include_video']) {
			$product['video'] = $custom['custom_video_panel'];			
		}

		$product['featurepanels'] = $custom['custom_feature_panels'];

	}
	
	
	# WHEW
	
	return $product;	

}


/*
PRODUCT LIST

Array
(
    [0] => WP_Post Object
        (
            [ID] => 347
            [post_author] => 1
            [post_date] => 2019-08-15 17:39:37
            [post_date_gmt] => 2019-08-15 17:39:37
            [post_content] => 
            [post_title] => ADB5 Plus
            [post_excerpt] => 
            [post_status] => publish
            [comment_status] => closed
            [ping_status] => closed
            [post_password] => 
            [post_name] => adb5-plus
            [to_ping] => 
            [pinged] => 
            [post_modified] => 2019-09-04 17:01:13
            [post_modified_gmt] => 2019-09-04 17:01:13
            [post_content_filtered] => 
            [post_parent] => 0
            [guid] => http://www.emfit.test/?post_type=emfit_product&#038;p=347
            [menu_order] => 0
            [post_type] => emfit_product
            [post_mime_type] => 
            [comment_count] => 0
            [filter] => raw
        )

*/

