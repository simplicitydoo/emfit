<?php

# EMFIT / MEMBERS

# create user role
# https://codex.wordpress.org/Function_Reference/add_role

setup_member_userrole();

function setup_member_userrole() {

	global $wp_roles;

	# already created
	if (isset($wp_roles->roles['emfit_member'])) { return; }

	# add member (matches subscriber)
	add_role(
		'emfit_member',
		__('EmFit Member'),
		array(
			# 'read' => true,
			'level_0' => true,
		)
	);
		
}


# CUSTOM POST TYPE
# http://wp.smashingmagazine.com/2012/11/08/complete-guide-custom-post-types/
# http://codex.wordpress.org/Function_Reference/register_post_type

# CUSTOM POST CATEGORY
# http://wp.smashingmagazine.com/2012/01/04/create-custom-taxonomies-wordpress/
# http://codex.wordpress.org/Function_Reference/register_taxonomy

add_action('init', 'setup_member_posttype');

function setup_member_posttype() {

	$labels = array(
		'name' => __('EmFit Member'),
		'singular_name' => __('EmFit Member'),
		'add_new' => __('Add Member'),
		'add_new_item' => __('Add Member'),
		'edit_item' => __('Edit Member'),
		'new_item' => __('New Member'),
		'all_items' => __('All Members'),
		'view_item' => __('View Member'),
		'search_items' => __('Search Members'),
		'not_found' => __('No Members found'),
		'not_found_in_trash' => __('No Members found in the Trash'),
		'menu_name' => 'Members',
	);

	# supports: wp post elements to include
	$arguments = array(
		'labels' => $labels,
		'description' => 'EmFit Member template',
		'supports' => array('title', 'revisions'),
		'show_in_admin_bar' => false,
		# 'menu_position' => 3,
		'public' => true,
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'member',
			'with_front' => false,
		),
		'capabilities' => array(
			'create_posts' => 'do_not_allow',
		),
		'map_meta_cap'    => true,
    );

	register_post_type('emfit_member', $arguments);

}

# member metaboxes
add_action('add_meta_boxes_emfit_member', 'setup_member_metabox', 100);

function setup_member_metabox() {

	# remove
	remove_meta_box('wpseo_meta', 'emfit_member', 'normal');

	# add
	add_meta_box( 
		'emfit_member_settings',
		__('Settings'),
		'setup_member_metabox_settings',
		'emfit_member',
		'side'
	);
	
}

function setup_member_metabox_settings ($post) {
	
	# ids
	$userid = get_post_meta($post->ID, 'user_id');

?>
<p>User ID: <?= $userid[0] ?></p>
<p><strong><a href="/wp-admin/user-edit.php?user_id=<?= $userid[0] ?>">Edit User</a></strong></p>
<?php
} 


# modify user profile
add_action('edit_user_profile', 'setup_member_profile', 10, 1);

function setup_member_profile ($profileuser) {

	# get shopify id
	$userid = $profileuser->ID;
	$wpid = get_user_meta($userid, 'wp_pageid', true);
	$shopifyid = get_user_meta($userid, 'shopify_id', true);
	if (!$wpid) { return; }
	
?>
<h2>EmFit Settings</h2>
<table class="form-table">
<tr><th>User ID</th><td><?= $userid ?></td></tr>
<tr><th>Shopify ID</th><td><?= $shopifyid ?></td></tr>
<tr><th>WP Page ID</th><td><?= $wpid ?></td></tr>
</table>
<?php	
}
