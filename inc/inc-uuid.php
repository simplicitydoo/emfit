<?php
	
# UUID

# uuid (php 7)
function uuid_create() {
	$data = random_bytes(16);
	$data[6] = chr(ord($data[6]) & 0x0f | 0x40); 
	$data[8] = chr(ord($data[8]) & 0x3f | 0x80); 
	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

# collapse all rows
add_action('acf/input/admin_head', 'uuid_collapse_repeaters');

function uuid_collapse_repeaters() {

	# admin only
	if (!is_admin()) { return; }

	# wrong screen
	$screen = get_current_screen();
	if ( (strpos($screen->id, 'acf-options-meal-plans') === FALSE) &&
		(strpos($screen->id, 'acf-options-facebook-groups') === FALSE) &&
		(strpos($screen->id, 'acf-options-resource-library') === FALSE) ) { return; }

?>
<style id="emfit-acf-repeater-collapse">.acf-repeater .acf-table {display:none;}</style>
<script type="text/javascript">
     jQuery(function($) {
          $('.acf-repeater .acf-row').addClass('-collapsed');
          $('#emfit-acf-repeater-collapse').detach();
     });
</script>
<?php
}

# before acf save
add_action('acf/save_post', 'uuid_acf', 1);

function uuid_acf ($post_id) {

	# admin only
	if (!is_admin()) { return; }

	# wrong screen
	$screen = get_current_screen();
	if ( (strpos($screen->id, 'acf-options-meal-plans') === FALSE) &&
		(strpos($screen->id, 'acf-options-facebook-groups') === FALSE) &&
		(strpos($screen->id, 'acf-options-resource-library') === FALSE) ) { return; }

	# nothing there
	if (empty($_POST['acf'])) { return; }

	# mealplan: field_5d44c97cacb54
	# resource: field_5d486fdefed80

	# file_put_contents('/z/logs/temp/resources-' . time() . '.txt', var_export($_POST['acf'], TRUE));

	# convenience
	$acfpost = $_POST['acf'];


	# FACEBOOK

	if (strpos($screen->id, 'acf-options-facebook-groups') !== FALSE) {

		$repeaterfield = 'field_5d44b190b8e50';
		$uuidfield = 'field_5d4891de4b156';

		foreach ($acfpost[$repeaterfield] as $key => $value) {
			
			if (!empty($value[$uuidfield])) { continue; }
			
			$_POST['acf'][$repeaterfield][$key][$uuidfield] = uuid_create();
			
		}

	}

	
	# MEAL PLANS

	if (strpos($screen->id, 'acf-options-meal-plans') !== FALSE) {

		$repeaterfield = 'field_5d44bb455a7ef';
		$uuidfield = 'field_5d44c97cacb54';

		foreach ($acfpost[$repeaterfield] as $key => $value) {
			
			if (!empty($value[$uuidfield])) { continue; }
			
			$_POST['acf'][$repeaterfield][$key][$uuidfield] = uuid_create();
			
		}

	}
	

	# RESOURCES	
	
	if (strpos($screen->id, 'acf-options-resource-library') !== FALSE) {

		$repeaterfield = 'field_5d44b317e888f';
		$uuidfield = 'field_5d486fdefed80';

		foreach ($acfpost[$repeaterfield] as $key => $value) {
			
			if (!empty($value[$uuidfield])) { continue; }
			
			$_POST['acf'][$repeaterfield][$key][$uuidfield] = uuid_create();
			
		}
		
	}

}


/*
RESOURCES

array (
  'field_5d44b317e888f' => 
  array (
    'row-0' => 
    array (
      'field_5d44b335e8890' => 'file',
      'field_5d44b39fac098' => '',
      'field_5d44b3adac099' => '',
      'field_5d44ba78da2e8' => '0',
      'field_5d486fdefed80' => 'resource',
    ),
    '5d487051e1d14' => 
    array (
      'field_5d44b335e8890' => 'file',
      'field_5d44b39fac098' => '',
      'field_5d44b3adac099' => '',
      'field_5d44ba78da2e8' => '0',
      'field_5d486fdefed80' => 'DOS',
    ),
  ),
)

MEAL PLANS
array (
  'field_5d44bb455a7ef' => 
  array (
    'row-0' => 
    array (
      'field_5d44bb455b9d7' => 'one',
      'field_5d44bb455b9f0' => '',
      'field_5d44bb455ba03' => '0',
      'field_5d44c97cacb54' => 'plan',
    ),
    'row-1' => 
    array (
      'field_5d44bb455b9d7' => 'two',
      'field_5d44bb455b9f0' => '',
      'field_5d44bb455ba03' => '0',
      'field_5d44c97cacb54' => '',
    ),
  ),
)

*/
