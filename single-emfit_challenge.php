<?php
	
# EMFIT CHALLENGE

# admin check
tools_adminonly();

# challenge info
$GLOBALS['emfit']['challengeid'] = get_the_ID();
$GLOBALS['emfit']['portalmode'] = 'admin';

get_template_part('portal/portal-challenge');
