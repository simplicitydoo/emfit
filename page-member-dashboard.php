<?php
/**
Template name: Member / Dashboard
 */

# MEMBER / DASHBOARD

# $count = shopifyproducts_process('full');
# echo "Products processed: $count";

# page flag
$GLOBALS['emfit']['pageid'] = 'dashboard';

# tools
require_once(TEMPLATE_MEMBERS . '/member-dashboard.php');

# check login
membertools_confirmuser();

# content
$hero = get_field('hero_panel');
$programs = memberdash_programs();

# echo email_challenge(648, 1, 1);
# exit;

# print_r($programs);
# exit;

# nonce
$nonce = wp_create_nonce(MEMBER_PROFILE_NONCE);

# header
get_header();

?>


<?php # HERO ?>

<section class="general-hero portal bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
Greetings, <?= $GLOBALS['member']['firstname'] ?>!
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # NAV ?>

<section class="portal-navbar dashboard text-center textuc">
<div class="container height100">
<div class="flex height100">
<div class="navwrap">

<a class="portalnavlink" href="#" data-panel="profile"><span class="my">My </span>Profile</a>

<a class="portalnavlink active" href="#" data-panel="fitlist"><span class="my">My </span>Fit List</a>

<?php wp_loginout(site_url(MEMBER_LOGIN)); ?>

</div>
</div>
</div>
</section>


<?php # FITLIST ?>
	
<section class="dashboard-panel fitlist" id="panel-fitlist">
<div class="container">
<div class="row">
<div class="col-sm-12 col-xl-10 offset-xl-1">

<?php if (empty($programs)) { ?>
<div class="empty text-center">
No programs available.
</div>
<?php
}

else {
	
	# print_r($programs);
	
	foreach ($programs as $key => $program) {
		
		$link = '';
		$color = $program['procolor'];
		
		if ($program['type'] == 'program') {
			$link = MEMBER_PROGRAM . '?programid=' . $program['id'];
		}
	
		if ($program['type'] == 'challenge') {
			$link = MEMBER_CHALLENGE . '?challengeid=' . $program['id'];
		}
	
		$version = '';
		if (!empty($program['version'])) {
			if (ctype_digit($program['version'])) {
				$version = 'Version ' . $program['version'];
			} else {
				$version = $program['version'];
			}
		}
	
		$startdate = '';
		if (!empty($program['startdate'])) { $startdate = date('l, F j', strtotime($program['startdate'])); }

?>
<a class="program flex flexrow flexalign flexleft" href="<?= $link ?>">
<div class="thumb bgcover" style="background-image:url(<?= $program['thumb'] ?>);"></div>

<div class="main">

<div class="kicker">
<?= $program['type'] ?>
<?php if (!empty($startdate)) { ?>
<span class="startdate"><?= $startdate ?></span>
<?php } ?>
</div>
<div class="name textlc <?= $color ?>">
<?= $program['name'] ?>
</div>
<?php if (!empty($version)) { ?>
<div class="version textlc <?= $color ?>">
<?= $version ?>
</div>	
<?php } ?>

</div>

<div class="datewrap">
<?= $startdate ?>
</div>

</a>
<?php
	}
}
?>

</div>
</div>
</div>
</section>


<?php # PROFILE ?>
	
<section class="dashboard-panel" id="panel-profile" style="display:none;">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-8 offset-md-2">

<div class="infowrap text-center">

<?php if (ENV_SERVER == 'bugs') { ?>
<!-- <div class="avatar"></div> -->
<?php } ?>

<div class="details">
<?php
/*
<div class="screennamewrap">
<div class="screenname flex">
</div>
</div>
*/
?>
<div class="memberwrap" id="panel-memberlive">

<div class="name">
<span id="profile-first"><?= $GLOBALS['member']['firstname'] ?></span> <span id="profile-last"><?= $GLOBALS['member']['lastname'] ?></span>
</div>

<div class="email" id="profile-email">
<?= $GLOBALS['member']['email'] ?>
</div>

<?php # <div class="date"></div> ?>
<div class="buttonwrap">
<button class="featurebutton btgray" id="profile-edit">Edit Profile</button>
</div>

</div>

<div class="memberwrap memberform" id="panel-memberform" style="display:none;">
<form class="general-form" data-nonce="<?= $nonce ?>" id="profileform">
<input type="hidden" value="<?= $GLOBALS['member']['userid'] ?>" id="profileform-id">

<div class="instruction">
Update Your Info Below
</div>	

<div class="field">
<input type="text" name="firstname" class="profileformtext profileformfirst" placeholder="First Name" value="<?= $GLOBALS['member']['firstname'] ?>" id="profileform-first">
</div>	

<div class="field">
<input type="text" name="lastname" class="profileformtext profileformlast" placeholder="Last Name" value="<?= $GLOBALS['member']['lastname'] ?>" id="profileform-last">
</div>	

<div class="field">
<input type="email" name="email" class="profileformtext profileformemail" placeholder=" Email" value="<?= $GLOBALS['member']['email'] ?>" id="profileform-email">
</div>	

<div class="buttonwrap" id="profileform-buttonwrap">
<button class="featurebutton btgreen" id="profileform-submit">Update Profile</button>
</div>	

<div class="response text-center" id="profileform-response" style="display:none;"></div>	

</form>
</div>

</div>

<?php if (ENV_SERVER == 'bugs') { ?>
<div class="history">

<div class="title">
Workout History
</div>

</div>
<?php } ?>

</div>
</div>
</div>
</section>


<?php get_footer(); ?>


<?php
/*
Array
(
    [0] => Array
        (
            [type] => program
            [id] => 135
            [name] => EmFit Get Strong: Arms and Abs
            [version] => 1
            [startdate] => 2019-05-12
            [enddate] => 2019-05-13
            [thumb] => 
        )

    [1] => Array
        (
            [type] => challenge
            [id] => 648
            [name] => Em's 7 Day Happy Gut Challenge
            [version] => 
            [startdate] => 
            [enddate] => 
            [thumb] => 
        )

)
*/
