<?php
	
# MEMBER / CHALLENGE


# VALIDATE

function memberchallenge_validate() {

	# oops
	if (!isset($_GET['challengeid'])) { return false; }

	# challenge id
	$challengeid = $_GET['challengeid'];
	
	# convenience
	$pageid = $GLOBALS['member']['pageid'];
	
	# member challenges
	$challenges = get_field('member_challenges', $pageid);

	# oops
	if (empty($challenges)) { return false; }

	# load program list
	$programlist = shopifyproducts_programlist_load(true);

	# oops
	if (!isset($programlist[$challengeid])) { return false; }

	# back at ya
	return $challengeid;

}


# CHALLENGE STATUS
# already validated

function memberchallenge_status ($challengeid = 0) {
	
	# oops
	if ($challengeid == 0) { return false; }

	# convenience
	$pageid = $GLOBALS['member']['pageid'];
	
	# member challenges
	$challenges = get_field('member_challenges', $pageid);

	# init status
	$status = array();

	# find the fish
	foreach ($challenges as $key => $value) {

		if ($value['challenge_settings']['challenge_id'] != $challengeid) { continue; }

		$rowid = $key + 1;

		$status = array(
			'rowid' => $rowid,
			'settings' => $value['challenge_settings'],
			# 'messages' => $value['challenge_messages'],
		);

		$messages = array();

		if ( (isset($value['challenge_messages'])) && (!empty($value['challenge_messages'])) ) {
			foreach ($value['challenge_messages'] as $mkey => $mvalue) {
				$messageid = $mvalue['message_settings']['message_id'];
				$points = unserialize($mvalue['challenge_points']);
				$daypoints = 0;
				if (!empty($points)) {
					foreach ($points as $pkey => $pvalue) {
						$daypoints = $daypoints + $pvalue;
					}
				}
				$messages[$messageid] = array(
					'message_settings' => $mvalue['message_settings'],
					'points' => $points,
					'daypoints' => $daypoints,
				);
			}
		}

		$status['messages'] = $messages;

	}

	# oops
	if (empty($status)) { return false; }

	# not set up
	if (empty($status['messages'])) { $status['messages'] = memberchallenge_setup($challengeid, $status['rowid']); }

	# back at ya
	return $status;
		
}


# SETUP CHALLENGE

function memberchallenge_setup ($challengeid, $rowid) {

	# convenience
	$pageid = $GLOBALS['member']['pageid'];

	# init entry row
	$row = array(
		'member_challenges', $rowid, 'challenge_messages'
	);

	# init points
	$points = array();
	$challengepoints = get_field('challenge_points', $challengeid);
	if ($challengepoints['include_points']) {
		$pointcount = count($challengepoints['points_questions']);
		for ($i = 1; $i <= $pointcount; $i++) {
			$points[$i] = 0;
		}
	}
	$entrypoints = serialize($points);

	# init entries
	$entries = array();

	# get messages
	$messages = get_field('challenge_messages', $challengeid);
	
	foreach ($messages as $key => $message) {
		
		$settings = $message['message_settings'];
		$messageday = $settings['message_day'];
		$messagedate = '';
		if (isset($settings['message_date'])) { $messagedate = $settings['message_date']; }
			
		$args = array(
			'message_settings' => array(
				'message_id' => $messageday,
				'message_date' => $messagedate,
			),
			'challenge_points' => $entrypoints,
		);
	
		$subrowid = add_sub_row($row, $args, $pageid);

		$entries[] = $args;
				
	}

	# back at ya
	return $entries;
		
}


# ADD CHALLENGE
# from shopify

function memberchallenge_add ($settings = array()) {

	# safety first!
	if (empty($settings)) { return false; }
	if ( (empty($settings['memberpageid'])) || (empty($settings['challengeid'])) || (empty($settings['orderid'])) || (empty($settings['orderdate'])) ) { return false; }

	$memberpageid = $settings['memberpageid'];
	$challengeid = $settings['challengeid'];
	# $shopifyid = settings['shopifyid'];
	# $variantid = settings['variantid'];
	$orderid = $settings['orderid'];
	$orderdate = $settings['orderdate'];	

	# check for existing challenge
	$currentchallenges = get_field('member_challenges', $memberpageid);
	if (!empty($currentchallenges)) {
		foreach ($currentchallenges as $key => $current) {
			$currentid = $current['challenge_settings']['challenge_id'];
			if ($currentid == $challengeid) { return true; }
		}
	}
		
	# create challenge
	$args = array(
		'acf_fc_layout' => 'member_challenge',
		'challenge_settings' => array(
			'challenge_id' => $challengeid,
			# 'shopify_id' => $shopifyid,
			# 'variant_id' => $variantid,
		),
		'challenge_order_details' => array(
			'challenge_order_id' => $orderid,
			'challenge_order_date' => $orderdate,
		),
	);

	$rowid = add_row('member_challenges', $args, $memberpageid);
	# echo "Row: $rowid<br>";

	# back at ya
	return true;

}


# AJAX TOOLS

function memberchallenge_ajax() {

	# check nonce
	check_ajax_referer(MEMBER_CHALLENGE_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['challengeid'])) || (!isset($_POST['userid'])) || (!isset($_POST['pageid'])) ) { membertools_ajax_return(array('status' => 'id')); }
	if ( (!ctype_digit($_POST['challengeid'])) || (!ctype_digit($_POST['userid'])) || (!ctype_digit($_POST['pageid'])) ) { membertools_ajax_return(array('status' => 'ctype')); }

	# convenience
	$challengeid = intval($_POST['challengeid']);
	$userid = intval($_POST['userid']);
	$pageid = intval($_POST['pageid']);

	# validate ids...

	# user
	$wpuser = get_current_user_id();
	if ($userid != $wpuser) { membertools_ajax_return(array('status' => 'user')); }

	# member page
	$userdata = get_userdata($userid);
	$wpid = get_user_meta($userid, 'wp_pageid', true);
	if ( (empty($wpid)) || ($wpid != $pageid) ) { membertools_ajax_return(array('status' => 'wp')); }

	# challenge
	$programlist = shopifyproducts_programlist_load();
	if (!isset($programlist[$challengeid])) { membertools_ajax_return(array('status' => 'list')); }

	# member challenge
	$rowid = false;
	$challenge = '';
	$challenges = get_field('member_challenges', $pageid);
	foreach ($challenges as $key => $value) {
		if ($value['challenge_settings']['challenge_id'] == $challengeid) {
			$rowid = $key + 1;
			$challenge = $value;
			break;
		}
	}
	
	# no row id
	if ($rowid === false) { membertools_ajax_return(array('status' => 'row')); }

	# bundle up
	$memberdata = array(
		'challengeid' => $challengeid,
		'userid' => $userid,
		'pageid' => $pageid,
		'rowid' => $rowid,
		'challenge' => $challenge,
	);

	# back at ya
	return $memberdata;

}


# SET CHALLENGE DATE

# logged-in only
add_action('wp_ajax_challenge_startdate', 'memberchallenge_startdate');

function memberchallenge_startdate() {

	# validate submission
	$data = memberchallenge_ajax();	

	# convenience
	$userid = $data['userid'];
	$challengeid = $data['challengeid'];
	$pageid = $data['pageid'];
	$rowid = $data['rowid'];
	$challenge = $data['challenge'];
	
	# check datechoice field
	if (!isset($_POST['datechoice'])) { membertools_ajax_return(array('status' => 'datechoice')); }
	$datechoice = $_POST['datechoice'];

	# validate date
	$datearray = explode('/', $datechoice);
	if (count($datearray) != 3) { membertools_ajax_return(array('status' => 'datearray')); }

	$datevalid = checkdate($datearray[0], $datearray[1], $datearray[2]);
	if (!$datevalid) { membertools_ajax_return(array('status' => 'datevalid')); }

	# validate start date
	$startdate = new DateTime($datechoice);	

	$minstart = new DateTime('now');
	$minstart->setTime(0,0);
	$minstart->modify('-2 days');
	
	if ($startdate < $minstart) { membertools_ajax_return(array('status' => 'error')); }

	# already set up
	if (!empty($challenge['challenge_settings']['start_date'])) { membertools_ajax_return(array('status' => 'success')); }

	# init dates
	# 2019-05-12
	$entrystart = $startdate->format('Y-m-d');
	$messagedate = null;

	# init db
	global $wpdb;
		
	# roll 'em
	foreach ($challenge['challenge_messages'] as $key => $value) {

		$subrowid = $key + 1;
		$settings = $value['message_settings'];
		$dayid = intval($settings['message_id']);
		$days = intval($settings['message_id']) -1;
		$date = clone $startdate;
		$messagedate = $date->modify('+' . $days . ' day')->format('Y-m-d');
	
		$field = array(
			'member_challenges', $rowid, 'challenge_messages', $subrowid, 'message_settings'
		);

		$args = array(
			'message_date' => $messagedate,
		);
		
		$response = update_sub_field($field, $args, $pageid);
		# echo "Message date: $response<br>";

		# add to email db
		$insert = $wpdb->insert( 
			EMFIT_EMAILDB, 
			array(
				'dateid' => $messagedate,
				'userid' => $userid,
				'type' => 'C',
				'programid' => $challengeid,
				'week' => 0,
				'day' => $dayid,
			)
		);
	
	}
	
	# echo "RowID: $rowid<br>";
	
	# set challenge dates
	$field = array(
		'member_challenges', $rowid, 'challenge_settings'
	);
	
	$args = array(
		'start_date' => $entrystart,
		'end_date' => $messagedate,
	);
	
	$response = update_sub_field($field, $args, $pageid);
	# echo "Main date: $response<br>";

	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}


# RECEIVE CHALLENGE POINTS

# logged-in only
add_action('wp_ajax_challenge_points', 'memberchallenge_points');

function memberchallenge_points() {

	# validate submission
	$data = memberchallenge_ajax();	

	# check point field
	if (!isset($_POST['pointid'])) { wp_die(); }
	$pointid = $_POST['pointid'];

	# points
	$pointarray = explode('-', $pointid);
	if (count($pointarray) != 3) { wp_die(); }
	
	$messageid = intval($pointarray[0]);
	$questionid = intval($pointarray[1]);
	$pointscore = intval($pointarray[2]);

	# convenience
	$challengeid = $data['challengeid'];
	$userid = $data['userid'];
	$pageid = $data['pageid'];
	$rowid = $data['rowid'];
	$challenge = $data['challenge'];

	# find the fish
	$currentpoints = '';

	$subrowid = false;
	foreach ($challenge['challenge_messages'] as $key => $value) {
		if ($value['message_settings']['message_id'] != $messageid) { continue; }
		$subrowid = $key + 1;
		$currentpoints = unserialize($value['challenge_points']);
	}

	# nobody home	
	if (empty($currentpoints)) { membertools_ajax_return(); }
	if (!isset($currentpoints[$questionid])) { membertools_ajax_return(); }

	# update points
	$currentpoints[$questionid]	= $pointscore;
	$entrypoints = serialize($currentpoints);
	
	# update field
	update_sub_field(array('member_challenges', $rowid, 'challenge_messages', $subrowid, 'challenge_points'), $entrypoints, $pageid);

	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}
