<?php
	
# MEMBERS / ACF


# CREATE MEMBER

add_action('acf/pre_save_post', 'acf_member_create', 1);

function acf_member_create ($postid) {

	# go away
	if ( ($_POST['_acf_screen'] != 'acf_form') || ($postid != 'new_post') ) { return $postid; }

	# convenience
	$acfpost = $_POST['acf'];	

	# really, go away
	if (!isset($acfpost['field_5cc876a50a92f'])) { return $postid; }

	if (ENV_SERVER == 'bugs') {
		file_put_contents('/z/logs/temp/signup-' . time() . '.txt', var_export($_POST, TRUE));
	}

	# map fields
	$firstname = $acfpost['field_5cc8768a0a92d'];
	$lastname = $acfpost['field_5cc876990a92e'];
	$email = strtolower($acfpost['field_5cc876a50a92f']);
	$pw = $acfpost['field_5cc876c50a930'];

	# create user
	$signup = array(
		'firstname' => $firstname,
		'lastname' => $lastname,
		'email' => $email,
		'pw' => $pw,
	);

	$userid = member_create($signup);

	# oops
	if (!$userid) {
		wp_redirect(site_url(MEMBER_LOGIN . '?return=signuperror'));	
		exit;
	}	

	# create member page
	$memberpageid = member_createpage($userid);

	# log in user
	$creds = array(
		'user_login' => $email,
		'user_password' => $pw,
		'remember' => true,
	);

	# ssl login cookie
	$cookie = true;
	if (ENV_SERVER == 'bugs') { $cookie = false; }

	# fire!
	wp_signon($creds, $cookie);

	# back at ya
	# redirected by acf settings
	
}


# PASSWORD FIELD

# default value
add_filter('acf/load_field/name=signup_password', 'acf_member_pwnew');

function acf_member_pwnew ($field) {
	$field['default_value'] = wp_generate_password(8, false);
    return $field;	
}

# validate length
add_filter('acf/validate_value/name=signup_password', 'acf_member_pwvalid', 10, 4);

function acf_member_pwvalid($valid, $value, $field, $input) {
	
	# bail if already invalid
	if (!$valid) { return $valid; }

	$length = strlen(trim($value));

	if ($length < 8) {
		$valid = 'Password must be minimum 8 characters';
	}

	return $valid;

}
