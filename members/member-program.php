<?php
	
# MEMBER / PROGRAM


# VALIDATE

function memberprogram_validate() {

	# oops
	if (!isset($_GET['programid'])) { return false; }

	# program id
	$programid = $_GET['programid'];
	
	# convenience
	$pageid = $GLOBALS['member']['pageid'];
	
	# member programs
	$programs = get_field('member_programs', $pageid);

	# oops
	if (empty($programs)) { return false; }

	# load program list
	$programlist = shopifyproducts_programlist_load(true);

	# oops
	if (!isset($programlist[$programid])) { return false; }

	# back at ya
	return $programid;

}


# PROGRAM STATUS
# already validated

function memberprogram_status ($programid = 0) {
	
	# oops
	if ($programid == 0) { return false; }

	# convenience
	$pageid = $GLOBALS['member']['pageid'];
	
	# member programs
	$programs = get_field('member_programs', $pageid);

	# init workouts
	$workouts = array();

	# find the fish
	foreach ($programs as $key => $value) {

		if ($value['program_settings']['program_id'] != $programid) { continue; }

		$rowid = $key + 1;

		$status = array(
			'rowid' => $rowid,
			'settings' => $value['program_settings'],
			'workouts' => $value['program_workouts'],
		);

	}

	# oops
	if (empty($status)) { return false; }

	# not set up
	if (empty($status['workouts'])) { $status['workouts'] = memberprogram_setup($programid, $status['rowid']); }

	# back at ya
	return $status;
		
}


# SETUP PROGRAM

function memberprogram_setup ($programid, $rowid) {

	# convenience
	$pageid = $GLOBALS['member']['pageid'];

	# init entry row
	$row = array(
		'member_programs', $rowid, 'program_workouts'
	);

	# init entries
	$entries = array();

	# get workouts
	$workouts = get_field('Workouts', $programid);

	foreach ($workouts as $key => $workout) {

		# init entry
		$entry = array(
			'workout_steps' => '',
		);
		
		$settings = $workout['workout_settings'];
		$workoutid = $settings['workout_week'] . '-' . $settings['workout_day'];
		$workouttype = $settings['workout_type'];

		$score = array(
			'all' => 0,
		);

		if (!empty($workout['workout_steps'])) {
			foreach ($workout['workout_steps'] as $wkey => $step) {

				$stepsettings = $step['step_settings'];
				if ($stepsettings['step_type'] == 'warmup') { $score['W'] = 0; }
				else {
					$stepid = $stepsettings['step_id'];
					$score[$stepid] = 0;
				}

			}
		}

		$entryscore = serialize($score);
	
		$args = array(
			'workout_settings' => array(
				'workout_id' => $workoutid,
				'workout_score' => $entryscore,
			),
		);
	
		$subrowid = add_sub_row($row, $args, $pageid);
		# echo "Subrow: $subrowid<br>";

		$entry['workout_settings'] = $args['workout_settings'];
		$entry['workout_settings']['workout_date'] = '';
		
		if (empty($workout['workout_steps'])) {
			$entries[] = $entry;
			continue;		
		}

		$steps = array();

		foreach ($workout['workout_steps'] as $wkey => $step) {
						
			$stepsettings = $step['step_settings'];
			if ($stepsettings['step_type'] == 'warmup') { continue; }
			
			$stepid = $stepsettings['step_id'];
	
			$stepargs = array(
				'step_id' => $stepid,
			);
	
			$steprow = array(
				'member_programs', $rowid, 'program_workouts', $subrowid, 'workout_steps'
			);
			
			$steprowid = add_sub_row($steprow, $stepargs, $pageid);
			# echo "Steprow: $steprowid<br>";
	
			$stepargs['step_status'] = 'pending';
			$stepargs['step_comment'] = '';
			$stepargs['step_images'] = '';

			$steps[] = $stepargs;
			
		}

		$entry['workout_steps'] = $steps;


		$entries[] = $entry;

	}


	# back at ya
	return $entries;
		
}


# ADD PROGRAM
# from shopify

function memberprogram_add ($settings = array()) {

	# safety first!
	if (empty($settings)) { return false; }
	if ( (empty($settings['memberpageid'])) || (empty($settings['programid'])) || (empty($settings['orderid'])) || (empty($settings['orderdate'])) ) { return false; }

	$memberpageid = $settings['memberpageid'];
	$programid = $settings['programid'];
	$orderid = $settings['orderid'];
	$orderdate = $settings['orderdate'];	

	# check for existing program
	$currentprograms = get_field('member_programs', $memberpageid);
	if (!empty($currentprograms)) {
		foreach ($currentprograms as $key => $current) {
			$currentid = $current['program_settings']['program_id'];
			if ($currentid == $programid) { return true; }
		}
	}
	
	# create program
	$args = array(
		'acf_fc_layout' => 'member_program',
		'program_settings' => array(
			'program_id' => $programid,
		),
		'program_order_details' => array(
			'program_order_id' => $orderid,
			'program_order_date' => $orderdate,
		),
	);
	
	$rowid = add_row('member_programs', $args, $memberpageid);
	# echo "Row: $rowid<br>";
	
	# back at ya
	return true;

}


# AJAX TOOLS

function memberprogram_ajax() {

	# check nonce
	check_ajax_referer(MEMBER_PROGRAM_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['programid'])) || (!isset($_POST['userid'])) || (!isset($_POST['pageid'])) ) { wp_die(); }
	if ( (!ctype_digit($_POST['programid'])) || (!ctype_digit($_POST['userid'])) || (!ctype_digit($_POST['pageid'])) ) { wp_die(); }

	# convenience
	$programid = intval($_POST['programid']);
	$userid = intval($_POST['userid']);
	$pageid = intval($_POST['pageid']);

	# validate ids...

	# user
	$wpuser = get_current_user_id();
	if ($userid != $wpuser) { wp_die(); }

	# member page
	$userdata = get_userdata($userid);
	$wpid = get_user_meta($userid, 'wp_pageid', true);
	if ( (empty($wpid)) || ($wpid != $pageid) ) { wp_die(); }

	# challenge
	$programlist = shopifyproducts_programlist_load();
	if (!isset($programlist[$programid])) { wp_die(); }

	# member program
	$rowid = false;
	$program = '';
	$programs = get_field('member_programs', $pageid);
	foreach ($programs as $key => $value) {
		if ($value['program_settings']['program_id'] == $programid) {
			$rowid = $key + 1;
			$program = $value;
			break;
		}
	}
	
	# no row id
	if ($rowid === false) { wp_die(); }

	# bundle up
	$memberdata = array(
		'programid' => $programid,
		'userid' => $userid,
		'pageid' => $pageid,
		'rowid' => $rowid,
		'program' => $program,
	);

	# back at ya
	return $memberdata;

}


# SET PROGRAM DATE

# logged-in only
add_action('wp_ajax_program_startdate', 'memberprogram_startdate');

function memberprogram_startdate() {

	# validate submission
	$data = memberprogram_ajax();	

	# convenience
	$userid = $data['userid'];
	$programid = $data['programid'];
	$pageid = $data['pageid'];
	$rowid = $data['rowid'];
	$program = $data['program'];

	# check datechoice field
	if (!isset($_POST['datechoice'])) { wp_die(); }
	$datechoice = $_POST['datechoice'];

	# validate date
	$datearray = explode('/', $datechoice);
	if (count($datearray) != 3) { wp_die(); }

	$datevalid = checkdate($datearray[0], $datearray[1], $datearray[2]);
	if (!$datevalid) { wp_die(); }

	# validate start date
	$startdate = new DateTime($datechoice);	

	$minstart = new DateTime('now');
	$minstart->setTime(0,0);
	$minstart->modify('-2 days');
	
	if ($startdate < $minstart) { membertools_ajax_return(array('status' => 'error')); }

	# already set up
	if (!empty($program['program_settings']['start_date'])) { membertools_ajax_return(array('status' => 'success')); }

	# init dates
	# 2019-05-12
	$entrystart = $startdate->format('Y-m-d');
	$workoutdate = null;

	# init db
	global $wpdb;
		
	# week 1 day 1
	# week 2 day 3
	# days = ((week - 1) * 7)) + (day - 1)
	
	foreach ($program['program_workouts'] as $key => $value) {
		$subrowid = $key + 1;
		$settings = $value['workout_settings'];
		$workoutid = $settings['workout_id'];
		list($week, $day) = explode('-', $workoutid);
		$week = intval($week);
		$day = intval($day);
		$days = ((intval($week) - 1) * 7) + (intval($day) - 1);
		$date = clone $startdate;
		$workoutdate = $date->modify('+' . $days . ' day')->format('Y-m-d');
	
		$field = array(
			'member_programs', $rowid, 'program_workouts', $subrowid, 'workout_settings'
		);

		$args = array(
			'workout_date' => $workoutdate,
		);
		
		$response = update_sub_field($field, $args, $pageid);
		# echo "Workout date: $response<br>";

		# rest day?
		$steps = $value['workout_steps'];
		if (empty($steps)) { continue; }

		# add to email db
		$insert = $wpdb->insert( 
			EMFIT_EMAILDB, 
			array(
				'dateid' => $workoutdate,
				'userid' => $userid,
				'type' => 'P',
				'programid' => $programid,
				'week' => $week,
				'day' => $day,
			)
		);

	}
	
	# echo "RowID: $rowid<br>";
	
	# set program dates
	$field = array(
		'member_programs', $rowid, 'program_settings'
	);

	$args = array(
		'start_date' => $entrystart,
		'end_date' => $workoutdate,
	);
	
	$response = update_sub_field($field, $args, $pageid);
	# echo "Main date: $response<br>";

	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}


# PROGRAM SCORE

# logged-in only
add_action('wp_ajax_program_score', 'memberprogram_score');

function memberprogram_score() {

	# validate submission
	$data = memberprogram_ajax();	

	# convenience
	$programid = $data['programid'];
	$pageid = $data['pageid'];
	$rowid = $data['rowid'];
	$program = $data['program'];

	# check stepid
	if (!isset($_POST['stepid'])) { wp_die(); }
	$stepid = $_POST['stepid'];

	# check toggle value
	if (!isset($_POST['togglevalue'])) { wp_die(); }
	if (!ctype_digit($_POST['togglevalue'])) { wp_die(); }
	$togglevalue = intval($_POST['togglevalue']);
	if ( ($togglevalue < 0) || ($togglevalue >1) ) { wp_die(); }

	# check workoutid
	if (!isset($_POST['workoutid'])) { wp_die(); }
	$workoutid = $_POST['workoutid'];

	# find the fish
	$settings = array();
	$currentscore = array();
	$subrowid = false;
	foreach ($program['program_workouts'] as $key => $value) {
		if ($value['workout_settings']['workout_id'] != $workoutid) { continue; }
		$subrowid = $key + 1;
		$settings = $value['workout_settings'];
		$currentscore = unserialize($settings['workout_score']);
	}

	# nobody home
	if (empty($settings['workout_id'])) { membertools_ajax_return(); }
	if (empty($currentscore)) { membertools_ajax_return(); }
	if (!isset($currentscore[$stepid])) { membertools_ajax_return(); }
	
	# update score
	$currentscore[$stepid] = $togglevalue;

	# update total
	$complete = 1;
	foreach ($currentscore as $key => $value) {
		if ($key == 'all') { continue; }
		if ($value == 0) {
			$complete = 0;
			break;
		}
	}

	$currentscore['all'] = $complete;
	$entryscore = serialize($currentscore);
	$settings['workout_score'] = $entryscore;

	# update field
	update_sub_field(array('member_programs', $rowid, 'program_workouts', $subrowid, 'workout_settings'), $settings, $pageid);

	# init response
	$response = array(
		'status' => 'success',
		'complete' => $complete,
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}


# PROGRAM COMMENT

# logged-in only
add_action('wp_ajax_program_comment', 'memberprogram_comment');

function memberprogram_comment() {

	# validate submission
	$data = memberprogram_ajax();	

	# convenience
	$programid = $data['programid'];
	$pageid = $data['pageid'];
	$rowid = $data['rowid'];
	$program = $data['program'];

	# check workoutid
	if (!isset($_POST['workoutid'])) { wp_die(); }
	$workoutid = $_POST['workoutid'];

	# check stepid
	if (!isset($_POST['stepid'])) { wp_die(); }
	$stepid = $_POST['stepid'];
	if ($stepid == 'W') { wp_die(); }

	# check comment
	if (!isset($_POST['comment'])) { wp_die(); }
	if ( (empty($_POST['comment'])) || (strlen($_POST['comment']) > 300) ) { wp_die(); }
	$comment = sanitize_text_field($_POST['comment']);
	
	# find the fish
	$subrowid = false;
	$steprowid = false;
	foreach ($program['program_workouts'] as $key => $value) {

		if ($value['workout_settings']['workout_id'] != $workoutid) { continue; }
		$subrowid = $key + 1;

		$steps = $value['workout_steps'];
		if (empty($steps)) { break; }

		foreach ($steps as $skey => $svalue) {
			if ($svalue['step_id'] != $stepid) { continue; }
			$steprowid = $skey + 1;
		}
		
	}

	# fish found?
	if ( (!$subrowid) || (!$steprowid) ) { wp_die(); }

	# update comment
	update_sub_field(array('member_programs', $rowid, 'program_workouts', $subrowid, 'workout_steps', $steprowid, 'step_comment'), $comment, $pageid);

	# init response
	$response = array(
		'status' => 'success',
	);
	
	# back at ya
	membertools_ajax_return($response);
	
}
