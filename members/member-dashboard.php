<?php
	
# MEMBER / DASHBOARD TOOLS


# PROGRAM INFO

function memberdash_programs() {

	# init entries
	$entries = array();

	# load id lists
	$idlist = shopifyproducts_idlist_load(true);
	$programlist = shopifyproducts_programlist_load(true);

/*
	print_r($idlist);
	print_r($programlist);
	exit;
*/

	# convenience
	$pageid = $GLOBALS['member']['pageid'];
	
	# programs
	$programs = get_field('member_programs', $pageid);

	if (!empty($programs)) {
		
		foreach ($programs as $key => $program) {
			
			$settings = $program['program_settings'];
			
			$programid = $settings['program_id'];

			# oops
			if (empty($programid)) { continue; }
			if (!isset($programlist[$programid])) { continue; }

			# convenience
			$shopifyid = $programlist[$programid]['shopifyid'];
			$variantid = $programlist[$programid]['variantid'];
			
			# oops
			if ( (!isset($idlist[$shopifyid])) || (!isset($idlist[$shopifyid]['variants'][$variantid])) ) { continue; }

			# gather info
			$productid = $idlist[$shopifyid]['postid'];
			$name = $idlist[$shopifyid]['name'];
			$variant = $idlist[$shopifyid]['variants'][$variantid];

			$version = '';
			if ($variant['name'] != '[default]') { $version = $variant['name']; }

			$startdate = $settings['start_date'];
			$enddate = $settings['end_date'];

			# color
			$procolor = get_field('program_color', $productid);
			if (!$procolor) { $procolor = 'propurple'; }

			# thumbnail
			$variants = get_field('shopify_product_variants', $productid);
			$thumbnail = $variants[0]['shopify_variant']['program_thumbnail'];

			if ($idlist[$shopifyid]['format'] == 'variants') {
				foreach ($variants as $vkey => $ventry) {
					if ($variantid == $ventry['shopify_variant']['shopify_variant_ids']['shopify_variant_id']) {
						$thumbnail = $ventry['shopify_variant']['program_thumbnail'];
					}
				}
			}

			# add entry
			$entries[] = array(
				'type' => 'program',
				'id' => $programid,
				'name' => $name,
				'version' => $version,
				'startdate' => $startdate,
				'enddate' => $enddate,
				'procolor' => $procolor,
				'thumb' => $thumbnail,
			);

		}
	
	}

	$challenges = get_field('member_challenges', $pageid);
	
	if (!empty($challenges)) {
		
		foreach ($challenges as $key => $challenge) {
			
			$settings = $challenge['challenge_settings'];
			
			$challengeid = $settings['challenge_id'];

			# oops
			if (empty($challengeid)) { continue; }
			if (!isset($programlist[$challengeid])) { continue; }

			# convenience
			$shopifyid = $programlist[$challengeid]['shopifyid'];
			$variantid = $programlist[$challengeid]['variantid'];

			# oops
			if ( (!isset($idlist[$shopifyid])) || (!isset($idlist[$shopifyid]['variants'][$variantid])) ) { continue; }

			# gather info
			$productid = $idlist[$shopifyid]['postid'];
			$name = $idlist[$shopifyid]['name'];
			$variant = $idlist[$shopifyid]['variants'][$variantid];

			$version = '';
			if ($variant['name'] != '[default]') { $version = $variant['name']; }

			$startdate = $settings['start_date'];
			$enddate = $settings['end_date'];

			# color
			$procolor = get_field('program_color', $productid);
			if (!$procolor) { $procolor = 'propurple'; }

			# thumbnail
			$variants = get_field('shopify_product_variants', $productid);
			$thumbnail = $variants[0]['shopify_variant']['program_thumbnail'];

			if ($idlist[$shopifyid]['format'] == 'variants') {
				foreach ($variants as $vkey => $ventry) {
					if ($variantid == $ventry['shopify_variant']['shopify_variant_ids']['shopify_variant_id']) {
						$thumbnail = $ventry['shopify_variant']['program_thumbnail'];
					}
				}
			}

			# add entry
			$entries[] = array(
				'type' => 'challenge',
				'id' => $challengeid,
				'name' => $name,
				'version' => $version,
				'startdate' => $startdate,
				'enddate' => $enddate,
				'procolor' => $procolor,
				'thumb' => $thumbnail,
			);

		}
	
	}

	# back at ya
	return $entries;
	
}
