<?php
	
# MEMBER TOOLS


# CHECK LOGIN

function membertools_confirmuser() {

	# convenience
	$loginurl = site_url(MEMBER_LOGIN);
	
	# not logged in
	if (!is_user_logged_in()) {
		wp_redirect($loginurl);
		exit;
	}

	# get info
	$userid = get_current_user_id();
	$user = get_userdata($userid);

	# get wp page
	$wpid = get_user_meta($userid, 'wp_pageid', true);

	# create wp page if needed
	if (empty($wpid)) {
		$wpid = member_createpage($userid);
	}

	# if (!empty($wpid)) { return $wpid; }

	# set globals
	$GLOBALS['member']['userid'] = $userid;
	$GLOBALS['member']['username'] = $user->display_name;
	$GLOBALS['member']['pageid'] = $wpid;
	$GLOBALS['member']['firstname'] = $user->first_name;
	$GLOBALS['member']['lastname'] = $user->last_name;
	$GLOBALS['member']['email'] = $user->user_email;
	
}


# DASHBOARD REDIRECT

function membertools_dashboard_redirect() {
	wp_redirect(site_url(MEMBER_DASHBOARD));	
	exit;
}


# PROGRAM INFO

function membertools_proginfo ($programid = 0) {

	# defaults
	$info = array(
		'name' => 'EmFit',
		'version' => '',
	);
	
	# oopsi
	if ($programid == 0) { return $info; }

	# load id lists
	$idlist = shopifyproducts_idlist_load();
	$programlist = shopifyproducts_programlist_load();

	# convenience
	$shopifyid = $programlist[$programid]['shopifyid'];
	$variantid = $programlist[$programid]['variantid'];

	# oops
	if ( (!isset($idlist[$shopifyid])) || (!isset($idlist[$shopifyid]['variants'][$variantid])) ) { return $info; }

	$name = $idlist[$shopifyid]['name'];
	$variant = $idlist[$shopifyid]['variants'][$variantid];

	$version = '';
	if ($variant['name'] != '[default]') {
		$version = $variant['name'];
		if (ctype_digit($variant['name'])) { 'Version ' . $variant['name']; }
	}

	# wrap 'em up
	$info = array(
		'name' => $name,
		'version' => $version,
	);

	# back at ya
	return $info;

}


# GET RESOURCE

function membertools_resource ($resourceid = '') {
	
	# oops
	if (empty($resourceid)) { return false; }

	# get list
	if (!isset($GLOBALS['emfit']['lists']['resources'])) {

		$resources = array();
		$list = get_field('settings_resource_library', 'options');

		foreach ($list as $key => $value) {
			$uuid = $value['uuid'];
			$resources[$uuid] = $value;
			$GLOBALS['emfit']['lists']['resources'] = $resources;
		}
		
	}

	# anybody home?
	if (!isset($GLOBALS['emfit']['lists']['resources'][$resourceid])) { return false; }

	# back at ya
	return $GLOBALS['emfit']['lists']['resources'][$resourceid];

}


# GET MEALPLAN

function membertools_mealplan ($planid = '') {
	
	# oops
	if (empty($planid)) { return false; }

	# get list
	if (!isset($GLOBALS['emfit']['lists']['mealplans'])) {

		$mealplans = array();
		$list = get_field('settings_meal_plans', 'options');

		foreach ($list as $key => $value) {
			$uuid = $value['uuid'];
			$mealplans[$uuid] = $value;
			$GLOBALS['emfit']['lists']['mealplans'] = $mealplans;
		}
		
	}

	# anybody home?
	if (!isset($GLOBALS['emfit']['lists']['mealplans'][$planid])) { return false; }

	# back at ya
	return $GLOBALS['emfit']['lists']['mealplans'][$planid];

}


# GET FACEBOOK GROUPS

function membertools_fbgroup ($groupid = '') {

	# oops
	if (empty($groupid)) { return false; }

	# get list
	if (!isset($GLOBALS['emfit']['lists']['fbgroups'])) {

		$fbgroups = array();
		$list = get_field('settings_facebook_groups', 'options');

		foreach ($list as $key => $value) {
			$uuid = $value['uuid'];
			$fbgroups[$uuid] = $value;
			$GLOBALS['emfit']['lists']['fbgroups'] = $fbgroups;
		}
		
	}

	# anybody home?
	if (!isset($GLOBALS['emfit']['lists']['fbgroups'][$groupid])) { return false; }

	# back at ya
	return $GLOBALS['emfit']['lists']['fbgroups'][$groupid];

}


# UPDATE PROFILE

# logged-in only
add_action('wp_ajax_updateprofile', 'membertools_updateprofile');

function membertools_updateprofile() {

	# check nonce
	check_ajax_referer(MEMBER_PROFILE_NONCE, 'security');

	# check fields
	if ( (!isset($_POST['firstname'])) || (!isset($_POST['lastname'])) || (!isset($_POST['email'])) || (!isset($_POST['userid'])) ) { wp_die(); }
	if (!ctype_digit($_POST['userid'])) { wp_die(); }

	# convenience
	$userid = intval($_POST['userid']);
	$firstname = wp_strip_all_tags(trim($_POST['firstname']));
	$lastname = wp_strip_all_tags(trim($_POST['lastname']));
	$email = trim($_POST['email']);

	# validate ids...

	# user
	$wpuser = get_current_user_id();
	if ($userid != $wpuser) { wp_die(); }

	# email
	if (!is_email($email)) { tools_returnjson(); }

	# update wp user
	$userdata = array(
		'ID' => $userid,
		'first_name' => $firstname,
		'last_name' => $lastname,
		'user_email' => $email,
	);

	# fire!
	wp_update_user($userdata);
	
	# update shopify
	$shopifyid = get_user_meta($userid, 'shopify_id', true);

	if (!empty($storedid)) {
		$args = array(
			'id' => $shopifyid,
			'email' => $email,
		);
		$success = shopify_api('customer_update', $args, $shopifyid);
	}

	# init response
	$response = array(
		'status' => 'success',
	);

	# back at ya
	membertools_ajax_return($response);
	
}


# AJAX RETURN

function membertools_ajax_return ($response = array()) {

	# headers
	header('Pragma: no-cache');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	if (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
		header('Content-type: application/json');
	}
	else { header('Content-type: text/plain'); }
	
	# default reponse
	if (empty($response)) {
		$response['status'] = 'error';
	}

	# away you go
	echo json_encode($response);
	
	# bye!
	wp_die();

}
