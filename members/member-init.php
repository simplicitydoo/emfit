<?php
	
# MEMBER INIT

# nonces
define('MEMBER_CHALLENGE_NONCE', 'member-challenge');
define('MEMBER_PROGRAM_NONCE', 'member-program');
define('MEMBER_SIGNUP_NONCE', 'member-signup');
define('MEMBER_PROFILE_NONCE', 'member-profile');

# includes
require_once(TEMPLATE_MEMBERS . '/member-acf.php');
require_once(TEMPLATE_MEMBERS . '/member-program.php');
require_once(TEMPLATE_MEMBERS . '/member-challenge.php');
require_once(TEMPLATE_MEMBERS . '/member-tools.php');


# CREATE MEMBER

function member_create ($signup = array()) {
	
	# safety first!
	if (empty($signup)) { return false; }
	if ( (!isset($signup['firstname'])) || (!isset($signup['lastname'])) || (!isset($signup['email'])) || (!isset($signup['pw'])) ) { return false; }

	$firstname = trim($signup['firstname']);
	$lastname = trim($signup['lastname']);
	$email = trim($signup['email']);
	$pw = trim($signup['pw']);

	# generate username
	$username = strtolower($firstname.$lastname);
	$valid = false;
	$count = 0;
	while ($valid == false) {
		$count++;
		if (!username_exists("$username-$count")) {
			$username .= "-$count";
			$valid = true;
		}		
	}
	
	# create user
	$userdata = array(
	    'user_login' => $username,
	    'user_pass' => $pw,
		'first_name' => $firstname,
		'last_name' => $lastname,
		'user_email' => $email,
		'role' => 'emfit_member',
		'nickname' => "$firstname $lastname",
	);

	$userid = wp_insert_user($userdata);

	# oops
	if (is_wp_error($userid)) { return false; }	

	# zap admin bar
	update_user_option($userid, 'show_admin_bar_front', false );
	
	# back at ya
	return $userid;
	
}


# CREATE MEMBER PAGE

function member_createpage ($userid = 0) {

	# safety first!
	if (!$userid) { return false; }

	# get user
	$user = get_user_by('id', $userid);
	if (!$user) { return false; }

	# already exist?
	$wpid = get_user_meta($userid, 'wp_pageid', true);
	if (!empty($wpid)) { return $wpid; }
		
	# create member page
	$posttitle = trim($user->first_name . ' ' . $user->last_name);
	if (empty($posttitle)) { $posttitle = $user->display_name; }

	$memberpage = array(
		'post_title' => $posttitle,
		'post_name' => 'member-' . $userid,
		'post_type' => 'emfit_member',
		'post_status' => 'private',
	);
		
	$memberpageid = wp_insert_post($memberpage);
	if (!$memberpageid) { return false; }
	
	add_post_meta($memberpageid, 'user_id', $userid, TRUE);
	
	# update user meta
	add_user_meta(
		$userid,
		'wp_pageid',
		$memberpageid,
		true
	);	
	
	# back at ya
	return $memberpageid;
		
}


# CREATE MEMBER SHOPIFY ID

function member_createshopifyid ($userid = 0, $shopifyid = 0) {

	# safety first!
	if ( (!$userid) || (!$shopifyid) ) { return false; }

	# get user
	$user = get_user_by('id', $userid);
	if ($user) { return false; }

	# already exist?
	$storedid = get_user_meta($userid, 'shopify_id', true);
	if (!empty($storedid)) { return $storedid; }

	# update user meta
	add_user_meta(
		$userid,
		'shopify_id',
		$shopifyid,
		true
	);	

	# back at ya
	return true;

}
