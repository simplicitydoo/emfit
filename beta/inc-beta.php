<?php

# BETA LOGIN

# check cookie
function beta_checklogincookie() {

	if (ENV_SERVER == 'beta') {

		# allow webhooks
		if (strpos($_SERVER['REQUEST_URI'], '/z/') === 0) { return; }

		if ( (!isset($_COOKIE['lemonbeta'])) || ($_COOKIE['lemonbeta'] != "howdy") ) {

			$betapw = 'emily';

			if ( (isset($_POST['howdy'])) && ((trim(strtolower($_POST['howdy']))) == $betapw) ) { beta_setlogincookie(); }

			else { beta_loginform(); }

		}

	}

}


# set cookie
function beta_setlogincookie() {

	# name
	$cookiename = 'lemonbeta';

	# time (days)
	$cookietime = time() + (60*60*24*180);

	# value
	$cookievalue = 'howdy';

	# url
	$cookieurl = $_SERVER['SERVER_NAME'];

	# secure
	$cookiesecure = false;
	if (isset($_SERVER['HTTPS'])) { $cookiesecure = true; }

	# set cookie
	setcookie($cookiename, $cookievalue, $cookietime, "/", $cookieurl, $cookiesecure);

}


# login form
function beta_loginform() {

	$http = "http://";
	if (isset($_SERVER['HTTPS'])) { $http = "https://"; }

?>
<!DOCTYPE html>
<html>
<head>
<title>Lemonade</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="icon" type="image/x-icon" href="<?= TEMPLATE_DIR ?>/beta/icons/lemonade-favicon.ico">
<link rel="apple-touch-icon" href="<?= TEMPLATE_DIR ?>/beta/icons/lemonade-60.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?= TEMPLATE_DIR ?>/beta/icons/lemonade-76.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?= TEMPLATE_DIR ?>/beta/icons/lemonade-120.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?= TEMPLATE_DIR ?>/beta/icons/lemonade-152.png">
<style>
body {
	margin: 0;
	padding: 0;
	line-height: 1;
	background-color: #161d24;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000;
}

div.masterwidth {
	height: 90vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
	text-align: center;
}
</style>
</head>

<body>
<div class="masterwidth">
<a href="/"><img style="width:300px; margin:80px auto 30px auto;" src="<?= TEMPLATE_DIR ?>/beta/site/alogo.svg" width="300" height="300" border="0" alt="Lemonade"></a>
<form method="post" action="<?php echo $http . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] ?>">
<input type="text" size="20" name="howdy">
<input type="submit" value="Go">
</form>

</div>
</body>
</html>

<?php

	exit;

}
