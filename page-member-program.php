<?php
/**
Template name: Member / Program
 */

# MEMBER / PROGRAM

# check login
membertools_confirmuser();

# check program
$programid = memberprogram_validate();
if (!$programid) { membertools_dashboard_redirect(); }

# program info
$GLOBALS['emfit']['programid'] = $programid;
$GLOBALS['emfit']['portalmode'] = 'member';

get_template_part('portal/portal-program');
