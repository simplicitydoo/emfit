<?php
	
# EMFIT FOOTER

# update
$updatetext = get_field('update_text', 'options');
$updatelink = get_field('update_link', 'options');

# nav
$footernav =  get_field('footer_nav', 'options');
$footerbar =  get_field('footer_bar', 'options');

# social
$facebook = get_field('social_facebook', 'options');
$instagram = get_field('social_instagram', 'options');
$twitter = get_field('social_twitter', 'options');
$youtube = get_field('social_youtube', 'options');
$itunes = get_field('social_itunes', 'options');

?>


<?php # FOOTER ?>

<footer class="emfitfooter">
<div class="container">

<?php # updates ?>

<div class="updates flex flexrow flexspace flexbottom">

<div class="logo bgcontain"></div>

<div class="update">

<div class="header textuc">
Recent Updates
</div>
	
<div class="text">
<a href="<?= $updatelink ?>"><?= $updatetext ?></a>
</div>
	
</div>

</div>

<?php # nav ?>

<div class="nav flex flexrow flexspace">

<?php
foreach ($footernav as $key => $value) {
	$nav = $value['footer_nav_column'];
?>
<div class="navcolumn">
<div class="colhead textuc">
<?= $nav['column_title'] ?>
</div>
<ul class="entries plainul">

<?php
	if ( ($nav['link_type'] == 'standard') && (!empty($nav['links'])) ) {
		foreach ($nav['links'] as $lkey => $lvalue) {
			$link = $lvalue['link'];
			$url = $link['page'];
			$target = '';
			if ($link['type'] == 'offsite') {
				$url = $link['url'];
				$target = 'target="_blank"';
			}
?>
<li><a href="<?= $url ?>" <?= $target ?>><?= $link['label'] ?></a>
<?php			
		}
	}
?>

<?php
	if ( ($nav['link_type'] == 'overview') && (!empty($nav['links_overview'])) ) {
		foreach ($nav['links_overview'] as $lkey => $lvalue) {
			$link = $lvalue['link'];
?>
<li><a href="<?= $link['page'] ?>"><?= $link['label'] ?></a>
<?php			
		}
	}
?>

</ul>
</div>
<?php } ?>

</div>

</div>
</footer>


<?php # CREDITS ?>

<div class="footercredits flex text-center">
<div class="container">	
&copy; 2012-<?php echo date('Y'); ?> Unleased Fitness, LLC
<?php foreach ($footerbar as $key => $value) { ?>
&nbsp;&bull;&nbsp; <a href="<?= $value['page'] ?>"><?= $value['label'] ?></a>
<?php } ?>
</div>
</div>

<?php # end main content ?>

</div>


<?php # PAGE ID ?>

<script type="application/json" id="emfitjson">
{"pageid":"<?= $GLOBALS['emfit']['pageid'] ?>"}
</script>


<?php wp_footer(); ?>

</body>
</html>


<?php
	
/*
Array
(
    [0] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => Support
                    [link_type] => standard
                    [links] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => My Account
                                            [type] => page
                                            [page] => http://www.emfit.test/members/dashboard/
                                            [url] => 
                                        )

                                )

                            [1] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Sign Up / Sign In
                                            [type] => page
                                            [page] => http://www.emfit.test/members/
                                            [url] => 
                                        )

                                )

                            [2] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => FAQs
                                            [type] => page
                                            [page] => http://www.emfit.test/faq/
                                            [url] => 
                                        )

                                )

                            [3] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Contact Us
                                            [type] => page
                                            [page] => http://www.emfit.test/contact/
                                            [url] => 
                                        )

                                )

                        )

                    [links_overview] => 
                )

        )

    [1] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => About
                    [link_type] => standard
                    [links] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => About Em
                                            [type] => page
                                            [page] => http://www.emfit.test/about/
                                            [url] => 
                                        )

                                )

                            [1] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Newsfeed
                                            [type] => page
                                            [page] => http://www.emfit.test/news/
                                            [url] => 
                                        )

                                )

                            [2] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Podcasts
                                            [type] => page
                                            [page] => http://www.emfit.test/podcasts/
                                            [url] => 
                                        )

                                )

                            [3] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Success Stories
                                            [type] => page
                                            [page] => http://www.emfit.test/success-stories/
                                            [url] => 
                                        )

                                )

                        )

                    [links_overview] => 
                )

        )

    [2] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => Shop
                    [link_type] => standard
                    [links] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Swag
                                            [type] => page
                                            [page] => http://www.emfit.test/swag/
                                            [url] => 
                                        )

                                )

                            [1] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Supplements
                                            [type] => page
                                            [page] => http://www.emfit.test/supplements/
                                            [url] => 
                                        )

                                )

                            [2] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => EmPack
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://www.evolvedmotion.com/
                                        )

                                )

                            [3] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Herbal Element
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://herbalelement.com/
                                        )

                                )

                            [4] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Body Awareness
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://www.thebodyawarenessproject.com/
                                        )

                                )

                        )

                    [links_overview] => 
                )

        )

    [3] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => Challenges
                    [link_type] => overview
                    [links] => 
                    [links_overview] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => challenge
                                            [page] => http://www.emfit.test/overview/overview-demo/
                                        )

                                )

                        )

                )

        )

    [4] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => Programs
                    [link_type] => overview
                    [links] => 
                    [links_overview] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => program overview
                                            [page] => http://www.emfit.test/overview/overview-demo/
                                        )

                                )

                        )

                )

        )

    [5] => Array
        (
            [footer_nav_column] => Array
                (
                    [column_title] => Follow
                    [link_type] => standard
                    [links] => Array
                        (
                            [0] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Facebook
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://www.facebook.com/superherounleashed
                                        )

                                )

                            [1] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Instagram
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://www.instagram.com/emilyschromm/
                                        )

                                )

                            [2] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => Twitter
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://twitter.com/emfitmtv
                                        )

                                )

                            [3] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => YouTube
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://www.youtube.com/user/EmilySchromm/feed
                                        )

                                )

                            [4] => Array
                                (
                                    [link] => Array
                                        (
                                            [label] => iTunes
                                            [type] => offsite
                                            [page] => 
                                            [url] => https://itunes.apple.com/us/podcast/meathead-hippie/id1212711812?mt=2
                                        )

                                )

                        )

                    [links_overview] => 
                )

        )

)
*/
