(function () {
	'use strict';

	// EMFIT TOOLS

	var emfitTools = (function($) {

		// INIT

		var init = function() {

			initCharacterFilter();
			initHeaderNav();
			initHamburger();
			initFeatureTabs();
			initContactForm();
			initOfferForm();
			initSuccessCarousel();
			initCartButtons();
			InitSortTrigger();
			
		};


		// CHARACTER FILTER
		
		var initCharacterFilter = function() {

			// L SEP (Chrome)
			$('body').children().each(function() {
				$(this).html($(this).html().replace(/\u2028/g, ' ')); 
			});

		};


		// HEADER NAV

		var initHeaderNav = function() {

			var navclear = $('.navclear');
			var navtrigger = $('.navtrigger');
			var subnav = $('.headersubnav');
			var choice;

			$('.navtrigger').on('hover', function(e) {
				if ($(this).hasClass('active')) { return; }
				navclear.removeClass('active');
				$(this).addClass('active');
				choice = $(this).data('subnav');
				subnav.slideUp();
				$('#headersubnav-' + choice).slideDown();
			});		

			$('.navclear').on('hover', function(e) {
				if ($(this).hasClass('active')) { return; }
				navclear.removeClass('active');
				$(this).addClass('active');
				subnav.slideUp();
			});		

			// hide
			$('.headersubnav').on('mouseleave', function(e) {
				subnav.slideUp();
				choice = $(this).data('subnav');
				$('#navtrigger-' + choice).removeClass('active');
			});		

		};


		// HAMBURGER

		var initHamburger = function() {

			$('#hamburger').on('click', function(e) {
				e.preventDefault();

				// close
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$('#slidernav').slideUp();
				}

				// show
				else {
					$(this).addClass('open');
					$('#slidernav').slideDown();
				}

			});		

		};


		// FEATURE TABS

		var initFeatureTabs = function() {

			var tabs = $('.featuretab');
			var panels = $('.featurepanel');

			tabs.on('click', function(e) {
				e.preventDefault();

				var panelid = $(this).data('panelid');

				tabs.removeClass('active');
				panels.removeClass('active');
				
				$(this).addClass('active');
				$('#featurepanel-' + panelid).addClass('active');
				
			});		

		};


		// CONTACT FORM

		var initContactForm = function() {

			var entry;
			var submitbutton = $('#emform-submit');
			var buttonwrap = $('#emform-buttonwrap');
			var response = $('#emform-response');
			
			var score = {
				email: 0,
				message: 0
			};

			// check fields
			$('.emformtext').on('keyup', function(e) {
				
				// get entry
				entry = $.trim($(this).val());

				// email
				if ($(this).hasClass('emformemail')) {
					if (isEmail(entry)) { score.email = 1; }
					else { score.email = 0; }			
				}
				
				// message
				else {
					if ( (entry.length > 0) && (entry.length <= 400) ) { score.message = 1; }
					else { score.message = 0; }			
				}

				// score
				// console.log(score);
				if ( (score.email == 1) && (score.message == 1) ) { submitbutton.removeClass('disabled'); }
				else { submitbutton.addClass('disabled'); }			
				
			});

			submitbutton.on('click', function(e) {
				e.preventDefault();

				submitbutton.addClass('disabled');
				buttonwrap.slideUp();
				response.text('Sending message...').slideDown();		

				var nonce = $('#emform').data('nonce');
				var name = $.trim($('#emform-name').val());
				var company = $.trim($('#emform-company').val());
				var email = $.trim($('#emform-email').val());
				var message = $.trim($('#emform-message').val());
				
				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'emform',
						name: name,
						company: company,
						email: email,
						message: message,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							response.text('Thanks! Your message has been sent.');
						}
						
						// boo
						else {
							response.text('Oops! Please reload the page and try again.');
						}
						
					},
					error: function() {
						response.text('Oops! Please reload the page and try again.');
					}
				});

			});		

		};


		// OFFER FORM

		var initOfferForm = function() {

			var entry;
			var submitbutton = $('#offerform-submit');
			var buttonwrap = $('#offerform-buttonwrap');
			var response = $('#offerform-response');
			
			var score = {
				email: 0,
				message: 0
			};

			// check fields
			$('.offerformtext').on('keyup', function(e) {
				
				// get entry
				entry = $.trim($(this).val());

				// email
				if ($(this).hasClass('offerformemail')) {
					if (isEmail(entry)) { score.email = 1; }
					else { score.email = 0; }			
				}
				
				// message
				else {
					if ( (entry.length > 0) && (entry.length <= 400) ) { score.message = 1; }
					else { score.message = 0; }			
				}

				// score
				// console.log(score);
				if ( (score.email == 1) && (score.message == 1) ) { submitbutton.removeClass('disabled'); }
				else { submitbutton.addClass('disabled'); }			
				
			});

			submitbutton.on('click', function(e) {
				e.preventDefault();

				submitbutton.addClass('disabled');
				buttonwrap.slideUp();
				response.text('Sending...').slideDown();		

				var nonce = $('#offerform').data('nonce');
				var name = $.trim($('#offerform-name').val());
				var email = $.trim($('#offerform-email').val());
				
				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'offerform',
						name: name,
						email: email,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							response.text('Thanks! You have been subscribed.');
						}
						
						// boo
						else {
							response.text('Oops! Please reload the page and try again.');
						}
						
					},
					error: function() {
						response.text('Oops! Please reload the page and try again.');
					}
				});

			});		

		};


		// EMAIL VALIDATOR
		
		function isEmail (email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;
			return regex.test(email);
		}


		// SUCCESS CAROUSEL
		
		var initSuccessCarousel = function() {
		
			var owl = $('#success-carousel');
			
			owl.owlCarousel({
				loop: true,
				margin: 0,
				items: 1,
				dots: false,
				// nav: true,
				autoWidth:true,
				// nestedItemSelector: 'success-nutrition',
				// navContainer: '#projectnavwrap',
				// dotsContainer: '#projectnavdots'
				// touchDrag: false,
				// mouseDrag: false
				// animateOut: 'fadeOut',
				// singleItem: true
			});

			// nav
			$('.successprev').on('click', function(e) {
				e.preventDefault();
				owl.trigger('prev.owl.carousel', [1000]);
			});

			$('.successnext').on('click', function(e) {
				e.preventDefault();
				owl.trigger('next.owl.carousel', [1000]);
			});

			// reload
			// owl.trigger('refresh.owl.carousel');

		};


		// CART BUTTONS
		
		var initCartButtons = function() {

			$('.cartbutton').on('click', function(e) {
				e.preventDefault();

				$(this).addClass('disabled');

				var productid = $(this).data('productid');
				var variantid = $(this).data('variantid');
				var nonce = $(this).data('nonce');

				// console.log(productid + ' / ' + variantid);

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'emfitcart_button',
						productid: productid,
						variantid: variantid,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						window.location.href = '/cart/';
					},
					error: function() {
						// console.log('oops');
						window.location.href = '/cart/';
					}
				});
				
			});

		};


		// SORT TRIGGER
		
		var InitSortTrigger = function() {

			var sorter = $('#listsorter');
			sorter.on('change', function(e) {
				sorter.submit();
			});

		};


		// RETURN

		return {
			init: init,
		};

	}(jQuery));

	emfitTools.init();

	// EMFIT HOMEPAGE

	var emfitHomepage = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'homepage') { return; }

			// toys
			initFAQ();
					
		};


		// FAQ

		var initFAQ = function() {

			var answers = $('.faqanswer');
			var toggles = $('.faqtoggle');
			
			$('.faqtoggle').on('click', function(e) {
				e.preventDefault();

				// close all
				if ($(this).hasClass('open')) {
					answers.slideUp();
					toggles.removeClass('open');
				}

				// open this
				else {
					answers.slideUp();
					toggles.removeClass('open');
					var faqchoice = $(this).data('answer');
					$(this).addClass('open');
					$('#faqanswer-' + faqchoice).slideDown();
				}
				
			});		

		};
		

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitHomepage.init();

	// EMFIT FAQ

	var emfitFAQ = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'faq') { return; }

			// toys
			initFAQ();
					
		};


		// FAQ

		var initFAQ = function() {

			var answers = $('.faqanswer');
			var toggles = $('.faqtoggle');
			
			$('.faqtoggle').on('click', function(e) {
				e.preventDefault();

				// close all
				if ($(this).hasClass('open')) {
					answers.slideUp();
					toggles.removeClass('open');
				}

				// open this
				else {
					answers.slideUp();
					toggles.removeClass('open');
					var faqchoice = $(this).data('answer');
					$(this).addClass('open');
					$('#faqanswer-' + faqchoice).slideDown();
				}
				
			});		

		};
		

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitFAQ.init();

	// EMFIT FINDMYFIT

	var emfitFindMyFit = (function($) {

		var programs, labels, sections, entries, tags, tagarray;

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'findmyfit') { return; }

			// vars
			programs = $('.programentry');
			labels = $('.catlabel');
			sections = $('.catentries');
			entries = $('.catentry');
			tags = $('.catentry');
			tagarray = [];

			// toys
			initCatNav();
			initTags();
					
		};


		// CAT NAV

		var initCatNav = function() {

			var catnav;
			var category;
					
			// label
			labels.on('click', function(e) {
				e.preventDefault();

				catnav = $(this).data('catnav');

				// close
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$('#catnav-' + catnav).slideUp();
				}

				// open
				else {
					labels.removeClass('open');
					$(this).addClass('open');
					sections.slideUp();
					$('#catnav-' + catnav).slideDown();
				}

			});		

			// category
			entries.on('click', function(e) {
				e.preventDefault();

				category = $(this).data('category');
				entries.removeClass('active');
				$(this).addClass('active');

				// hide all
				programs.hide();
				
				// show cats
				$('.' + category).show();

				// reset tags
				tagarray = [];
				$('#selected .tag').remove();
				$('#tagrow .tag').show();
				$('#tagclear-wrap').slideUp();
				
			});		

		};


		// TAGS

		var initTags = function() {

			var placeholder = $('#selected-placeholder');

			var choice;

			$('#selected, #tagrow').on('click', '.tag', function(e) {
				e.preventDefault();

				choice = $(this).data('tag');

				// remove tag
				if ($(this).hasClass('active')) {
					$(this).remove();
					$('#tag-' + choice).show();
					var i = tagarray.indexOf(choice);
					if (i != -1) { tagarray.splice(i, 1); }
				}
			
				// add tag
				else {
					placeholder.hide();
					$(this).clone().addClass('active').removeAttr('id').appendTo('#selected');
					$(this).hide();
					tagarray.push(choice);
					$('#tagclear-wrap').slideDown();
				}

				// close categories
				labels.removeClass('open');
				entries.removeClass('active');
				sections.slideUp();

				// reset panels
				if (tagarray.length == 0) {
					programs.show();
					$('#tagclear-wrap').slideUp();
				}

				else {
					programs.hide();
					tagarray.forEach(function (item, index) {
						$('.' + item).show();
					});						
				}			
			
			});		

			$('#tagclear').on('click', function(e) {
				e.preventDefault();

				tagarray = [];
				$('#selected .tag').remove();
				$('#tagrow .tag').show();
				$('#tagclear-wrap').slideUp();
				programs.show();
				
			});		

		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitFindMyFit.init();

	// EMFIT NEWS

	var emfitNews = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'newslanding') { return; }

			// toys
			initMore();
					
		};


		// NEWS / MORE

		var initMore = function() {

			var nextbatch = 1;
			var nonce = $('#newsmore').data('nonce');
			var newsentry;

			$('#loadmore').on('click', function(e) {
				e.preventDefault();

				$(this).addClass('disabled');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'news_loadmore',
						nextbatch: nextbatch,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							// console.log(json.batch);

							// roll 'em
							$.each(json.batch.entries, function(key, entry) {
								// console.log(entry);

								// clone entry
								newsentry = $('#entryclone').clone().removeAttr('id');

								// custom logo
								if (entry.logo_image.length > 0) {
									$(newsentry).find('#clone-image').css('background-image', 'url(' + entry.logo_image + ')' );								
								}

								// update clone
								$(newsentry).find('#clone-image').addClass(entry.logo).removeAttr('id');
								$(newsentry).find('#clone-category').text(entry.category).addClass(entry.category).removeAttr('id');
								$(newsentry).find('#clone-source').text(entry.source).removeAttr('id');
								$(newsentry).find('#clone-title').text(entry.title).removeAttr('id');
								$(newsentry).find('#clone-url').attr('href', entry.url).removeAttr('id');
								$(newsentry).find('#clone-date').text(entry.date).removeAttr('id');
								$(newsentry).find('#clone-blurb').text(entry.excerpt).removeAttr('id');

								// reveal clone
								$(newsentry).appendTo('#entrywrap').slideDown();

							});		

							// update batch
							$('#loadmore').removeClass('disabled');
							nextbatch = json.batch.nextbatch;
							if (nextbatch == 0) { $('#newsmore').slideUp(); }

						}
						
						// boo
						else {
							$('#newsmore').slideUp();
						}
						
					},
					error: function() {
						$('#newsmore').slideUp();
					}
				});

			});		
				
		};
		

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitNews.init();

	// EMFIT OVERVIEW

	var emfitOverview = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'overview') { return; }

			// toys
			initCallouts();
			initSubav();
			initDetails();

		};


		// CALLOUTS

		var initCallouts = function() {

			var calloutid = 0;
			var overdot = $('.overdot');
			var arrow = $('.calloutarrow');
			var callout = $('.callout');
			var close = $('.calloutclose');

			// dots
			overdot.on('click', function(e) {
				e.preventDefault();
				calloutid = $(this).data('callout');
				updateCallouts();
			});		

			// arrows
			arrow.on('click', function(e) {
				e.preventDefault();
				calloutid = $(this).data('callout');
				updateCallouts();
			});		

			// close
			close.on('click', function(e) {
				e.preventDefault();
				calloutid = 0;
				updateCallouts();
			});		
			
			function updateCallouts() {
				overdot.removeClass('active');
				callout.removeClass('active');
				$('#overdot-' + calloutid).addClass('active');
				$('#callout-' + calloutid).addClass('active');
			}

		};


		// SUBNAV

		var initSubav = function() {

			$('.overviewnavitem').on('click', function(e) {
				e.preventDefault();

				var navchoice = $(this).data('scroll');
				var newposition = $('#' + navchoice).offset();
				$('html, body').stop().animate({ scrollTop: newposition.top }, 500);

			});		

		};


		// DETAILS

		var initDetails = function() {

			var detailchoice;
			var selectors = $('.detailselector');
			var detailsets = $('.detailset');
			
			$('.detailselector').on('click', function(e) {
				e.preventDefault();

				detailchoice = $(this).data('selection');

				selectors.removeClass('selected');
				$(this).addClass('selected');

				detailsets.slideUp();			$('#details-' + detailchoice).slideDown();

			});		

		};


		// RETURN

		return {
			init: init,
		};

	}(jQuery));

	emfitOverview.init();

	// EMFIT PODCASTS

	var emfitPodcasts = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'podcastlanding') { return; }

			// toys
			initMore();
					
		};


		// PODCASTS / MORE

		var initMore = function() {

			var nextbatch = 1;
			var nonce = $('#podmore').data('nonce');
			var podentry;

			$('#loadmore').on('click', function(e) {
				e.preventDefault();

				$(this).addClass('disabled');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'podcast_loadmore',
						nextbatch: nextbatch,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							console.log(json.batch);

							// roll 'em
							$.each(json.batch.entries, function(key, entry) {
								// console.log(entry);

								// clone entry
								podentry = $('#entryclone').clone().attr('href', entry.url).removeAttr('id');

								// update clone
								$(podentry).find('#clone-image').css('background-image', 'url(' + entry.image + ')' );				
								$(podentry).find('#clone-source').text(entry.source).removeAttr('id');
								$(podentry).find('#clone-title').text(entry.title).removeAttr('id');
								$(podentry).find('#clone-date').text(entry.date).removeAttr('id');

								// reveal clone
								$(podentry).appendTo('#entrywrap').slideDown(function() {
									$(this).css('display', '');				
								});

							});		

							// update batch
							$('#loadmore').removeClass('disabled');
							nextbatch = json.batch.nextbatch;
							if (nextbatch == 0) { $('#podmore').slideUp(); }

						}
						
						// boo
						else {
							$('#podmore').slideUp();
						}
						
					},
					error: function() {
						$('#podmore').slideUp();
					}
				});

			});		
				
		};
		

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitPodcasts.init();

	// EMFIT PRODUCT

	var emfitProduct = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'product') { return; }

			// toys
			initFAQ();
			initOptions();
			initCartAdd();
			initCarousel();
					
		};


		// FAQ

		var initFAQ = function() {

			var answers = $('.faqanswer');
			var toggles = $('.faqtoggle');
			
			$('.faqtoggle').on('click', function(e) {
				e.preventDefault();

				// close all
				if ($(this).hasClass('open')) {
					answers.slideUp();
					toggles.removeClass('open');
				}

				// open this
				else {
					answers.slideUp();
					toggles.removeClass('open');
					var faqchoice = $(this).data('answer');
					$(this).addClass('open');
					$('#faqanswer-' + faqchoice).slideDown();
					
				}
				
			});		

		};

		
		// OPTIONS

		var initOptions = function() {

			var variants = JSON.parse($('#variantjson').html());
			var options = JSON.parse($('#optionsjson').html());
			var idfield = $('#variantid');
			var amountfield = $('#product-amount');
			var hilites = $('.hilites');

			var currentid = idfield.val();
			var currentvariant = variants[currentid];
			
			var current = {};
			var choice = {};
			$.each(options, function(optionid, option) {
				current[optionid] = currentvariant[optionid];
			});		
			
			var buttons = $('.featurebutton');

			var buttonid, arr, choicetype, choiceoption, valid, buttontype, buttonoption, variantprice, compareprice, available, validoption;
			var optionid, variant, variantprice;
			var quantity, maxquantity;

			amountfield.on('change', function(e) {

				quantity = parseInt(amountfield.val());
				if ( (isNaN(quantity)) || (quantity < 1) ) { amountfield.val(1); }

				maxquantity = amountfield.attr('max');
				if (maxquantity == '') { return; }

				maxquantity = parseInt(maxquantity);
				if (quantity > maxquantity) { amountfield.val(maxquantity); }

			});		

			buttons.on('click', function(e) {
				e.preventDefault();

				// get button
				buttonid = $(this).attr('id');
				arr = buttonid.split('-');
				choicetype = arr[0];
				choiceoption = arr[1];

				// set choice
				choice = current;
				choice[choicetype] = choiceoption;
				
				// check validity
				$.each(variants, function(variantid, variant) {

					valid = true;

					$.each(choice, function(optionid, optionname) {
						if (variant[optionid] != optionname) { valid = false; }
					});		
									
					if (valid == true) {
						currentid = variantid;
						currentvariant = variants[currentid];
						current = choice;
						idfield.val(variantid);
						return false;
					}

				});		
				
				// next available choice
				if (valid == false) {

					$.each(variants, function(variantid, variant) {
						if (variant[choicetype] == choice[choicetype]) {
							currentid = variantid;
							currentvariant = variant;
							idfield.val(variantid);
							$.each(choice, function(optionid, optionname) {
								current[optionid] = variant[optionid];
							});		
							return false;
						}
					
					});		
					
				}

				// update price
				variantprice = currentvariant['price'];
				compareprice = currentvariant['compareprice'];
				$('#mainprice').text(variantprice);
				$('#btprice').text(variantprice);

				// available optoins
				available = {};
				$.each(variants, function(variantid, variant) {
					if (variant[choicetype] != choiceoption) { return; }
					$.each(options, function(optionid, optionname) {
						validoption = variant[optionid];
						if (typeof available[optionid] === 'undefined') { available[optionid] = {}; }
						available[optionid][validoption] = 1;
					});		
				});		
					
				// update buttons
				buttons.each(function() {

					buttonid = $(this).attr('id');
					arr = buttonid.split('-');
					buttontype = arr[0];
					buttonoption = arr[1];
					
					// check button against current
					if (currentvariant[buttontype] == buttonoption) { $('#' + buttonid).addClass('selected'); }
					else { $('#' + buttonid).removeClass('selected'); }

					// console.log(buttontype + ' / ' + choicetype);

					// check availability
					if (buttontype != choicetype) {
						if (typeof available[buttontype][buttonoption] !== 'undefined') { $('#' + buttonid).removeClass('disabled'); } 
						else { $('#' + buttonid).addClass('disabled'); }
					}

					else {
						 $('#' + buttonid).removeClass('disabled');
					}
					
				});

				// update max quantity
				maxquantity = parseInt(currentvariant['maxquantity']);
				amountfield.attr('max', maxquantity);
				quantity = amountfield.val();
				if ( (maxquantity != '') && (quantity > maxquantity) ) {
					console.log(quantity + ' / ' + maxquantity);
					amountfield.val(maxquantity);
				}

				// update hilites
				hilites.slideUp();
				$('#hilite-' + currentid).slideDown();

				// compile optionid
				optionid = '';
				var keys = Object.keys(current);
				// console.log (keys);

				keys.forEach(augmentOptionID);

				function augmentOptionID (key) {
					optionid = optionid + current[key];
				}

				// console.log ('id: ' + optionid);
				
				// update page if valid
				if (variants.hasOwnProperty(optionid)) {

					variant = variants[optionid];

					$('#variantid').val(variant.id);

					$('#variantname').text(variant.name);

					variantprice = variant.price;
					if (parseFloat(variant.compareprice) > parseFloat(variant.price)) {
						variantprice = variantprice + ' <span>' + variant.compareprice + '</span>';
					}
					$('#variantprice').html(variantprice);

					$('#addbutton').removeClass('dim');
				
					if (variant.cart) {
						$('#incart').removeClass('hide');
						$('#addbutton').addClass('hide');
					} else {
						$('#incart').addClass('hide');
						$('#addbutton').removeClass('hide');
					}
				
				}

				// invalid variant
				else {
					$('#addbutton').addClass('dim');
				}

			});		

		};


		// CART ADD

		var initCartAdd = function() {

			$('#cartadd').on('click', function(e) {
				e.preventDefault();
				$('#cartform').submit();
			});		

		};
		

		// CAROUSEL

		var initCarousel = function() {

			var owl = $('#book-carousel');
			
			owl.owlCarousel({
				loop: true,
				margin: 0,
				items: 1,
				dots: false
				// autoplay: true,
				// autoplayTimeout: 5000,
				// autoplayHoverPause: true,
				// autoplaySpeed: 750
			});

			// nav
			$('#bookprev').on('click', function(e) {
				e.preventDefault();
				owl.trigger('stop.owl.autoplay');
				owl.trigger('prev.owl.carousel', [1000]);
			});

			$('#booknext').on('click', function(e) {
				e.preventDefault();
				owl.trigger('stop.owl.autoplay');
				owl.trigger('next.owl.carousel', [1000]);
			});

		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitProduct.init();

	// EMFIT FAQ

	var emfitSuccessPage = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'successstories') { return; }

			// toys
			initModal();
					
		};


		// MODAL

		var initModal = function() {
		
			var successid;
			var successmodal = $('#success-modal');
			var nonce = $('#success-nonce').data('nonce');

			$('.successentry').on('click', function(e) {
				e.preventDefault();

				successid = $(this).data('successid');

				$('#modal-livewrap').hide();
				$('#modal-fetchwrap').show();
				successmodal.modal('show');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'success_popup',
						successid: successid,
						security: nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {

							// after image
							$('#success-after-image').attr('src', json.entry.after);
							if (json.entry.after.length > 0) { $('#success-after').show(); }
							else { $('#success-after').hide(); }

							// kicker
							$('#success-kicker').text(json.entry.kicker);
							if (json.entry.kicker.length > 0) { $('#success-kicker').show(); }
							else { $('#success-kicker').hide(); }

							// name
							$('#success-name').text(json.entry.displayname);

							// text
							$('#success-story').html(json.entry.text);

							// before image
							$('#success-before-image').attr('src', json.entry.before);
							if (json.entry.before.length > 0) { $('#success-before').show(); }
							else { $('#success-before').hide(); }

							// reveal
							$('#modal-fetchwrap').hide();
							$('#modal-livewrap').slideDown();

						}
						
						// boo
						else {
							successmodal.modal('nide');
						}
						
					},
					error: function() {
						successmodal.modal('nide');
					}
				});

			});		

		};
		

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitSuccessPage.init();

	// EMFIT CART

	var emfitCart = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'cart') { return; }

			// toys
			initRemoveItem();
			initRemoveAll();
			initCheckoutButton();
			initQuantities();
						
		};


		// REMOVE ITEM

		var initRemoveItem = function() {

			$('.removeitem').on('click', function(e) {
				e.preventDefault();

				var removeid = $(this).data('removeid');
				$('#remove-' + removeid).submit();

			});		

		};


		// REMOVE ALL

		var initRemoveAll = function() {

			$('#button-removeall').on('click', function(e) {
				e.preventDefault();
				$('#form-removeall').submit();
			});		

		};


		// QUANTITY

		var initQuantities = function() {

			var quantity, maxquantity, productid, variantid, nonce, qfield;
			var cartsubtotal = $('#cart-subtotal');
			var checkoutbutton = $('#button-checkout');
			
			$('.optionamount').on('change', function(e) {

				qfield = $(this);
				qfield.addClass('disabled');
				checkoutbutton.addClass('disabled');

				quantity = parseInt(qfield.val());
				if (isNaN(quantity)) { quantity = 1; }
				if (quantity < 1) { quantity = 1; }

				maxquantity = qfield.attr('max');
				if (maxquantity != '') {
					maxquantity = parseInt(maxquantity);
					if (quantity > maxquantity) { quantity = maxquantity; }
				}

				// console.log(quantity + ' / ' + maxquantity);

				productid = qfield.data('productid');
				variantid = qfield.data('variantid');
				nonce = qfield.data('nonce');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'emfitcart_quantity',
						productid: productid,
						variantid: variantid,
						quantity: quantity,
						security: nonce
					},
					success: function(json) {
						console.log(json);
						qfield.val(quantity);
						qfield.removeClass('disabled');
						cartsubtotal.text(json.subtotal);
						checkoutbutton.removeClass('disabled');
					},
					error: function() {
						// console.log('oops');
						location.reload(true);
					}
				});

			});		

		};


		// CHECKOUT BUTTON

		var initCheckoutButton = function() {

			$('#button-checkout').on('click', function(e) {
				e.preventDefault();
				$(this).addClass('disabled');
				$('#checkoutwrap').slideUp();
				$('#checkoutmessage').slideDown();
				$('#form-checkout').submit();
			});		

		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitCart.init();

	// EMFIT PORTAL LANDING

	var emfitLanding = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'login') { return; }

			// toys
			initLandingNav();
					
		};


		// LANDING NAV

		var initLandingNav = function() {

			var navitems = $('.portalnavlink');
			var panels = $('.portal-landing');
			var panelid;
			
			navitems.on('click', function(e) {
				e.preventDefault();

				// get id
				panelid = $(this).data('panel');

				// close all
				navitems.removeClass('active');
				panels.hide();
				
				// open choice
				$('#portalnav-' + panelid).addClass('active');
				$('#panel-' + panelid).show();
			
			});		


		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitLanding.init();

	// EMFIT DASHBOARD

	var emfitDashboard = (function($) {

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'dashboard') { return; }

			// toys
			initDashboardNav();
			initProfileForm();
					
		};


		// DASHBOARD NAV

		var initDashboardNav = function() {

			var navitems = $('.portalnavlink');
			var panels = $('.dashboard-panel');
			var panelid;
			
			navitems.on('click', function(e) {
				e.preventDefault();

				// get id
				panelid = $(this).data('panel');

				// close all
				navitems.removeClass('active');
				panels.hide();
				
				// open choice
				$(this).addClass('active');
				$('#panel-' + panelid).show();
			
			});		


		};


		// PROFILE FORM

		var initProfileForm = function() {

			var entry;
			var submitbutton = $('#profileform-submit');
			var buttonwrap = $('#profileform-buttonwrap');
			var response = $('#profileform-response');

			// pre-filled form		
			var score = {
				email: 1,
				firstname: 1,
				lastname: 1,
			};

			// form slider
			$('#profile-edit').on('click', function(e) {
				$('#panel-memberlive').slideUp();
				$('#panel-memberform').slideDown();
			});		

			// check fields
			$('.profileformtext').on('keyup', function(e) {
				
				// get entry
				entry = $.trim($(this).val());

				// email
				if ($(this).hasClass('profileformemail')) {
					if (isEmail(entry)) { score.email = 1; }
					else { score.email = 0; }			
				}
				
				// first name
				else if ($(this).hasClass('profileformfirst')) {
					if (entry.length > 0) { score.firstname = 1; }
					else { score.firstname = 0; }			
				}

				// last name
				else if ($(this).hasClass('profileformlast')) {
					if (entry.length > 0) { score.lastname = 1; }
					else { score.lastname = 0; }			
				}

				// score
				// console.log(score);
				if ( (score.email == 1) && (score.firstname == 1) && (score.lastname == 1) ) { submitbutton.removeClass('disabled'); }
				else { submitbutton.addClass('disabled'); }			
				
			});

			submitbutton.on('click', function(e) {
				e.preventDefault();

				submitbutton.addClass('disabled');
				buttonwrap.slideUp();
				response.text('Updating Profile...').slideDown();		

				var nonce = $('#profileform').data('nonce');
				var userid = $.trim($('#profileform-id').val());
				var firstname = $.trim($('#profileform-first').val());
				var lastname = $.trim($('#profileform-last').val());
				var email = $.trim($('#profileform-email').val());
				
				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'updateprofile',
						userid: userid,
						firstname: firstname,
						lastname: lastname,
						email: email,
						security: nonce
					},
					success: function(json) {
						console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							$('#profile-first').text(firstname);
							$('#profile-last').text(lastname);
							$('#profile-email').text(email);
							response.text('Thanks! Your Profile has been updated.');
						}
						
						// boo
						else {
							response.text('Oops! Please reload the page and try again.');
						}
						
					},
					error: function() {
						response.text('Oops! Please reload the page and try again.');
					}
				});

			});		

		};

		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitDashboard.init();

	// EMFIT CHALLENGE

	var emfitChallenge = (function($) {

		var gestalt;

		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'challenge') { return; }

			// gestalt
			gestalt = JSON.parse($('#challenge-gestalt').html());

			// toys
			initChallengeNav();
			initMessages();
			initPoints();
			initDatepicker();
			initEmailTest();
					
		};


		// CHALLENGE NAV

		var initChallengeNav = function() {

			var navitems = $('.portalnavlink');
			var panels = $('.challenge-panel');
			var panelid;
			
			navitems.on('click', function(e) {
				e.preventDefault();

				// get id
				panelid = $(this).data('panel');

				// close all
				navitems.removeClass('active');
				panels.hide();
				
				// open choice
				$(this).addClass('active');
				$('#panel-' + panelid).show();
			
			});		


		};


		// DATEPICKER
		// https://bootstrap-datepicker.readthedocs.io/en/latest/options.html

		var initDatepicker = function() {

			$('#challenge-datemodal').on('click', function(e) {
				e.preventDefault();
				$('#modal-datepickier').modal('show');
			});		

			var datechoice;

			$('#datepicker').datepicker({
			    todayHighlight: true,
				format: 'mm/dd/yy',
			    startDate: 'today',
			    endDate: '+4m',
			    maxViewMode: 'years'
			});

			$('#datepicker').datepicker('update', new Date());
			datechoice = $('#datepicker').datepicker('getFormattedDate');

			$('#datepicker-date').text(datechoice);

			$('#datepicker').on('changeDate', function() {
				datechoice = $('#datepicker').datepicker('getFormattedDate');
				$('#datepicker-date').text(datechoice);
			});

			$('#pickerdate').on('click', function(e) {
				e.preventDefault();

				// return if admin
				if (gestalt.mode != 'member') { return; }

				$(this).addClass('disabled');
				$('#datepickermessage').text('Updating your Challenge…').slideDown();
				// console.log(datechoice);

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'challenge_startdate',
						challengeid: gestalt.challengeid,
						datechoice: datechoice,
						userid: gestalt.userid,
						pageid: gestalt.pageid,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {

							// reload page
							document.location.reload();

						}
						
						// boo
						else {
							$('#pickerdate').removeClass('disabled');
							$('#datepickermessage').text('Oops! Please select another date.');
						}
						
					},
					error: function() {
						$('#pickerdate').removeClass('disabled');
						$('#datepickermessage').text('Oops! Server error. Please try again.');
					}
				});

			});

		};


		// MESSAGES

		var initMessages = function() {

			var naventries = $('.entryday');
			var messages = $('.messageday');
			var messageid;
					
			$('.entryday').on('click', function(e) {
				e.preventDefault();

				// get id
				messageid = $(this).data('message');

				// update nav
				naventries.removeClass('active');
				$('.navday-' + messageid).addClass('active');

				// open choice
				if ($(this).hasClass('navmobile')) {
					messages.hide();
					$('#message-' + messageid).show();
					$('html, body').scrollTop($(this).offset().top);
				}
				
				else {
					messages.slideUp();
					$('#message-' + messageid).slideDown();
				}
				
			});		

		};


		// POINTS

		var initPoints = function() {

			var toggles = $('.pointtoggle');
			var messageid;
			var questionid;
			var pointid;
			var points;
			var pointlabel;
			
			toggles.on('click', function(e) {
				e.preventDefault();

				// get ids
				messageid = $(this).data('messageid');
				questionid = $(this).data('questionid');

				// points
				if (gestalt.mode == 'member') {
					points = parseInt($('#headerpoints-' + messageid).text());
				}

				// remove check
				if ($(this).hasClass('checked')) {
					pointid = messageid + '-' +  questionid + '-0';
					$(this).removeClass('checked');
					if (gestalt.mode == 'member') { points = points - 1; }
				}

				// add check
				else{
					pointid = messageid + '-' +  questionid + '-1';
					$(this).addClass('checked');
					if (gestalt.mode == 'member') { points = points + 1; }
				}

				// return if admin
				if (gestalt.mode != 'member') { return; }

				// point label
				pointlabel = ' points';
				if (points == 1) { pointlabel = ' point'; }

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'challenge_points',
						challengeid: gestalt.challengeid,
						pointid: pointid,
						userid: gestalt.userid,
						pageid: gestalt.pageid,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							
							// update display
							$('.navpoints-' + messageid).text(points + pointlabel);
							$('#headerpoints-' + messageid).text(points);

							// nav peekaboo
							if (points == 0) { $('.navpoints-' + messageid).hide(); }
							else { $('.navpoints-' + messageid).show(); }

						}
						
					},
					error: function() {
					}
				});

			});

		};


		// EMAIL TEST

		var initEmailTest = function() {

			// admin only
			if (gestalt.mode != 'admin') { return; }

			var day;

			$('.emailtest').on('click', function(e) {
				e.preventDefault();

				// get ids
				day = $(this).data('day');

				// remove link
				$('#testemail-' + day).text('Sending email...');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'challenge_test',
						challengeid: gestalt.challengeid,
						day: day,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							$('#testemail-' + day).text('Test email sent.');
						}
						
						// boo
						else {
							$('#testemail-' + day).text('Error: Test email not sent.');
						}
						
					},
					error: function() {
						$('#testemail-' + day).text('Server Error: Test email not sent.');
					}
				});

			});

		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitChallenge.init();

	// EMFIT PROGRAM

	var emfitProgram = (function($) {

		var gestalt;


		// INIT

		var init = function() {

			// setup
			var pagejson = JSON.parse($('#emfitjson').html());
			if (pagejson.pageid != 'program') { return; }

			// gestalt
			gestalt = JSON.parse($('#program-gestalt').html());

			// toys
			initProgramNav();
			initWeekNav();
			initDayNav();
			initDatepicker();
			initScore();
			initComments();
			initEmailTest();
						
		};


		// PROGRAM NAV

		var initProgramNav = function() {

			var navitems = $('.portalnavlink');
			var panels = $('.program-panel');
			var panelid;
			
			navitems.on('click', function(e) {
				e.preventDefault();

				// get id
				panelid = $(this).data('panel');

				// close all
				navitems.removeClass('active');
				panels.hide();
				
				// open choice
				$(this).addClass('active');
				$('#panel-' + panelid).show();
			
			});		

		};


		// WEEK NAV

		var initWeekNav = function() {

			var naventries = $('.entryweek');
			var weeks = $('.weekwrap');
			var weekid;
					
			naventries.on('click', function(e) {
				e.preventDefault();

				// get id
				weekid = $(this).data('week');

				// close all
				naventries.removeClass('active');
				weeks.hide();

				// open choice
				$('.navweek-' + weekid).addClass('active');
				$('#week-' + weekid).show();
				if ($(this).hasClass('navmobile')) {
					$('html, body').scrollTop($(this).offset().top);
				}

			});		

		};


		// DAY NAV

		var initDayNav = function() {

			var dayheads = $('.dayhead');
			var weekid;
			
			dayheads.on('click', function(e) {
				e.preventDefault();

				// get id
				weekid = $(this).data('week');

				// close all
				$('.head' + weekid).removeClass('active');
				$('.week' + weekid).slideUp();
				
				// open content
				$(this).addClass('active');
				$(this).next('.daycontent').slideDown();
			
			});		

		};


		// DATEPICKER
		// https://bootstrap-datepicker.readthedocs.io/en/latest/options.html

		var initDatepicker = function() {

			$('#program-datemodal').on('click', function(e) {
				e.preventDefault();
				$('#modal-datepickier').modal('show');
			});		

			var datechoice;

			$('#datepicker').datepicker({
			    todayHighlight: true,
				format: 'mm/dd/yy',
			    startDate: 'today',
			    endDate: '+4m',
			    maxViewMode: 'years'
			});

			$('#datepicker').datepicker('update', new Date());
			datechoice = $('#datepicker').datepicker('getFormattedDate');

			$('#datepicker-date').text(datechoice);

			$('#datepicker').on('changeDate', function() {
				datechoice = $('#datepicker').datepicker('getFormattedDate');
				$('#datepicker-date').text(datechoice);
			});

			$('#pickerdate').on('click', function(e) {
				e.preventDefault();

				// return if admin
				if (gestalt.mode != 'member') { return; }

				$('#pickerdate').addClass('disabled');
				$('#datepickermessage').text('Updating your Program…').slideDown();
				// console.log(datechoice);

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'program_startdate',
						programid: gestalt.programid,
						datechoice: datechoice,
						userid: gestalt.userid,
						pageid: gestalt.pageid,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {

							// reload page
							document.location.reload();

						}
						
						// boo
						else {
							$('#pickerdate').removeClass('disabled');
							$('#datepickermessage').text('Oops! Please select another date.');
						}
						
					},
					error: function() {
						$('#pickerdate').removeClass('disabled');
						$('#datepickermessage').text('Oops! Server error. Please try again.');
					}
				});

			});

		};


		// SCORE

		var initScore = function() {

			var toggles = $('.steptoggle');
			var workoutid;
			var stepid;
			var togglevalue;
			
			toggles.on('click', function(e) {
				e.preventDefault();

				// get ids
				workoutid = $(this).data('workoutid');
				stepid = $(this).data('stepid');

				// remove check
				if ($(this).hasClass('checked')) {
					togglevalue = 0;
					$(this).removeClass('checked');
				}

				// add check
				else{
					togglevalue = 1;
					$(this).addClass('checked');
				}

				// return if admin
				if (gestalt.mode != 'member') { return; }

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'program_score',
						programid: gestalt.programid,
						workoutid: workoutid,
						stepid: stepid,
						togglevalue: togglevalue,
						userid: gestalt.userid,
						pageid: gestalt.pageid,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
													
							// update display
							if (json.complete == 1) { $('#complete-' + workoutid).addClass('hilite'); }
							else { $('#complete-' + workoutid).removeClass('hilite'); }
							
						}
						
					},
					error: function() {
					}
				});

			});

		};


		// COMMENT

		var initComments = function() {

			// return if admin
			if (gestalt.mode != 'member') { return; }

			var trigger = $('.commentsubmit');
			var workoutid;
			var stepid;
			var commentid;
			var comment;
			var textlength;
			
			trigger.on('click', function(e) {
				e.preventDefault();

				// one click only
				$(this).addClass('disabled');

				// get ids
				workoutid = $(this).data('workoutid');
				stepid = $(this).data('stepid');
				commentid = workoutid + '-' + stepid;
				
				// get comment
				comment = $.trim($('#comment-' + commentid).val());
				// console.log(commentid + ' / ' + comment);
				
				// check size
				textlength = comment.length;			
				if ( (textlength <= 0) || (textlength > 300) ) {
					$(this).removeClass('disabled');
					return;
				}

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'program_comment',
						programid: gestalt.programid,
						workoutid: workoutid,
						stepid: stepid,
						comment: comment,
						userid: gestalt.userid,
						pageid: gestalt.pageid,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							// console.log(json.comment);
													
							// update display
							$('#comment-' + commentid).addClass('disabled').slideUp();
							$('#commentbutton-' + commentid).slideUp();
							$('#livecomment-' + commentid).text(comment).slideDown();

						}
						
						// boo
						else {
							$(this).removeClass('disabled');
						}
						
					},
					error: function() {
						$(this).removeClass('disabled');
					}
				});
				
			});

				
		};


		// EMAIL TEST

		var initEmailTest = function() {

			// admin only
			if (gestalt.mode != 'admin') { return; }

			var week;
			var day;
			var workoutid;		

			$('.emailtest').on('click', function(e) {
				e.preventDefault();

				// get ids
				week = $(this).data('week');
				day = $(this).data('day');
				workoutid = week + '-' + day;

				// remove link
				$('#testemail-' + workoutid).text('Sending email...');

				// fire!
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					data: {
						action: 'program_test',
						programid: gestalt.programid,
						week: week,
						day: day,
						security: gestalt.nonce
					},
					success: function(json) {
						// console.log(json);
						
						// yayyy
						if (json.status == 'success') {
							// console.log(json.comment);
													
							$('#testemail-' + workoutid).text('Test email sent.');

						}
						
						// boo
						else {
							$('#testemail-' + workoutid).text('Error: Test email not sent.');
						}
						
					},
					error: function() {
						$('#testemail-' + workoutid).text('Server Error: Test email not sent.');
					}
				});

			});

		};


		// RETURN
		
		return {
			init: init
		};

	}(jQuery));

	emfitProgram.init();

}());
