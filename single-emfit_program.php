<?php
	
# EMFIT PROGRAM

# admin check
tools_adminonly();

# challenge info
$GLOBALS['emfit']['programid'] = get_the_ID();
$GLOBALS['emfit']['portalmode'] = 'admin';

get_template_part('portal/portal-program');
