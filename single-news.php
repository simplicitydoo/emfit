<?php
	
# EMFIT NEWS

# page flag
$GLOBALS['emfit']['pageid'] = 'news';

# content
$posttype = get_field('news_category');
$hero = get_field('hero_image');
$source = get_field('news_source');
$title = get_field('news_title');
$news = get_field('news');
$podcast = get_field('podcast');

# type: news / podcast
$posttype = 'news';


# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero">
<div class="container">

<div class="livewrap">

<div class="title">
xxx
</div>

<div class="date">
xxx
</div>

</div>

</div>
</section>


<?php # RETURN ?>

<div class="news-return">
<div class="container">

<div class="arrowwrap">
<a class="arrow arrowrleft" href="#">Go Back</a>
</div>

</div>
</div>


<?php # CONTENT ?>

<?php if ($posttype == 'news') { ?>
<section class="news-content">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-10 offset-md-1">

<div class="intro">
<div class="source">
xxx	
</div>
<div class="title">
xxx	
</div>
</div>

<div class="content">
xxx	
</div>

</div>
</div>
</div>
</section>
<?php } ?>


<?php # PODCAST ?>

<?php if ($posttype == 'podcast') { ?>
<section class="news-podcast">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-10 offset-md-1">

<div class="intro">
<div class="source">
xxx	
</div>
<div class="title">
xxx	
</div>
</div>

<div class="podcast">

<div class="imagewrap bgcover"></div>

<div class="textwrap">

<div class="text">
xxx
</div>

<div class="services">
<a href="#" class="service itunes bgcontain hidetext">iTunes</a>
<a href="#" class="service stitcher bgcontain hidetext">Stitcher</a>
<a href="#" class="service spotify bgcontain hidetext">Spotify</a>
</div>

</div>

</div>

<div class="arrowwrap">
<a class="arrow arrowrright" href="#">Share This Post</a>
</div>

<div class="content">
xxx	
</div>

<div class="links">

<div class="entry">
<div class="entrytitle">
xxx	
</div>
<div class="entrylinks">
xxx	
</div>
</div>

<div class="entry">
<div class="entrytitle">
xxx	
</div>
<div class="entrylinks">
xxx	
</div>
</div>

</div>

</div>
</div>
</div>
</section>
<?php } ?>


<?php # REGISTER ?>

<div class="news-register">
<div class="container">

<div class="arrowwrap">
<a class="arrow arrowrright" href="#">To Post Comments — Log In, or Register Today!</a>
</div>

</div>
</div>


<?php # COMMENTS ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();
