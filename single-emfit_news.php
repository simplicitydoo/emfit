<?php
	
# EMFIT NEWS

# page flag
$GLOBALS['emfit']['pageid'] = 'news';

# content
$posttype = get_field('news_category');
$herobg = get_field('hero_image');
$source = get_field('news_source');
$title = get_field('news_title');
$news = get_field('news');
$podcast = get_field('podcast');

# formats
$formats = array();
$content = $news['story_text'];
$related = array();
$backlink = '/news/';

if ($posttype == 'podcast') {
	$formats = $podcast['formats'];
	$content = $podcast['description'];
	$related = $podcast['related_links'];
	$backlink = '/podcasts/';
}

else { $posttype = 'news'; }


# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $herobg ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $title ?>
</div>

<div class="date textuc">
Posted on: <?php the_date(); ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # RETURN ?>

<div class="general-return">
<div class="container height100">
<div class="flex height100">

<div class="arrowwrap">
<a class="arrow arrowleft" href="<?= $backlink ?>">Go Back</a>
</div>

</div>
</div>
</div>


<?php # INTRO ?>

<section class="news-intro <?= $posttype ?>">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">

<div class="source">
<?= $source ?>:
</div>
<div class="title">
<?= $title ?>
</div>

</div>
</div>
</div>
</section>


<?php # PODCAST ?>

<?php if ($posttype == 'podcast') { ?>

<?php # podcast ?>

<section class="news-podcast">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">

<div class="podwrap flex flexrow flexspace">

<div class="imagewrap bgcover" style="background-image:url(<?= $podcast['feature_image'] ?>);"></div>

<div class="textwrap flex flexspace">

<div class="text">
listen on any of the formats below!
</div>

<div class="services">

<?php if (!empty($formats['itunes_url'])) { ?>
<a class="service" href="<?= $formats['itunes_url'] ?>" target="_blank"><img src="<?= TEMPLATE_ASSETS ?>/news/logo-itunes-2x.png" alt="iTunes"></a>
<?php } ?>

<?php if (!empty($formats['stitcher_url'])) { ?>
<a class="service" href="<?= $formats['stitcher_url'] ?>" target="_blank"><img src="<?= TEMPLATE_ASSETS ?>/news/logo-stitcher-2x.png" alt="Stitcher"></a>
<?php } ?>

<?php if (!empty($formats['spotify_url'])) { ?>
<a class="service" href="<?= $formats['spotify_url'] ?>" target="_blank"><img src="<?= TEMPLATE_ASSETS ?>/news/logo-spotify-2x.png" alt="Spotify"></a>
<?php } ?>

</div>

</div>

</div>

</div>
</div>
</div>
</section>

<?php # share ?>

<?php if (ENV_SERVER == 'bugs') { ?>
<div class="news-share">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2 height100">
<div class="livewrap flex height100">

<div class="arrowwrap">
<a class="arrow arrowright arrowgreen arrowpad" href="#">Share This Post</a>
</div>

</div>
</div>
</div>
</div>
</div>
<?php } ?>


<?php } ?>


<?php # CONTENT ?>

<section class="news-content">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">

<div class="content">
<?= $content ?>	
</div>

<div class="footerline"></div>

</div>
</div>
</div>
</section>


<?php # RELATED ?>

<?php if (!empty($related)) { ?>
<section class="news-related">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">

<div class="livewrap">

<?php
	foreach ($related as $key => $value) {
		$group = $value['link_group'];
		if (!empty($group['group_links'])) {
?>
<div class="entry">
<div class="entrytitle">
<?= $group['group_title'] ?>	
</div>
<div class="entrylinks">
<?php foreach ($group['group_links'] as $gkey => $gvalue) { ?>
<p><a href="<?= $gvalue['link_url'] ?>" target="_blank"><?= $gvalue['link_label'] ?></a></p>
<?php } ?>
</div>
</div>
<?php
		}
	}
?>

</div>

</div>
</div>
</div>
</section>
<?php } ?>


<?php # REGISTER ?>

<?php if (ENV_SERVER == 'bugs') { ?>
<div class="news-register">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2 height100">
<div class="flex height100">

<div class="arrowwrap">
<a class="arrow arrowright arrowgreen arrowpad" href="#">To Post Comments — Log In, or Register Today!</a>
</div>

</div>
</div>
</div>
</div>
</div>
<?php } ?>


<?php # COMMENTS ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();
