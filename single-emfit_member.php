<?php
	
# EMFIT MEMBER

$pageid = get_the_ID();
print_r($pageid);

$programs = get_field('member_programs');
$challenges = get_field('member_challenges');

print_r($programs);
print_r($challenges);


/*
Array
(
    [0] => Array
        (
            [acf_fc_layout] => member_program
            [program_settings] => Array
                (
                    [program_id] => 135
                    [start_date] => 2019-05-12
                    [end_date] => 2019-05-13
                )

            [program_order_details] => Array
                (
                    [program_order_id] => 
                    [program_order_date] => 
                )

            [program_workouts] => Array
                (
                    [0] => Array
                        (
                            [workout_settings] => Array
                                (
                                    [workout_id] => 1-1
                                    [workout_type] => rest
                                    [workout_date] => 2019-05-12
                                )

                            [workout_steps] => 
                        )

                    [1] => Array
                        (
                            [workout_settings] => Array
                                (
                                    [workout_id] => 1-2
                                    [workout_type] => workout
                                    [workout_date] => 2019-05-13
                                )

                            [workout_steps] => Array
                                (
                                    [0] => Array
                                        (
                                            [step_id] => A
                                            [step_status] => pending
                                            [step_comment] => 
                                            [step_images] => 
                                        )

                                    [1] => Array
                                        (
                                            [step_id] => B
                                            [step_status] => pending
                                            [step_comment] => 
                                            [step_images] => 
                                        )

                                )

                        )

                )

        )

)

Array
(
    [0] => Array
        (
            [acf_fc_layout] => member_challenge
            [challenge_settings] => Array
                (
                    [challenge_id] => 648
                    [start_date] => 
                    [end_date] => 
                )

            [challenge_order_details] => Array
                (
                    [challenge_order_id] => 
                    [challenge_order_date] => 
                )

            [challenge_points] => 
            [challenge_messages] => Array
                (
                    [0] => Array
                        (
                            [message_settings] => Array
                                (
                                    [message_id] => 1
                                    [message_date] => 2019-08-06
                                )

                            [challenge_points] => 
                        )

                )

        )

)
*/
