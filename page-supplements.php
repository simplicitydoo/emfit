<?php
/**
Template name: Supplements / Landing
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'supplements';

# content
$hero = get_field('hero_panel');
$intro = get_field('intro_panel');
$products = products_list('supplement');
$showfeatured = get_field('show_featured_product');
$productpanel = get_field('featured_product');

# print_r($products);
# exit;

# sorter
$sorter = 'latest';
if (isset($_GET['sort'])) { $sorter = trim(strtolower($_GET['sort'])); };

$sellatest = 'selected';
$seltype = '';

if ($sorter == 'type') {
	$sellatest = '';
	$seltype = 'selected';
}

$sorturl =  strtok($_SERVER["REQUEST_URI"], '?');

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # SUBHERO ?>

<div class="shop-subhero">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 text-center flex height100">
<div class="blurbwrap">
Need Help? Have Questions? We <a href="/faq/">are here</a> for you!
</div>
</div>
</div>
</div>
</div>


<?php # INTRO ?>

<section class="shop-supplements-intro">
<div class="container">

<div class="introwrap">

<div class="maintext">
<?= $intro['main_text'] ?>
</div>

<div class="rowwrap flex flexrow flexspace">

<div class="imagewrap">
<img src="<?= $intro['feature_image'] ?>">	
</div>

<div class="textwrap">
<?= $intro['additional_text'] ?>
</div>

</div>

</div>

</div>
</section>


<?php # ENTRIES ?>

<?php if (!empty($products)) { ?>
<section class="shop-entries sorting">
<div class="container">

<div class="sorter flex">
<div class="sortwrap flex flexrow flexalign">
<div class="label textuc">
Sort Items By
</div>
<div class="chooser">
<form method="get" action="<?= $sorturl ?>" id="listsorter">
<select name="sort" class="selectarrows">
<option value="latest" <?= $sellatest ?>>latest</option>	
<option value="type" <?= $seltype ?>>type</option>	
</select>
</form>
</div>
</div>	
</div>

<div class="entries flex flexrow flexspace flexwrap">

<?php
	# first 8
	$counter = 0;

	foreach ($products as $key => $value) {
		
		# price
		$price = str_replace('.00', '', $value['price']);
		if (intval($price) == 0) { $price = 'Free'; }
		else { $price = '$' . $price; }

?>
<a href="<?= $value['url'] ?>" class="entry">
<?php if ($value['newlabel']) { ?>
<div class="newtag">New!</div>
<?php } ?>
<div class="imagewrap bgcover" style="background-image:url(<?= $value['image'] ?>);"></div>
<div class="textwrap flex text-center">
<div class="name">
<?= $value['name'] ?>
</div>
<?php if (!empty($value['type'])) { ?>
<div class="type textuc">
<?= $value['type'] ?>
</div>	
<?php } ?>
<div class="price textuc">
<?= $price ?>
</div>
</div>
</a>
<?php

		# counter
		$counter++;
		if ($counter == 8) { break; }
		
	}
?>

</div>

</div>
</section>
<?php } ?>


<?php # FEATURED ?>

<?php
if ($showfeatured) {

	$topline = $productpanel['panel_title']['top_line'];
	$bottomline = $productpanel['panel_title']['bottom_line'];
	$hilite = $productpanel['panel_title']['highlight_color'];
	
	if ($productpanel['panel_title']['bold_text'] == 'top') {
		$topline = '<span class="' . $hilite . '">'. $topline . '</span>';
	} else {
		$bottomline = '<span class="' . $hilite . '">'. $bottomline . '</span>';
	}
	
	$productid = $productpanel['select_product'];
	$button = products_button($productid);
	$variant = false;
	if (isset($button['variants']['single'])) {
		$variant = $button['variants']['single'];
	}
?>

<section class="panel-product panelreverse">
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace flexrowreverse">

<div class="imagewrap bgcontain" style="background-image:url(<?= $productpanel['product_image'] ?>);"></div>

<div class="textwrap">

<div class="kicker textuc textpad">
<?= $productpanel['kicker'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<?= $topline ?><br>
<?= $bottomline ?>
</div>

<div class="blurb">
<?= $productpanel['product_blurb'] ?>
</div>

</div>

<?php if (!empty($variant)) { ?>
<div class="buttonwrap textpad">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $button['shopifyid'] ?>" data-variantid="<?= $variant['variantid'] ?>" data-nonce="<?= $button['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $variant['price'] ?></div>
</a>
</div>	
<?php } ?>

<div class="arrowwrap textpad">
<a class="arrow arrowright arrowgreen" href="<?= $button['url'] ?>"><?= $productpanel['link_label'] ?></a>
</div>
	
</div>

</div>

</div>
</section>
<?php } ?>


<?php # ENTRIES / MORE ?>

<?php if (count($products) > 8) { ?>
<section class="shop-entries supplements">
<div class="container">

<div class="entries flex flexrow flexspace flexwrap">

<?php
	# skip first 8
	$counter = 0;

	foreach ($products as $key => $value) {

		# counter
		$counter++;
		if ($counter < 9) { continue; }
		
		# price
		$price = str_replace('.00', '', $value['price']);
		if (intval($price) == 0) { $price = 'Free'; }
		else { $price = '$' . $price; }

?>
<a href="<?= $value['url'] ?>" class="entry">
<?php if ($value['newlabel']) { ?>
<div class="newtag">New!</div>
<?php } ?>
<div class="imagewrap bgcover" style="background-image:url(<?= $value['image'] ?>);"></div>
<div class="textwrap flex text-center">
<div class="name">
<?= $value['name'] ?>
</div>
<?php if (!empty($value['type'])) { ?>
<div class="type textuc">
<?= $value['type'] ?>
</div>	
<?php } ?>
<div class="price textuc">
<?= $price ?>
</div>
</div>
</a>
<?php } ?>

</div>

</div>
</section>
<?php } ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();


/*
Array
(
    [0] => Array
        (
            [newlabel] => 
            [image] => 
            [name] => ADB5 Plus
            [tags] => 
            [price] => 45.00
            [compare] => 
            [url] => http://www.emfit.test/product/adb5-plus/
        )

    [1] => Array
        (
            [newlabel] => 
            [image] => 
            [name] => Apex Acetyl-CH
            [tags] => 
            [price] => 53.00
            [compare] => 
            [url] => http://www.emfit.test/product/apex-acetyl-ch/
        )

)
*/
