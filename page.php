<?php

# header
get_header();

?>

<section class="general-content">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">

<h1><?php the_title(); ?></h1>

<?php the_content(); ?>

</div>
</div>
</div>
</section>

<?php
	
# footer
get_footer();
