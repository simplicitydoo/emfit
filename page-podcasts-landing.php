<?php
/**
Template name: Podcasts / Landing
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'podcastlanding';

# content
$hero = get_field('hero_panel');
$podbatch = podcast_entries();
$entries = $podbatch['entries'];
$nextbatch = $podbatch['nextbatch'];
$nonce = wp_create_nonce(PAGE_PODCAST_NONCE);

# $nextbatch = 1;

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # SORTER ?>

<div class="podcasts-sorter">
<div class="container">
</div>
</div>


<?php # ENTRIES ?>

<?php if (!empty($entries)) { ?>
<section class="podcasts-entries">
<div class="container">

<div class="entrywrap flex flexrow flexspace flexwrap" id="entrywrap">

<?php foreach ($entries as $entry) { ?>
<a class="podentry" href="<?= $entry['url'] ?>">

<div class="image bgcover" style="background-image:url(<?= $entry['image'] ?>);"></div>

<div class="textwrap">

<div class="text">
<strong><?= $entry['source'] ?>:</strong> <?= $entry['title'] ?>
</div>

<div class="details">
<div class="date textuc flex">
<?= $entry['date'] ?>
</div>
</div>

</div>

</a>
<?php } ?>


<?php # CLONE ?>

<a class="podentry" href=""  id="entryclone" style="display:none;">
<div class="image bgcover" id="clone-image"></div>
<div class="textwrap">
<div class="text">
<strong><span id="clone-source"></span>:</strong> <span id="clone-title"></span>
</div>
<div class="details">
<div class="date textuc flex" id="clone-date"></div>
</div>
</div>
</a>


<?php # end entry wrap ?>
</div>


<?php # MORE ?>

<?php if (!empty($nextbatch)) { ?>
<div class="podcasts-more text-center" data-nonce="<?= $nonce ?>" id="podmore">
<a href="#" class="arrow arrowdown loadarrow" id="loadmore">Load More</a>
</div>
<?php } ?>

</div>
</section>
<?php } ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();

/*
Array
(
    [entries] => Array
        (
            [0] => Array
                (
                    [image] => http://www.emfit.test/wp-content/uploads/2019/08/beta-coverart.jpg
                    [source] => Meathead Hippie Podcast #104
                    [title] => A wonderful conversation with Amy Jo Martin
                    [date] => August 16, 2019
                    [url] => http://www.emfit.test/news/demo-podcast/
                )

        )

    [nextbatch] => 0
)
*/
