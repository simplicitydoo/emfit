<?php
	
# EMFIT SUCCESS

# admin check
tools_adminonly();

# page flag
$GLOBALS['emfit']['pageid'] = 'success';

# content
$type = get_field('type');
$name = get_field('name');
$images = get_field('images');
$content = get_field('story');

# compile
$kicker = '';
if ($type != 'other') {
	$kicker = 'Success in ' . $type;
}

$before = $images['before'];
$after = $images['after'];

if (empty($after)) {
	if (!empty($before)) { $after = $before; }
	$before = '';
}

# header
get_header();

?>

<?php # ADMIN PREVIEW ?>

<section class="portal-adminbar text-center textuc">
<div class="container height100">
<div class="flex height100">
Admin Preview
</div>
</div>
</section>


<?php # MODAL MOCKUP ?>

<div class="success-modal adminpreview">
<div class="modal-dialog">

<div class="livewrap">

<?php if (!empty($after)) { ?>
<div class="imagewrap">
<img src="<?= $after ?>" alt="">
</div>
<?php } ?>

<div class="textwrap">

<?php if (!empty($kicker)) { ?>
<div class="kicker textuc">
<?= $kicker ?>
</div>
<?php } ?>

<div class="name">
<?= $name ?>
</div>

<div class="story">
<?= $content ?>
</div>

</div>

<?php if (!empty($before)) { ?>
<div class="imagewrap">
<img src="<?= $before ?>" alt="">
</div>
<?php } ?>

</div>

</div>
</div>
</div>


<?php
	
# footer
get_footer();
