<?php
/**
Template name: News / Landing
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'newslanding';

# content
$hero = get_field('hero_panel');
$categories = news_categories();
$newsbatch = news_entries();
$entries = $newsbatch['entries'];
$nextbatch = $newsbatch['nextbatch'];
$nonce = wp_create_nonce(PAGE_NEWS_NONCE);

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # SORTER ?>

<div class="news-sorter">
<div class="container">
</div>
</div>


<?php # ENTRIES ?>

<?php if (!empty($entries)) { ?>
<section class="news-entries">
<div class="container">

<div class="entrywrap" id="entrywrap">

<?php
	foreach ($entries as $entry) {
		$logostyle = '';
		if (!empty($entry['logo_image'])) { $logostyle = 'background-image:url(' . $entry['logo_image'] . ')'; }
?>
<div class="newsentry flex flexrow flexspace flexalign">

<div class="imagewrap">
<div class="image <?= $entry['logo'] ?> bgcontain" style="<?= $logostyle ?>"></div>
</div>

<div class="textwrap">

<div class="tagwrap">
<div class="tag textuc <?= $entry['category'] ?>">
<?= $entry['category'] ?>
</div>
</div>

<div class="intro">
<div class="source">
<?= $entry['source'] ?>:
</div>
<div class="title">
<a href="<?= $entry['url'] ?>"><?= $entry['title'] ?></a>
</div>
</div>

<div class="date textuc flex">
<?= $entry['date'] ?>	
</div>

<div class="blurb">
<?= $entry['excerpt'] ?>	
</div>

</div>

</div>
<?php } ?>


<?php # CLONE ?>

<div class="newsentry flex flexrow flexspace flexalign" id="entryclone" style="display:none;">

<div class="imagewrap">
<div class="image bgcontain" id="clone-image"></div>
</div>

<div class="textwrap">

<div class="tagwrap">
<div class="tag textuc" id="clone-category"></div>
</div>

<div class="intro">
<div class="source" id="clone-source"></div>
<div class="title">
<a href="" id="clone-url"><span id="clone-title"></span></a>
</div>
</div>

<div class="date textuc flex" id="clone-date"></div>

<div class="blurb" id="clone-blurb"></div>

</div>

</div>

<?php # end entry wrap ?>
</div>

<?php # MORE ?>

<?php if (!empty($nextbatch)) { ?>
<div class="entrywrap" data-nonce="<?= $nonce ?>" id="newsmore">
<div class="newsentry morewrap">
<a href="#" class="arrow arrowdown loadarrow" id="loadmore">Load More</a>
</div>
</div>
<?php } ?>

</div>
</section>
<?php } ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();


/*
Array
(
    [entries] => Array
        (
            [0] => Array
                (
                    [category] => podcast
                    [logo] => meathead
                    [source] => Meathead Hippie Podcast #104
                    [title] => A wonderful conversation with Amy Jo Martin
                    [date] => August 16, 2019
                    [excerpt] => 
                    [url] => http://www.emfit.test/news/demo-podcast/
                )

            [1] => Array
                (
                    [category] => television
                    [logo] => denver
                    [source] => Denver 9 News
                    [title] => Alternative coffee sweeteners and creamers
                    [date] => August 16, 2019
                    [excerpt] => 
                    [url] => http://www.emfit.test/news/demo-news-story/
                )

        )

    [nextbatch] => 0
)
*/
