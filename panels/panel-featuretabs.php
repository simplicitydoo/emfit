<?php
	
# FEATURE TABS PANEL

# content
$features = get_field('settings_features', 'options');

?>

<section class="panel-featuretabs">

<div class="container">
<div class="row">
<div class="col-sm-12 tabrow">
<div class="tabwrap flex flexrow flexspace">

<?php
foreach ($features as $key => $feature) {
	$css = '';
	if ($key == 0) { $css = 'active'; }
?>
<div class="featuretab <?= $css ?> height100" data-panelid="<?= $key ?>">
<div class="kicker textuc">
<?= $feature['label'] ?>
</div>
<div class="title">
<?= $feature['title'] ?>
</div>
</div>	
<?php } ?>

</div>
</div>
</div>
</div>

<div class="panelwrap">

<?php
foreach ($features as $key => $feature) {

	$css = '';
	if ($key == 0) { $css = 'active'; }

	$link = $feature['link_url'];
	$target = 'target="_blank"';

	if ($feature['link_type'] == 'page') {
		$link = $feature['link_page'];
		$target = '';
	}

?>

<div class="featurepanel <?= $css ?> bgcover" id="featurepanel-<?= $key ?>" style="background-image:url(<?= $feature['background_image'] ?>);">
<div class="scrim flex height100">
<div class="container">
<div class="row">
<div class="col-sm-12">

<div class="contentwrap height100 flex flexrow flexalign">

<div class="logowrap">
<img class="logo" src="<?= $feature['logo'] ?>" atl="">
</div>

<div class="textwrap">

<div class="blurb">
<?= $feature['text'] ?>
</div>

<div class="buttonwrap">
<a href="<?= $link ?>" <?= $target ?>><button class="featurebutton btgreen"><?= $feature['link_label'] ?></button></a>
</div>

</div>

</div>

</div>
</div>
</div>
</div>
</div>
<?php } ?>

</div>
	
</section>
