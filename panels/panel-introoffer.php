<?php
	
# INTRO OFFER PANEL

# nonce
$nonce = wp_create_nonce(FORM_OFFER_NONCE);

# cutout
$class = '';
$cutout = false;
if ($GLOBALS['emfit']['pageid'] == 'homepage') {
	$class = 'cutout';
	$cutout = true;
}

?>

<section class="panel-introoffer <?= $class ?>">

<?php if ($cutout) { ?>
<div class="overlay bgcover"></div>	
<?php } ?>

<div class="container">

<div class="rowwrap flex flexrow">

<div class="titlewrap emgreen">
<div class="textlive flex">

<div class="title textlc">
Free
</div>

<div class="subtitle">
5-day<br>
intro to Em
</div>

<div class="blurb">
Sign up to receive  my free 5 Day intro, exclusive updates, special deals
</div>

</div>
</div>

<div class="formwrap">
<div class="formlive flex">

    <?php
    $before_form = get_field( 'footer_before_form', 'option' );
    $before_submit = get_field( 'footer_before_submit', 'option' );
    $append = '';
    if ( isset( $before_form ) && ! empty( $before_form ) ) {

        $append .= ' before_form="' . esc_attr( $before_form ) . '"';

    }
    if ( isset( $before_submit ) && ! empty( $before_submit ) ) {

        $append .= ' before_submit="' . esc_attr( $before_submit ) . '"';

    }
    $append .= ' styles="0"';
    echo do_shortcode( str_replace( ']', $append . ']', get_field( 'footer_shortcode', 'option' ) ) );
    ?>

<!--<form class="general-form" data-nonce="<?/*= $nonce */?>" id="offerform">

<div class="instruction">
Enter Your Info Below *	
</div>	

<div class="field">
<input type="text" name="name" class="offerformtext" placeholder="Your First Name" id="offerform-name">
</div>	

<div class="field">
<input type="email" name="email" class="offerformtext offerformemail" placeholder="Your Email" id="offerform-email">
</div>	

<div class="fineprint">
*I Promise I will NEVER spam you!
</div>	

<div class="buttonwrap" id="emform-buttonwrap">
<button class="featurebutton btgreen disabled" id="offerform-submit">Get the Free 5-Day Intro</button>
</div>	

<div class="response flex text-center" id="offerform-response" style="display:none;"></div>	

</form>-->

</div>
</div>

</div>

</div>
</div>

</div>
</section>
