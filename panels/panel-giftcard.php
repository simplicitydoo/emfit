<?php
	
# GIFT CARD PANEL

# product info
$giftcard = get_field('panel_giftcard', 'options');
$productid = $giftcard['product_giftcard'];

$button = products_button($productid);

if (!empty($button)) {
	$variant = array_shift($button['variants']);
?>

<section class="panel-giftcard flex">

<div class="rowwrap flex flexrow flexspace flexalign flexrowreverse">

<div class="imagewrap bgcontain"></div>

<div class="textwrap">

<div class="kicker textpad textuc">
EMFIT GIFT CARDS
</div>

<div class="borderwrap textpad">

<div class="title textlc">
<span>give the gift</span><br>
of greatness
</div>

<div class="blurb">
Give the gift of health by sending someone a gift card for any of Em’s programs
</div>

</div>

<div class="buttonwrap textpad">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $button['shopifyid'] ?>" data-variantid="<?= $variant['variantid'] ?>" data-nonce="<?= $button['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $variant['price'] ?></div>
</a>
</div>

<div class="viewmore textpad textlc">
<a href="<?= $button['url'] ?>">View More Details + Options</a>
</div>
	
</div>

</div>

</section>
<?php } ?>
