<?php
	
# EMFIT PRODUCT / CUSTOM

# get product data
$product = products_page('custom');

# print_r($product); exit;

# convenience
$productname = $product['productname'];
$productblurb = $product['productblurb'];
$productid = $product['productid'];
$currentid = $product['currentid'];
$images = $product['images'];
$variants = $product['variants'];
$options = $product['options'];
$unavailable = $product['unavailable'];
$jsonoptions = $product['jsonoptions'];
$related = $product['related'];

$hilitecolor = $product['hilite_color'];
$herobg = $product['herobg'];
$optionsubheads = $product['optionsubheads'];
$addnote = $product['addnote'];
$testimonials = $product['testimonials'];
$carousel = $product['carousel'];
$video = $product['video'];
$featurepanels = $product['featurepanels'];

# header
get_header();

?>

<?php # HILITE COLOR ?>

<?php if (!empty($hilitecolor)) { ?>
<style>
.product-subhero a {
	color: <?= $hilitecolor ?>;
}		
.product-main .mainprice {
	color: <?= $hilitecolor ?>;
}
.product-main .optionwrap .featurebutton {
	border-color: <?= $hilitecolor ?>;
}
.product-main .optionwrap .featurebutton.selected {
	background-color: <?= $hilitecolor ?>;
}
.product-main .optionwrap .optionamount {
	border-color: <?= $hilitecolor ?>;	
}
.product-main .actionwrap .pricebutton {
	border-color: <?= $hilitecolor ?>;		
}
.product-main .actionwrap .pricebutton .price {
	background-color: <?= $hilitecolor ?>;		
}
</style>
<?php } ?>


<?php # HERO ?>

<section class="general-hero customproduct">
<img src="<?= $herobg ?>"	>
</section>


<?php # SUBHERO ?>

<div class="product-subhero">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 text-center flex height100">
<div class="blurbwrap">
if you have any questions, just <a href="/contact/">contact us</a> or visit the <a href="/faq/">FAQ page</a>
</div>
</div>
</div>
</div>
</div>


<?php # MAIN ?>

<section class="product-main book">
<div class="container">
<div class="productwrap flex flexrow flexspace">


<?php # IMAGES ?>

<div class="imagecol">

<?php
$display = $images['display_style'];
$pairs = $images['images'];

if ( (!empty($pairs[0]['left_image'])) && (!empty($pairs[0]['right_image'])) ) {
?>		
<div class="imagewrap swag">

<?php foreach ($pairs as $pair => $value) { ?>
<div class="subimages flex flexrow flexspace">
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?=  $value['left_image'] ?>);"></div>
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?=  $value['right_image'] ?>);"></div>
</div>
<?php } ?>

</div>
<?php } ?>

</div>


<?php # PRODUCT INFO ?>

<div class="infocol">

<div class="mainprice emgreen">
<?php if ($unavailable) { ?>
Out of Stock
<?php } else { ?>
<span id="mainprice"><?= $variants[$currentid]['price'] ?></span>
<?php } ?>
</div>

<div class="namewrap">
<div class="name">
<?= $productname ?>
</div>
<div class="version">
</div>
</div>

<div class="blurb">
<?= $productblurb ?>
</div>


<?php # FORM ?>

<?php if (!$unavailable) { ?>
<form method="post" action="<?php echo admin_url('admin-post.php') ?>" id="cartform">
<input type="hidden" name="action" value="emfitcart_add">
<input type="hidden" name="productid" value="<?= $productid ?>">
<input type="hidden" name="variantid" id="variantid" value="<?= $currentid ?>">
	
<?php
if (!empty($options)) {
	foreach ($options as $key => $value) {

		$optionntitle = 'Select ' . $value['name'];
		if ( (isset($optionsubheads[$key])) && (!empty($optionsubheads[$key])) ) {
			$optionntitle = $optionsubheads[$key];
		}
		$optionid = $value['optionid'];
		$optionclass = 'wide';
		
?>
<div class="optionwrap">
<div class="title textuc">
<?= $optionntitle ?>
</div>
<div class="options flex flexrow flexspace flexwrap">
<?php
		foreach ($value['options'] as $okey => $ovalue) {

			# echo "$currentid / $optionid";

			$buttonid = $optionid . '-' . $okey;
			$selected = $disabled = '';

			$label = str_replace('.00 USD', '', $ovalue);

			if ($variants[$currentid][$optionid] == $okey) {
				$selected = 'selected';
				if ($variants[$currentid]['cart']) { $disabled = 'disabled'; }
			}			
?>			
<button type="button" class="featurebutton wide noshadow btgreen <?= $optionclass ?> <?= $selected ?> <?= $disabled ?>" id="<?= $buttonid ?>">
<?= $label ?>
</button>
<?php } ?>
</div>
</div>
<?php
	}
}	
?>

<div class="optionwrap">
<div class="title textuc">
Quantity
</div>
<input type="number" name="quantity" min="1" max="<?= $variants[$currentid]['maxquantity'] ?>" step="1" value="1" class="optionamount" id="product-amount">
</div>

<div class="actionwrap tight">
<div class="addbutton">
<a class="pricebutton btgreen flex flexrow flexspace" href="#" id="cartadd">
<div class="label text-center">Add to Cart</div><div class="price" ><span id="btprice"><?= $variants[$currentid]['price'] ?></span></div>
</a>
</div>
</div>

</form>
<?php } ?>


<?php # ADD NOTE ?>

<?php if (!empty($addnote)) { ?>
<div class="imageblurbwrap">
<div class="imageblurb">
<?= $addnote['note_text'] ?>
</div>

<?php if (!empty($addnote['note_image'])) { ?>
<div class="image">
<img src="<?= $addnote['note_image'] ?>">
</div>
<?php } ?>

</div>
<?php } ?>


</div>

</div>
</div>
</section>


<?php # TESTIMONIALS ?>

<?php if (!empty($testimonials['testimonials'])) { ?>
<section class="product-testimonials text-center bg<?= $testimonials['panel_color'] ?>">
<div class="container">
<div class="innerwrap">

<div class="title">
<?= $testimonials['panel_title'] ?>
</div>

<div class="quotewrap flex flexrow flexspace flexwrap">

<?php foreach ($testimonials['testimonials'] as $key => $value) { ?>
<div class="entry">
<div class="quote">
<?= $value['quote'] ?>	
</div>
<div class="credit">
- <?= $value['name'] ?>
</div>
</div>	
<?php } ?>

</div>

</div>
</section>
<?php } ?>


<?php # CAROUSEL ?>

<?php
if (!empty($carousel)) {
	$hilite = $carousel['highlight_color'];
?>
<section class="product-carousel">
<div class="container height100">
<div class="carouselwrap height100 flex flexrow flexspace">

<div class="arrow arrowleft bgcover" id="bookprev"></div>
<div class="arrow arrowright bgcover" id="booknext"></div>

<div class="entrywrap height100 owl-carousel owl-theme" id="book-carousel">

<?php
	foreach ($carousel['panels'] as $key => $value) {
		$upper = 'lite';
		$lower = 'heavy ' . $hilite;
		if ($value['bold_title'] == 'upper') {
			$upper = 'heavy ' . $hilite;
			$lower = 'lite';
		}
?>
<div class="entry flex">

<div class="imagecol bgcontain" style="background-image:url(<?= $value['panel_image'] ?>);"></div>

<div class="header">
<div class="<?= $upper ?>">
<?= $value['title_upper'] ?>	
</div>
<div class="<?= $lower ?>">
<?= $value['title_lower'] ?>	
</div>
</div>

<div class="text">
<?= $value['panel_text'] ?>	
</div>

</div>
<?php } ?>

</div>

</div>
</div>
</div>
</section>
<?php } ?>


<?php # VIDEO ?>

<?php if (!empty($video)) { ?>

<section class="product-video bgcover" style="background-image:url(<?= $video['background_image'] ?>);">
<div class="container height100 flex">

<div class="buttonwrap">
<a href="<?= $video['youtube_url'] ?>" data-lity><button class="featurebutton btgreen"><?= $video['button_label'] ?></button></a>
</div>

</div>
</section>
<?php } ?>


<?php # PANELS ?>

<?php
if (!empty($featurepanels)) {

	foreach ($featurepanels as $key => $value) {
	
		$panelbg = '';
		$panelcss = 'panelreverse';
		$flexcss = 'flexrowreverse';

		$hilite = $value['highlight_color'];
		
		$upper = 'lite';
		$lower = 'heavy ' . $hilite;
		if ($value['bold_title'] == 'upper') {
			$upper = 'heavy ' . $hilite;
			$lower = 'lite';
		}
	
		if (($key % 2) == 1) {
			$panelbg = 'bggray';
			$panelcss = '';
			$flexcss = '';
		}

		# cart button
		$cartbutton = false;
		if ( (isset($value['include_cart_button'])) && ($value['include_cart_button']) ) {
			
			$cartbutton = products_button($product['pageid']);
			$buttonvariant = false;

			if (isset($cartbutton['variants']['single'])) {
				$buttonvariant = $cartbutton['variants']['single'];
			}

			else if ( (isset($value['cartbutton_select_variant'])) && (!empty($value['cartbutton_select_variant'])) ) {
				$variantid = $value['cartbutton_select_variant'];
				foreach ($cartbutton['variants'] as $vkey => $vvalue) {
					if ($vvalue['variantid'] == $variantid) {
						$buttonvariant = $cartbutton['variants'][$vkey];
					}
				}

			}

			if (!$buttonvariant) { $cartbutton = false; }

		}
		
?>
<section class="panel-product <?= $panelcss ?> <?= $panelbg ?>">
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace <?= $flexcss ?>">

<div class="imagewrap bgcontain" style="background-image:url(<?= $value['panel_image'] ?>);"></div>

<div class="textwrap">
<div class="kicker textuc textpad">
<?= $value['panel_subhead'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<div class="<?= $upper ?>">
<?= $value['title_upper'] ?>	
</div>
<div class="<?= $lower ?>">
<?= $value['title_lower'] ?>	
</div>
</div>

<div class="blurb smalltext">
<?= $value['panel_text'] ?>
</div>

<?php if ($cartbutton) { ?>
<div class="cartbuttonwrap">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $cartbutton['shopifyid'] ?>" data-variantid="<?= $buttonvariant['variantid'] ?>" data-nonce="<?= $cartbutton['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $buttonvariant['price'] ?></div>
</a>
</div>	
<?php } ?>

</div>
	
</div>

</div>
</section>
<?php
	}
}	
?>


<?php # RELATED ?>

<?php if (!empty($related)) { ?>
<section class="product-related">
<div class="container">

<div class="paneltitle text-center">
Other Products You Might Like
</div>

<div class="shop-entries related">
<div class="entries flex flexrow flexspace flexwrap">
<?php
	foreach ($related as $key => $entryid) {
		
		$entry = products_related($entryid);
		if (empty($entry)) { continue; }		

?>
<a href="<?= $entry['url'] ?>" class="entry">
<?php if ($entry['newlabel']) { ?>
<div class="newtag">New!</div>
<?php } ?>
<div class="imagewrap bgcover" style="background-image:url(<?= $entry['image'] ?>);"></div>
<div class="textwrap flex text-center">
<div class="name">
<?= $entry['name'] ?>
</div>
<?php if (!empty($entry['type'])) { ?>
<div class="type textuc">
<?= $entry['type'] ?>
</div>	
<?php } ?>
<div class="price textuc">
<?= $entry['price'] ?>
</div>
</div>
</a>
<?php } ?>
</div>
</div>

</div>
</section>
<?php } ?>


<?php # JSON ?>

<script type="application/json" id="variantjson">
<?php echo json_encode($variants); ?>
</script>

<script type="application/json" id="optionsjson">
<?php echo json_encode($jsonoptions); ?>
</script>

<?php get_footer(); ?>

<?php
/*
Array
(
    [productid] => 3529101475945
    [producttype] => swag
    [productname] => Product Name to Launch
    [productblurb] => 
    [images] => 
    [variants] => Array
        (
            [28093330096233] => Array
                (
                    [name] => XL
                    [price] => $25
                    [compareprice] => $
                    [hilites] => Array
                        (
                            [hilite_difficulty] => 
                            [hilite_length] => 
                            [hilite_workouts] => none
                            [hilite_focus] => 
                            [hilite_equipment] => 
                            [hilite_how] => 
                        )

                    [current] => 1
                    [cart] => 
                    [maxquantity] => 5
                    [size] => xl
                )

            [30323534594153] => Array
                (
                    [name] => L
                    [price] => $25
                    [compareprice] => $
                    [hilites] => Array
                        (
                            [hilite_difficulty] => 
                            [hilite_length] => 
                            [hilite_workouts] => none
                            [hilite_focus] => 
                            [hilite_equipment] => 
                            [hilite_how] => 
                        )

                    [current] => 
                    [cart] => 
                    [maxquantity] => 2
                    [size] => l
                )

        )

    [options] => Array
        (
            [option_1] => Array
                (
                    [name] => Size
                    [optionid] => size
                    [options] => Array
                        (
                            [xl] => XL
                            [l] => L
                        )

                )

        )

    [currentid] => 28093330096233
    [jsonoptions] => Array
        (
            [size] => Array
                (
                    [xl] => XL
                    [l] => L
                )

        )

    [unavailable] => 
    [herobg] => http://www.emfit.test/wp-content/uploads/2019/04/herobg-2x.jpg
    [optionsubheads] => Array
        (
            [option_1] => Make a Choice
            [option_2] => 
            [option_3] => 
        )

    [addnote] => Array
        (
            [note_text] => <p>$1 of every book sold goes to</p>

            [note_image] => http://www.emfit.test/wp-content/uploads/2019/04/girlsinc-2x.jpg
        )

    [testimonials] => Array
        (
            [panel_color] => progreen
            [panel_title] => First Impressions
            [testimonials] => Array
                (
                    [0] => Array
                        (
                            [quote] => Praesent finibus aliquam tincidunt. Donec in orci ac justo venenatis vestibulum.
                            [name] => Person’s Name
                        )

                    [1] => Array
                        (
                            [quote] => Nullam gravida tincidunt sollicitudin. Ut egestas ligula dictum.
                            [name] => Person’s Name
                        )

                    [2] => Array
                        (
                            [quote] => Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
                            [name] => Person’s Name
                        )

                    [3] => Array
                        (
                            [quote] => Ut egestas ligula dictum, eleifend tellus etligula dictum
                            [name] => Person’s Name
                        )

                )

        )

    [carousel] => Array
        (
            [highlight_color] => progreen
            [panels] => Array
                (
                    [0] => Array
                        (
                            [bold_title] => lower
                            [title_upper] => Chapter
                            [title_lower] => One
                            [panel_text] => <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet lectus vel sapien dictum elementum. Vestibulum viverra dignissim quam, venenatis lobortis purus ullamcorper ut.</p>
<p>Nam sapien nibh, interdum in molestie ut, finibus eget purus. Duis id arcu nec tellus tristique feugiat at sed nisi. Mauris ullamcorper auctor ante viverra fringilla. Curabitur tempus elit a tempus faucibus.</p>

                            [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/caterpillar-2x.jpg
                        )

                    [1] => Array
                        (
                            [bold_title] => lower
                            [title_upper] => Chapter
                            [title_lower] => Two
                            [panel_text] => <p>Nam sapien nibh, interdum in molestie ut, finibus eget purus. Duis id arcu nec tellus tristique feugiat at sed nisi. Mauris ullamcorper auctor ante viverra fringilla. Curabitur tempus elit a tempus faucibus.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet lectus vel sapien dictum elementum. Vestibulum viverra dignissim quam, venenatis lobortis purus ullamcorper ut.</p>

                            [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/caterpillar-2x.jpg
                        )

                    [2] => Array
                        (
                            [bold_title] => lower
                            [title_upper] => Chapter
                            [title_lower] => Three
                            [panel_text] => <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet lectus vel sapien dictum elementum. Vestibulum viverra dignissim quam, venenatis lobortis purus ullamcorper ut.</p>
<p>Nam sapien nibh, interdum in molestie ut, finibus eget purus. Duis id arcu nec tellus tristique feugiat at sed nisi. Mauris ullamcorper auctor ante viverra fringilla. Curabitur tempus elit a tempus faucibus</p>

                            [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/caterpillar-2x.jpg
                        )

                )

        )

    [video] => Array
        (
            [background_image] => http://www.emfit.test/wp-content/uploads/2019/04/book-videobg-2x.jpg
            [button_label] => Watch Now
            [youtube_url] => https://www.youtube.com/watch?v=6FStEivMXGQ
        )

    [featurepanels] => Array
        (
            [0] => Array
                (
                    [highlight_color] => proturquoise
                    [bold_title] => upper
                    [panel_subhead] => The Author
                    [title_upper] => About
                    [title_lower] => Emily
                    [panel_text] => <p>Maecenas turpis purus, porttitor sit amet tristique eu, cursus rutrum augue.</p>
<p>Ut imperdiet vestibulum libero, nec aliquet erat fermentum in. Aenean sed elit dignissim, laoreet leo ut, cursus nibh.</p>
<p>Phasellus quis turpis pretium, laoreet metus in, laoreet magna. Etiam sollicitudin porttitor tortor, euismod vehicula ligula mattis in.</p>
<p>Curabitur efficitur eleifend nisi, vitae hendrerit justo semper sed. Morbi nec euismod lorem. Aenean vitae luctus velit, non ultrices magna.</p>

                    [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/bookpanel-about-2x.jpg
		            [include_cart_button] => 1
		            [cartbutton_select_variant] => 31081850765417
                )

            [1] => Array
                (
                    [highlight_color] => proturquoise
                    [bold_title] => upper
                    [panel_subhead] => Can’t Read?
                    [title_upper] => The Book in
                    [title_lower] => Audio Form
                    [panel_text] => <p>Maecenas turpis purus, porttitor sit amet tristique eu, cursus rutrum augue. Ut imperdiet vestibulum libero, nec aliquet erat fermentum in. Aenean sed elit dignissim, laoreet leo ut, cursus nibh.</p>

                    [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/bookpanel-audio-2x.jpg
                )

            [2] => Array
                (
                    [highlight_color] => proturquoise
                    [bold_title] => upper
                    [panel_subhead] => The Artist
                    [title_upper] => Sketches By
                    [title_lower] => Hilary Thomas
                    [panel_text] => <p>Spirit Animal: Polar Bear<br />
Location: NY</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed sem laoreet, ultricies lacus eget, condimentum ligula. Donec malesuada cursus dui. Mauris non lectus lacinia, blandit mauris vel, pellentesque lacus. In hac habitasse platea dictumst. Nullam maximus mattis ligula, id finibus tellus bibendum sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

                    [panel_image] => http://www.emfit.test/wp-content/uploads/2019/04/bookpanel-sketches-2x.jpg
                )

        )

)

*/
