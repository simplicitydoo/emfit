<?php
	
# PROGRAMS PANEL

# content
$panel = get_field('settings_programs_panel', 'options');

$topline = $panel['panel_title']['top_line'];
$bottomline = $panel['panel_title']['bottom_line'];
$hilite = $panel['panel_title']['highlight_color'];

if ($panel['panel_title']['bold_text'] == 'top') {
	$topline = '<span class="' . $hilite . ' large">'. $topline . '</span>';
} else {
	$bottomline = '<span class="' . $hilite . ' large">'. $bottomline . '</span>';
}

?>

<section class="panel-product programs">
<div class="podwrap bgcover"></div>
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace">

<div class="imagewrap bgcontain"></div>

<div class="textwrap">

<div class="kicker textuc textpad">
<?= $panel['kicker'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<?= $topline ?><br>
<?= $bottomline ?>
</div>

<div class="blurb">
<?= $panel['panel_blurb'] ?>
</div>

</div>

<div class="buttonwrap textpad">
<a href="<?= $panel['page_link'] ?>"><button class="featurebutton btgreen"><?= $panel['button_label'] ?></button></a>
</div>
	
</div>

</div>

</div>
</section>
