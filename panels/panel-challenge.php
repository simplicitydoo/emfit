<?php
	
# CHALLENGE PANEL

# content
$panel = get_field('settings_challenge_panel', 'options');
$title = $panel['panel_title'];
$title = str_replace('<span>', '<span class="emorange">', $title);

?>

<section class="panel-challenge text-center">
<div class="bgwrap height100">
<div class="container height100">

<div class="livewrap height100 flex">

<div class="title">
<?= $title ?>
</div>

<div class="blurb">
<?= $panel['panel_blurb'] ?>
</div>

<div class="buttonwrap">
<a href="<?= $panel['challenge_product'] ?>"><button class="featurebutton btgreen"><?= $panel['button_label'] ?></button></a>
</div>	

</div>	

</div>
</div>
</section>
