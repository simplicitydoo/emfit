<?php
	
# GYM PANEL

?>

<section class="panel-gym bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-panel-gym-bg-2x.jpg);">
<div class="scrim height100">
<div class="container height100">

<div class="textwrap height100 flex">

<div class="title">
My <span>Location</span>
</div>

<div class="subtitle">
Platform Strength
</div>

<div class="blurb">
Platform is a place to grow as an individual among a community. We’re about the barbell and all it’s benefits, lifting heavy shit, and putting it back down…
</div>

<div class="buttonwrap">
<a href="https://www.platformstrength.com/" target="_blank"><button class="featurebutton btgreen">Check Out My Gym</button></a>
</div>

</div>

</div>
</div>
</section>
