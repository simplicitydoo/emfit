<?php
	
# QUIZ PANEL

# content
$quiz = get_field('settings_quizpanel', 'options');

?>

<section class="panel-quiz text-center">
<div class="container">

<div class="title">
<?= $quiz['panel_title'] ?>
</div>

<div class="blurb">
<?= $quiz['blurb'] ?>
</div>

<div class="buttonwrap">
<a href="<?= $quiz['button_url'] ?>" target="_blank"><button class="featurebutton btgreen"><?= $quiz['button_label'] ?></button></a>
</div>	

</div>
</section>
