<?php
	
# CONTACT FORM

# nonce
$nonce = wp_create_nonce(FORM_CONTACT_NONCE);

?>

<form class="general-form" data-nonce="<?= $nonce ?>" id="emform">

<div class="instruction">
Enter Your Info Below *	
</div>	

<div class="field">
<input type="text" name="name" placeholder="Your Name" id="emform-name">
</div>	

<div class="field">
<input type="text" name="company" placeholder="Company" id="emform-company">
</div>	

<div class="field">
<input type="email" name="email" class="emformtext emformemail" placeholder="Email *" id="emform-email">
</div>	

<div class="field">
<textarea name="message" class="emformtext" placeholder="Your Message *" maxlength="400" id="emform-message"></textarea>
</div>	

<div class="buttonwrap" id="emform-buttonwrap">
<button class="featurebutton btgreen disabled" id="emform-submit">Submit Form</button>
</div>	

<div class="response flex text-center" id="emform-response" style="display:none;"></div>	

</form>
