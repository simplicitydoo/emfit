<?php
	
# SUCCESS PANEL

# content
$entries = success_all();

if (!empty($entries)) {
?>
<section class="panel-success single">

<div class="panelhead text-center">
<div class="container">

<div class="paneltitle emgreen textlc">
<span>Success</span> Stories
</div>

<?php if (ENV_SERVER == 'bugs') { ?>
<div class="paneltabs textuc">
<a class="successtab" href="#">All</a>
<a class="successtab" href="#">Training</a>
<a class="successtab" href="#">Exercise</a>
<a class="successtab" href="#">Nutrition</a>
</div>
<?php } ?>

</div>
</div>


<div class="owl-carousel owl-theme carousel" id="success-carousel">

<?php foreach ($entries as $key => $entry) { ?>
<div class="successentry withnav success-<?= $entry['type'] ?>">

<div class="imagewrap bgcover" style="background-image:url(<?= $entry['after'] ?>);"></div>

<div class="textwrap flex">

<?php if (!empty($entry['kicker'])) { ?>
<div class="kicker textuc">
<?= $entry['kicker'] ?>
</div>
<?php } ?>

<div class="title">
<?= $entry['displayname'] ?>
</div>

<div class="excerpt">
<?= $entry['excerpt'] ?>
</div>

<div class="actionwrap">

<div class="buttonwrap">
<a href="/success-stories/"><button class="featurebutton btbrown">View All Success Stories</button></a>
</div>

<div class="navwrap flex">
<div class="arrowwrap">
<a class="arrow arrowleft successnav successprev" href="#">Back</a>
<a class="arrow arrowright successnav successnext" href="#">Next</a>
</div>
</div>

</div>

</div>

</div>
<?php } ?>


</div>

</section>
<?php } ?>


<?php
/*
<section class="panel-success">

<div class="paneltitle">
Success Stories
</div>

<div class="paneltabs">
<a href="#">All</a>
<a href="#">Training</a>
<a href="#">Exercise</a>
<a href="#">Nutrition</a>
</div>

<div class="entries">

<div class="entry">
<div class="container">

<div class="textwrap">
<div class="livewrap">

<div class="kicker">
xxx
</div>

<div class="content">
xxx
</div>

<div class="actionwrap">

<div class="buttonwrap">
<a href="#"><button class="xxx">View All the Success Stories</button></a>
</div>

<div class="navwrap">
<a class="nav navleft" href="#">Back</a>
<a class="nav navright" href="#">Next</a>
</div>

</div>

</div>
</div>

</div>
</div>
	
</div>

</section>
*/
