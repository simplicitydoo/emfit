<?php
	
# EMFIT PRODUCT / STANDARD

# get product data
$product = products_page();

# convenience
$producttype = $product['producttype'];
$productname = $product['productname'];
$productblurb = $product['productblurb'];
$productid = $product['productid'];
$currentid = $product['currentid'];
$images = $product['images'];
$variants = $product['variants'];
$options = $product['options'];
$faq = $product['faq'];
$unavailable = $product['unavailable'];
$jsonoptions = $product['jsonoptions'];
$related = $product['related'];

# header
get_header();

?>

<?php # SUBHERO ?>

<div class="product-subhero">
<div class="container height100">
<div class="row height100">
<div class="col-sm-12 text-center flex height100">
<div class="blurbwrap">
if you have any questions, just <a href="/contact/">contact us</a> or visit the <a href="/faq/">FAQ page</a>
</div>
</div>
</div>
</div>
</div>


<?php # MAIN ?>

<section class="product-main">
<div class="container">

<div class="productwrap flex flexrow flexspace">


<?php # IMAGES ?>

<div class="imagecol">

<?php
# program
if ($producttype == 'program') {

	$subimages = $images['subimages'];
	$display = $subimages['display_style'];
	$pair = $subimages['images'];

?>
<div class="imagewrap <?= $producttype ?>">

<?php if (!empty($images['main_image'])) { ?>
<div class="mainimage bgcover" style="background-image:url(<?= $images['main_image'] ?>);"></div>
<?php } ?>
	
<div class="subimages flex flexrow flexspace">
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?= $pair['left_image'] ?>);"></div>
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?= $pair['right_image'] ?>);"></div>
</div>	

</div>	
<?php } ?>


<?php
# supplement
if ( ($producttype == 'supplement') && (!empty($images)) ) {
?>
<div class="imagewrap <?= $producttype ?>">

<?php if (!empty($images['main_image'])) { ?>
<div class="mainimage bgcontain" style="background-image:url(<?= $images['main_image'] ?>);"></div>
<?php } if (!empty($images['detail_image'])) { ?>
<a href="<?= $images['detail_image'] ?>" class="subimage" data-lity>
<img src="<?= $images['detail_image'] ?>" alt="Click to Zoom">
</a>

<?php } ?>

</div>	
<?php } ?>


<?php
# swag
if ($producttype == 'swag') {

	$display = $images['display_style'];
	$pairs = $images['images'];

	if ( (!empty($pairs[0]['left_image'])) && (!empty($pairs[0]['right_image'])) ) {
?>		
<div class="imagewrap <?= $producttype ?>">

<?php foreach ($pairs as $pair => $value) { ?>
<div class="subimages flex flexrow flexspace">
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?=  $value['left_image'] ?>);"></div>
<div class="subimage <?= $display ?> bgcover" style="background-image:url(<?=  $value['right_image'] ?>);"></div>
</div>
<?php } ?>

</div>
<?php
	}
}
?>


<?php
# other
if ( ($producttype == 'other') && (!empty($images)) ) {
?>
<div class="imagewrap <?= $producttype ?>">
<div class="mainimage bgcontain" style="background-image:url(<?= $images['main_image'] ?>);"></div>
</div>	
<?php } ?>


<?php # PROGRAM HILITES ?>

<?php
if ($producttype == 'program') {
	foreach ($variants as $variantid => $variant) {
		if (!empty($variant['hilites'])) {
			$hilites = $variant['hilites'];
			$style = 'display:none;';
			if ($variantid == $currentid) { $style = ''; }
?>
<div class="hilites" id="hilite-<?= $variantid ?>" style="<?= $style ?>">

<div class="titlewrap">
<div class="name textlc">
<?= $productname ?>
</div>
<div class="version">
</div>
</div>

<div class="hilitetable">

<div class="tablerow">
<div class="tablecell label textuc text-right">
Difficulty:
</div>
<div class="tablecell text">
<?= ucfirst($hilites['hilite_difficulty']) ?>
</div>
</div>

<div class="tablerow">
<div class="tablecell label textuc text-right">
Length:
</div>
<div class="tablecell text">
<?php if ($hilites['hilite_length'] == 'less60') { ?>
Less than 60 minutes each day
<?php } else if ($hilites['hilite_length'] == 'more60') { ?>
60+ minutes each day
<?php } ?>
</div>
</div>

<?php
if ( (isset($hilites['hilite_workouts'])) && ($hilites['hilite_workouts'] != 'none') ) {
	$wnumber = 3;
	if ($hilites['hilite_workouts'] == 'four') { $wnumber = 4; }
?>
<div class="tablerow">
<div class="tablecell label textuc text-right">
Workouts:
</div>
<div class="tablecell text">
<?= $wnumber ?> workouts per week
</div>
</div>
<?php } ?>

<div class="tablerow">
<div class="tablecell label textuc text-right">
Focus:
</div>
<div class="tablecell text">
<?php if ($hilites['hilite_focus'] == 'isolated') { ?>
Isolated Muscle Groups
<?php } else if ($hilites['hilite_focus'] == 'full') { ?>
Full Body Workouts
<?php } ?>
</div>
</div>

<div class="tablerow">
<div class="tablecell label textuc text-right">
Equipment:
</div>
<div class="tablecell text">
<?php if ($hilites['hilite_equipment'] == 'none') { ?>
<a href="<?= LINK_EMPACK ?>" target="_blank">EmPack</a>/Dumbbells Only or No Equipment
<?php } else if ($hilites['hilite_equipment'] == 'dumbbells') { ?>
<a href="<?= LINK_EMPACK ?>" target="_blank">EmPack</a>/Dumbbells and some gym access
<?php } else if ($hilites['hilite_equipment'] == 'gym') { ?>
Full Gym Access (Barbell + bumper plates, dumbbells, bands, etc)
<?php } ?>
</div>
</div>

<?php if ( (isset($hilites['hilite_how'])) && (!empty($hilites['hilite_how'])) ) { ?>
<div class="tablerow">
<div class="tablecell label textuc text-right">
How Does It Work?
</div>
<div class="tablecell text">
<?= $hilites['hilite_how'] ?>
</div>
</div>
<?php } ?>

</div>

</div>
<?php
		}
	}
}
?>

</div>


<?php # PRODUCT INFO ?>

<div class="infocol">

<div class="mainprice emgreen">
<?php if ($unavailable) { ?>
Out of Stock
<?php } else { ?>
<span id="mainprice"><?= $variants[$currentid]['price'] ?></span>
<?php } ?>
</div>

<div class="namewrap">
<div class="name">
<?= $productname ?>
</div>
<div class="version">
</div>
</div>

<div class="blurb">
<?= $productblurb ?>
</div>


<?php # FORM ?>

<?php if (!$unavailable) { ?>
<form method="post" action="<?php echo admin_url('admin-post.php') ?>" id="cartform">
<input type="hidden" name="action" value="emfitcart_add">
<input type="hidden" name="productid" value="<?= $productid ?>">
<input type="hidden" name="variantid" id="variantid" value="<?= $currentid ?>">
	
<?php
if (!empty($options)) {
	foreach ($options as $key => $value) {
	
		$optionntitle = 'Select ' . $value['name'];
		$optionid = $value['optionid'];

		$optionprefix = '';
		$optionclass = '';

		if ($value['name'] == 'Version') {
			$optionprefix = 'v0';
			$optionclass = 'textul';
		}

		if ($value['name'] == 'Size') {
			$optionclass = 'sizes';
		}
		
?>
<div class="optionwrap">
<div class="title textuc">
<?= $optionntitle ?>
</div>
<div class="options flex flexrow flexspace flexwrap">
<?php
		foreach ($value['options'] as $okey => $ovalue) {

			# echo "$okey / $ovalue";

			$buttonid = $optionid . '-' . $okey;
			$selected = $disabled = '';

			$label = str_replace('.00 USD', '', $ovalue);

			if ($variants[$currentid][$optionid] == $okey) {
				$selected = 'selected';
				if ($variants[$currentid]['cart']) { $disabled = 'disabled'; }
			}			
?>			
<button type="button" class="featurebutton noshadow btgreen <?= $optionclass ?> <?= $selected ?> <?= $disabled ?>" id="<?= $buttonid ?>">
<?= $label ?>
</button>
<?php } ?>
</div>
</div>
<?php
	}
}	
?>

<?php if ($producttype != 'program') { ?>
<div class="optionwrap">
<div class="title textuc">
Quantity
</div>
<input type="number" name="quantity" min="1" max="<?= $variants[$currentid]['maxquantity'] ?>" step="1" value="1" class="optionamount" id="product-amount">
</div>
<?php } ?>

<div class="actionwrap">

<div class="addbutton">
<a class="pricebutton btgreen flex flexrow flexspace" href="#" id="cartadd">
<div class="label text-center">Add to Cart</div><div class="price" ><span id="btprice"><?= $variants[$currentid]['price'] ?></span></div>
</a>
</div>

<?php
/*
<div class="favwrap">
<a href="#"><button class="favbutton">Favorite</button></a>
</div>
*/
?>

</div>

</form>
<?php } ?>


<?php # FAQ ?>

<?php if (!empty($faq)) { ?>
<div class="faqwrap">

<?php foreach ($faq as $key => $value) { ?>
<div class="faq">

<div class="faqquestion">
<div class="faqtoggle" data-answer="<?= $key ?>"></div>
<?= $value['detail_title'] ?>	
</div>

<div class="faqanswer" id="faqanswer-<?= $key ?>" style="display: none;">
<?php
if ($value['detail_type'] == 'text') { echo $value['detail_text']; }

else if ($value['detail_type'] == 'bullets') {
?>
<ul>
<?php foreach ($value['detail_bullets'] as $bkey => $bvalue) { ?>
<li><?= $bvalue['item'] ?></li>
<?php } ?>
</ul>
<?php } ?>
</div>

</div>
<?php } ?>

</div>
<?php } ?>

</div>

</div>
</div>
</section>


<?php # QUIZ / OFFER ?>

<?php
if ($producttype == 'program') {
	get_template_part('panels/panel-quiz');
	get_template_part('panels/panel-introoffer');
}
?>


<?php # RELATED ?>

<?php if (!empty($related)) { ?>
<section class="product-related">
<div class="container">

<div class="paneltitle text-center">
Other Products You Might Like
</div>

<div class="shop-entries related">
<div class="entries flex flexrow flexspace flexwrap">
<?php
	foreach ($related as $key => $entryid) {
		
		$entry = products_related($entryid);
		if (empty($entry)) { continue; }		

?>
<a href="<?= $entry['url'] ?>" class="entry">
<?php if ($entry['newlabel']) { ?>
<div class="newtag">New!</div>
<?php } ?>
<div class="imagewrap bgcover" style="background-image:url(<?= $entry['image'] ?>);"></div>
<div class="textwrap flex text-center">
<div class="name">
<?= $entry['name'] ?>
</div>
<?php if (!empty($entry['type'])) { ?>
<div class="type textuc">
<?= $entry['type'] ?>
</div>	
<?php } ?>
<div class="price textuc">
<?= $entry['price'] ?>
</div>
</div>
</a>
<?php } ?>
</div>
</div>

</div>
</section>
<?php } ?>


<?php # JSON ?>

<script type="application/json" id="variantjson">
<?php echo json_encode($variants); ?>
</script>

<script type="application/json" id="optionsjson">
<?php echo json_encode($jsonoptions); ?>
</script>

<?php get_footer(); ?>
