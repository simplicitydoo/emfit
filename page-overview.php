<?php
/**
Template name: Program Overview
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'overview';

# content
$callouts = get_field('hero_callouts');
$intro = get_field('introduction');
$hilites = get_field('highlights');
$bullets = get_field('bullet_points');
$info = get_field('additional_info');

# header
get_header();

?>

<?php # HERO ?>

<section class="overview-hero">
<div class="container height100">

<div class="panelwrap height100">
	
<div class="headerwrap flex flexrow flexspace">

<div class="headerleft">

<div class="name textlc empurple">
arms + abs
</div>

<div class="arrowwrap">
<a class="arrow arrowleft" href="#">Go Back</a>
</div>

</div>

<div class="headerright">

<div class="buttonwrap">
<a class="pricebutton btgreen btlarge" href="#">
<div class="label">Get Started Now</div><div class="price">$65</div>
</a>
</div>

</div>

</div>

<div class="livewrap">

<div class="dot dot1"></div>
<div class="dot dot2"></div>
<div class="dot dot3"></div>

<div class="callout callout1"></div>

</div>

<div class="herobutton text-center">
<a class="pricebutton btgreen" href="#">
<div class="label">A Message from Emily</div><div class="arrow arrowright"></div>
</a>
</div>

</div>

</div>
</section>


<?php # NAV ?>

<div class="overview-nav flex">
<div class="container">

<div class="navwrap textuc text-center">
<a class="overviewnavitem" href="#" data-scroll="overview-intro">Introduction</a>
<a class="overviewnavitem" href="#" data-scroll="overview-hilites">Highlights</a>
<a class="overviewnavitem" href="#" data-scroll="overview-details">What You Get + Need</a>
<a class="overviewnavitem" href="#" data-scroll="overview-info">Additional Info</a>
</div>

</div>
</div>


<?php # INTRO ?>

<section class="overview-intro text-center bgpurple" id="overview-intro">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
	
<div class="name">
Arms + Abs
</div>

<div class="title">
Introduction
</div>

<div class="hilite">
<p>Are you ready to get that strong core you have always dreamed of,
that ass to fill out your jeans or the arms that you want to feel proud of??</p>
</div>

<div class="text">
<p>Sure, we love a good six pack of abs, and defined arms are pretty nice too. BUT what is not talked about is the importance of a strong core and strong arms when it comes to being injury free. Shoulder issues? Bad posture? Let's fix it with smart programming! </p>
</div>

</div>
</div>
</div>
</section>


<?php # HILITES ?>

<section class="overview-hilites"  id="overview-hilites">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-10 offset-md-1">

<div class="titlerow">

<div class="name">
Ass + Abs
</div>

<div class="title empurple">
Highlights
</div>

</div>

<div class="hilites hipurple flex flexrow flexspace flexwrap">

<div class="entry">
<div class="label textuc">
DIFFICULTY
</div>
<div class="blurb">
Good for anyone at any level of fitness
</div>
</div>

<div class="entry">
<div class="label textuc">
LENGTH
</div>
<div class="blurb">
6 Weeks from start to finish - workout three times a week.
</div>
</div>

<div class="entry">
<div class="label textuc">
FOCUS
</div>
<div class="blurb">
Accessory Work Isolating Muscle Groups
</div>
</div>

<div class="entry">
<div class="label textuc">
MIN EQUIPMENT
</div>
<div class="blurb">
Minimun required equipment is the EmPack / Dumbells
</div>
</div>

<div class="entry">
<div class="label textuc">
SUGGESTED EQUIP.
</div>
<div class="blurb">
We recommend you use the EmPack / Dumbells and TRX/RINGS
</div>
</div>

<div class="entry">
<div class="label textuc">
FORMAT
</div>
<div class="blurb">
You will be sent PDFs via email, or you can visit the site when you sign up.
</div>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # DETAILS ?>

<section class="overview-details" id="overview-details">

<div class="container">
<div class="row">
<div class="col-sm-12 col-md-10 offset-md-1">

<div class="titlerow text-center">

<div class="name">
Ass + Abs
</div>

<div class="title emorange">
What You Get + Need
</div>

</div>

</div>
</div>
</div>

<div class="rowwrap flex flexrow flexspace">

<div class="selections textlc">
<div class="detailselector flex selected" data-selection="get">What You Get</div>
<div class="detailselector flex" data-selection="need">What You Need</div>
</div>

<div class="details">

<div class="detailset" id="details-get">

<div class="detail">
<strong>3 Workouts a Week</strong> (~45 minutes daily)
</div>

<div class="detail">
90+ <strong>Video Tutorials</strong>
</div>

</div>

<div class="detailset" id="details-need" style="display:none;">

<div class="detail">
A <strong>Private Facebook Group</strong> for my Strength Training Badasses ONLY to build a community, post videos on form, and get feedback on all your questions
</div>

<div class="detail">
Your own account to <strong>log results</strong>
</div>

</div>



</div>

</div>

</section>


<?php # INFO ?>

<section class="overview-info bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-overview-infobg-2x.jpg);" id="overview-info">
<div class="scrim height100">
<div class="container">

<div class="rowwrap flex flexrow flexspace flexalign">

<div class="titlewrap flex">

<div class="name">
Ass + Abs
</div>

<div class="title empurple">
Additional Info
</div>

</div>

<div class="infowrap">

<div class="entry">
<div class="question">
What is the Difference between Version 01 and Version 02?
</div>
<div class="answer">
Version 1 and Version 2 has the same difficulty level, but version 2 programming incorporates new movements. 
</div>
</div>

<div class="entry">
<div class="question">
What do you recommend I start with?
</div>
<div class="answer">
We always recommend starting with version 1 and then moving to version 2. 
</div>
</div>

<div class="entry">
<div class="question">
What’s it like each day?
</div>
<div class="answer">
Each day we provide the workout and videos demonstrating each movement
</div>
</div>

<div class="entry">
<div class="question">
What if I have Questions or need help?
</div>
<div class="answer">
If there are any questions or movement modifications needed the FB Group is a great space to search for those or to ask in! 
</div>
</div>

</div>

</div>

</div>
</div>
</section>


<?php # GET IN TOUCH ?>

<section class="overview-intouch text-center">
<div class="container">

<div class="blurb">
If you need more guidance or have ANY questions please contact us!
</div>

<div class="buttonwrap">
<a href="#"><button class="featurebutton btgreen">Get in Touch Now!</button></a>
</div>

</div>
</section>


<?php # SUCCESS ?>


<section class="panel-success single">

<div class="entry">

<div class="imagewrap bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-success-allison-2x.jpg);"></div>

<div class="textwrap flex">

<div class="kicker textuc">
SUCCESS IN ARMS + ABS VERSION 01
</div>

<div class="title">
Allison
</div>

<div class="excerpt">
<p>I’ve been thinking about writing you all for literally over a year now. I did my first 21 day Superhero Challenge back in August of 2016, a few months after I had my third daughter. I went all in, completely trusting the plan and my life was changed. It went against everything that I previously stood for and believed: counting calories, extended steady cardio sessions, low fat foods, whole grains, and measuring everything before it was consumed.</p>
<p>All of these were awful, yes, but looking back now I realize worst of all I really didn’t like myself. I considered myself the “designated ugly fat friend” to my group of college girlfriends and seriously lacked self-esteem and... self-love (I didn’t even know that was a thing for most of my life)….</p>
</div>

<div class="actionwrap">

<div class="buttonwrap">
<a href="#"><button class="featurebutton btbrown">View All the Success Stories</button></a>
</div>

</div>

</div>

</div>

</section>



<?php # GALLERY ?>


<?php # QUIZ ?>

<?php get_template_part('panels/panel-quiz'); ?>


<?php # RELATED ?>

<section class="overview-related">
<div class="container">

<div class="titlewrap text-center">

<div class="subhead">
Some Suggestions
</div>

<div class="title">
Other Programs You Might Like
</div>

</div>

<div class="programwrap flex flexrow flexspace">

<a href="#" class="programentry">

<div class="header textuc flex">
<div class="flex flexrow flexspace">
<div class="price">
$76
</div>
<div class="favorite">
Favorite
</div>
</div>
</div>

<div class="imagewrap bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-program-thumb1.jpg);"></div>

<div class="content text-center">
	
<div class="type textuc">
Program
</div>

<div class="namewrap empurple">
<div class="name">
Ass + Abs
</div>
</div>

<div class="textwrap">
- eat clean<br>
- become fat adaptive<br>
- gut + adrenal health<br>
- basics of macronutrients<br>
- great home + gym workouts<br>
</div>

</div>

</a>

<a href="#" class="programentry">

<div class="header textuc flex">
<div class="flex flexrow flexspace">
<div class="price">
$76
</div>
<div class="favorite">
Favorite
</div>
</div>
</div>

<div class="imagewrap bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-program-thumb2.jpg);"></div>

<div class="content text-center">
	
<div class="type textuc">
Program
</div>

<div class="namewrap emdarkorange">
<div class="name">
Burn
</div>
</div>

<div class="textwrap">
- eat clean<br>
- become fat adaptive<br>
- gut + adrenal health<br>
- basics of macronutrients<br>
- great home + gym workouts<br>
</div>

</div>

</a>

<a href="#" class="programentry">

<div class="header textuc flex">
<div class="flex flexrow flexspace">
<div class="price">
$76
</div>
<div class="favorite">
Favorite
</div>
</div>
</div>

<div class="imagewrap bgcover" style="background-image:url(<?= TEMPLATE_ASSETS ?>/beta/beta-program-thumb3.jpg);"></div>

<div class="content text-center">
	
<div class="type textuc">
Program
</div>

<div class="namewrap emyellow">
<div class="name">
Busy Bee
</div>
</div>

<div class="textwrap">
- eat clean<br>
- become fat adaptive<br>
- gut + adrenal health<br>
- basics of macronutrients<br>
- great home + gym workouts<br>
</div>

</div>

</a>

</div>

</div>
</section>


<?php # GIFT CARD ?>

<?php get_template_part('panels/panel-giftcard'); ?>


<?php
	
# footer
get_footer();
