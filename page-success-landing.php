<?php
/**
Template name: Success Stories
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'successstories';

# get content
$hero = get_field('hero_panel');
$entries = success_list();
$nonce = wp_create_nonce(PAGE_SUCCESS_NONCE);

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # ENTRIES ?>

<?php if (!empty($entries)) { ?>
<section class="success-entries" data-nonce="<?= $nonce ?>" id="success-nonce">
<div class="container">

<div class="entries flex flexrow flexspace flexwrap">

<?php
	$modal = array();

	foreach ($entries as $key => $value) {
		$modal = $value;
		$bgimage = $value['images']['after'];
		if (empty($bgimage)) { $bgimage = $value['images']['before']; }
		$kicker = '';
		if ($value['type'] != 'other') { $kicker = 'Success in '. $value['type']; } 

?>	
<div class="entry successentry bgcover" data-successid="<?= $value['id'] ?>" style="background-image:url(<?= $bgimage ?>);">
<div class="scrim flex height100">

<div class="kicker textuc">
<?= $kicker ?>
</div>

<div class="name">
<?= $value['name'] ?>
</div>
	
</div>
</div>
<?php } ?>

</div>

</div>
</section>
<?php } ?>


<?php # MODAL ?>

<div class="modal fade success-modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="Success Story">
<div class="modal-dialog" role="document">
<div class="modal-content">

<div class="fetchwrap text-center" id="modal-fetchwrap">
Getting Success Story…
</div>

<div class="livewrap" id="modal-livewrap" style="display:none;">

<div class="closewrap text-right">
<button type="button" class="btn btn-modal textlc closebutton" data-dismiss="modal" aria-label="Close" id="success-close">Close</button>
</div>

<div class="imagewrap" id="success-after">
<img src="" alt="" id="success-after-image">
</div>

<div class="textwrap">

<div class="kicker textuc" id="success-kicker"></div>

<div class="name" id="success-name"></div>

<div class="story" id="success-story"></div>

</div>

<div class="imagewrap" id="success-before">
<img src="" alt="" id="success-before-image">
</div>

</div>

</div>
</div>
</div>


<?php
	
# footer
get_footer();

/*
Array
(
    [0] => Array
        (
            [id] => 808
            [type] => excercise
            [images] => Array
                (
                    [before] => http://www.emfit.test/wp-content/uploads/2019/08/beta-success-adriana.jpg
                    [after] => http://www.emfit.test/wp-content/uploads/2019/08/beta-success-allison.png
                )

            [name] => Allison
            [text] => <p>I’ve been thinking about writing you all for literally over a year now. I did my first 21 day Superhero Challenge back in August of 2016, a few months after I had my third daughter. I went all in, completely trusting the plan and my life was changed. It went against everything that I previously stood for and believed: counting calories, extended steady cardio sessions, low fat foods, whole grains, and measuring everything before it was consumed.</p>
<p>All of these were awful, yes, but looking back now I realize worst of all I really didn’t like myself. I considered myself the “designated ugly fat friend” to my group of college girlfriends and seriously lacked self-esteem and self-love (I didn’t even know that was a thing for most of my life). I had not one, but two long term boyfriends who cheated on me during high school and college, and I was dealing with some serious daddy issues all of my 20’s. Clearly I had trust issues, but in some sick and twisted way I thought that was what I deserved and that I wasn’t worthy of being happy or loved. So, I found comfort in food.</p>
<p>I met my husband and things go immensely better. He made me feel loved and valued, but still I wasn’t happy with myself (and I definitely didn’t know how to nourish my body or exercise). All of this changed with that first 21 day Challenge. I lost some weight and quite a few inches (which was awesome), but I gained SO MUCH more…. I gained knowledge on so many topics I was previously clueless about (adrenals, eating fats, supplements, flower essences, etc), I gained a community of positive and encouraging people, I gained confidence in the kitchen and cooking, I gained physical strength, but I believe most importantly I gained self-freaking-love. This process has taught me how to be kind to myself, to positive self-talk, to believe in myself and to not give a FUH about what others think of me. That self-loathing, “designated fat ugly” girl is dead, replaced by the best version of me I could envision. Since then I’ve done multiple 21 Day Challenges, the Build v1 twice and I’m currently crushing Build v2, feeling strong as hell.</p>
<p>Because of your help, I’m thriving like I never anticipated I could. As a 33 year old mother to three girls working as a Nurse Practitioner, I’m in the best shape of my life. My girls see me working hard in the gym and they WANT to be strong like their mom. (And speaking of my kids, side note is that during my first challenge we inadvertently discovered my middle child has a milk allergy that resulted in allergies and eczema. I made the family dairy free for the challenge and her skin cleared up amazingly fast!) Please advertise next time you are seeking out more EMbassadors – that is a dream of mine.</p>
<p>From the bottom of my heart, thank you. Keep doing your amazing, life-changing work.</p>
<p>Allison F.<br />
Hebron, KY</p>

            [url] => http://www.emfit.test/success/allison/
        )

)

*/
