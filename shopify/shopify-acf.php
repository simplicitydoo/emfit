<?php
	
# SHOPIFY ACF

# NON-EDITABLE FIELDS

# shopify details
add_filter('acf/load_field/name=shopify_product_name', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_id', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_type', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_format', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_updated', 'acf_disable_field');

# variants
add_filter('acf/load_field/name=shopify_variant_name', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_variant_id', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_variant_updated', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_price', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_compare_price', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_option_1', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_option_2', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_option_3', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_position', 'acf_disable_field');

# options
add_filter('acf/load_field/name=shopify_product_option_1', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_option_2', 'acf_disable_field');
add_filter('acf/load_field/name=shopify_product_option_3', 'acf_disable_field');

# disable field
function acf_disable_field ($field) {
	$field['disabled'] = 1;
	return $field;
}
