<?php
	
# WEBHOOK / INVENTORY
# https://emfit.lemonadeisgreat.com/z/inventory/

# load entry
$entry = $GLOBALS['emfit']['decode'];

# have a peek
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-inventory-' . time() . '.txt', var_export($entry, TRUE));
}

else if (ENV_SERVER == 'server') {
	file_put_contents('/z/logs/emfit/webhook-inventory-' . time() . '.txt', var_export($entry, TRUE));
}

# test only
if ( (isset($_SERVER['HTTP_X_SHOPIFY_TEST'])) && ($_SERVER['HTTP_X_SHOPIFY_TEST'] == true) ) {
	exit;
}

# process entry
shopifyproducts_inventory($entry);
