<?php
	
# SHOPIFY PRODUCTS

/*
"published_at": "2007-12-31T19:00:00-05:00"
The date and time (ISO 8601 format) when the product was published. Can be set to null to unpublish the product from the Online Store channel.
*/

# PROCESS PRODUCTS

function shopifyproducts_process ($mode = 'live') {

	# debug / live
	$products = shopifyproducts_compile($mode);

	$count = 0;
	
	foreach ($products as $key => $product) {
		$count++;
		# if ($count > 3) { break; }
		shopifyproducts_update($product, $mode);
	}

	# echo "Done!";

	# update id list
	shopifyproducts_idlist();

	# back at ya
	return $count;
	
}


# COMPILE LIST

function shopifyproducts_compile ($mode = 'live') {

	# init args
	$args = array();

	# last fetch
	if ($mode == 'live') {
		$lastfetch = get_option('shopify_product_fetch');
		if ($lastfetch) {
			$args = array(
				'condition' => 'updated_at_min',
				'setting' => $lastfetch,
			);
		}
	}

	# api
	$data = shopify_api('products_all', $args);

	# print_r($data);
	# exit;
		
	# oops
	if ( (empty($data)) || (isset($data['error'])) ) { return; }

	# set current time
	update_option('shopify_product_fetch', date('c'));

	# nothing new
	if (empty($data['products'])) { return; }

	# init products
	$products = array();

	# process
	foreach ($data['products'] as $key => $entry) {
		$product = shopifyproducts_entry($entry);
		$products[] = $product;
	}

	# print_r($products);
	# exit;
	# echo var_export($products);

	return $products;
	
}


# CREATE ENTRY FROM API DATA

function shopifyproducts_entry ($entry) {

	$type = strtolower($entry['product_type']);
	$type = str_replace(' ', '', $type);

	# init
	$product = array(
		'id' => $entry['id'],
		'title' => $entry['title'],
		'type' => $type,
		'format' => 'variants',
		'published' => $entry['published_scope'],
		'updated' => $entry['updated_at'],
	);

	$variants = array();

	# single product
	if ( (count($entry['variants']) == 1) && ($entry['variants'][0]['title'] == 'Default Title') ) {
		
		$value = $entry['variants'][0];
		$compare = $value['compare_at_price'];
		if (is_null($compare)) { $compare = ''; }

		$inventorypolicy = 'none';
		if (!empty($value['inventory_management'])) { $inventorypolicy = $value['inventory_policy']; }	

		$single = array(
			'id' => $value['id'],
			'price' => $value['price'],
			'compare' => $compare,
			'updated' => $value['updated_at'],
			'inventory_item_id' => $value['inventory_item_id'],
			'inventory_policy' => $inventorypolicy,
			'available' => $value['inventory_quantity'],
		);
		
		# $product['single'] = $single;
		$variants[] = $single;
		$product['variants'] = $variants;
		$product['format'] = 'single';

	}
	
	# variants
	else {
		
		foreach ($entry['variants'] as $vkey => $value) {

			$compare = $value['compare_at_price'];
			if (is_null($compare)) { $compare = ''; }

			$inventorypolicy = 'none';
			if (!empty($value['inventory_management'])) { $inventorypolicy = $value['inventory_policy']; }	

			$variant = array(
				'id' => $value['id'],
				'title' => $value['title'],
				'price' => $value['price'],
				'compare' => $compare,
				'position' => $value['position'],
				'updated' => $value['updated_at'],
				'inventory_item_id' => $value['inventory_item_id'],
				'inventory_policy' => $inventorypolicy,
				'available' => $value['inventory_quantity'],
				'option1' => '',
				'option2' => '',
				'option3' => '',
			);

			if (!empty($value['option1'])) {
				$variant['option1'] = $value['option1'];
			}

			if (!empty($value['option2'])) {
				$variant['option2'] = $value['option2'];
			}

			if (!empty($value['option3'])) {
				$variant['option3'] = $value['option3'];
			}

			$variants[] = $variant;
			
		}

		$product['variants'] = $variants;

		# options
		$options = array();

		foreach ($entry['options'] as $okey => $value) {
			$slot = 'option' . $value['position'];
			$options[$slot] = $value['name'];
		}

		$product['options'] = $options;
		
	}

	# back at ya
	return $product;
	
}


# UPDATE PRODUCT

function shopifyproducts_update ($product, $mode = 'live') {

	# echo 'mode: ' . $mode . '<br>';

	# find existing product
	$args = array(
		'post_type' => 'emfit_product',
		'post_status' => 'any',
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
				'key' => 'shopify_details_shopify_product_settings_shopify_product_id',
				'value' => $product['id'],
				'compare' => 'LIKE',
			),
		),
	);

	$result = new WP_Query($args);
	# print_r($result);

	# if ($result->post_count > 0) { echo $result->posts[0]->post_title . "\n"; }
	# else { echo "New product\n"; }
	# continue;

	$postid = 0;
	$newpost = false;
	
	# found
	if ($result->post_count > 0) {

		# get id
		$postid = $result->posts[0]->ID;

		# check update
		$details = get_field('shopify_details', $postid);
		$updated = $details['shopify_product_settings']['shopify_product_updated'];
		$newupdate = $product['updated'];
		if ( ($mode == 'live') && ($updated == $newupdate) ) { return false; }
		
	}

	# new
	else {

		# create product page
		$productpage = array(
			'post_title' => $product['title'],
			# 'post_name' => $slug,
			'post_type' => 'emfit_product',
			'post_status' => 'draft',
		);
	
		$postid = wp_insert_post($productpage);
		$newpost = true;
		
	}

	# echo 'postid: ' . $postid . '<br>';
	
	# oops
	if (!$postid) { return false; }
	
	# update main details
	$details = array(
		'shopify_product_name' => $product['title'],
		'shopify_product_settings' => array(
			'shopify_product_id' => $product['id'],
			'shopify_product_type' => $product['type'],
			'shopify_product_format' => $product['format'],
			'shopify_product_updated' => $product['updated'],
		),
	);
	
	update_field('shopify_details', $details, $postid);

	# variant arrays
	$currentvariants = array();
	$newvariantids = array();
	
	# get existing variants
	if (!$newpost) {
		$variants = get_field('shopify_product_variants', $postid);
		if (!empty($variants)) {
			foreach ($variants as $vkey => $variant) {
				$id = $variant['shopify_variant']['shopify_variant_ids']['shopify_variant_id'];
				$updated = $variant['shopify_variant']['shopify_variant_ids']['shopify_variant_updated'];
				$rowid = $vkey + 1;
				$currentvariants[$id] = array(
					'rowid' => $rowid,
					'updated' => $updated,
				);
			}
		}
	}

	# update variants
	foreach ($product['variants'] as $vkey => $variant) {

		$id = $variant['id'];
		$newvariantids[$id] = 1;

		# check update
		if (isset($currentvariants[$id])) {
			$updated = $currentvariants[$id]['updated'];
			$newupdate = $variant['updated'];
			if ( ($mode == 'live') && ($updated == $newupdate) ) { continue; }
		}
		
		$name = '[default]';
		if (isset($variant['title'])) { $name = $variant['title']; }
		
		$details = array(
			'shopify_variant' => array(
				'shopify_variant_name' => $name,
				'shopify_variant_ids' => array(
					'shopify_variant_id' => $id,
					'shopify_variant_updated' => $variant['updated'],
				),
				'shopify_variant_inventory' => array(
					'inventory_item_id' => $variant['inventory_item_id'],
					'inventory_policy' => $variant['inventory_policy'],
					'available' => $variant['available'],
				),
				'shopify_variant_details' => array(
					'shopify_price' => $variant['price'],
					'shopify_compare_price' => $variant['compare'],
					'shopify_option_1' => '',
					'shopify_option_2' => '',
					'shopify_option_3' => '',
					'shopify_option_3' => '',
					'shopify_position' => '',
				),
			),
		);

		if (isset($variant['option1'])) {
			$details['shopify_variant']['shopify_variant_details']['shopify_option_1'] = $variant['option1'];
		}
		
		if (isset($variant['option2'])) {
			$details['shopify_variant']['shopify_variant_details']['shopify_option_2'] = $variant['option2'];
		}

		if (isset($variant['option3'])) {
			$details['shopify_variant']['shopify_variant_details']['shopify_option_3'] = $variant['option3'];
		}

		if (isset($variant['position'])) {
			$details['shopify_variant']['shopify_variant_details']['shopify_position'] = $variant['position'];
		}

		# existing variant
		if (isset($currentvariants[$id])) {
			$rowid = $currentvariants[$id]['rowid'];
			update_row('shopify_product_variants', $rowid, $details, $postid);
		}

		# new variant
		else {
			add_row('shopify_product_variants', $details, $postid);
		}
		
	}

	# update options
	$options = array(
		'shopify_product_option_1' => '',
		'shopify_product_option_2' => '',
		'shopify_product_option_3' => '',
	);

	if (isset($product['options']['option1'])) {
		$options['shopify_product_option_1'] = $product['options']['option1'];
	}

	if (isset($product['options']['option2'])) {
		$options['shopify_product_option_2'] = $product['options']['option2'];
	}

	if (isset($product['options']['option3'])) {
		$options['shopify_product_option_3'] = $product['options']['option3'];
	}

	update_field('shopify_product_options', $options, $postid);

	# delete old variants (last)
	foreach ($currentvariants as $variantid => $value) {
		
		# still in use
		if (isset($newvariantids[$variantid])) { continue; }
		
		$rowid = $value['rowid'];
		
		# zap it
		delete_row('shopify_product_variants', $rowid, $postid);
		
	}
			
}


# DELETE PRODUCT
# changes to draft

function shopifyproducts_delete ($productid = 0) {

	# safety first
	if ($productid == 0) { return; }

	# find existing product
	$args = array(
		'post_type' => 'emfit_product',
		'post_status' => 'any',
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
				'key' => 'shopify_details_shopify_product_settings_shopify_product_id',
				'value' => $productid,
				'compare' => 'LIKE',
			),
		),
	);

	$result = new WP_Query($args);

	# nothing
	if ($result->post_count == 0) { return; }
	
	# post id
	$postid = $result->posts[0]->ID;

	# update args
	$args = array(
		'ID' => $postid,
		'post_status' => 'draft',
	);

	# fire!
	wp_update_post($args);

	# update id list
	shopifyproducts_idlist();

}


# UPDATE INVENTORY

function shopifyproducts_inventory ($data = array()) {

	# safety first
	if ( (!isset($data['inventory_item_id'])) || (!isset($data['available'])) ) { return; }

	# convenience
	$inventoryid = $data['inventory_item_id'];
	$available = $data['available'];

	# load id list
	$idlist = shopifyproducts_idlist_load();

	# find the fish
	$productid = 0;
	$variantid = 0;
	
	foreach ($idlist as $shopifyid => $value) {
		foreach ($value['variants'] as $vkey => $vvalue) {
			if ($vvalue['inventoryid'] != $inventoryid) { continue; }
			$productid = $value['postid'];
			$variantid = $vkey;
			break;
		}
	}

	# nobody home
	if (!$productid) { return; }	

	# get variants
	$variants = get_field('shopify_product_variants', $productid);
	if (empty($variants)) { return; }

	# init add
	$rowid = 0;
	$details = array();

	foreach ($variants as $key => $value) {

		$entry = $value['shopify_variant'];
		$itemid = $entry['shopify_variant_inventory']['inventory_item_id'];

		if ($itemid != $inventoryid) { continue; }

		$rowid = $key + 1;

		$details = $value;
		$details['shopify_variant']	['shopify_variant_inventory']['available'] = $available;

		break;
		
	}

	# shot a blank;
	if (empty($details)) { return; }

	# fire!
	update_row('shopify_product_variants', $rowid, $details, $productid);

	# back at ya
	return true;
	
}


# PRODUCT / PROGRAM ID LIST

# compile
function shopifyproducts_idlist() {

	# find published products
	$args = array(
		'post_type' => 'emfit_product',
		'post_status' => 'publish,private,draft',
		'posts_per_page' => -1,
	);

	$posts = get_pages($args);
	
	if (empty($posts)) { return; }

	$products = array();
	$programs = array();

	foreach ($posts as $key => $post) {
		
		$postid = $post->ID;
		
		# product
		$product = get_fields($postid);
		# print_r($product);
		# exit;
		
		$details = $product['shopify_details'];
		$settings = $product['shopify_details']['shopify_product_settings'];
		$productvariants = $product['shopify_product_variants'];
		
		$productid = $settings['shopify_product_id'];
		
		$variants = array();
		
		if (!empty($productvariants)) {
			foreach ($productvariants as $key => $value) {
			
				$variant = $value['shopify_variant'];
				$variantdetails = $variant['shopify_variant_details'];
				$variantid = $variant['shopify_variant_ids']['shopify_variant_id'];
				$inventoryid = $variant['shopify_variant_inventory']['inventory_item_id'];
	
				$programid = '';
				if (isset($variant['exercise_program'])) {
					$programid = $variant['exercise_program'];
					# echo $programid . '<br>';
				}
	
				$variants[$variantid] = array(
					'name' => $variant['shopify_variant_name'],
					'price' => $variantdetails['shopify_price'],
					'inventoryid' => $inventoryid,
					'programid' => $programid,
				);
	
				if (!empty($programid)) {
					$programs[$programid] = array(
						'shopifyid' => $productid,
						'variantid' => $variantid,
					);
				}
	
			}				
		}
		
		$products[$productid] = array(
			'name' => $details['shopify_product_name'],
			'url' => get_permalink($postid),
			'postid' => $postid,
			'status' => $post->post_status,
			'type' => $settings['shopify_product_type'],
			'format' => $settings['shopify_product_format'],
			'variants' => $variants,
		);
		
	}

	# print_r($products);
	# print_r($programs);

	# convert lists
	$jsonproducts = json_encode($products);
	$jsonprograms = json_encode($programs);

	# store as option
	update_option('shopify_idlist', $jsonproducts, 'no');
	update_option('program_idlist', $jsonprograms, 'no');

}

# load products
function shopifyproducts_idlist_load ($useglobal = false) {
	if (isset($GLOBALS['emfit']['idlist'])) {
		return $GLOBALS['emfit']['idlist'];
	}
	$json = get_option('shopify_idlist');
	$idlist = json_decode($json, true);
	if (empty($idlist)) {
		shopifyproducts_idlist();
		$json = get_option('shopify_idlist');
		$idlist = json_decode($json, true);
	}
	if ($useglobal) { $GLOBALS['emfit']['idlist'] = $idlist; }
	return $idlist;
}

# load programs
function shopifyproducts_programlist_load ($useglobal = false) {
	if (isset($GLOBALS['emfit']['programlist'])) {
		return $GLOBALS['emfit']['programlist'];
	}
	$json = get_option('program_idlist');
	$idlist = json_decode($json, true);
	if (empty($idlist)) {
		shopifyproducts_idlist();
		$json = get_option('program_idlist');
		$idlist = json_decode($json, true);
	}
	if ($useglobal) { $GLOBALS['emfit']['programlist'] = $idlist; }
	return $idlist;
}
