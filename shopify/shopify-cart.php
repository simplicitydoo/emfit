<?php
	
# SHOPIFY CART


# VALIDATE REQUEST

function shopifycart_validate() {

	# anybody home?
	if ( (!isset($_POST['productid'])) || (!isset($_POST['variantid'])) || (empty($_POST['productid'])) || (empty($_POST['variantid'])) ) {
		wp_redirect(site_url('/'));	
		exit;
	}

	# let's double-check
	if ( (!ctype_digit($_POST['productid'])) || (!ctype_digit($_POST['variantid'])) ) {
		wp_redirect(site_url('/'));	
		exit;
	}

	# quantity
	$quantity = 1;
	if ( (isset($_POST['quantity'])) && (ctype_digit($_POST['quantity'])) ) {
		$quantity = $_POST['quantity'];
		if ($quantity < 1) { $quantity = 1; }
	}

	# compile entry
	$entry = array(
		'productid' => $_POST['productid'],
		'variantid' => $_POST['variantid'],
		'comboid' => $_POST['productid'] . ':' . $_POST['variantid'],
		'quantity' => $quantity,
	);

	# back at ya
	return $entry;

}


# ADD TO CART

add_action('admin_post_emfitcart_add', 'shopifycart_add');
add_action('admin_post_nopriv_emfitcart_add', 'shopifycart_add');

function shopifycart_add() {

	# validate entry
	$entry = shopifycart_validate();

	# load cart
	$cart = shopifycart_load();
	
	# update cart
	$comboid = $entry['comboid'];
	$cart['contents'][$comboid] = $entry['quantity'];
	
	# save cart
	shopifycart_save($cart);

	# bye!
	wp_redirect(site_url(EMFIT_CART));	
	exit;

}


# UPDATE QUANTITY

define('CART_QUANTITY_NONCE', 'cartquantity');

add_action('wp_ajax_emfitcart_quantity', 'shopifycart_quantity');
add_action('wp_ajax_nopriv_emfitcart_quantity', 'shopifycart_quantity');

function shopifycart_quantity() {

	# check nonce
	check_ajax_referer(CART_QUANTITY_NONCE, 'security');

	# validate
	if ( (!isset($_POST['productid'])) || (!isset($_POST['variantid'])) || (!isset($_POST['quantity'])) ) { die(); }
	if ( (!ctype_digit($_POST['productid'])) || (!ctype_digit($_POST['variantid'])) || (!ctype_digit($_POST['quantity'])) ) { die(); }

	# convenience
	$productid = trim($_POST['productid']);
	$variantid = trim($_POST['variantid']);
	$quantity = trim($_POST['quantity']);
	$comboid = $productid . ':' . $variantid;

	# load cart
	$cart = shopifycart_load();
	
	# update cart
	$cart['contents'][$comboid] = $quantity;
	
	# save cart
	shopifycart_save($cart);

	# load id list
	$idlist = shopifyproducts_idlist_load();

	# recalculate subtotal
	$subtotal = 0;

	foreach ($cart['contents'] as $comboid => $value) {

		list($shopifyid, $variantid) = explode(':', $comboid);
		if (!isset($idlist[$shopifyid]['variants'][$variantid])) { continue; }
		if ($idlist[$shopifyid]['status'] != 'publish') { continue; }

		$product = $idlist[$shopifyid];
		$variant = $product['variants'][$variantid];
		$price = $variant['price'];

		$quantity = $value;
		$subtotal += ($price * $quantity);

	}

	$subtotal = str_replace('.00', '', $subtotal);

	# init response
	$response = array(
		'status' => 'success',
		'subtotal' => $subtotal,
	);

	# back at ya
	tools_returnjson($response);

}


# CART BUTTON

define('CART_BUTTON_NONCE', 'cartbutton');

add_action('wp_ajax_emfitcart_button', 'shopifycart_button');
add_action('wp_ajax_nopriv_emfitcart_button', 'shopifycart_button');

function shopifycart_button() {

	# check nonce
	check_ajax_referer(CART_BUTTON_NONCE, 'security');

	# validate
	if ( (!isset($_POST['productid'])) || (!isset($_POST['variantid'])) ) { wp_die(); }
	if ( (!ctype_digit($_POST['productid'])) || (!ctype_digit($_POST['variantid'])) ) { wp_die(); }

	# convenience
	$productid = trim($_POST['productid']);
	$variantid = trim($_POST['variantid']);
	$comboid = $productid . ':' . $variantid;

	# load cart
	$cart = shopifycart_load();
	
	# update cart
	$cart['contents'][$comboid] = 1;
	
	# save cart
	shopifycart_save($cart);

	# init response
	$response = array(
		'status' => 'success',
	);

	# back at ya
	tools_returnjson($response);

}


# REMOVE FROM CART

add_action('admin_post_emfitcart_remove', 'shopifycart_remove');
add_action('admin_post_nopriv_emfitcart_remove', 'shopifycart_remove');

function shopifycart_remove() {

	# validate entry
	$entry = shopifycart_validate();

	# load cart
	$cart = shopifycart_load();

	# remove entry
	$comboid = $entry['comboid'];
	unset($cart['contents'][$comboid]);
	
	# save cart
	shopifycart_save($cart);

	# bye!
	wp_redirect(site_url(EMFIT_CART));	
	exit;	

}


# REMOVE ALL FROM CART

add_action('admin_post_emfitcart_removeall', 'shopifycart_removeall');
add_action('admin_post_nopriv_emfitcart_removeall', 'shopifycart_removeall');

function shopifycart_removeall() {

	# load cart
	$cart = shopifycart_load();

	# remove cart
	if ($cart['id'] != 0) {
		shopifycart_delete($cart['id']);
	}
	
	# bye!
	wp_redirect(site_url(EMFIT_CART));	
	exit;	

}


# CHECKOUT
# (logged-in if program)

add_action('admin_post_emfitcart_checkout', 'shopifycart_checkout');
add_action('admin_post_nopriv_emfitcart_checkout', 'shopifycart_checkout');

function shopifycart_checkout() {

	# load cart
	$cart = shopifycart_load();

	# check contents
	if (empty($cart['contents'])) {
		wp_redirect(site_url(EMFIT_CART));	
		exit;	
	}

	# init flag
	$requirelogin = false;
	if (is_user_logged_in()) { $requirelogin = true; }
	
	# load id list
	$idlist = shopifyproducts_idlist_load();
	
	# compile line items
	$lineitems = '';
	foreach ($cart['contents'] as $comboid => $value) {
		list($productid, $variantid) = explode(':', $comboid);
		if (!isset($idlist[$productid]['variants'][$variantid])) { continue; }
		if ($idlist[$productid]['status'] != 'publish') { continue; }
		if ( ($idlist[$productid]['type'] == 'strength') || ($idlist[$productid]['type'] == 'challenge') ) {
			$requirelogin = true;
		}
		$base64 = base64_encode("gid://shopify/ProductVariant/$variantid");
		# echo $base64 . "\n";
		$lineitems .= "{variantId: \"$base64\", quantity: $value},";
	}
	$lineitems = rtrim($lineitems, ',');

	# cartid
	$cartid = $cart['id'];

	# init args
	$args = array(
		'lineitems' => $lineitems,
		'cartid' => $cartid,
		'requirelogin' => $requirelogin,
	);
	
	# get user info
	if ($requirelogin) {
		$userid = get_current_user_id();
		$userdata = get_userdata($userid);
		$args['userid'] = $userid;
		$args['email'] = $userdata->user_email;
	}

	# print_r($args);
		
	# create checkout
	$checkout = shopify_storefront('checkout', $args);

	# print_r($checkout);
	# exit;

	# oops
	if (isset($checkout['error'])) {
		wp_redirect(site_url(EMFIT_CART));	
		exit;	
	}

	# still oops
	if (!isset($checkout['data']['checkoutCreate']['checkout'])) {
		wp_redirect(site_url(EMFIT_CART));	
		exit;	
	}

	# grab...
	$checkouturl = $checkout['data']['checkoutCreate']['checkout']['webUrl'];

	# ...and go
	wp_redirect($checkouturl);	
	exit;	

}


# LOAD CART

function shopifycart_load() {

	# init db
	global $wpdb;

	# init cart
	$cartid = 0;
	$contents = array();

	# init cart cookie
	$cookiename = EMFIT_CARTCOOKIE;

	# get current cart
	if (isset($_COOKIE[$cookiename])) {
		$cookieid = $_COOKIE[$cookiename];
		if (ctype_digit($cookieid)) {
			$cartid = intval($cookieid);
			$stored = $wpdb->get_var('SELECT cart FROM '. EMFIT_CARTDB . ' WHERE id=' . $cartid);
			if (empty($stored)) {
				$cartid = 0;
			} else {
				$contents = json_decode($stored, true);
			}
		}
	}

	# compile cart
	$cart = array(
		'id' => $cartid,
		'contents' => $contents,
	);

	# back at ya
	return $cart;

}


# SAVE CART

function shopifycart_save ($cart = array()) {
	
	# safety first!
	if (empty($cart)) { return false; }

	# init db
	global $wpdb;

	# convert contents
	$contents = json_encode($cart['contents']);
	
	# cart id
	$cartid = $cart['id'];
	
	# new cart
	if ($cartid == 0) {

		$insert = $wpdb->insert( 
			EMFIT_CARTDB, 
			array(
				'updated' => current_time('mysql'),
				'cart' => $contents
			)
		);

		if ($insert == false) { return false; }

		$cartid = $wpdb->insert_id;	
				
	}

	# update cart
	else {
		
		$update = $wpdb->update( 
			EMFIT_CARTDB, 
			array(
				'updated' => current_time('mysql'),
				'cart' => $contents
			),
			array('id' => $cartid)
		);

		if ($update == false) { return false; }
		
	}

	# set cookie
	$cookiename = EMFIT_CARTCOOKIE;
	$cookietime = time() + WEEK_IN_SECONDS;
	$cookieurl = $_SERVER['SERVER_NAME'];
	$cookiesecure = false;
	if (isset($_SERVER['HTTPS'])) { $cookiesecure = true; }

	# set cookie
	setcookie($cookiename, $cartid, $cookietime, "/", $cookieurl, $cookiesecure);

	# back at ya
	return true;
	
}


# DELETE CART

function shopifycart_delete ($cartid = 0) {

	# safety first!
	if ($cartid == 0) { return; }

	# fire!
	global $wpdb;
	$wpdb->query('DELETE FROM ' . EMFIT_CARTDB . ' WHERE id=' . $cartid);

}


# TIDY DATABASE

function shopifycart_dbtidy() {

	global $wpdb;

	# updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	$wpdb->query('DELETE FROM ' . EMFIT_CARTDB . ' WHERE updated < DATE_SUB(NOW(), INTERVAL 1 WEEK)');
	
}


# CART DATABASE
# https://codex.wordpress.org/Creating_Tables_with_Plugins

function shopifycart_db() {

	# already there
	$installed = get_option('emfitcartdb_version');
	if ($installed == '1') { return; }

	# create table
	global $wpdb;
	$table_name = EMFIT_CARTDB; 
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
 id mediumint(9) NOT NULL AUTO_INCREMENT,
 updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
 cart longtext NOT NULL,
 PRIMARY KEY  (id),
 KEY updated (updated)
) $charset_collate;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

	update_option('emfitcartdb_version', '1');   

	echo 'Table added!';

}

