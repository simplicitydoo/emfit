<?php

# WEBHOOK / INIT
# https://emfit.lemonadeisgreat.com/z/webhook/

# get type
if (!isset($_SERVER['HTTP_X_SHOPIFY_TOPIC'])) { webhook_bail(); }
$webhookid = $_SERVER['HTTP_X_SHOPIFY_TOPIC'];

$valid = array(
	'inventory_levels/update' => 'webhook-inventory.php',
	'orders/create' => 'webhook-order.php',
	'products/create' => 'webhook-product.php',
	'products/update' => 'webhook-product.php',
	'products/delete' => 'webhook-product-delete.php',
);

/*
if (ENV_SERVER == 'server') {
	$jsonin = file_get_contents('php://input');
	file_put_contents('/z/logs/emfit/webhook-json-' . time() . '.txt', $jsonin);
	file_put_contents('/z/logs/emfit/webhook-topic-' . time() . '.txt', $webhookid);
}
*/

if (!isset($valid[$webhookid])) { webhook_bail(); }

# incoming!
$jsonin = file_get_contents('php://input');
if (empty($jsonin)) { webhook_bail(); }

# have a peek
/*
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-json-' . time() . '.txt', $jsonin);
}
*/

# validate message
# https://community.shopify.com/c/Shopify-APIs-SDKs/validating-webhook-using-HMAC-in-PHP/td-p/212169
$hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$calculated_hmac = base64_encode(hash_hmac('sha256', $jsonin, SHOPIFY_WEBHOOK, true));

/*
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-server-' . time() . '.txt', var_export($_SERVER, TRUE));
	file_put_contents('/z/logs/temp/webhook-hmac-' . time() . '.txt', $hmac . ' / ' . $calculated_hmac);
}
*/

# invalid
if ($hmac != $calculated_hmac) { webhook_bail(); }

# webhook response
http_response_code(200);

# convert json
$decode = json_decode($jsonin, true);
$GLOBALS['emfit']['decode'] = $decode;

# process
require_once(TEMPLATE_SHOPIFY . '/' . $valid[$webhookid]);

# bye!
exit;

# bail
function webhook_bail() {
	wp_redirect('/');
	exit;
}
