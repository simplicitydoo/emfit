<?php
	
# WEBHOOK / PRODUCT DELETE
# https://emfit.lemonadeisgreat.com/z/product-delete/

# load product
$entry = $GLOBALS['emfit']['decode'];

# have a peek
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-product-delete-' . time() . '.txt', var_export($entry, TRUE));
}

# have a peek
else if (ENV_SERVER == 'server') {
	file_put_contents('/z/logs/emfit/webhook-product-delete-' . time() . '.txt', var_export($entry, TRUE));
}

# test only
if ( (isset($_SERVER['HTTP_X_SHOPIFY_TEST'])) && ($_SERVER['HTTP_X_SHOPIFY_TEST'] == true) ) {
	exit;
}

# get product id
$productid = $entry['id'];

# delete product
shopifyproducts_delete($productid);

# update id list
shopifyproducts_idlist();
