<?php
	
# SHOPIFY DATA

# load products
function shopifydata_products() {

$products = array(
  0 => 
  array (
    'id' => 3529101639785,
    'title' => 'Be Brave Muscle Tank',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093330260073,
        'gid' => 'gid://shopify/ProductVariant/28093330260073',
        'price' => '25.00',
        'compare' => NULL,
      ),
    ),
  ),
  1 => 
  array (
    'id' => 3529103016041,
    'title' => 'Be Brave T-Shirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116635241,
        'gid' => 'gid://shopify/ProductVariant/28097116635241',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097116668009,
        'gid' => 'gid://shopify/ProductVariant/28097116668009',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097116700777,
        'gid' => 'gid://shopify/ProductVariant/28097116700777',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097116733545,
        'gid' => 'gid://shopify/ProductVariant/28097116733545',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  2 => 
  array (
    'id' => 3529101475945,
    'title' => 'Be Nerdy Muscle Tank',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093330096233,
        'gid' => 'gid://shopify/ProductVariant/28093330096233',
        'price' => '25.00',
        'compare' => NULL,
      ),
    ),
  ),
  3 => 
  array (
    'id' => 3529106325609,
    'title' => 'Be Nerdy T-Shirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116012649,
        'gid' => 'gid://shopify/ProductVariant/28097116012649',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Large',
      ),
      1 => 
      array (
        'id' => 28097116045417,
        'gid' => 'gid://shopify/ProductVariant/28097116045417',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  4 => 
  array (
    'id' => 3529102000233,
    'title' => 'Be Nerdy T-Shirt (Black)',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097117257833,
        'gid' => 'gid://shopify/ProductVariant/28097117257833',
        'title' => 'XS',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097117290601,
        'gid' => 'gid://shopify/ProductVariant/28097117290601',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Small',
      ),
      2 => 
      array (
        'id' => 28097117323369,
        'gid' => 'gid://shopify/ProductVariant/28097117323369',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097117356137,
        'gid' => 'gid://shopify/ProductVariant/28097117356137',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  5 => 
  array (
    'id' => 3529102819433,
    'title' => 'Be Strong Muscle Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116799081,
        'gid' => 'gid://shopify/ProductVariant/28097116799081',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Medium',
      ),
      1 => 
      array (
        'id' => 28097116831849,
        'gid' => 'gid://shopify/ProductVariant/28097116831849',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Large',
      ),
      2 => 
      array (
        'id' => 28097116864617,
        'gid' => 'gid://shopify/ProductVariant/28097116864617',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  6 => 
  array (
    'id' => 3529101869161,
    'title' => 'Be Strong T-Shirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097117421673,
        'gid' => 'gid://shopify/ProductVariant/28097117421673',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097117454441,
        'gid' => 'gid://shopify/ProductVariant/28097117454441',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097117487209,
        'gid' => 'gid://shopify/ProductVariant/28097117487209',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097117519977,
        'gid' => 'gid://shopify/ProductVariant/28097117519977',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  7 => 
  array (
    'id' => 3529103114345,
    'title' => 'Body By Butter Racerback',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116504169,
        'gid' => 'gid://shopify/ProductVariant/28097116504169',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Medium',
      ),
      1 => 
      array (
        'id' => 28097116536937,
        'gid' => 'gid://shopify/ProductVariant/28097116536937',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Large',
      ),
      2 => 
      array (
        'id' => 28097116569705,
        'gid' => 'gid://shopify/ProductVariant/28097116569705',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  8 => 
  array (
    'id' => 3529103999081,
    'title' => 'Body By Butter Tank',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093348872297,
        'gid' => 'gid://shopify/ProductVariant/28093348872297',
        'price' => '25.00',
        'compare' => NULL,
      ),
    ),
  ),
  9 => 
  array (
    'id' => 3529081847913,
    'title' => 'Em\'s 7 Day Happy Gut Challenge',
    'type' => 'challenge',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093158719593,
        'gid' => 'gid://shopify/ProductVariant/28093158719593',
        'price' => '39.00',
        'compare' => '49.00',
      ),
    ),
  ),
  10 => 
  array (
    'id' => 3529080438889,
    'title' => 'EmFit Challenge: Don\'t Overthink It - Pick Your Start Date',
    'type' => 'challenge',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093146234985,
        'gid' => 'gid://shopify/ProductVariant/28093146234985',
        'price' => '21.00',
        'compare' => NULL,
      ),
    ),
  ),
  11 => 
  array (
    'id' => 3529079881833,
    'title' => 'EmFit Challenge: Pick Your Start Date',
    'type' => 'challenge',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093139910761,
        'gid' => 'gid://shopify/ProductVariant/28093139910761',
        'title' => '1',
        'price' => '21.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093139943529,
        'gid' => 'gid://shopify/ProductVariant/28093139943529',
        'title' => '2',
        'price' => '21.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  12 => 
  array (
    'id' => 3529085386857,
    'title' => 'EmFit Get Strong: Arms and Abs',
    'type' => 'strength',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093178347625,
        'gid' => 'gid://shopify/ProductVariant/28093178347625',
        'title' => '1',
        'price' => '49.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093178380393,
        'gid' => 'gid://shopify/ProductVariant/28093178380393',
        'title' => '2',
        'price' => '49.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  13 => 
  array (
    'id' => 3529084862569,
    'title' => 'EmFit Get Strong: Ass and Abs',
    'type' => 'strength',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093176086633,
        'gid' => 'gid://shopify/ProductVariant/28093176086633',
        'title' => '1',
        'price' => '49.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093176119401,
        'gid' => 'gid://shopify/ProductVariant/28093176119401',
        'title' => '2',
        'price' => '49.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  14 => 
  array (
    'id' => 3529083322473,
    'title' => 'EmFit Gift Card',
    'type' => 'giftcard',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093165305961,
        'gid' => 'gid://shopify/ProductVariant/28093165305961',
        'title' => '$21.00 USD',
        'price' => '21.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '$21.00 USD',
      ),
      1 => 
      array (
        'id' => 28093165338729,
        'gid' => 'gid://shopify/ProductVariant/28093165338729',
        'title' => '$31.00 USD',
        'price' => '31.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '$31.00 USD',
      ),
      2 => 
      array (
        'id' => 28093165371497,
        'gid' => 'gid://shopify/ProductVariant/28093165371497',
        'title' => '$40.00 USD',
        'price' => '40.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => '$40.00 USD',
      ),
      3 => 
      array (
        'id' => 28093165404265,
        'gid' => 'gid://shopify/ProductVariant/28093165404265',
        'title' => '$44.00 USD',
        'price' => '44.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => '$44.00 USD',
      ),
      4 => 
      array (
        'id' => 28093165437033,
        'gid' => 'gid://shopify/ProductVariant/28093165437033',
        'title' => '$49.00 USD',
        'price' => '49.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => '$49.00 USD',
      ),
      5 => 
      array (
        'id' => 28093165469801,
        'gid' => 'gid://shopify/ProductVariant/28093165469801',
        'title' => '$99.00 USD',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 6,
        'option1' => '$99.00 USD',
      ),
      6 => 
      array (
        'id' => 28093165502569,
        'gid' => 'gid://shopify/ProductVariant/28093165502569',
        'title' => '$249.00 USD',
        'price' => '249.00',
        'compare' => NULL,
        'position' => 7,
        'option1' => '$249.00 USD',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Title',
    ),
  ),
  15 => 
  array (
    'id' => 3529102622825,
    'title' => 'EmFit Pocket Tee',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116930153,
        'gid' => 'gid://shopify/ProductVariant/28097116930153',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097116962921,
        'gid' => 'gid://shopify/ProductVariant/28097116962921',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  16 => 
  array (
    'id' => 3529101082729,
    'title' => 'EmFit Swagga Pants',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097118503017,
        'gid' => 'gid://shopify/ProductVariant/28097118503017',
        'title' => 'XS',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097118535785,
        'gid' => 'gid://shopify/ProductVariant/28097118535785',
        'title' => 'Small',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Small',
      ),
      2 => 
      array (
        'id' => 28097118568553,
        'gid' => 'gid://shopify/ProductVariant/28097118568553',
        'title' => 'Medium',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Medium',
      ),
      3 => 
      array (
        'id' => 28097118601321,
        'gid' => 'gid://shopify/ProductVariant/28097118601321',
        'title' => 'XL',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  17 => 
  array (
    'id' => 3529102524521,
    'title' => 'Empowered Men\'s Loose T',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116995689,
        'gid' => 'gid://shopify/ProductVariant/28097116995689',
        'title' => 'XS',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097117028457,
        'gid' => 'gid://shopify/ProductVariant/28097117028457',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Small',
      ),
      2 => 
      array (
        'id' => 28097117061225,
        'gid' => 'gid://shopify/ProductVariant/28097117061225',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Medium',
      ),
      3 => 
      array (
        'id' => 28097117093993,
        'gid' => 'gid://shopify/ProductVariant/28097117093993',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Large',
      ),
      4 => 
      array (
        'id' => 28097117126761,
        'gid' => 'gid://shopify/ProductVariant/28097117126761',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  18 => 
  array (
    'id' => 3529102295145,
    'title' => 'Empowered Moon Capped Sleeve Muscle Tank',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093335306345,
        'gid' => 'gid://shopify/ProductVariant/28093335306345',
        'price' => '25.00',
        'compare' => NULL,
      ),
    ),
  ),
  19 => 
  array (
    'id' => 3529102131305,
    'title' => 'Empowered Mountain Flow Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097117192297,
        'gid' => 'gid://shopify/ProductVariant/28097117192297',
        'title' => 'Black / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Small',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  20 => 
  array (
    'id' => 3529099673705,
    'title' => 'Everything Is Aiming Women\'s Cropped Tee',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097119486057,
        'gid' => 'gid://shopify/ProductVariant/28097119486057',
        'title' => 'Black / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097119518825,
        'gid' => 'gid://shopify/ProductVariant/28097119518825',
        'title' => 'Black / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Black',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097119551593,
        'gid' => 'gid://shopify/ProductVariant/28097119551593',
        'title' => 'Black / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Black',
        'option2' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  21 => 
  array (
    'id' => 3529099608169,
    'title' => 'Everything Is Aiming Women\'s Flowy High Neck Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097119617129,
        'gid' => 'gid://shopify/ProductVariant/28097119617129',
        'title' => 'Rose / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Rose',
        'option2' => 'Medium',
      ),
      1 => 
      array (
        'id' => 28097119649897,
        'gid' => 'gid://shopify/ProductVariant/28097119649897',
        'title' => 'Rose / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Rose',
        'option2' => 'Large',
      ),
      2 => 
      array (
        'id' => 28097119682665,
        'gid' => 'gid://shopify/ProductVariant/28097119682665',
        'title' => 'Rose / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Rose',
        'option2' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  22 => 
  array (
    'id' => 3529081487465,
    'title' => 'FREE Try It Before You Buy It: 5 Day EmFit Challenge',
    'type' => 'challenge',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093154033769,
        'gid' => 'gid://shopify/ProductVariant/28093154033769',
        'price' => '0.00',
        'compare' => NULL,
      ),
    ),
  ),
  23 => 
  array (
    'id' => 3529101246569,
    'title' => 'Get Strong #ASFUH Flowy Muscle',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097117814889,
        'gid' => 'gid://shopify/ProductVariant/28097117814889',
        'title' => 'Blue / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Blue',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097117847657,
        'gid' => 'gid://shopify/ProductVariant/28097117847657',
        'title' => 'Blue / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Blue',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097117880425,
        'gid' => 'gid://shopify/ProductVariant/28097117880425',
        'title' => 'Blue / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Blue',
        'option2' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097117913193,
        'gid' => 'gid://shopify/ProductVariant/28097117913193',
        'title' => 'Blue / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Blue',
        'option2' => 'XL',
      ),
      4 => 
      array (
        'id' => 28097117945961,
        'gid' => 'gid://shopify/ProductVariant/28097117945961',
        'title' => 'Olive / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'Olive',
        'option2' => 'Small',
      ),
      5 => 
      array (
        'id' => 28097117978729,
        'gid' => 'gid://shopify/ProductVariant/28097117978729',
        'title' => 'Olive / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 6,
        'option1' => 'Olive',
        'option2' => 'Medium',
      ),
      6 => 
      array (
        'id' => 28097118011497,
        'gid' => 'gid://shopify/ProductVariant/28097118011497',
        'title' => 'Olive / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 7,
        'option1' => 'Olive',
        'option2' => 'Large',
      ),
      7 => 
      array (
        'id' => 28097118044265,
        'gid' => 'gid://shopify/ProductVariant/28097118044265',
        'title' => 'Olive / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 8,
        'option1' => 'Olive',
        'option2' => 'XL',
      ),
      8 => 
      array (
        'id' => 28097118109801,
        'gid' => 'gid://shopify/ProductVariant/28097118109801',
        'title' => 'Mint / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 9,
        'option1' => 'Mint',
        'option2' => 'Small',
      ),
      9 => 
      array (
        'id' => 28097118142569,
        'gid' => 'gid://shopify/ProductVariant/28097118142569',
        'title' => 'Mint / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 10,
        'option1' => 'Mint',
        'option2' => 'Large',
      ),
      10 => 
      array (
        'id' => 28097118175337,
        'gid' => 'gid://shopify/ProductVariant/28097118175337',
        'title' => 'Mint / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 11,
        'option1' => 'Mint',
        'option2' => 'XL',
      ),
      11 => 
      array (
        'id' => 28097118208105,
        'gid' => 'gid://shopify/ProductVariant/28097118208105',
        'title' => 'Mint / XXL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 12,
        'option1' => 'Mint',
        'option2' => 'XXL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  24 => 
  array (
    'id' => 3529101377641,
    'title' => 'Get Strong #ASFUH Racerback Crop',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097117618281,
        'gid' => 'gid://shopify/ProductVariant/28097117618281',
        'title' => 'Black / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097117651049,
        'gid' => 'gid://shopify/ProductVariant/28097117651049',
        'title' => 'Black / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Black',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097117683817,
        'gid' => 'gid://shopify/ProductVariant/28097117683817',
        'title' => 'Black / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Black',
        'option2' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097117716585,
        'gid' => 'gid://shopify/ProductVariant/28097117716585',
        'title' => 'Coffee / Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Coffee',
        'option2' => 'Small',
      ),
      4 => 
      array (
        'id' => 28097117749353,
        'gid' => 'gid://shopify/ProductVariant/28097117749353',
        'title' => 'Olive / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'Olive',
        'option2' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  25 => 
  array (
    'id' => 3529081127017,
    'title' => 'Get Strong 1-0-1: Em\'s FREE 8 Day Challenge',
    'type' => 'challenge',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093152198761,
        'gid' => 'gid://shopify/ProductVariant/28093152198761',
        'price' => '0.00',
        'compare' => NULL,
      ),
    ),
  ),
  26 => 
  array (
    'id' => 3529088663657,
    'title' => 'GET STRONG 6 Week Strength Program: BUILD',
    'type' => 'strength',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093202759785,
        'gid' => 'gid://shopify/ProductVariant/28093202759785',
        'title' => '1',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093202792553,
        'gid' => 'gid://shopify/ProductVariant/28093202792553',
        'title' => '2',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
      2 => 
      array (
        'id' => 28093202825321,
        'gid' => 'gid://shopify/ProductVariant/28093202825321',
        'title' => '3',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => '3',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  27 => 
  array (
    'id' => 3529087680617,
    'title' => 'GET STRONG 6 Week Strength Program: BURN',
    'type' => 'strength',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093193551977,
        'gid' => 'gid://shopify/ProductVariant/28093193551977',
        'title' => '1',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093193584745,
        'gid' => 'gid://shopify/ProductVariant/28093193584745',
        'title' => '2',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  28 => 
  array (
    'id' => 3529087287401,
    'title' => 'GET STRONG 6 Week Strength Program: BUSY BEE',
    'type' => 'strength',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093189161065,
        'gid' => 'gid://shopify/ProductVariant/28093189161065',
        'title' => '1',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => '1',
      ),
      1 => 
      array (
        'id' => 28093189193833,
        'gid' => 'gid://shopify/ProductVariant/28093189193833',
        'title' => '2',
        'price' => '99.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => '2',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Version',
    ),
  ),
  29 => 
  array (
    'id' => 3529098625129,
    'title' => 'Glute Activation Band',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093318824041,
        'gid' => 'gid://shopify/ProductVariant/28093318824041',
        'price' => '15.00',
        'compare' => NULL,
      ),
    ),
  ),
  30 => 
  array (
    'id' => 3529099837545,
    'title' => 'Good Human Jersey T-Shirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097118863465,
        'gid' => 'gid://shopify/ProductVariant/28097118863465',
        'title' => 'XS',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097118896233,
        'gid' => 'gid://shopify/ProductVariant/28097118896233',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Small',
      ),
      2 => 
      array (
        'id' => 28097118929001,
        'gid' => 'gid://shopify/ProductVariant/28097118929001',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Medium',
      ),
      3 => 
      array (
        'id' => 28097118961769,
        'gid' => 'gid://shopify/ProductVariant/28097118961769',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Large',
      ),
      4 => 
      array (
        'id' => 28097118994537,
        'gid' => 'gid://shopify/ProductVariant/28097118994537',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'XL',
      ),
      5 => 
      array (
        'id' => 28097119027305,
        'gid' => 'gid://shopify/ProductVariant/28097119027305',
        'title' => 'XXL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 6,
        'option1' => 'XXL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  31 => 
  array (
    'id' => 3529098788969,
    'title' => 'Good Human Swagga Sweats',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097120174185,
        'gid' => 'gid://shopify/ProductVariant/28097120174185',
        'title' => 'Black / XS',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097120206953,
        'gid' => 'gid://shopify/ProductVariant/28097120206953',
        'title' => 'Black / XL',
        'price' => '34.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Black',
        'option2' => 'XL',
      ),
      2 => 
      array (
        'id' => 28097120239721,
        'gid' => 'gid://shopify/ProductVariant/28097120239721',
        'title' => 'Grey / XS',
        'price' => '25.50',
        'compare' => '34.00',
        'position' => 3,
        'option1' => 'Grey',
        'option2' => 'XS',
      ),
      3 => 
      array (
        'id' => 28097120272489,
        'gid' => 'gid://shopify/ProductVariant/28097120272489',
        'title' => 'Grey / Small',
        'price' => '25.50',
        'compare' => '34.00',
        'position' => 4,
        'option1' => 'Grey',
        'option2' => 'Small',
      ),
      4 => 
      array (
        'id' => 28097120305257,
        'gid' => 'gid://shopify/ProductVariant/28097120305257',
        'title' => 'Grey / Medium',
        'price' => '25.50',
        'compare' => '34.00',
        'position' => 5,
        'option1' => 'Grey',
        'option2' => 'Medium',
      ),
      5 => 
      array (
        'id' => 28097120338025,
        'gid' => 'gid://shopify/ProductVariant/28097120338025',
        'title' => 'Grey / Large',
        'price' => '25.50',
        'compare' => '34.00',
        'position' => 6,
        'option1' => 'Grey',
        'option2' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  32 => 
  array (
    'id' => 3529099051113,
    'title' => 'Good Human Unisex Baseball Tee',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097119944809,
        'gid' => 'gid://shopify/ProductVariant/28097119944809',
        'title' => 'XS',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'XS',
      ),
      1 => 
      array (
        'id' => 28097119977577,
        'gid' => 'gid://shopify/ProductVariant/28097119977577',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Small',
      ),
      2 => 
      array (
        'id' => 28097120010345,
        'gid' => 'gid://shopify/ProductVariant/28097120010345',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Medium',
      ),
      3 => 
      array (
        'id' => 28097120043113,
        'gid' => 'gid://shopify/ProductVariant/28097120043113',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Large',
      ),
      4 => 
      array (
        'id' => 28097120075881,
        'gid' => 'gid://shopify/ProductVariant/28097120075881',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  33 => 
  array (
    'id' => 3529099214953,
    'title' => 'Good Human Unisex Hoodie',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097119748201,
        'gid' => 'gid://shopify/ProductVariant/28097119748201',
        'title' => 'Maroon / Small',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Maroon',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097119780969,
        'gid' => 'gid://shopify/ProductVariant/28097119780969',
        'title' => 'Maroon / Medium',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Maroon',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097119813737,
        'gid' => 'gid://shopify/ProductVariant/28097119813737',
        'title' => 'Maroon / Large',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Maroon',
        'option2' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097119846505,
        'gid' => 'gid://shopify/ProductVariant/28097119846505',
        'title' => 'Maroon / XL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Maroon',
        'option2' => 'XL',
      ),
      4 => 
      array (
        'id' => 28097119879273,
        'gid' => 'gid://shopify/ProductVariant/28097119879273',
        'title' => 'Maroon / XXL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'Maroon',
        'option2' => 'XXL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  34 => 
  array (
    'id' => 3529090170985,
    'title' => 'Good Human Unisex Hoodless Hoodie',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093208592489,
        'gid' => 'gid://shopify/ProductVariant/28093208592489',
        'title' => 'Grey / XS',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Grey',
        'option2' => 'XS',
      ),
      1 => 
      array (
        'id' => 28093208625257,
        'gid' => 'gid://shopify/ProductVariant/28093208625257',
        'title' => 'Grey / Small',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Grey',
        'option2' => 'Small',
      ),
      2 => 
      array (
        'id' => 28093208658025,
        'gid' => 'gid://shopify/ProductVariant/28093208658025',
        'title' => 'Grey / Medium',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Grey',
        'option2' => 'Medium',
      ),
      3 => 
      array (
        'id' => 28093208690793,
        'gid' => 'gid://shopify/ProductVariant/28093208690793',
        'title' => 'Grey / Large',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Grey',
        'option2' => 'Large',
      ),
      4 => 
      array (
        'id' => 28093208723561,
        'gid' => 'gid://shopify/ProductVariant/28093208723561',
        'title' => 'Grey / XL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'Grey',
        'option2' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  35 => 
  array (
    'id' => 3529097871465,
    'title' => 'Good Human Unisex Sweatshirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097120665705,
        'gid' => 'gid://shopify/ProductVariant/28097120665705',
        'title' => 'Blue / Small',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Blue',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097120698473,
        'gid' => 'gid://shopify/ProductVariant/28097120698473',
        'title' => 'Blue / Medium',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Blue',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097120731241,
        'gid' => 'gid://shopify/ProductVariant/28097120731241',
        'title' => 'Blue / Large',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Blue',
        'option2' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097120764009,
        'gid' => 'gid://shopify/ProductVariant/28097120764009',
        'title' => 'Blue / XL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Blue',
        'option2' => 'XL',
      ),
      4 => 
      array (
        'id' => 28097120796777,
        'gid' => 'gid://shopify/ProductVariant/28097120796777',
        'title' => 'Blue / XXL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 5,
        'option1' => 'Blue',
        'option2' => 'XXL',
      ),
      5 => 
      array (
        'id' => 28097120829545,
        'gid' => 'gid://shopify/ProductVariant/28097120829545',
        'title' => 'Rose / XS',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 6,
        'option1' => 'Rose',
        'option2' => 'XS',
      ),
      6 => 
      array (
        'id' => 28097120862313,
        'gid' => 'gid://shopify/ProductVariant/28097120862313',
        'title' => 'Rose / Small',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 7,
        'option1' => 'Rose',
        'option2' => 'Small',
      ),
      7 => 
      array (
        'id' => 28097120895081,
        'gid' => 'gid://shopify/ProductVariant/28097120895081',
        'title' => 'Rose / Medium',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 8,
        'option1' => 'Rose',
        'option2' => 'Medium',
      ),
      8 => 
      array (
        'id' => 28097120927849,
        'gid' => 'gid://shopify/ProductVariant/28097120927849',
        'title' => 'Rose / Large',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 9,
        'option1' => 'Rose',
        'option2' => 'Large',
      ),
      9 => 
      array (
        'id' => 28097120960617,
        'gid' => 'gid://shopify/ProductVariant/28097120960617',
        'title' => 'Rose / XL',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 10,
        'option1' => 'Rose',
        'option2' => 'XL',
      ),
      10 => 
      array (
        'id' => 28097120993385,
        'gid' => 'gid://shopify/ProductVariant/28097120993385',
        'title' => 'Grey / Small',
        'price' => '38.00',
        'compare' => NULL,
        'position' => 11,
        'option1' => 'Grey',
        'option2' => 'Small',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  36 => 
  array (
    'id' => 3529103868009,
    'title' => 'Kale (& Bacon) Green Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116110953,
        'gid' => 'gid://shopify/ProductVariant/28097116110953',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Large',
      ),
      1 => 
      array (
        'id' => 28097116143721,
        'gid' => 'gid://shopify/ProductVariant/28097116143721',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  37 => 
  array (
    'id' => 3529100722281,
    'title' => 'Meathead Hippie Crop Tee',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097118797929,
        'gid' => 'gid://shopify/ProductVariant/28097118797929',
        'title' => 'Black / Large',
        'price' => '21.25',
        'compare' => '25.00',
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Large',
      ),
      1 => 
      array (
        'id' => 28097231323241,
        'gid' => 'gid://shopify/ProductVariant/28097231323241',
        'title' => 'Cream / Small',
        'price' => '21.25',
        'compare' => '25.00',
        'position' => 2,
        'option1' => 'Cream',
        'option2' => 'Small',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  38 => 
  array (
    'id' => 3529091350633,
    'title' => 'Meathead Hippie Unisex Crewneck Sweatshirt',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093221273705,
        'gid' => 'gid://shopify/ProductVariant/28093221273705',
        'title' => 'Light Grey / Large',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 1,
        'option1' => 'Light Grey',
        'option2' => 'Large',
      ),
      1 => 
      array (
        'id' => 28093221306473,
        'gid' => 'gid://shopify/ProductVariant/28093221306473',
        'title' => 'Light Grey / XL',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 2,
        'option1' => 'Light Grey',
        'option2' => 'XL',
      ),
      2 => 
      array (
        'id' => 28093221339241,
        'gid' => 'gid://shopify/ProductVariant/28093221339241',
        'title' => 'Light Grey / XXL',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 3,
        'option1' => 'Light Grey',
        'option2' => 'XXL',
      ),
      3 => 
      array (
        'id' => 28093221372009,
        'gid' => 'gid://shopify/ProductVariant/28093221372009',
        'title' => 'Maroon / Small',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 4,
        'option1' => 'Maroon',
        'option2' => 'Small',
      ),
      4 => 
      array (
        'id' => 28093221404777,
        'gid' => 'gid://shopify/ProductVariant/28093221404777',
        'title' => 'Maroon / Medium',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 5,
        'option1' => 'Maroon',
        'option2' => 'Medium',
      ),
      5 => 
      array (
        'id' => 28093221437545,
        'gid' => 'gid://shopify/ProductVariant/28093221437545',
        'title' => 'Maroon / Large',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 6,
        'option1' => 'Maroon',
        'option2' => 'Large',
      ),
      6 => 
      array (
        'id' => 28093221470313,
        'gid' => 'gid://shopify/ProductVariant/28093221470313',
        'title' => 'Maroon / XL',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 7,
        'option1' => 'Maroon',
        'option2' => 'XL',
      ),
      7 => 
      array (
        'id' => 28093221503081,
        'gid' => 'gid://shopify/ProductVariant/28093221503081',
        'title' => 'Maroon / XXL',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 8,
        'option1' => 'Maroon',
        'option2' => 'XXL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  39 => 
  array (
    'id' => 3529099968617,
    'title' => 'Meathead Hippie Unisex T-Shirt',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093323182185,
        'gid' => 'gid://shopify/ProductVariant/28093323182185',
        'price' => '21.25',
        'compare' => '25.00',
      ),
    ),
  ),
  40 => 
  array (
    'id' => 3529100853353,
    'title' => 'Meathead Hippie Women‚Äôs Crop Hoodie',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097118666857,
        'gid' => 'gid://shopify/ProductVariant/28097118666857',
        'title' => 'Grey / Small',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 1,
        'option1' => 'Grey',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097118699625,
        'gid' => 'gid://shopify/ProductVariant/28097118699625',
        'title' => 'Grey / Medium',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 2,
        'option1' => 'Grey',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097118732393,
        'gid' => 'gid://shopify/ProductVariant/28097118732393',
        'title' => 'Grey / Large',
        'price' => '32.30',
        'compare' => '38.00',
        'position' => 3,
        'option1' => 'Grey',
        'option2' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  41 => 
  array (
    'id' => 3529103212649,
    'title' => 'Men\'s Fit Maroon Pineapple',
    'type' => 'swag',
    'format' => 'single',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28093338386537,
        'gid' => 'gid://shopify/ProductVariant/28093338386537',
        'price' => '25.00',
        'compare' => NULL,
      ),
    ),
  ),
  42 => 
  array (
    'id' => 3529103376489,
    'title' => 'My Superpower Is... Crop Top',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116340329,
        'gid' => 'gid://shopify/ProductVariant/28097116340329',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097116373097,
        'gid' => 'gid://shopify/ProductVariant/28097116373097',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097116405865,
        'gid' => 'gid://shopify/ProductVariant/28097116405865',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  43 => 
  array (
    'id' => 3529103573097,
    'title' => 'My Superpower Is... Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097116176489,
        'gid' => 'gid://shopify/ProductVariant/28097116176489',
        'title' => 'Small',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097116209257,
        'gid' => 'gid://shopify/ProductVariant/28097116209257',
        'title' => 'Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097116242025,
        'gid' => 'gid://shopify/ProductVariant/28097116242025',
        'title' => 'Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Large',
      ),
      3 => 
      array (
        'id' => 28097116274793,
        'gid' => 'gid://shopify/ProductVariant/28097116274793',
        'title' => 'XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Size',
    ),
  ),
  44 => 
  array (
    'id' => 3529098100841,
    'title' => 'Phases of the Entrepreneur Women\'s Side Slit Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097120534633,
        'gid' => 'gid://shopify/ProductVariant/28097120534633',
        'title' => 'Black / Medium',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Medium',
      ),
      1 => 
      array (
        'id' => 28097120567401,
        'gid' => 'gid://shopify/ProductVariant/28097120567401',
        'title' => 'Black / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Black',
        'option2' => 'Large',
      ),
      2 => 
      array (
        'id' => 28097120600169,
        'gid' => 'gid://shopify/ProductVariant/28097120600169',
        'title' => 'Black / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Black',
        'option2' => 'XL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  45 => 
  array (
    'id' => 3529098231913,
    'title' => 'Phases of the Entrepreneur Women\'s Side Slit Tee',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097120436329,
        'gid' => 'gid://shopify/ProductVariant/28097120436329',
        'title' => 'Black / Small',
        'price' => '30.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Black',
        'option2' => 'Small',
      ),
      1 => 
      array (
        'id' => 28097120469097,
        'gid' => 'gid://shopify/ProductVariant/28097120469097',
        'title' => 'Black / Medium',
        'price' => '30.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Black',
        'option2' => 'Medium',
      ),
      2 => 
      array (
        'id' => 28097120501865,
        'gid' => 'gid://shopify/ProductVariant/28097120501865',
        'title' => 'Black / Large',
        'price' => '30.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Black',
        'option2' => 'Large',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
  46 => 
  array (
    'id' => 3529101148265,
    'title' => 'Strong. Capable. Badass. Muscle Tank',
    'type' => 'swag',
    'format' => 'variants',
    'published' => 'web',
    'variants' => 
    array (
      0 => 
      array (
        'id' => 28097118306409,
        'gid' => 'gid://shopify/ProductVariant/28097118306409',
        'title' => 'Blue / Large',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 1,
        'option1' => 'Blue',
        'option2' => 'Large',
      ),
      1 => 
      array (
        'id' => 28097118339177,
        'gid' => 'gid://shopify/ProductVariant/28097118339177',
        'title' => 'Blue / XL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 2,
        'option1' => 'Blue',
        'option2' => 'XL',
      ),
      2 => 
      array (
        'id' => 28097118371945,
        'gid' => 'gid://shopify/ProductVariant/28097118371945',
        'title' => 'Blue / XXL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 3,
        'option1' => 'Blue',
        'option2' => 'XXL',
      ),
      3 => 
      array (
        'id' => 28097118404713,
        'gid' => 'gid://shopify/ProductVariant/28097118404713',
        'title' => 'Army Green / XXL',
        'price' => '25.00',
        'compare' => NULL,
        'position' => 4,
        'option1' => 'Army Green',
        'option2' => 'XXL',
      ),
    ),
    'options' => 
    array (
      'option1' => 'Color',
      'option2' => 'Size',
    ),
  ),
);

	return $products;

}
