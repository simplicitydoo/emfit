<?php
	
# WEBHOOK / PRODUCT
# https://emfit.lemonadeisgreat.com/z/product/ 

# load product
$entry = $GLOBALS['emfit']['decode'];

# have a peek
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-product-' . time() . '.txt', var_export($entry, TRUE));
}

else if (ENV_SERVER == 'server') {
	file_put_contents('/z/logs/emfit/webhook-product-' . time() . '.txt', var_export($entry, TRUE));
}

# test only
if ( (isset($_SERVER['HTTP_X_SHOPIFY_TEST'])) && ($_SERVER['HTTP_X_SHOPIFY_TEST'] == true) ) {
	exit;
}

# process product
$product = shopifyproducts_entry($entry);

# update product
shopifyproducts_update($product);

# update id list
shopifyproducts_idlist();
