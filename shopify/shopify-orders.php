<?php
	
# SHOPIFY ORDERS


# PROCESS ORDERS

function shopifyorders_process() {

	# last fetch
	$args = array();
	$lastfetch = get_option('shopify_orders_fetch');
	if ($lastfetch) {
		$args = array(
			'condition' => 'updated_at_min',
			'setting' => $lastfetch,
		);
	}

	# api
	$data = shopify_api('orders_all', $args);

	# oops
	if ( (empty($data)) || (isset($data['error'])) ) { return false; }

	# print_r($data);

	# set current time
	update_option('shopify_orders_fetch', date('c'));

	# nothing new
	if (empty($data['orders'])) { return true; }

	# load id list
	$idlist = shopifyproducts_idlist_load(true);

	# process
	foreach ($data['orders'] as $key => $order) {
		shopifyorders_single($order);
	}

	# back at ya
	return true;

}


# SINGLE ORDER

function shopifyorders_single ($order = array()) {

	if (ENV_SERVER == 'server') {
		file_put_contents('/z/logs/emfit/webhook-order-single-' . time() . '.txt', var_export($order, TRUE));
	}

	# safety first!
	if (empty($order)) { return false; }


	# CUSTOM INFO

	# echo	 'custom info<br>';
	
	# get custom info
	$userid = $cartid = 0;
	$custom = $order['note_attributes'];
	foreach ($custom as $key => $pair) {
		if ($pair['name'] == 'wordpress_id') {
			$userid = intval($pair['value']);
		}
		if ($pair['name'] == 'emfit_cartid') {
			$cartid = intval($pair['value']);
		}
	}

	# CHECK ORDER
	
	# load id list
	$idlist = shopifyproducts_idlist_load();

	# echo	 'id list<br>';
	# print_r($idlist);
	
	# get programs
	$programs = array();
	$challenges = array();
	$entries = $order['line_items'];
	foreach ($entries as $key => $entry) {
		
		# get ids
		$productid = $entry['product_id'];
		$variantid = $entry['variant_id'];
	
		# anything there?
		if (!isset($idlist[$productid]['variants'][$variantid])) { continue; }
	
		# skip clothing
		if ( ($idlist[$productid]['type'] != 'challenge') && ($idlist[$productid]['type'] != 'strength') ) { continue; }
	
		# get program id
		$programid = intval($idlist[$productid]['variants'][$variantid]['programid']);
		if (empty($programid)) { continue; }
		
		# add / strength
		if ($idlist[$productid]['type'] == 'strength') {
			$programs[] = array(
				'programid' => $programid,
				'shopifyid' => $productid,
				'variantid' => $variantid,
			);
		}

		# add / challenge
		if ($idlist[$productid]['type'] == 'challenge') {
			$challenges[] = array(
				'challengeid' => $programid,
				'shopifyid' => $productid,
				'variantid' => $variantid,
			);
		}
	
	}

	# echo	 'programs/challenges<br>';
	# print_r($programs);
	# print_r($challenges);
	
	# anybody home?
	if ( (empty($programs)) && (empty($challenges)) ) {
		shopifycart_delete($cartid);
		return false;
	}
	
	
	# ORDER INFO
	
	$orderid = $order['id'];
	$orderdate = $order['created_at'];
	$customerid = $order['customer']['id'];
	$customeremail = strtolower($order['customer']['email']);
	
	
	# CHECK USER

	# echo "userid: $userid<br>";
	
	if (!$userid) {
		$user = get_user_by('email', $customeremail);
		if ($user) { $userid = $user->ID; }
	}
	
	
	# CREATE USER
	
	if (!$userid) {
	
		$firstname = $order['customer']['default_address']['first_name'];
		$lastname = $order['customer']['default_address']['last_name'];
		$pw = wp_generate_password(8, false);
	
		$signup = array(
			'firstname' => $firstname,
			'lastname' => $lastname,
			'email' => $customeremail,
			'pw' => $pw,
		);
	
		$userid = member_create($signup);
		
	}
	
	# well, we tried
	if (!$userid) {
		shopifycart_delete($cartid);
		return false;
	}
	
	
	# CREATE SHOPIFY ID
	
	member_createshopifyid($userid, $customerid);
	
	
	# GET MEMBER PAGE
	
	$memberpageid = member_createpage($userid);
	
	# oy
	if (!$memberpageid) {
		shopifycart_delete($cartid);
		return false;
	}
	
	
	# CREATE PROGRAMS
	
	if (!empty($programs)) {

		foreach ($programs as $key => $program) {
		
			$settings = array(
				'memberpageid' => $memberpageid,
				'programid' => $program['programid'],
				# 'shopifyid' => $program['shopifyid'],
				# 'variantid' => $program['variantid'],
				'orderid' => $orderid,
				'orderdate' => date('Y-m-d', strtotime($orderdate)),
			);

			# echo 'add program<br>';
			# print_r($settings);
		
			$added = memberprogram_add($settings);
			
		}
		
	}


	# CREATE CHALLENGES

	if (!empty($challenges)) {

		foreach ($challenges as $key => $challenge) {
		
			$settings = array(
				'memberpageid' => $memberpageid,
				'challengeid' => $challenge['challengeid'],
				# 'shopifyid' => $challenge['shopifyid'],
				# 'variantid' => $challenge['variantid'],
				'orderid' => $orderid,
				'orderdate' => date('Y-m-d', strtotime($orderdate)),
			);
		
			$added = memberchallenge_add($settings);
			
		}
		
	}
	

	# WRAP IT UP
	
	# delete cart
	shopifycart_delete($cartid);
	
	# back at ya
	return true;
	
}
