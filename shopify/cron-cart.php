<?php
	
# CRON / CART
# weekly tidy

# db hook
global $wpdb;

# updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
$wpdb->query('DELETE FROM ' . EMFIT_CARTDB . ' WHERE updated < DATE_SUB(NOW(), INTERVAL 1 WEEK)');
