<?php
	
# WEBHOOK / ORDER
# https://emfit.lemonadeisgreat.com/z/order/ ‎

# load order
$order = $GLOBALS['emfit']['decode'];

# have a peek
if (ENV_SERVER == 'beta') {
	file_put_contents('/z/logs/temp/webhook-order-' . time() . '.txt', var_export($order, TRUE));
}

else if (ENV_SERVER == 'server') {
	file_put_contents('/z/logs/emfit/webhook-order-' . time() . '.txt', var_export($order, TRUE));
}

# test only
if ( (isset($_SERVER['HTTP_X_SHOPIFY_TEST'])) && ($_SERVER['HTTP_X_SHOPIFY_TEST'] == true) ) {
	# exit;
}

# process order
shopifyorders_single($order);
