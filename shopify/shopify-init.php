<?php
	
# SHOPIFY INIT

define('SHOPIFY_ID', 'emfitstore.myshopify.com');
define('SHOPIFY_KEY', 'c47259fb6b51676a314b70e819d99473');
define('SHOPIFY_PW', '88d485200d0f9a6a7b256cd92a49e0b1');
define('SHOPIFY_SECRET', '10af42f5626f042809b3c6cd4d3ee78b');
define('SHOPIFY_TOKEN', '902621bcced915135b0a71cf65adfecb');
define('SHOPIFY_WEBHOOK', '8ea864226bc66c1101d99fe47ecaf28d180355f200c50378c2f910e1fb48ebcf');
define('SHOPIFY_GEEK', 'geek@lemonadeisgreat.com');

define('EMFIT_CARTDB', $wpdb->prefix . 'emfitcarts');
define('EMFIT_CARTCOOKIE', 'emfitcart');


# INCLUDES

require_once(TEMPLATE_SHOPIFY . '/shopify-products.php');
require_once(TEMPLATE_SHOPIFY . '/shopify-cart.php');
require_once(TEMPLATE_SHOPIFY . '/shopify-orders.php');
require_once(TEMPLATE_SHOPIFY . '/cron-shopify.php');


# API REQUEST

function shopify_api ($request = '', $args = array(), $id = '') {

	# error init
	$error = array(
		'error' => array(
			'source' => '',
			'message' => '',
		),
	);

	# safety first!
	if (empty($request)) {
		$error['error']['source'] = 'funk';
		$error['error']['message'] = 'missing request';
		return $error;
	}

	# valid requests
	$valid = array(

		# products
		'products_all' => array(
			'method' => 'GET',
			'url' => 'products.json',
		),

		# customers		 
		'customer_create' => array(
			'method' => 'POST',
			'url' => 'customers.json',
		),

		'customer_get' => array(
			'method' => 'GET',
			'url' => 'customers/{id}.json',
		),

		'customer_metafields' => array(
			'method' => 'GET',
			'url' => 'customers/{id}/metafields.json',
		),

		'customer_update' => array(
			'method' => 'PUT',
			'url' => 'customers/{id}.json',
		),

		'customers_all' => array(
			'method' => 'GET',
			'url' => 'customers.json',
		),

		'orders_all' => array(
			'method' => 'GET',
			'url' => 'orders.json',
		),

		'order_get' => array(
			'method' => 'GET',
			'url' => 'orders/{id}.json',
		),

	);

	# oops
	if (!isset($valid[$request])) {
		$error['error']['source'] = 'funk';
		$error['error']['message'] = 'invalid request';
		return $error;
	}

	# api url
	$url = 'https://' . SHOPIFY_KEY . ':' . SHOPIFY_PW . '@' . SHOPIFY_ID;
	$url .= '/admin/api/2019-04/' . $valid[$request]['url'];
	
	# insert id
	if (!empty($id)) {
		$url = str_replace('{id}', $id, $url);
	}

	# convenience
	$method = $valid[$request]['method'];

	# query string
	if ( ($method == 'GET') && (!empty($args)) ) {
		$url .= '?' . $args['condition'] . '=' . $args['setting'];
	}

	# payload
	$payload = '';
	if ( ($method != 'GET') && (!empty($args)) ) {
		$payload = json_encode($args);
	}

	# curl options
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_USERAGENT, 'EmFitBot');
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	
	if (!empty($payload)) {
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	}
	
	if (ENV_SERVER == 'bugs') {
		$log = fopen('/z/logs/temp/curl-' . time() . '.txt', 'w');
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $log);
	}

	# fire!
	$response = curl_exec($ch);
	# echo $response;

	# errors?
	$errnum = curl_errno($ch);
	$errmessage = curl_error($ch);

	# close curl
	curl_close($ch);

	# oops
	if ($errnum != 0) {
		$error['error']['source'] = 'curl';
		$error['error']['message'] = $errnum . ': ' . $errmessage;
		return $error;
	}

	# process response
	list($headers, $json) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);

	# decode json
	$data = json_decode($json, true);

	# print_r($data);

	# oops
	if (isset($data['errors'])) {
		$error['error']['source'] = 'shopify';
		$error['error']['shopify'] = $data['errors'];
		return $error;
	}

	# print_r($data);

	# back at ya
	return $data;
		
}


# STOREFRONT REQUEST

function shopify_storefront ($funk = '', $data = '') {

	# error init
	$error = array(
		'error' => array(
			'source' => '',
			'message' => '',
		),
	);

	# valid
	$valid = array(
		'checkout' => 1,
	);
	
	if (!isset($valid[$funk])) {
		$error['error']['source'] = 'funk';
		$error['error']['message'] = 'invalid request';
		return $error;
	}

	# init payload
	$payload = '';
	

	# CHECKOUT
	
	if ($funk == 'checkout') {

		# oops
		# if ( (!isset($data['email'])) || (!isset($data['lineitems'])) || (!isset($data['userid'])) || (!isset($data['cartid'])) ) {
		if ( (!isset($data['lineitems'])) || (!isset($data['cartid'])) || (!isset($data['requirelogin'])) ) {
			$error['error']['source'] = 'funk';
			$error['error']['message'] = 'invalid data';
			return $error;
		}

		# convenience
		$lineitems = $data['lineitems'];
		$cartid = $data['cartid'];
		$requirelogin = $data['requirelogin'];
		
		# require login
		if ($requirelogin) {

			if ( (!isset($data['email'])) || (!isset($data['userid'])) ) {
				$error['error']['source'] = 'funk';
				$error['error']['message'] = 'invalid data';
				return $error;
			}

			$email = $data['email'];
			$userid = $data['userid'];
	
			# compile payload
			$payload = <<<EOD
mutation {
  checkoutCreate(input: {
    email: "$email",
    lineItems: [$lineitems],
    customAttributes: [
      {
	    key: "wordpress_id",
	    value: "$userid"
	  },
      {
	    key: "emfit_cartid",
	    value: "$cartid"
	  }
	]
  }) {
    checkout {
      id
      webUrl
    }
  }
}
EOD;
			
		}

		# swag/supplements only
		else {

			# compile payload
			$payload = <<<EOD
mutation {
  checkoutCreate(input: {
    lineItems: [$lineitems],
    customAttributes: [
      {
	    key: "emfit_cartid",
	    value: "$cartid"
	  }
	]
  }) {
    checkout {
      id
      webUrl
    }
  }
}
EOD;
			
		}
		
	}
	
	# settings
	$url = 'https://' . SHOPIFY_ID . '/api/graphql';

	$headers = array();
	$headers[] = 'Content-Type: application/graphql';
	$headers[] = 'X-Shopify-Storefront-Access-Token: ' . SHOPIFY_TOKEN;

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_USERAGENT, 'EmFitBot');
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		
	if (ENV_SERVER == 'bugs') {
		$log = fopen('/z/logs/temp/curl-' . time() . '.txt', 'w');
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $log);
	}

	# fire!
	$json = curl_exec($ch);

	# errors?
	$errnum = curl_errno($ch);
	$errmessage = curl_error($ch);

	# close curl
	curl_close($ch);

	# curl error
	if ($errnum != 0) {
		$error['error']['source'] = 'curl';
		$error['error']['message'] = $errnum . ': ' . $errmessage;
		return $error;
	}
	
	# no response
	if (empty($json)) {
		$error['error']['source'] = 'shopify';
		$error['error']['message'] = 'no response';
		return $error;
	}

	$response = json_decode($json, true);

	# shpify errors
	if (isset($response['errors'])) {
		$error['error']['source'] = 'shopify';
		$error['error']['shopify'] = $response['errors'];
		return $error;
	}
	
	# back at ya	
	return $response;
	
}

/*
	$request = <<<EOD
mutation {
  checkoutCreate(input: {
    lineItems: [{ variantId: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8xMjYwMDAwNDY3NQ==", quantity: 1 }]
  }) {
    checkout {
       id
       webUrl
       lineItems(first: 5) {
         edges {
           node {
             title
             quantity
           }
         }
       }
    }
  }
}
EOD;
*/