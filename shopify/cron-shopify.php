<?php
	
# CRON / SHOPIFY

# cart tidy / weekly
add_action('shopifycron_cart_hook', 'shopifycron_cart');

if (!wp_next_scheduled('shopifycron_cart_hook')) {
	wp_schedule_event(time(), 'twicedaily', 'shopifycron_cart_hook');
}

function shopifycron_cart() {
	shopifycart_dbtidy();
}


# product update
add_action('shopifycron_product_hook', 'shopifycron_products');

if (!wp_next_scheduled('shopifycron_product_hook')) {
	wp_schedule_event(time(), 'twicedaily', 'shopifycron_product_hook');
}

function shopifycron_products() {
	$count = shopifyproducts_process();
	# echo "Products processed: $count";
}


# order update
add_action('shopifycron_order_hook', 'shopifycron_orders');

if (!wp_next_scheduled('shopifycron_order_hook')) {
	wp_schedule_event(time(), 'twicedaily', 'shopifycron_order_hook');
}

function shopifycron_orders() {
	shopifyorders_process();
}
