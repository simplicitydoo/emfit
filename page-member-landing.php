<?php
/**
Template name: Member / Landing
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'login';

# dashboard redirect
if (is_user_logged_in() ) {
	wp_redirect(site_url(MEMBER_DASHBOARD));	
	exit;
}

# https://codex.wordpress.org/Customizing_the_Login_Form#Make_a_Custom_Login_Page

$redirect = site_url(MEMBER_DASHBOARD);

if ( (isset($_GET['return'])) && ($_GET['return'] == 'cart') ) {
	$redirect = site_url(EMFIT_CART);
}

# login settings
$loginargs = array(
	'redirect' => $redirect, 
	'form_id' => 'member-loginform',
	'label_username' => __('Email'),
	'label_password' => __('Password'),
	'label_remember' => __('Remember Me'),
	'label_log_in' => __('Log In'),
	'remember' => true,
);

# signup settings
$signupargs = array(
	'id' => 'acf_form_signup',
	'post_id' => 'new_post',
	'submit_value'	=> 'Get Started',
	'return' => $redirect, 
	'new_post' => array(
		'post_type' => 'acfstub',
		'post_status' => 'private',
	),
);

# acf helper
acf_form_head();

# header
get_header();

?>

<section class="portal-navbar dashboard text-center textuc">
<div class="container height100">
<div class="flex height100">
<div class="navwrap">

<a class="portalnavlink active" href="#" data-panel="signin" id="portalnav-signin">Sign In</a>

<a class="portalnavlink" href="#" data-panel="signup" id="portalnav-signup">Sign Up</a>

</div>
</div>
</div>
</section>


<?php # SIGN IN ?>

<section class="portal-landing" id="panel-signin">
<div class="container">

<div class="livewrap flex flexrow">

<div class="titlecol login flex flexspace">
<div class="title textlc">
welcome<br>
back!
</div>
<div class="info textlc">
don’t<br>
have an<br>
account?
</div>
</div>

<div class="formcol">
<div class="formwrap">

<div class="general-form signin">

<div class="instruction">
Sign in to your account below
</div>	

<?php wp_login_form($loginargs); ?>

</div>

<div class="linkwrap text-center">
<a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>" title="Lost Password">Lost Your Password?</a>
</div>

<div class="info text-center textlc">
don’t have an account?
</div>

<div class="buttonwrap">
<a class="portalnavlink" href="#" data-panel="signup"><button class="featurebutton btgray noshadow">Sign Up for Free!</button></a>
</div>

</div>
</div>

</div>

</div>
</section>


<?php # SIGN UP ?>

<section class="portal-landing" id="panel-signup" style="display:none;">
<div class="container">

<div class="livewrap flex flexrow">

<div class="titlecol signup">
<div class="title textlc">
let’s<br>
get<br>
going!
</div>
</div>

<div class="formcol signup">
<div class="formwrap">

<div class="general-form">

<div class="instruction">
Create your account below
</div>	

<?php acf_form($signupargs); ?>

</div>

<div class="linkwrap text-center">
<a class="portalnavlink" href="#" data-panel="signin">&lt; Go Back</a>
</div>	

</div>
</div>

</div>

</div>
</section>


<?php get_footer(); ?>
