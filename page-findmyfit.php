<?php
/**
Template name: Find My Fit
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'findmyfit';

# content
$programs = products_programs_list();
$tags = products_programs_tags($programs);
$categories = products_programs_cats($programs);
$catmenu = products_programs_catmenu();

# print_r($programs);
# exit;

# incoming!
$sort = '';
if (isset($_GET['type'])) {
	if ( ($_GET['type'] == 'challenge') || ($_GET['type'] == 'strength') ) {
		$sort = $_GET['type'];
	}
}
	
# header
get_header();

?>


<?php # HEADER ?>

<section class="findmyfit-header">
<div class="container">

<div class="title textuc flex">
Find My Fit
</div>

<?php if (!empty($tags)) { ?>
<div class="selectrow flex">
<div class="selectwrap" id="selected">
	
<div class="placeholder" id="selected-placeholder">
<div id="tags-empty">Choose tags, or click the sub navigation below</div>
</div>

</div>
</div>

<div class="tagwrap">
<div class="tagrow" id="tagrow">

<?php foreach ($tags as $key => $value) { ?>
<div class="tag" data-tag="<?= $key ?>" id="tag-<?= $key ?>">
<?= $value ?>
</div>	
<?php } ?>

</div>
</div>

<div class="tagclear textuc text-right" id="tagclear-wrap" style="display:none;">
<a href="#" id="tagclear">Clear Tags</a>
</div>
<?php } ?>

</div>
</section>


<?php # PANELS ?>

<section class="findmyfit-panels">
<div class="container">

<div class="panelwrap flex flexrow flexspace">

<?php # panelnav ?>

<div class="panelnav textlc">

<?php
$labelcss = '';
$challengecss = '';
$strengthcss = '';
$entrystyle = 'display:none;';

if (!empty($sort)) {
	$labelcss = 'open';
	$entrystyle = '';
	if ($sort == 'challenge') { $challengecss = 'active'; }	
	if ($sort == 'strength') { $strengthcss = 'active'; }	
}
	
?>

<div class="catnav">
<div class="catlabel flex <?= $labelcss ?>" data-catnav="type">
Type
</div>
<div class="catentries" id="catnav-type" style="<?= $entrystyle ?>">
<div class="catentry flex <?= $challengecss ?>" data-category="challenge">
Challenges
</div>
<div class="catentry flex <?= $strengthcss ?>" data-category="strength">
Strength
</div>
</div>
</div>

<?php
foreach ($catmenu as $key => $value) {
	if (empty($value['entries'])) { continue; }
?>
<div class="catnav">
<div class="catlabel flex" data-catnav="<?= $key ?>">
<?= $value['name'] ?>
</div>
<div class="catentries" id="catnav-<?= $key ?>" style="display:none;">
<?php	
	foreach ($value['entries'] as $skey => $svalue) {
		if (!isset($categories[$key][$skey])) { continue; }
?>		
<div class="catentry flex" data-category="<?= $skey ?>">
<?= $svalue ?>
</div>
<?php } ?>
</div>
</div>
<?php } ?>
	
</div>

<?php # entries ?>

<div class="entries flex flexrow flexspace flexwrap">

<?php
foreach ($programs as $key => $value) {
	
	# price
	$price = str_replace('.00', '', $value['price']);
	if (intval($price) == 0) { $price = 'Free'; }
	else { $price = '$' . $price; }

	# type
	$style = '';
	if ( (!empty($sort)) && ($sort != $value['type']) ) {
		$style = 'display:none;';
	}

	$procolor = 'propurple';
	if ($value['procolor']) { $procolor = $value['procolor']; }
	
?>
<a href="<?= $value['url'] ?>" class="programentry <?= $value['type'] ?> <?= $value['classes'] ?>" style="<?= $style ?>">

<div class="header textuc flex">
<div class="flex flexrow flexspace">
<div class="price">
<?= $price ?>
</div>
<div class="favorite" style="display:none;">
Favorite
</div>
</div>
</div>

<div class="imagewrap bgcover" style="background-image:url(<?= $value['image'] ?>);"></div>

<div class="content text-center">
	
<div class="type textuc">
<?= $value['type'] ?>
</div>

<div class="namewrap <?= $procolor ?>">
<div class="name">
<?= $value['name'] ?>
</div>
<?php if (!empty($value['variantname'])) { ?>
<div class="version textlc">
<?= $value['variantname'] ?>
</div>
<?php } ?>
</div>

<div class="textwrap">
<?= $value['blurb'] ?>
</div>

</div>

</a>	
<?php } ?>

</div>

</div>

</div>
</section>


<?php # QUIZ ?>

<?php get_template_part('panels/panel-quiz'); ?>


<?php # GIFT CARD ?>

<?php get_template_part('panels/panel-giftcard'); ?>


<?php
	
# footer
get_footer();

/*
# PROGRAM LIST

    [2] => Array
        (
            [newlabel] => 
            [image] => 
            [type] => challenge
            [name] => EmFit Challenge
            [variantname] => Version 01
            [blurb] => 
            [price] => 21.00
            [compare] => 
            [url] => http://www.emfit.test/product/emfit-challenge-pick-your-start-date/
            [tags] => Array
                (
                    [0] => body-work
                    [1] => inner-health
                )

            [cats] => Array
                (
                    [cat_features] => Array
                        (
                            [0] => fitness-routine
                        )

                    [cat_workout] => Array
                        (
                            [0] => at-home
                        )

                    [cat_focus] => Array
                        (
                            [0] => arms
                            [1] => gains
                        )

                    [cat_nutrition] => Array
                        (
                            [0] => cut-sugar
                            [1] => thyroid-health
                        )

                )

            [classes] => fitness-routine at-home arms gains cut-sugar thyroid-health body-work inner-health 
        )


# PROGRAM TAGS

Array
(
    [body-work] => Body Work
    [inner-health] => Inner Health
    [too-busy] => Too Busy
)




# PROGRAM CATS

Array
(
    [cat_features] => Array
        (
            [fitness-routine] => 1
        )

    [cat_workout] => Array
        (
            [at-home] => 1
        )

    [cat_focus] => Array
        (
            [arms] => 1
            [gains] => 1
        )

    [cat_nutrition] => Array
        (
            [cut-sugar] => 1
            [thyroid-health] => 1
        )

    [cat_price] => Array
        (
        )

)



# CAT MENU

Array
(
    [cat_features] => Array
        (
            [name] => Features
            [entries] => Array
                (
                    [food-+-fitness] => Food + Fitness
                    [fitness-routine] => Fitness Routine
                )

        )

    [cat_workout] => Array
        (
            [name] => Workout Space
            [entries] => Array
                (
                    [at-home] => At home
                    [in-the-gym] => In the gym
                )

        )

    [cat_focus] => Array
        (
            [name] => Focus
            [entries] => Array
                (
                    [full-body] => Full Body
                    [glutes] => Glutes
                    [arms] => Arms
                    [abs] => Abs
                    [gains] => Gains
                    [hiit] => HIIT
                    [isolated-muscle-groups] => Isolated muscle groups
                )

        )

    [cat_nutrition] => Array
        (
            [name] => Nutrition Interests
            [entries] => Array
                (
                    [fat-as-fuel] => Fat as fuel
                    [macros] => Macros
                    [cut-sugar] => Cut Sugar
                    [adrenals] => Adrenals
                    [gut-health] => Gut Health
                    [thyroid-health] => Thyroid HealtH
                    [overcoming-roadblocks] => Overcoming roadblocks
                )

        )

    [cat_price] => Array
        (
            [name] => Price
            [entries] => Array
                (
                )

        )

)



*/
