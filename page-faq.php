<?php
/**
Template name: FAQ
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'faq';

# content
$hero = get_field('hero_panel');
$faqs = get_field('faqs');
$faqpanel = get_field('faq_panel');
$formpanel = get_field('form_panel');

# print_r($faqs);
# exit;

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # QUESTIONS ?>

<?php
$upper = 'title empurple';
$lower = 'subtitle';

if ($faqpanel['bold_text'] == 'lower') {
	$upper = 'subtitle';
	$lower = 'title empurple';
}
?>

<section class="faq-questions">

<div class="rowwrap flex flexrow">

<div class="titlewrap">

<div class="<?= $upper ?> textlc">
<?= $faqpanel['upper_title'] ?>
</div>

<div class="<?= $lower ?> textlc">
<?= $faqpanel['lower_title'] ?>
</div>

</div>

<div class="faqwrap">
<div class="faqlive">

<?php foreach ($faqs as $key => $value) { ?>
<div class="faq">

<div class="faqquestion">
<div class="faqtoggle" data-answer="<?= $key ?>"></div>
<?= $value['question'] ?>	
</div>

<div class="faqanswer" id="faqanswer-<?= $key ?>" style="display: none;">
<?= $value['answer'] ?>

<?php
if (!empty($value['links'])) {
	foreach ($value['links'] as $lkey => $link) {
?>
<div class="arrowwrap">
<a class="arrow arrowright arrowgreen" href="<?= $link['link_page'] ?>"><?= $link['link_label'] ?></a>
</div>
<?php
	}
}
?>

</div>

</div>
<?php } ?>

</div>
</div>

</div>

</section>


<?php # FORM ?>

<?php
$upper = 'subtitle';
$lower = 'title';

if ($formpanel['bold_text'] == 'upper') {
	$upper = 'title';
	$lower = 'subtitle';
}
?>

<section class="faq-form bgcover">
<div class="scrim height100">
<div class="container">

<div class="rowwrap flex flexrow">

<div class="titlewrap">

<div class="<?= $upper ?>">
<?= $formpanel['upper_title'] ?>
</div>

<div class="<?= $lower ?>">
<?= $formpanel['lower_title'] ?>
</div>

</div>

<div class="formwrap">
<div class="formlive">

<?php get_template_part('panels/panel-emform'); ?>

</div>
</div>

</div>

</div>

</div>

</div>
</div>
</section>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();
