<?php
	
# EMFIT PRODUCT

# page flag
$GLOBALS['emfit']['pageid'] = 'product';

# display type
$displaytype = get_field('display_type');

# switch
if ($displaytype == 'custom') {
	get_template_part('panels/product-custom');
}

else {
	get_template_part('panels/product-standard');
}

/*
Array
(
    [shopify_details] => Array
        (
            [shopify_product_name] => Be Brave Muscle Tank
            [shopify_product_settings] => Array
                (
                    [shopify_product_id] => 3529101639785
                    [shopify_product_type] => swag
                    [shopify_product_format] => single
                )

        )

    [shopify_product_variants] => Array
        (
            [0] => Array
                (
                    [shopify_variant] => Array
                        (
                            [shopify_variant_name] => [default]
                            [shopify_variant_ids] => Array
                                (
                                    [shopify_variant_id] => 28093330260073
                                    [shopify_variant_gid] => gid://shopify/ProductVariant/28093330260073
                                )

                            [shopify_variant_details] => Array
                                (
                                    [shopify_price] => 25.00
                                    [shopify_compare_price] => 
                                    [shopify_option_1] => 
                                    [shopify_option_2] => 
                                    [shopify_option_3] => 
                                    [shopify_position] => 
                                )

                            [extraneous_detail] => 
                        )

                )

        )

    [shopify_product_options] => Array
        (
            [shopify_product_option_1] => 
            [shopify_product_option_2] => 
            [shopify_product_option_3] => 
        )

)


Array
(
    [shopify_details] => Array
        (
            [shopify_product_name] => Be Brave T-Shirt
            [shopify_product_settings] => Array
                (
                    [shopify_product_id] => 3529103016041
                    [shopify_product_type] => swag
                    [shopify_product_format] => variants
                )

        )

    [shopify_product_variants] => Array
        (
            [0] => Array
                (
                    [shopify_variant] => Array
                        (
                            [shopify_variant_name] => Small
                            [shopify_variant_ids] => Array
                                (
                                    [shopify_variant_id] => 28097116635241
                                    [shopify_variant_gid] => gid://shopify/ProductVariant/28097116635241
                                )

                            [shopify_variant_details] => Array
                                (
                                    [shopify_price] => 25.00
                                    [shopify_compare_price] => 
                                    [shopify_option_1] => Small
                                    [shopify_option_2] => 
                                    [shopify_option_3] => 
                                    [shopify_position] => 1
                                )

                            [extraneous_detail] => YEAH IT ME.
                        )

                )

            [1] => Array
                (
                    [shopify_variant] => Array
                        (
                            [shopify_variant_name] => Medium
                            [shopify_variant_ids] => Array
                                (
                                    [shopify_variant_id] => 28097116668009
                                    [shopify_variant_gid] => gid://shopify/ProductVariant/28097116668009
                                )

                            [shopify_variant_details] => Array
                                (
                                    [shopify_price] => 25.00
                                    [shopify_compare_price] => 
                                    [shopify_option_1] => Medium
                                    [shopify_option_2] => 
                                    [shopify_option_3] => 
                                    [shopify_position] => 2
                                )

                            [extraneous_detail] => 
                        )

                )

            [2] => Array
                (
                    [shopify_variant] => Array
                        (
                            [shopify_variant_name] => Large
                            [shopify_variant_ids] => Array
                                (
                                    [shopify_variant_id] => 28097116700777
                                    [shopify_variant_gid] => gid://shopify/ProductVariant/28097116700777
                                )

                            [shopify_variant_details] => Array
                                (
                                    [shopify_price] => 25.00
                                    [shopify_compare_price] => 
                                    [shopify_option_1] => Large
                                    [shopify_option_2] => 
                                    [shopify_option_3] => 
                                    [shopify_position] => 3
                                )

                            [extraneous_detail] => 
                        )

                )

            [3] => Array
                (
                    [shopify_variant] => Array
                        (
                            [shopify_variant_name] => XL
                            [shopify_variant_ids] => Array
                                (
                                    [shopify_variant_id] => 28097116733545
                                    [shopify_variant_gid] => gid://shopify/ProductVariant/28097116733545
                                )

                            [shopify_variant_details] => Array
                                (
                                    [shopify_price] => 25.00
                                    [shopify_compare_price] => 
                                    [shopify_option_1] => XL
                                    [shopify_option_2] => 
                                    [shopify_option_3] => 
                                    [shopify_position] => 4
                                )

                            [extraneous_detail] => 
                        )

                )

        )

    [shopify_product_options] => Array
        (
            [shopify_product_option_1] => Size
            [shopify_product_option_2] => 
            [shopify_product_option_3] => 
        )

)

*/