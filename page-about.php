<?php
/**
Template name: About
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'about';

# content
$hero = get_field('hero_panel');
$intro = get_field('intro_panel');
$seenon = get_field('seen_on_logos');
$news = get_field('news_logos');
$partners = get_field('partner_logos');
$certs = get_field('certifications');

# print_r($certs);
# exit;

# header
get_header();

?>

<?php # HERO ?>

<section class="general-hero bgcover" style="background-image:url(<?= $hero['background_image'] ?>);">
<div class="scrim height100">
<div class="container height100">
<div class="col-sm-12 col-xl-10 offset-xl-1 height100">

<div class="livewrap flex text-center height100">

<div class="title">
<?= $hero['hero_title'] ?>
</div>

<div class="blurb">
<?= $hero['hero_subtitle'] ?>
</div>

</div>

</div>
</div>
</div>
</section>


<?php # INTRO ?>

<section class="about-intro text-center">
<div class="container">
<div class="row">
<div class="col-sm-12 col-lg-10 offset-lg-1">

<div class="title">
A little bit <span>about</span> Em
</div>

<div class="text">
<p><strong><?= $intro['bold_text'] ?></strong></p>
<?= $intro['regular_text'] ?>
</div>

</div>
</div>
</div>
</section>


<?php # SEEN ?>

<section class="about-seen">
<div class="container">

<div class="about-paneltitle text-center">
As <span>seen</span> on
</div>

<div class="logowrap flex flexrow flexalign flexwrap">

<?php foreach ($seenon as $key => $value) { ?>
<div class="logo bgcontain" style="background-image:url(<?= $value['logo'] ?>);"></div>	
<?php } ?>

</div>

</div>
</section>


<?php # NEWS ?>

<section class="about-news">
<div class="container">

<div class="about-paneltitle text-center">
Podcasts, Magazines, <span>and</span> news
</div>

<div class="thumbwrap flex flexrow flexwrap">

<?php foreach ($news['thumbnails'] as $key => $value) { ?>
<div class="thumb bgcover" style="background-image:url(<?= $value['image'] ?>);"></div>	
<?php } ?>

</div>

<div class="detailwrap flex flexrow flexwrap">

<?php foreach ($news['logo_name'] as $key => $value) { ?>
<div class="detailbox">
<div class="logo bgcontain" style="background-image:url(<?= $value['logo'] ?>);"></div>	
<div class="name text-center">
<?= $value['name'] ?>
</div>
</div>
<?php } ?>

</div>

</div>
</section>


<?php # PARTNERS ?>

<section class="about-partners">
<div class="container">

<div class="about-paneltitle text-center">
My <span>Partners</span>
</div>

<div class="logowrap flex flexrow flexalign flexwrap">

<?php foreach ($partners as $key => $value) { ?>
<div class="logo bgcontain" style="background-image:url(<?= $value['logo'] ?>);"></div>	
<?php } ?>

</div>

</div>
</section>


<?php # CERTS ?>

<section class="about-certs">
<div class="container">

<div class="about-paneltitle text-center">
My <span>Certifications</span>
</div>

<div class="detailwrap flex flexrow flexwrap">

<?php foreach ($certs as $key => $value) { ?>
<div class="detailbox">
<div class="logo bgcontain" style="background-image:url(<?= $value['logo'] ?>);"></div>	
<div class="name text-center">
<?= $value['name'] ?>
</div>
</div>
<?php } ?>
	
</div>

</div>
</section>


<?php # GYM ?>

<?php get_template_part('panels/panel-gym'); ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();

