<?php
/**
Template name: Homepage
 */

# page flag
$GLOBALS['emfit']['pageid'] = 'homepage';

# content
$hero = get_field('hero_panel');
$splitpanel = get_field('split_panel');
$challengepanel = get_field('challenge_panel');
$swagpanel = get_field('swag_panel');
$empiricapanel = get_field('empirica_panel');
$productpanel = get_field('product_panel');
$podpanel = get_field('podcast_panel');
$diagpanel = get_field('diagonal_panel');


# header
get_header();

?>


<?php # HERO ?>

<section class="home-hero bgorange">
<div class="container height100">

<div class="livewrap height100 flex">

<div class="title">
<?= $hero['headline']['line_1'] ?><br>
<span class="bold"><?= $hero['headline']['line_2'] ?></span><br>
<span class="space"><?= $hero['headline']['line_3'] ?></span>
</div>

<div class="blurb">
<?= $hero['blurb'] ?>
</div>

<div class="buttonwrap">
<a href="/findmyfit/"><button class="featurebutton btclear noshadow"><?= $hero['button_label'] ?></button></a>
</div>

</div>

</div>
</section>


<?php # SPLIT PANEL ?>

<section class="home-splitpanel">

<div class="backgrounds">
<div class="panel bgleft bggreen"></div>
<div class="panel bgright bgcover"></div>
</div>

<div class="foreground height100">
<div class="container height100">

<div class="panel panelleft">

<div class="textwrap">

<div class="kicker textuc">
<?= $splitpanel['left_panel']['kicker'] ?>
</div>

<?php
$topcss = 'bold';
$bottomcss = 'lite';

if ($splitpanel['left_panel']['headline']['bold_text'] == 'bottom') {
	$topcss = 'lite';
	$bottomcss = 'bold';
}
?>

<div class="headline textlc">
<div class="<?= $topcss ?>">
<?= $splitpanel['left_panel']['headline']['top_lines'] ?>
</div>
<div class="<?= $bottomcss ?>">
<?= $splitpanel['left_panel']['headline']['bottom_lines'] ?>
</div>
</div>

<div class="blurb">
<?= $splitpanel['left_panel']['blurb'] ?>
</div>

</div>

<div class="buttonwrap">
<a href="<?= $splitpanel['left_panel']['button']['button_url'] ?>"><button class="featurebutton btgreen"><?= $splitpanel['left_panel']['button']['button_label'] ?></button></a>
</div>

</div>

<div class="panel panelright">

<div class="textwrap">

<div class="kicker textuc">
<?= $splitpanel['right_panel']['kicker'] ?>
</div>

<?php
$topcss = 'bold';
$bottomcss = 'lite';

if ($splitpanel['right_panel']['headline']['bold_text'] == 'bottom') {
	$topcss = 'lite';
	$bottomcss = 'bold';
}
?>

<div class="headline textlc">
<div class="<?= $topcss ?>">
<?= $splitpanel['right_panel']['headline']['top_lines'] ?>
</div>
<div class="<?= $bottomcss ?>">
<?= $splitpanel['right_panel']['headline']['bottom_lines'] ?>
</div>
</div>

<div class="blurb">
<?= $splitpanel['right_panel']['blurb'] ?>
</div>

</div>

<div class="buttonwrap">
<a href="<?= $splitpanel['right_panel']['button']['button_url'] ?>"><button class="featurebutton btgreen"><?= $splitpanel['right_panel']['button']['button_label'] ?></button></a>
</div>

</div>

</div>
</div>

</section>


<?php # QUIZ ?>

<?php get_template_part('panels/panel-quiz'); ?>


<?php # CHALLENGE PANEL ?>

<?php
$topcss = 'bold';
$bottomcss = 'lite';

if ($challengepanel['panel_title']['bold_text'] == 'bottom') {
	$topcss = 'lite';
	$bottomcss = 'bold';
}

$pickdateid = $challengepanel['products']['pick_your_start_date'];
$overthinkid = $challengepanel['products']['dont_overthink_it'];

$pickdatebutton = products_button($pickdateid);
$overthinkbutton = products_button($overthinkid);

# print_r($overthinkbutton);

$version1 = $pickdatebutton['variants'][1];
$version2 = $pickdatebutton['variants'][2];
$oversion = $overthinkbutton['variants']['single'];

$variants = get_field('shopify_product_variants', $pickdateid);
$v1overview = $variants[0]['shopify_variant']['program_overview'];
$v2overview = $variants[1]['shopify_variant']['program_overview'];

$variants = get_field('shopify_product_variants', $overthinkid);
$thinkoverview = $variants[0]['shopify_variant']['program_overview'];

?>

<section class="home-program bgcover">
<div class="scrim height100">
<div class="container">

<div class="livewrap">

<div class="kicker textuc">
<?= $challengepanel['kicker'] ?>
</div>

<div class="headline">
<div class="<?= $topcss ?>">
<?= $challengepanel['panel_title']['top_line'] ?>
</div>
<div class="<?= $bottomcss ?>">
<?= $challengepanel['panel_title']['bottom_line'] ?>
</div>
</div>

<div class="faqwrap">


<?php # v1 ?>

<div class="faq">

<div class="faqquestion">
<div class="faqtoggle" data-answer="v1"></div>
version 1
</div>

<div class="faqanswer" id="faqanswer-v1" style="display: none;">

<?= $challengepanel['version1_blurb'] ?>

<div class="buttonwrap">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $pickdatebutton['shopifyid'] ?>" data-variantid="<?= $version1['variantid'] ?>" data-nonce="<?= $pickdatebutton['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $version1['price'] ?></div>
</a>
</div>

<div class="arrowwrap">
<a class="arrow arrowright arrowgreen" href="<?= $v1overview ?>">View More Details</a>
</div>

</div>

</div>


<?php # v2 ?>

<div class="faq">

<div class="faqquestion">
<div class="faqtoggle" data-answer="v2"></div>
version 2
</div>

<div class="faqanswer" id="faqanswer-v2" style="display: none;">

<?= $challengepanel['version2_blurb'] ?>

<div class="buttonwrap">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $pickdatebutton['shopifyid'] ?>" data-variantid="<?= $version2['variantid'] ?>" data-nonce="<?= $pickdatebutton['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $version2['price'] ?></div>
</a>
</div>

<div class="arrowwrap">
<a class="arrow arrowright arrowgreen" href="<?= $v2overview ?>">View More Details</a>
</div>

</div>

</div>


<?php # overthink ?>

<div class="faq">

<div class="faqquestion">
<div class="faqtoggle" data-answer="overthink"></div>
don’t overthink it
</div>

<div class="faqanswer" id="faqanswer-overthink" style="display: none;">

<?= $challengepanel['overthink_blurb'] ?>

<div class="buttonwrap">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $overthinkbutton['shopifyid'] ?>" data-variantid="<?= $oversion['variantid'] ?>" data-nonce="<?= $overthinkbutton['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $oversion['price'] ?></div>
</a>
</div>

<div class="arrowwrap">
<a class="arrow arrowright arrowgreen" href="<?= $thinkoverview ?>">View More Details</a>
</div>

</div>

</div>

</div>

</div>

</div>
</div>
</section>


<?php # SWAG ?>

<?php
$topcss = 'bold';
$bottomcss = 'lite';

if ($swagpanel['panel_title']['bold_text'] == 'bottom') {
	$topcss = 'lite';
	$bottomcss = 'bold';
}
?>

<section class="home-swag">
<div class="container">

<div class="swagwrap">

<div class="imagewrap bgcover" style="background-image:url(<?= $swagpanel['background_image'] ?>);">
<div class="hilitebox text-center">

<div class="headline">
<div class="<?= $topcss ?>">
<?= $swagpanel['panel_title']['top_line'] ?>
</div>
<div class="<?= $bottomcss ?>">
<?= $swagpanel['panel_title']['bottom_line'] ?>
</div>
</div>

<div class="blurb">
<?= $swagpanel['panel_blurb'] ?>
</div>

</div>
</div>

<div class="buttonwrap">
<a href="/swag/"><button class="featurebutton btgreen"><?= $swagpanel['button_label'] ?></button></a>
</div>

</div>

</div>
</section>


<?php # EMPIRICA ?>

<?php
$topline = $empiricapanel['panel_title']['top_line'];
$bottomline = $empiricapanel['panel_title']['bottom_line'];
$hilite = $empiricapanel['panel_title']['highlight_color'];

if ($empiricapanel['panel_title']['bold_text'] == 'top') {
	$topline = '<span class="' . $hilite . '">'. $topline . '</span>';
} else {
	$bottomline = '<span class="' . $hilite . '">'. $bottomline . '</span>';
}
?>

<section class="panel-product">
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace">

<div class="imagewrap bgcontain" style="background-image:url(<?= $empiricapanel['product_image'] ?>);"></div>

<div class="textwrap">

<div class="kicker textuc textpad">
<?= $empiricapanel['kicker'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<?= $topline ?><br>
<?= $bottomline ?>
</div>

<div class="blurb">
<?= $empiricapanel['product_blurb'] ?>
</div>

</div>

<div class="buttonwrap textpad">
<a href="/supplements/"><button class="featurebutton btgreen"><?= $empiricapanel['button_label'] ?></button></a>
</div>
	
</div>

</div>

</div>
</section>


<?php # PRODUCT ?>

<?php
$topline = $productpanel['panel_title']['top_line'];
$bottomline = $productpanel['panel_title']['bottom_line'];
$hilite = $productpanel['panel_title']['highlight_color'];

if ($productpanel['panel_title']['bold_text'] == 'top') {
	$topline = '<span class="' . $hilite . '">'. $topline . '</span>';
} else {
	$bottomline = '<span class="' . $hilite . '">'. $bottomline . '</span>';
}

$productid = $productpanel['select_product'];
$button = products_button($productid);
$variant = false;
if (isset($button['variants']['single'])) {
	$variant = $button['variants']['single'];
}
?>

<section class="panel-product panelreverse">
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace flexrowreverse">

<div class="imagewrap bgcontain" style="background-image:url(<?= $productpanel['product_image'] ?>);"></div>

<div class="textwrap">

<div class="kicker textuc textpad">
<?= $productpanel['kicker'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<?= $topline ?><br>
<?= $bottomline ?>
</div>

<div class="blurb">
<?= $productpanel['product_blurb'] ?>
</div>

</div>

<?php if (!empty($variant)) { ?>
<div class="buttonwrap textpad">
<a class="pricebutton btgreen cartbutton" href="#" data-productid="<?= $button['shopifyid'] ?>" data-variantid="<?= $variant['variantid'] ?>" data-nonce="<?= $button['nonce'] ?>">
<div class="label">Add to Cart</div><div class="price"><?= $variant['price'] ?></div>
</a>
</div>	
<?php } ?>

<div class="arrowwrap textpad">
<a class="arrow arrowright arrowgreen" href="<?= $button['url'] ?>"><?= $productpanel['link_label'] ?></a>
</div>
	
</div>

</div>

</div>
</section>


<?php # PODCAST ?>

<?php
$topline = $podpanel['panel_title']['top_line'];
$bottomline = $podpanel['panel_title']['bottom_line'];
$hilite = $podpanel['panel_title']['highlight_color'];

if ($podpanel['panel_title']['bold_text'] == 'top') {
	$topline = '<span class="' . $hilite . '">'. $topline . '</span>';
} else {
	$bottomline = '<span class="' . $hilite . '">'. $bottomline . '</span>';
}
?>

<section class="panel-product podcast">
<div class="container">

<div class="panelwrap flex flexrow flexalign flexspace">

<div class="imagewrap bgcontain"></div>

<div class="textwrap">

<div class="kicker textuc textpad">
<?= $podpanel['kicker'] ?>
</div>

<div class="borderwrap textpad">

<div class="title">
<?= $topline ?><br>
<?= $bottomline ?>
</div>

<div class="blurb">
<?= $podpanel['panel_blurb'] ?>
</div>

</div>

<div class="buttonwrap textpad">
<a href="/podcasts/"><button class="featurebutton btgreen"><?= $podpanel['button_label'] ?></button></a>
</div>
	
</div>

</div>

</div>
</section>


<?php # DIAGONAL ?>

<?php
$righttopcss = 'bold emrust';
$rightbottomcss = 'lite';

if ($diagpanel['right_side']['headline']['bold_text'] == 'bottom') {
	$righttopcss = 'lite';
	$rightbottomcss = 'bold emrust';
}

$lefttopcss = 'bold emmauve';
$leftbottomcss = 'lite';

if ($diagpanel['left_side']['headline']['bold_text'] == 'bottom') {
	$lefttopcss = 'lite';
	$leftbottomcss = 'bold emmauve';
}
?>

<section class="home-diagonal bgcover">
<div class="container height100">

<div class="livewrap flex flexrow flexrowreverse flexspace text-center">

<div class="item itemright">
	
<div class="headline textlc">
<div class="<?= $righttopcss ?>">
<?= $diagpanel['right_side']['headline']['top_line'] ?>
</div>
<div class="<?= $rightbottomcss ?>">
<?= $diagpanel['right_side']['headline']['bottom_line'] ?>
</div>
</div>

<div class="blurb">
<?= $diagpanel['right_side']['blurb'] ?>
</div>

<div class="buttonwrap">
<a href="<?= $diagpanel['right_side']['button_url'] ?>"><button class="featurebutton btgreen"><?= $diagpanel['right_side']['button_label'] ?></button></a>
</div>

</div>

<div class="item itemleft">

<div class="headline textlc">
<div class="<?= $lefttopcss ?>">
<?= $diagpanel['left_side']['headline']['top_line'] ?>
</div>
<div class="<?= $leftbottomcss ?>">
<?= $diagpanel['left_side']['headline']['bottom_line'] ?>
</div>
</div>

<div class="blurb">
<?= $diagpanel['left_side']['blurb'] ?>
</div>

<div class="buttonwrap">
<a href="<?= $diagpanel['left_side']['button_url'] ?>"><button class="featurebutton btgreen"><?= $diagpanel['left_side']['button_label'] ?></button></a>
</div>

</div>

</div>

</div>
</section>


<?php # SUCCESS ?>

<?php get_template_part('panels/panel-success'); ?>


<?php # CHALLENGE ?>

<?php get_template_part('panels/panel-challenge'); ?>


<?php # FEATURE TABS ?>

<?php get_template_part('panels/panel-featuretabs'); ?>


<?php # PROGRAMS ?>

<?php get_template_part('panels/panel-programs'); ?>


<?php # INTRO OFFER ?>

<?php get_template_part('panels/panel-introoffer'); ?>


<?php
	
# footer
get_footer();
